package com.dray.dray.helpActivities.experimentUI

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import com.dray.dray.R
import com.dray.dray.helpActivities.HelpActivity
import org.jetbrains.anko.*

class PendingTicketComponent : AnkoComponent<HelpActivity> , AnkoLogger{
    val SMALL_GAP = 1
    val EXTRA_SMALL_SIZE_TEXT = 8f
    val MEDIUM_SIZE_TEXT = 18f
    val LABEL_WIDTH = 100
    override fun createView(ui: AnkoContext<HelpActivity>): View = with(ui){
        verticalLayout {
            lparams(width = matchParent)
            linearLayout(){
                backgroundColor = Color.WHITE
                lparams(width = matchParent){
                    verticalMargin = dip(SMALL_GAP)
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }

                verticalLayout{
                    lparams(){
                        weight = 1f
                    }

                    textView("CONTAINER"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                    }

                    textView("LOAD"){
                        textColor = R.color.colorPrimary
                        textSize = MEDIUM_SIZE_TEXT
                    }
                }

                textView("ABCD0000000"){
                    textColor = R.color.colorPrimary
                    textSize = 24f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams{
                    weight = 0f
                }

            }

            verticalLayout{
                backgroundColor = Color.WHITE
                lparams(width = matchParent){
                    verticalMargin = dip(SMALL_GAP)
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }
                linearLayout(){
                    textView("BOL #"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(80))
                    textView("YMEW134823857210451230")
                }
                linearLayout(){
                    textView("Customer"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(80))
                    textView("ASHLEY FURNITURE")
                }
            }

            linearLayout{
                backgroundColor = Color.WHITE
                lparams(width = matchParent){
                    verticalMargin = dip(SMALL_GAP)
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }
                verticalLayout{
                    lparams{
                        weight = 1f
                    }

                    textView("Pick up from"){
                        typeface = Typeface.DEFAULT_BOLD
                    }
                    textView("ADDRESS"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        topMargin = dimen(R.dimen.item_vertical_margin)
                    }
                    textView("123 SDFWIE, CA, 90712")
                    textView("TIME"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        topMargin = dimen(R.dimen.item_vertical_margin)
                    }
                    textView("12/12/2018 23:00:12")
                }

                verticalLayout{
                    lparams{
                        weight = 1f
                    }

                    textView("Deliver to"){
                        typeface = Typeface.DEFAULT_BOLD
                    }
                    textView("ADDRESS"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        topMargin = dimen(R.dimen.item_vertical_margin)
                    }
                    textView("123 SDFWIE, CA, 90712")
                    textView("TIME"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        topMargin = dimen(R.dimen.item_vertical_margin)
                    }
                    textView("12/12/2018 23:00:12")
                }
            }

            verticalLayout{
                backgroundColor = Color.WHITE
                lparams(width = matchParent){
                    verticalMargin = dip(SMALL_GAP)
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }

                linearLayout{
                    lparams(width = matchParent){
                        bottomMargin = dip(10)
                    }

                    textView("Flat Rate"){
                        textColor = R.color.colorPrimary
                        typeface = Typeface.DEFAULT_BOLD
                        textSize = 16f
                    }.lparams{
                        weight = 1f
                    }
                    textView("$322.00"){
                        textColor = Color.BLACK
                        typeface = Typeface.DEFAULT_BOLD
                        textSize = 16f
                    }.lparams{
                        weight = 0f
                    }
                }
                linearLayout{
                    textView("Bonus Rate"){
                        textColor = R.color.colorPrimary

                    }.lparams{
                        leftPadding = dip(20)
                        weight = 1f
                    }
                    textView("$322.00")
                }
                linearLayout{
                    textView("Chassis Rate"){
                        textColor = R.color.colorPrimary
                    }.lparams{
                        leftPadding = dip(20)
                        weight = 1f
                    }
                    textView("$322.00")
                }
                linearLayout{
                    textView("Fuel Rate"){
                        textColor = R.color.colorPrimary
                    }.lparams{
                        leftPadding = dip(20)
                        weight = 1f
                    }
                    textView("$322.00")
                }
                linearLayout{
                    textView("Additional Rate"){
                        textColor = R.color.colorPrimary
                    }.lparams{
                        leftPadding = dip(20)
                        weight = 1f
                    }
                    textView("$322.00")
                }

                linearLayout{
                    lparams(width = matchParent){
                        topMargin = dip(10)
                    }

                    textView("Deduction Rate"){
                        textColor = Color.parseColor("#b22222")
                    }.lparams{
                        weight = 1f
                    }
                    textView("$322.00"){
                        textColor = Color.parseColor("#b22222")
                    }
                }
            }

            linearLayout{
                backgroundColor = Color.WHITE
                lparams(width = matchParent){
                    verticalMargin = dip(SMALL_GAP)
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }
                textView("Total Pay").lparams{
                    gravity = Gravity.CENTER_VERTICAL
                    weight = 1f
                }

                textView("$500.00"){
                    textColor = Color.BLACK
                    textSize = 24f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    weight = 0f
                }
            }



            /*linearLayout{
                backgroundColor = Color.WHITE
                lparams(width = matchParent){
                    topMargin = dip(SMALL_GAP)
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }
                verticalLayout(){
                    textView("FLAT RATE"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                    textView("$322.00"){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                }.lparams(){
                    weight = 1f
                }
                verticalLayout(){
                    textView("BONUS"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                    textView("$322.00"){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                }.lparams(){
                    weight = 1f
                }
                verticalLayout(){
                    textView("CHASSIS"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                    textView("$322.00"){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                }.lparams(){
                    weight = 1f
                }
            }
            linearLayout{
                backgroundColor = Color.WHITE
                lparams(width = matchParent){
                    bottomMargin = dip(SMALL_GAP)
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }
                verticalLayout(){
                    textView("ADDITIONAL"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                    textView("$9.20"){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                }.lparams(){
                    weight = 1f
                }
                verticalLayout(){
                    textView("FUEL"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                    textView("$40.45"){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                }.lparams(){
                    weight = 1f
                }
                verticalLayout(){
                    textView("DEDUCTIONS"){
                        textSize = EXTRA_SMALL_SIZE_TEXT
                        textColor = R.color.colorPrimary
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                    textView("$12.12"){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        textColor = Color.parseColor("#b22222")
                    }
                }.lparams(){
                    weight = 1f
                }
            }*/
        }

    }

}