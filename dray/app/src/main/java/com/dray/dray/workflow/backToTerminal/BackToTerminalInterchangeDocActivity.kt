package com.dray.dray.workflow.backToTerminal

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.dray.dray.MainActivity
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.workflow.backToTerminal.ui.BackToTerminalInterchangeDocComponent
import org.jetbrains.anko.setContentView

class BackToTerminalInterchangeDocActivity : AppCompatActivity() {
    val ui = BackToTerminalInterchangeDocComponent()
    val CAMERA_REQUEST_CODE = 133

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)
        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Photo the Interchange Document")

        val wantNextLoad = intent.getStringExtra("WANT_NEXT_LOAD") == "true"
        println("want_next_load $wantNextLoad")

        ui.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }

        ui.photoBtn.setOnClickListener {
            captureImage()
        }

        if (wantNextLoad) {
            ui.nextBtn.setOnClickListener {
                // read new data from databse and put into firstLegObject...
            }
            ui.backBtn.setOnClickListener {
                goBackHome()
            }
        } else {
            ui.nextBtn.visibility = View.GONE
            ui.backBtn.setOnClickListener {
                goBackHome()
            }
        }
    }

    private fun captureImage() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CAMERA_REQUEST_CODE -> try {
                //When image is captured successfully
                if (resultCode == RESULT_OK) {
                    val imageBitmap = data!!.extras.get("data") as Bitmap

                    // Show buttons
                    ui.photoBtn.visibility = View.GONE
                    ui.nextBtn.visibility = View.VISIBLE
                    ui.backBtn.visibility = View.VISIBLE
                    ui.photoDisplayArea.visibility = View.VISIBLE
                    //After image capture show captured image over image view
                    showCapturedImage(imageBitmap)

                } else
                    Toast.makeText(this, R.string.cancel_message, Toast.LENGTH_SHORT).show()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    /*  Show Captured over ImageView  */
    private fun showCapturedImage(image : Bitmap) {
        ui.photoDisplayArea!!.setImageBitmap(image)
    }

    private fun goBackHome () {
        val i = Intent(this, MainActivity::class.java)
        startActivity(i)
    }
}
