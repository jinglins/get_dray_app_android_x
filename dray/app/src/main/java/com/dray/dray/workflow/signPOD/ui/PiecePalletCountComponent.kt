package com.dray.dray.workflow.signPOD.ui

import android.graphics.Color
import android.graphics.Typeface
import android.text.InputType
import android.view.Display
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.signPOD.PiecePalletCountActivity
import org.jetbrains.anko.*

class PiecePalletCountComponent : AnkoComponent <PiecePalletCountActivity>, AnkoLogger {
    lateinit var pieceCountTxt : TextView
    lateinit var palletCountTxt : TextView

    lateinit var pieceCountInputBox : EditText
    lateinit var palletCountInputBox : EditText

    lateinit var pieceCountDisplayGroup : LinearLayout
    lateinit var pieceCountEditGroup : LinearLayout
    lateinit var palletCountDisplayGroup : LinearLayout
    lateinit var palletCountEditGroup : LinearLayout

    lateinit var pieceCountEditBtn : LinearLayout
    lateinit var pieceCountFinishBtn : LinearLayout
    lateinit var palletCountEditBtn : LinearLayout
    lateinit var palletCountFinishBtn : LinearLayout

    lateinit var reportBtn : LinearLayout
    lateinit var signPODBtn : Button

    val MEDIUM_SIZE_TEXT = 18f

    override fun createView(ui: AnkoContext<PiecePalletCountActivity>): View = with(ui){
        verticalLayout {
            verticalPadding = dimen(R.dimen.activity_vertical_padding)
            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout {
                lparams(width = matchParent){
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }

                backgroundColor = R.color.colorPrimary
                textView("For Warehouse Personnel"){
                    textColor = Color.WHITE
                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                }.lparams(width = matchParent)
            }

            linearLayout {
                backgroundColor = Color.WHITE
                lparams(width = matchParent){
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    weight = 1f
                    bottomMargin = dimen(R.dimen.small_gap)
                }

                textView("Piece Count "){
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = R.color.colorPrimary
                }.lparams(width = dip(120)){
                    this.gravity = Gravity.CENTER_VERTICAL
                }

                pieceCountDisplayGroup = linearLayout{
                    lparams(width = matchParent){
                        this.gravity = Gravity.CENTER_VERTICAL
                    }

                    pieceCountTxt = textView("None"){
                        textSize = MEDIUM_SIZE_TEXT
                    }.lparams{
                        weight = 1f
                        this.gravity = Gravity.CENTER_VERTICAL
                    }

                    pieceCountEditBtn = linearLayout {
                        backgroundResource = R.drawable.green_rounded_button
                        horizontalPadding = dip(10)
                        gravity = Gravity.CENTER
                        imageView(R.mipmap.ic_edit) {
                            padding = dip(0)
                        }
                    }
                }

                pieceCountEditGroup = linearLayout{
                    lparams(width = matchParent)
                    visibility = View.GONE

                    pieceCountInputBox = editText(){
                        hint = "Please enter piece count. "
                        inputType = InputType.TYPE_NUMBER_FLAG_SIGNED
                        maxLines = 1
                    }.lparams{
                        weight = 1f
                    }

                    pieceCountFinishBtn = linearLayout {
                        backgroundResource = R.drawable.green_rounded_button
                        horizontalPadding = dip(10)
                        gravity = Gravity.CENTER
                        imageView(R.mipmap.ic_check) {
                            padding = dip(0)
                        }
                    }
                }
            }

            linearLayout {
                backgroundColor = Color.WHITE
                lparams(width = matchParent){
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    weight = 1f
                    topMargin = dimen(R.dimen.small_gap)
                }

                textView("Pallet Count"){
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = R.color.colorPrimary
                }.lparams(width = dip(120)){
                    this.gravity = Gravity.CENTER_VERTICAL
                }

                palletCountDisplayGroup = linearLayout{
                    lparams(width = matchParent){
                        this.gravity = Gravity.CENTER_VERTICAL
                    }

                    palletCountTxt = textView("None"){
                        textSize = MEDIUM_SIZE_TEXT
                    }.lparams{
                        weight = 1f
                        this.gravity = Gravity.CENTER_VERTICAL
                    }

                    palletCountEditBtn = linearLayout {
                        backgroundResource = R.drawable.green_rounded_button
                        horizontalPadding = dip(10)
                        gravity = Gravity.CENTER
                        imageView(R.mipmap.ic_edit) {
                            padding = dip(0)
                        }
                    }
                }

                palletCountEditGroup = linearLayout{
                    lparams(width = matchParent)
                    visibility = View.GONE

                    palletCountInputBox = editText(){
                        hint = "Please enter pallet count. "
                        inputType = InputType.TYPE_NUMBER_FLAG_SIGNED
                        maxLines = 1
                    }.lparams{
                        weight = 1f
                    }

                    palletCountFinishBtn = linearLayout {
                        backgroundResource = R.drawable.green_rounded_button
                        horizontalPadding = dip(10)
                        gravity = Gravity.CENTER
                        imageView(R.mipmap.ic_check) {
                            padding = dip(0)
                        }
                    }
                }
            }

            signPODBtn = button("Sign Proof of Delivery"){
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}