package com.dray.dray.adapter

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.MainActivity
import com.dray.dray.R
import com.dray.dray.customClasses.AssignedLoadItem
import com.dray.dray.customClasses.assignedLoadCardRow
import com.dray.dray.customClasses.getDatabase
import org.jetbrains.anko.*

class assignedLoadsAdapter(val activity: MainActivity, var assignedLoadItems : ArrayList<AssignedLoadItem>) : BaseAdapter(){
    val SMALL_LABEL_TEXT_SIZE = 8f

    override fun getView(i : Int, v : View?, parent : ViewGroup?) : View {
        val item = getItem(i)
        println("Is freeflow: " + item.isFreeflow)
        return with(parent!!.context) {
            linearLayout {
                verticalLayout(R.style.ThemeOverlay_AppCompat_Dark){
                    backgroundResource = R.drawable.purple_header
                    lparams(){
                        verticalPadding = dip(15)
                        horizontalPadding = dip(15)
                    }
                    textView("APPOINTMENT"){
                        textSize = SMALL_LABEL_TEXT_SIZE
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                    textView(item.apptTime.substring(11 .. 15)){
                        textSize = 35f
                        typeface = Typeface.DEFAULT_BOLD
                    }.lparams(){
                        bottomMargin = dip(10)
                    }

                    when {
                        item.isFreeflow -> {
                            textView("FREE-FLOW CODE"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                            }.lparams(){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                topMargin = dip(10)
                            }
                            textView(item.freeflowCode){
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }

                        item.containerNumber.isEmpty() -> {
                            textView("BOOKING#"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                            }.lparams(){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                topMargin = dip(10)
                            }
                            textView(item.bookingNumber){
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }

                        else -> {
                            textView("CONTAINER#"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                            }.lparams(){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                topMargin = dip(10)
                            }
                            textView(item.containerNumber){
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }
                    }
                }


                verticalLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dip(10)
                    horizontalPadding = dip(10)
                    lparams(height = matchParent){
                        weight = 1f
                    }

                    verticalLayout {
                        gravity = Gravity.CENTER
                        lparams(){
                            weight = 1f
                        }

                        linearLayout(){
                            imageView(R.drawable.origin_dot){
                                adjustViewBounds = true
                            }.lparams(width = dip(30), height = dip(30))
                            textView(item.origin){
                                textColor = Color.BLACK
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 18f
                            }.lparams(){
                                this.gravity = Gravity.CENTER_VERTICAL
                            }
                        }

                        linearLayout(){
                            imageView(R.drawable.dest_dot){
                                adjustViewBounds = true
                            }.lparams(width = dip(30), height = dip(30))
                            textView(item.destination){
                                textColor = Color.BLACK
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 18f
                            }.lparams(){
                                this.gravity = Gravity.CENTER_VERTICAL
                            }
                        }
                    }

                    if (!item.isFreeflow){
                        linearLayout {
                            if (!item.containerNumber.isEmpty()){
                                verticalLayout {
                                    lparams(width = matchParent){
                                        weight = 1f
                                    }

                                    textView("WEIGHT (lb)"){
                                        textSize = SMALL_LABEL_TEXT_SIZE
                                        textColor = Color.parseColor("#aaaaaa")
                                    }
                                    textView(item.weight){
                                        textSize = 18f
                                        textColor = Color.parseColor("#aaaaaa")
                                        typeface = Typeface.DEFAULT_BOLD
                                    }
                                }
                            }


                            verticalLayout {
                                lparams(width = matchParent){
                                    weight = 1f
                                }

                                textView("EQUIPMENT"){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                    textColor = Color.parseColor("#aaaaaa")
                                }
                                textView("Chassis"){
                                    textSize = 18f
                                    typeface = Typeface.DEFAULT_BOLD
                                    textColor = Color.parseColor("#aaaaaa")
                                }
                            }


                            verticalLayout {
                                lparams(width = matchParent){
                                    weight = 1f
                                }

                                textView("SIZE"){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                    textColor = Color.parseColor("#aaaaaa")
                                }
                                textView(item.size){
                                    textSize = 18f
                                    typeface = Typeface.DEFAULT_BOLD
                                    textColor = Color.parseColor("#aaaaaa")
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    override fun getItem(p0: Int): AssignedLoadItem {
        return assignedLoadItems[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return assignedLoadItems.count()
    }

}