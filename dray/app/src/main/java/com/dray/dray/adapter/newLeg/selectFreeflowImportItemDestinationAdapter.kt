package com.dray.dray.adapter.newLeg

import android.graphics.Color
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.customClasses.newLeg.SelectFreeflowImportDestinationItem
import com.dray.dray.workflow.newLeg.select.SelectNewLegActivity
import org.jetbrains.anko.*

/* Render the list view of {destination, rate} on FreeflowImportItemComponent
*  Pass in arraylist of String where odd indexes are destination name and even indexes are rates.
* */

class selectFreeflowImportItemDestinationAdapter (val activity: SelectNewLegActivity,
                                                  var destinationsAndRates : ArrayList<SelectFreeflowImportDestinationItem>) : BaseAdapter(){
    val MEDIUM_SIZE_TEXT =18f

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val item = getItem(p0)
        return with (p2!!.context){
            linearLayout{
                textView(item.destinationName).lparams(){
                    weight = 1f
                }
                textView("$"+item.rate){
                    textColor = Color.BLACK
                    typeface = Typeface.DEFAULT_BOLD
                    textSize = MEDIUM_SIZE_TEXT
                }
            }
        }

    }

    override fun getItem(p0: Int): SelectFreeflowImportDestinationItem {
        return destinationsAndRates[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return destinationsAndRates.count()
    }

}