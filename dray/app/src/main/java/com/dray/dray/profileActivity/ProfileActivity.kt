package com.dray.dray.profileActivity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.view.MenuItem
import android.view.View
import com.dray.dray.R
import com.dray.dray.profileActivity.ui.ProfileActivityComponent
import org.jetbrains.anko.setContentView
import java.text.NumberFormat
import java.util.*
import android.graphics.Color
import androidx.appcompat.app.AlertDialog
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.dataClasses.profile.ProfileWorkPreference
import com.dray.dray.database.AppDatabase
import org.jetbrains.anko.*
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit


class ProfileActivity : AppCompatActivity() {

    val ui = ProfileActivityComponent()
    lateinit var db : AppDatabase
    var modifiedIndicator = 0 // Any button that has been tapped will turn this value into 1

    val MEDICAL_EXP_GRACE_PERIOD = 45
    val INSURANCE_EXP_GRACE_PERIOD = 45
    val TWIC_EXP_GRACE_PERIOD = 45
    val BIT_EXP_GRACE_PERIOD = 45
    val ANNUAL_EXP_GRACE_PERIOD = 45
    val DDL_EXP_GRACE_PERIOD = 45

    @SuppressLint("SetTextI18n") // Used for suppressing "ui.xxx.text = some concatenation"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        var toolbar = supportActionBar
        setToolBar(toolbar!!, "Profile")

        db = getDatabase(applicationContext)

        /* --------------------------- Basic Info -------------------------- */
        val profileBasicInfo = db.profileDao().getProfileBasicInfoById(1)

        ui.nameTxt.text = profileBasicInfo.firstName + " " + profileBasicInfo.lastName
        ui.vendorCodeTxt.text = "Vendor Code: " + profileBasicInfo.vendorCode
        ui.phoneTxt.text = "Phone: " + PhoneNumberUtils.formatNumber(profileBasicInfo.phone)
        ui.emailTxt.text = if (profileBasicInfo.email == "") "Not set" else profileBasicInfo.email

        /* --------------------------- Basic Info -------------------------- */
        val profileQualifiedToDrive = db.profileDao().getProfileQualifiedToDriveById(1)

        ui.medicalExpTxt.text = profileQualifiedToDrive.medicalExpire
        ui.insuranceExpTxt.text = profileQualifiedToDrive.insuranceExpire
        ui.twiccardExpTxt.text = profileQualifiedToDrive.TWICExpire
        ui.bitExpTxt.text =profileQualifiedToDrive.BITExpire
        ui.annualExpTxt.text = profileQualifiedToDrive.annualExpire
        ui.ddlExpTxt.text = profileQualifiedToDrive.DDLExpire

        ui.medicalStatus.setImageResource(dotColor(MEDICAL_EXP_GRACE_PERIOD, profileQualifiedToDrive.medicalExpire))
        ui.insuranceStatus.setImageResource(dotColor(INSURANCE_EXP_GRACE_PERIOD, profileQualifiedToDrive.insuranceExpire))
        ui.twiccardStatus.setImageResource(dotColor(TWIC_EXP_GRACE_PERIOD, profileQualifiedToDrive.TWICExpire))
        ui.bitStatus.setImageResource(dotColor(BIT_EXP_GRACE_PERIOD, profileQualifiedToDrive.BITExpire))
        ui.annualStatus.setImageResource(dotColor(ANNUAL_EXP_GRACE_PERIOD, profileQualifiedToDrive.annualExpire))
        ui.ddlStatus.setImageResource(dotColor(DDL_EXP_GRACE_PERIOD, profileQualifiedToDrive.DDLExpire))

        ui.dmvPrintoutStatus.text = if (profileQualifiedToDrive.dmvPrintOutReceived) "Received" else "Not Received"
        if (!profileQualifiedToDrive.dmvPrintOutReceived){
            ui.dmvPrintoutStatus.textColor = Color.parseColor("#B22222")
        }
        ui.pointsAndViolation.text = profileQualifiedToDrive.dmvViolationPoints.toString()

        /* --------------------------- Preference -------------------------- */
        val profileWorkPreference = db.profileDao().getProfileWorkPreferenceById(1)
        ui.shiftTxt.text = profileWorkPreference.shift
        ui.distanceTxt.text = profileWorkPreference.distance

        /* --------------------------- TMC Info -------------------------- */
        val profileTMC = db.profileDao().getProfileTMCInfoById(1)
        ui.companyTxt.text = profileTMC.TMCName
        ui.scacTxt.text = profileTMC.SCACCode

        /* --------------------------- Carrier -------------------------- */
        val profileCarrier = db.profileDao().getProfileCarrierInfoById(1)
        ui.docketTxt.text = "MC" + profileCarrier.docketNumber
        ui.dotTxt.text =  profileCarrier.DOTNumber
        ui.insuredTxt.text = "$" + NumberFormat.getNumberInstance(Locale.US).format(profileCarrier.cargoInsured.toBigDecimal())
        ui.plateTxt.text = profileCarrier.licensePlate

        ui.shiftEditBtn.setOnClickListener {
            modifiedIndicator = 1

            // Show dialog
            val shifts = arrayOf("Day", "Night")

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Please select a shift preference")
            builder.setItems(shifts){_, which ->
                ui.shiftTxt.text = shifts[which]
            }
            builder.show()
        }

        ui.distanceEditBtn.setOnClickListener {
            modifiedIndicator = 1
            // Show dialog
            val distances = arrayOf("Short", "Long")

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Please select a distance preference")
            builder.setItems(distances){_, which ->
                ui.distanceTxt.text = distances[which]
            }
            builder.show()
        }

        ui.emailEditBtn.setOnClickListener {
            modifiedIndicator = 1
            // hide textView, show editView. Hide edit button, show ok button
            ui.emailTxt.visibility = View.GONE
            ui.emailEditBtn.visibility = View.GONE
            ui.emailInput.visibility = View.VISIBLE
            ui.emailUpdateBtn.visibility = View.VISIBLE
        }

        ui.emailUpdateBtn.setOnClickListener{
            val userEmailInput = ui.emailInput.text
            ui.emailTxt.text = userEmailInput

            ui.emailTxt.visibility = View.VISIBLE
            ui.emailEditBtn.visibility = View.VISIBLE
            ui.emailInput.visibility = View.GONE
            ui.emailUpdateBtn.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            if (modifiedIndicator == 1){
                // send new data to backend
                db.profileDao().updateProfileWorkPreference(ProfileWorkPreference(1,
                        ui.shiftTxt.text.toString(), ui.distanceTxt.text.toString()))
                val curBasicInfo = db.profileDao().getProfileBasicInfoById(1)
                curBasicInfo.email = ui.emailTxt.text.toString()
                db.profileDao().updateProfileBasicInfo(curBasicInfo)
            }
            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun dotColor(gracePeriod : Int, expDate : String) : Int {
        val sdf = SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH)
        val expirationDate = sdf.parse(expDate)
        val currentDate = Date()

        val diffInMillies = expirationDate.time - currentDate.time
        val diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS)

        return when {
            diff < 0           -> R.drawable.red_rounded_button
            diff > gracePeriod -> R.drawable.orange_rounded_item
            else               -> R.drawable.green_rounded_button
        }
    }
}
