package com.dray.dray.database

import androidx.room.*
import com.dray.dray.daos.*
import com.dray.dray.dataClasses.User
import com.dray.dray.dataClasses.leg.*
import com.dray.dray.dataClasses.profile.*

@Database(entities = [User::class,
    Profile::class,
    ProfileBasicInfo::class, ProfileQualifiedToDrive::class,ProfileTMC::class,
    ProfileWorkPreference::class, ProfileCarrier::class,
    Leg::class,
    LegContainerInfo::class, LegPickUpInfo::class, LegDeliverToInfo::class,
    LegEquipmentInfo::class, LegRate::class], version = 6)
abstract class AppDatabase : RoomDatabase() {
        abstract fun profileDao(): ProfileDao
        abstract fun legDao() : LegDao
}