package com.dray.dray.workflow.backToTerminal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.backToTerminal.ui.BackToTerminalExportComponent
import com.dray.dray.workflow.backToTerminal.ui.BackToTerminalImportComponent
import org.jetbrains.anko.setContentView

class BackToTerminalActivity : AppCompatActivity() {

    val importUI = BackToTerminalImportComponent()
    val exportUI = BackToTerminalExportComponent()

    lateinit var db : AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Terminal In-Gate")

        db = getDatabase(applicationContext)
        val firstLegInfo = db.legDao().getLegInfoById(1)
        val firstLegEquipmentInfo = db.legDao().getLegEquipmentInfoById(1)
        val secondLegContainerInfo = db.legDao().getLegContainerInfo(2)

        if (firstLegInfo.legType == "IMPORT") {
            importUI.setContentView(this)

            importUI.containerNo.text = secondLegContainerInfo.containerNumber
            importUI.SCACCode.text = db.profileDao().getProfileTMCInfoById(1).SCACCode

            val apptNumber = db.legDao().getLegDeliverToInfo(2).apptNumber
            if (apptNumber.isEmpty()) {
                importUI.appointmentBlock.visibility = View.GONE
            } else {
                importUI.appointmentNo.text = apptNumber
            }

            importUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                displayReportWindow(applicationContext, "false", "false")
            }

            importUI.nextBtn.setOnClickListener {
                goToNextActivity()
            }
        } else {
            exportUI.setContentView(this)

            exportUI.bookingNo.text = firstLegInfo.bolBookingNumber
            exportUI.SCACCode.text = db.profileDao().getProfileTMCInfoById(1).SCACCode

            val containerNo = secondLegContainerInfo.containerNumber
            if (containerNo.isEmpty()){
                exportUI.containerNoBox.visibility = View.GONE
            } else {
                exportUI.containerNo.text = containerNo
            }

            exportUI.containerSize.text = secondLegContainerInfo.containerSize

            val sealNo  = secondLegContainerInfo.sealNumber
            if (sealNo.isEmpty()) {
                exportUI.sealNoBox.visibility = View.GONE
            } else {
                exportUI.sealNo.text = secondLegContainerInfo.sealNumber
            }
            exportUI.chassisOwner.text = firstLegEquipmentInfo.providerName

            val apptNumber = db.legDao().getLegDeliverToInfo(2).apptNumber
            if (apptNumber.isEmpty()) {
                exportUI.appointmentBox.visibility = View.GONE
            } else {
                exportUI.appointmentNo.text = apptNumber
            }

            exportUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                displayReportWindow(applicationContext, "false", "false")
            }

            exportUI.nextBtn.setOnClickListener {
                goToNextActivity()
            }
        }
    }

    private fun goToNextActivity () {
        val wantNextLoad = intent.getStringExtra("WANT_NEXT_LOAD")
        var i = Intent(this, BackToTerminalInterchangeDocActivity :: class.java)
        i.putExtra("WANT_NEXT_LOAD", wantNextLoad)
        startActivity(i)
    }

}
