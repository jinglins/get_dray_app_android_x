package com.dray.dray.workflow.newLeg.confirm.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.newLeg.confirm.ConfirmNewLegActivity
import org.jetbrains.anko.*

class RegularImportItemComponent : AnkoComponent<ConfirmNewLegActivity>, AnkoLogger {
    lateinit var containerNumberTxt : TextView
    lateinit var originTxt : TextView
    lateinit var destinationTxt : TextView
    lateinit var sizeTxt : TextView
    lateinit var weightTxt : TextView
    lateinit var flatRateTxt : TextView
    lateinit var bonusTxt : TextView
    lateinit var chassisTxt : TextView
    lateinit var fuelTxt : TextView
    lateinit var totalTxt : TextView

    lateinit var receiveTypeTxt : TextView
    lateinit var equipmentTxt: TextView

    lateinit var backBtn : Button
    lateinit var confirmBtn : Button

    val MEDIUM_TEXT_SIZE = 18f
    val DATA_TEXT_SIZE = 24f
    val LARGE_TEXT_SIZE = 30f

    override fun createView(ui: AnkoContext<ConfirmNewLegActivity>): View = with(ui) {

        verticalLayout(){
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            scrollView{

                verticalLayout {
                    verticalLayout {
                        lparams(width = matchParent, height = wrapContent) {
                            backgroundColor = Color.WHITE
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            bottomMargin = dimen(R.dimen.small_gap)
                        }

                        textView("Container #") {
                            textColor = R.color.colorPrimary
                        }

                        containerNumberTxt = textView("ABCD1234567") {
                            textColor = Color.BLACK
                            textSize = LARGE_TEXT_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    verticalLayout {
                        lparams(width = matchParent, height = wrapContent) {
                            backgroundColor = Color.WHITE
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.small_gap)
                        }

                        textView("Origin") {
                            textColor = R.color.colorPrimary
                        }

                        originTxt = textView("ORIGIN") {
                            textColor = Color.BLACK
                            textSize = DATA_TEXT_SIZE
                        }
                    }

                    verticalLayout {
                        lparams(width = matchParent, height = wrapContent) {
                            backgroundColor = Color.WHITE
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.small_gap)
                        }

                        textView("Destination") {
                            textColor = R.color.colorPrimary
                        }

                        destinationTxt = textView("RANCHO CUCAMONGA") {
                            textColor = Color.BLACK
                            textSize = DATA_TEXT_SIZE
                        }
                    }

                    linearLayout {
                        lparams(width = matchParent){verticalMargin = dimen(R.dimen.small_gap)}

                        verticalLayout {
                            backgroundColor = Color.WHITE
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            lparams(width = matchParent){
                                weight = 1f
                                rightMargin = dimen(R.dimen.small_gap)
                            }

                            textView("Type/Size") {
                                textColor = R.color.colorPrimary
                            }

                            sizeTxt = textView("40HC") {
                                textColor = Color.BLACK
                                textSize = DATA_TEXT_SIZE
                            }
                        }

                        verticalLayout {
                            backgroundColor = Color.WHITE
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            lparams(width = matchParent){
                                weight = 1f
                                leftMargin = dimen(R.dimen.small_gap)
                            }

                            textView("Weight") {
                                textColor = R.color.colorPrimary
                            }

                            weightTxt = textView("0000.00(lb)") {
                                textColor = Color.BLACK
                                textSize = DATA_TEXT_SIZE
                            }
                        }

                    }

                    linearLayout {
                        lparams(width = matchParent){
                            topMargin = dimen(R.dimen.small_gap)
                            bottomMargin = dimen(R.dimen.item_vertical_margin)
                        }

                        verticalLayout {
                            backgroundColor = Color.WHITE
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            lparams(width = matchParent){
                                weight = 1f
                                rightMargin = dimen(R.dimen.small_gap)
                            }

                            textView("Receive Type") {
                                textColor = R.color.colorPrimary
                            }

                            receiveTypeTxt = textView("Drop-pull") {
                                textColor = Color.BLACK
                                textSize = DATA_TEXT_SIZE
                            }
                        }

                        verticalLayout {
                            backgroundColor = Color.WHITE
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            lparams(width = matchParent){
                                weight = 1f
                                leftMargin = dimen(R.dimen.small_gap)
                            }

                            textView("Equipment") {
                                textColor = R.color.colorPrimary
                            }

                            equipmentTxt = textView("Chassis") {
                                textColor = Color.BLACK
                                textSize = DATA_TEXT_SIZE
                            }
                        }

                    }

                    verticalLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent){
                            topMargin = dimen(R.dimen.item_vertical_margin)
                            bottomMargin = dimen(R.dimen.small_gap)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        }

                        linearLayout{
                            lparams(width = matchParent){
                                bottomMargin = dip(5)
                            }

                            textView("Flat Rate"){
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 16f
                            }.lparams{
                                weight = 1f
                            }
                            flatRateTxt = textView("$0.00"){
                                textColor = Color.BLACK
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 16f
                            }.lparams{
                                weight = 0f
                            }
                        }
                        linearLayout{
                            textView("Fuel Rate"){
                                textColor = R.color.colorPrimary
                            }.lparams{
                                leftPadding = dip(20)
                                weight = 1f
                            }
                            fuelTxt = textView("$0.00")
                        }
                        linearLayout{
                            textView("Bonus Rate"){
                                textColor = R.color.colorPrimary

                            }.lparams{
                                leftPadding = dip(20)
                                weight = 1f
                            }
                            bonusTxt = textView("$0.00")
                        }
                        linearLayout{
                            textView("Chassis Off-Hire"){
                                textColor = R.color.colorPrimary
                            }.lparams{
                                leftPadding = dip(20)
                                weight = 1f
                            }
                            chassisTxt = textView("$0.00")
                        }


                    }
                    linearLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent){
                            verticalMargin = dimen(R.dimen.small_gap)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        }
                        textView("Total Pay").lparams{
                            gravity = Gravity.CENTER_VERTICAL
                            weight = 1f
                        }

                        totalTxt = textView("$400.00"){
                            textColor = Color.BLACK
                            textSize = LARGE_TEXT_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            weight = 0f
                        }
                    }
                }
            }.lparams(width = matchParent){
                weight = 1f
            }

            // Button Row
            linearLayout(){
                lparams(width = matchParent, height = wrapContent){
                    topMargin = dimen(R.dimen.activity_vertical_margin)
                    gravity = Gravity.BOTTOM
                    weight = 0f
                }

                backBtn = button("back"){
                    backgroundResource = R.drawable.left_rounded_button
                    textSize = MEDIUM_TEXT_SIZE
                    textColor = resources.getColor(R.color.defaultLightTxtColor)
                }.lparams(){
                    weight = 0.5f
                    rightMargin = dimen(R.dimen.countdown_gap)
                }

                confirmBtn = button("confirm"){
                    backgroundResource = R.drawable.right_rounded_button
                    textColor = Color.WHITE
                    textSize = MEDIUM_TEXT_SIZE
                }.lparams(){
                    weight = 0.5f
                    leftMargin = dimen(R.dimen.countdown_gap)
                }
            }
        }
    }

}