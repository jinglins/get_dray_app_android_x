package com.dray.dray.feedbackActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.dray.dray.R
import com.dray.dray.feedbackActivity.ui.FeedbackComponent
import org.jetbrains.anko.setContentView

class FeedbackActivity : AppCompatActivity() {
    val ui = FeedbackComponent()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setTitle(R.string.feedback_activity_title)

        ui.submitBtn.setOnClickListener {
            val feedback = ui.feedbackInput.text
            // Send feedback to backend
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId === android.R.id.home) {
            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }

}
