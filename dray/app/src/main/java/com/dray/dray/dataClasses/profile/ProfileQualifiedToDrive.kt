package com.dray.dray.dataClasses.profile

import androidx.room.*

@Entity (tableName = "profile_qualified_to_drive")
data class ProfileQualifiedToDrive (
    @PrimaryKey var id : Int,

    @ColumnInfo (name = "medical_expire")   var medicalExpire : String,
    @ColumnInfo (name = "insurance_expire") var insuranceExpire : String,
    @ColumnInfo (name = "twic_expire")      var TWICExpire : String,
    @ColumnInfo (name = "bit_expire")       var BITExpire : String,
    @ColumnInfo (name = "annual_expire")    var annualExpire : String,
    @ColumnInfo (name = "ddl_expire")       var DDLExpire : String ,
    @ColumnInfo (name = "ddl_number")       var DDLNumber : String,
    @ColumnInfo (name = "dmv_printout_received") var dmvPrintOutReceived : Boolean,
    @ColumnInfo (name = "dmv_violation_points")  var dmvViolationPoints : Int
)