package com.dray.dray.paymentActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.dray.dray.R
import com.dray.dray.adapter.checkTripHistoryAdapter
import com.dray.dray.customClasses.CheckTripHistoryItem
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.paymentActivity.ui.CheckTripHistoryComponent
import org.jetbrains.anko.*

class CheckTripHistoryActivity : AppCompatActivity() {
    val ui = CheckTripHistoryComponent()
    lateinit var mAdapter : checkTripHistoryAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)
        var checkTripHistoryItems = ArrayList<CheckTripHistoryItem>()
        checkTripHistoryItems.add(CheckTripHistoryItem("SFGE0248924", "53403965",
                "4h 23m", false, "WBCT", "12/30/2018 13:04:34",
                "RANCHO CUCAMONGA", "12/30/2018 19:34:29",
                "234.45", "23.86", "0.00", "32.43","0.00", "2.03",
                false, false,"", "", ""))

        checkTripHistoryItems.add(CheckTripHistoryItem("DSAF2345634", "48694332",
                "3h 21m", true, "RANCHO CUCAMONGA", "12/30/2018 20:34:53",
                "EAGLE MARINE", "12/30/2018 22:24:52",
                "124.53", "23.53", "0.00", "0.00","23.92", "0.00",
                false, false,"", "", ""))

        checkTripHistoryItems.add(CheckTripHistoryItem("VZXC3423513", "28096233",
                "5h 53m", false, "YARD", "12/31/2018 00:04:34",
                "REDLANDS", "12/31/2018 01:45:56",
                "300.45", "30.86", "12.00", "64.45", "0.00","12.32",
                false, false,"", "", ""))

        checkTripHistoryItems.add(CheckTripHistoryItem("SFGE0248924", "53403965",
                "2h 23m", true, "REDLANDS", "12/31/2018 01:14:46",
                "LBCT", "12/31/2018 02:32:29",
                "134.45", "34.86", "0.00", "34.30", "0.00","0.00",
                false, false,"", "", ""))

        checkTripHistoryItems.add(CheckTripHistoryItem("FKOE2453245", "23154243",
                "2h 35m", false, "PIER E", "12/31/2018 08:30:34",
                "YARD", "12/31/2018 09:29:23",
                "56.45", "12.86", "0.00", "0.00", "0.00","0.00",
                false, false,"", "", ""))


        mAdapter = checkTripHistoryAdapter(this, checkTripHistoryItems)
        ui.lv.adapter = mAdapter

        var toolbar = supportActionBar!!
        setToolBar(toolbar, "My Driver Manifest")

        ui.submitBtn.setOnClickListener {
            var allSelected = 1
            checkTripHistoryItems.forEach { it ->
                if (!it.approved && !it.disputed){
                    allSelected = 0
                }
            }

            if (allSelected == 0) {
                toast("Please scroll down and make sure that for each leg, you either approved it or dispute it. ")
            } else {
                toast ("Thank you! We will process the check for the approved legs, and review your disputes. ")
                finish()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId === android.R.id.home) {
            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }
}
