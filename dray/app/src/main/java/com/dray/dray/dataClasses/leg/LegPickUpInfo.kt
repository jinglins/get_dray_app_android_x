package com.dray.dray.dataClasses.leg

import androidx.room.*

@Entity (tableName = "leg_pick_up_info")
data class LegPickUpInfo (
        @PrimaryKey var id : Int ,

        @ColumnInfo (name = "origin_name") var originName :       String,
        @ColumnInfo (name = "origin_address") var originAddress : String,
        @ColumnInfo (name = "appt_time") var apptTime :           String,
        @ColumnInfo (name = "appt_number") var apptNumber :       String
)