package com.dray.dray.customClasses

class PaymentHistoryItem (pAmount : Number,pDate : String ,pMethod : String){
    var paymentAmount : Number ?= null
    var paymentDate : String ?= null
    var paymentMethod : String ?= null

    init {
        this.paymentAmount = pAmount
        this.paymentDate = pDate
        this.paymentMethod = pMethod
    }
}