package com.dray.dray.dataClasses.profile

import androidx.room.*

@Entity (tableName = "profile_carrier_info")
data class ProfileCarrier (
    @PrimaryKey var id : Int,

    @ColumnInfo (name = "docket_number") var docketNumber : String,
    @ColumnInfo (name = "dot_number")    var DOTNumber : String,
    @ColumnInfo (name = "cargo_insured") var cargoInsured : String,
    @ColumnInfo (name = "license_plate") var licensePlate : String
)