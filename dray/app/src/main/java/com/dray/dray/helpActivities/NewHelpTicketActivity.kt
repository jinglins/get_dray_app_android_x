package com.dray.dray.helpActivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import android.view.MenuItem
import com.dray.dray.R
import com.dray.dray.customClasses.HelpTicketItem
import com.dray.dray.helpActivities.ui.NewHelpTicketComponent
import org.jetbrains.anko.setContentView
import java.sql.Timestamp
import java.time.LocalDate

class NewHelpTicketActivity : AppCompatActivity() {

    val ui = NewHelpTicketComponent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setTitle(R.string.title_activity_help)

        ui.submitBtn.setOnClickListener {
            // Get inputs from user
            val description : String = ui.inputText.text.toString()
            val timestamp = Timestamp(System.currentTimeMillis())

            println("description: $description")
            println("Time Stamp : $timestamp")

            val newTicket = HelpTicketItem("Received", "",
                    description, timestamp, "")

            // TODO: Push newTicket to the backend. Get the ticket#.

            // Show a confirmation dialog
            val builder = AlertDialog.Builder(this@NewHelpTicketActivity)
            builder.setMessage("Thank you. We have received your help ticket. " +
                    "The ticket number is A000. We will send you a push notification " +
                    "when we solve the issue. ")

            val dialog: AlertDialog = builder.create()
            dialog.show()
            Handler().postDelayed(Runnable {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
                finish()
            }, 5000) //change 5000 with a specific time you want


        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId === android.R.id.home) {
            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }
}
