package com.dray.dray.customClasses

import java.io.FileDescriptor
import java.sql.Timestamp

class HelpTicketItem (val status : String, val ticketNumber : String,
                      val description : String, val receiveDate : Timestamp,
                      val solution : String){

}