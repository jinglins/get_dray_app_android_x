package com.dray.dray.workflow.newLeg.confirm.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.newLeg.confirm.ConfirmNewLegActivity
import org.jetbrains.anko.*

class FreeflowImportItemComponent : AnkoComponent<ConfirmNewLegActivity>, AnkoLogger {

    lateinit var freeflowCodeTxt : TextView
    lateinit var originTxt : TextView
    lateinit var equipmentTxt : TextView

    lateinit var lv : ListView

    lateinit var backBtn : Button
    lateinit var confirmBtn : Button

    val MEDIUM_TEXT_SIZE = 18f
    val DATA_TEXT_SIZE = 24f
    val LARGE_TEXT_SIZE = 30f

    override fun createView(ui: AnkoContext<ConfirmNewLegActivity>): View = with(ui){
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            verticalLayout{
                lparams(){
                    weight = 1f
                }

                verticalLayout{
                    lparams(width = matchParent, height = wrapContent) {
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        bottomMargin = dimen(R.dimen.small_gap)
                    }

                    textView("Free-flow Code/Release #") {
                        textColor = R.color.colorPrimary
                    }

                     freeflowCodeTxt = textView("CMABN") {
                        textColor = Color.BLACK
                        textSize = LARGE_TEXT_SIZE
                        typeface = Typeface.DEFAULT_BOLD
                    }
                }

                verticalLayout{
                    lparams(width = matchParent, height = wrapContent) {
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        bottomMargin = dimen(R.dimen.small_gap)
                    }

                    textView("Equipment") {
                        textColor = R.color.colorPrimary
                    }

                    equipmentTxt = textView("Chassis") {
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }

                verticalLayout{
                    lparams(width = matchParent, height = wrapContent) {
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        topMargin = dimen(R.dimen.small_gap)
                        bottomMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    textView("Origin") {
                        textColor = R.color.colorPrimary
                    }

                    originTxt = textView("TERMINAL") {
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }

                verticalLayout {
                    backgroundColor = R.color.colorPrimary
                    lparams(width = matchParent){
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                    }
                    textView("ALL POSSIBLE DESTINATIONS"){
                        textColor = Color.WHITE
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }
                }

                lv = listView(){
                    // init adapter in orCustomerActivity
                    divider = null
                    dividerHeight = dip(10)
                }

            }

            // Button Row
            linearLayout(){
                lparams(width = matchParent, height = wrapContent){
                    topMargin = dimen(R.dimen.activity_vertical_margin)
                    gravity = Gravity.BOTTOM
                    weight = 0f
                }

                backBtn = button("back"){
                    backgroundResource = R.drawable.left_rounded_button
                    textSize = MEDIUM_TEXT_SIZE
                    textColor = resources.getColor(R.color.defaultLightTxtColor)
                }.lparams(){
                    weight = 0.5f
                    rightMargin = dimen(R.dimen.countdown_gap)
                }

                confirmBtn = button("confirm"){
                    backgroundResource = R.drawable.right_rounded_button
                    textColor = Color.WHITE
                    textSize = MEDIUM_TEXT_SIZE
                }.lparams(){
                    weight = 0.5f
                    leftMargin = dimen(R.dimen.countdown_gap)
                }
            }
        }
    }

}