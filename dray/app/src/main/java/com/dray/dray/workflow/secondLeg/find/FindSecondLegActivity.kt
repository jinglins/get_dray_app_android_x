package com.dray.dray.workflow.secondLeg.find

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.dataClasses.leg.*
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.secondLeg.find.ui.FindEmptySecondLegComponent
import com.dray.dray.workflow.hookUpInstruction.HookUpInstructionActivity
import com.dray.dray.workflow.outgate.customer.CustomerOutgateActivity
import com.dray.dray.workflow.secondLeg.select.SelectSecondLegActivity
import org.jetbrains.anko.setContentView

class FindSecondLegActivity : AppCompatActivity() {

    val ui = FindEmptySecondLegComponent()
    lateinit var db : AppDatabase

    // TODO: Pass container number to the backend and get other data
    val locationData = "SA1"
    val steamshiplineData = "YANGMING"
    val sizeData = "40H"
    val flatRate = "230.23"
    val offHireRate = "23.00"

    lateinit var containerNumber : String
    lateinit var returnToTerminals : String
    lateinit var distance : String

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        db = getDatabase(applicationContext)

        // Get passed string
        containerNumber = intent.getStringExtra("CONTAINER_NO")
        returnToTerminals = intent.getStringExtra("RETURN_TO")
        distance = intent.getStringExtra("DISTANCE")
        println("distance : $distance")

        val toolbar = supportActionBar!!
        val activityTitle = if (distance == "0") "Find the Container" else "Confirm Your Selection"
        setWorkFlowToolBar(toolbar, activityTitle)

        // Render UI
        if (distance !== "0"){
            ui.confirmBtn.text = "Confirm"
        }

        displayData()

        // Button on-click
        ui.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }

        ui.backBtn.setOnClickListener {
            val i = Intent(this, SelectSecondLegActivity::class.java)
            startActivity(i)
        }

        ui.confirmBtn.setOnClickListener {
            if (distance == "0") {
                injectSecondLegData(containerNumber,returnToTerminals, locationData)
                val i = Intent(this, HookUpInstructionActivity::class.java)
                i.putExtra("EXIT_STATUS", "empty")
                startActivity(i)
            } else {
                injectSecondLegData(containerNumber,returnToTerminals, locationData)
                val i = Intent (this, CustomerOutgateActivity::class.java)
                i.putExtra("DISTANCE", distance)
                i.putExtra("EXIT_STATUS", "bobtail")
                startActivity(i)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun displayData (){
        ui.containerNumber.text = containerNumber
        ui.location.text = locationData
        ui.returnTerminal.text = returnToTerminals
        ui.steamshipline.text = steamshiplineData
        ui.size.text = sizeData

        ui.flatRate.text = "$$flatRate"
        ui.chassisRate.text = "$$offHireRate"
        val totalRate = (flatRate.toBigDecimal() + offHireRate.toBigDecimal()).toString()
        ui.totalRate.text = "$$totalRate"
    }

    private fun injectSecondLegData (containerNumber : String, returnTo: String, location: String) {
        // ids of second leg are 2
        val sealNumber = if (db.legDao().getLegInfoById(1).receiveType == "LIVE"){
            db.legDao().getLegContainerInfo(1).sealNumber
        } else {
            ""
        }

        val containerInfo = LegContainerInfo(2, containerNumber, "REGULAR",
                sizeData, "", sealNumber)
        val pickUpInfo = LegPickUpInfo(2, location,
                "cur_loc_address",
                "", "")
        val deliverToInfo = LegDeliverToInfo(2, returnTo,
                "returnToAddr",
                "", "")
        var equipmentInfo = db.legDao().getLegEquipmentInfoById(1)
        equipmentInfo.id = 2
        val rate = LegRate(2, flatRate, "12.03", offHireRate,
                "12.33", "", "")

        if (db.legDao().getLegInfoById(2)== null) {
            db.legDao().insertLegContainerInfo(containerInfo)
            db.legDao().insertPickUpInfo(pickUpInfo)
            db.legDao().insertDeliverToInfo(deliverToInfo)
            db.legDao().insertEquipmentInfo(equipmentInfo)
            db.legDao().insertLegRate(rate)

            db.legDao().insertAnLeg(Leg(2, "", "",
                    "",
                    "", "","",
                    steamshiplineData,
                    2, 2, 2, 2, 2))
        } else {
            db.legDao().updateLegContainerInfo(containerInfo)
            db.legDao().updateLegPickUpInfo(pickUpInfo)
            db.legDao().updateLegDeliverToInfo(deliverToInfo)
            db.legDao().updateLegEquipmentInfo(equipmentInfo)
            db.legDao().updateLegRate(rate)

            db.legDao().updateAnLeg(Leg(2, "IMPORT", "DROPPULL",
                    "",
                    "ONKEYE8JSDFVASF034", "","VDFV",
                    steamshiplineData,
                    2, 2, 2, 2, 2))
        }
    }
}

