package com.dray.dray.dataClasses.leg

import androidx.room.*

@Entity (tableName = "leg_rate")
data class LegRate (
        @PrimaryKey var id : Int,

        @ColumnInfo (name = "flat_rate")       var flatRate : String,
        @ColumnInfo (name = "fuel_rate")       var fuelRate : String,
        @ColumnInfo (name = "hire_off_rate")   var hireOffRate : String,
        @ColumnInfo (name = "bonus_rate")      var bonusRate : String,
        @ColumnInfo (name = "additional_rate") var additionalRate : String,
        @ColumnInfo (name = "deduction_rate")  var deductionRate : String
)