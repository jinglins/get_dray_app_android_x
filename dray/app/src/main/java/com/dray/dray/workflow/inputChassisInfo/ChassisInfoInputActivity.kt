package com.dray.dray.workflow.inputChassisInfo

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.view.View
import android.widget.Button
import com.dray.dray.R
import com.dray.dray.workflow.inputChassisInfo.ui.ChassisInfoInputComponent
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.dataClasses.*
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.atPlace.AtTerminalActivity
import com.dray.dray.workflow.mapActivity.MapActivity
import org.jetbrains.anko.*


class ChassisInfoInputActivity : AppCompatActivity() {
    val ui = ChassisInfoInputComponent()
    lateinit var db : AppDatabase

    lateinit var chassisType : String

    var chassisNumberPhotoTaken  = 0
    var chassisTypeSelected = 0

    val CAMERA_REQUEST_CODE = 133

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)
        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Tell Us about the Chassis")
        db = getDatabase(applicationContext)

        val chassisInfo = db.legDao().getLegEquipmentInfoById(1)

        if (chassisInfo.equipmentType == "PRIVATE"){
            ui.chassisTypeSelectionArea.visibility = View.GONE
            ui.chassisTypeTitleArea.visibility = View.GONE
            chassisType = "PRIVATE"
            chassisTypeSelected = 1
        }

        userInputInteraction ()

        ui.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }

        ui.chassisNumberPhotoBox.setOnClickListener {
            captureImage()
        }

        ui.submitBtn.setOnClickListener {
            if (chassisType == "WCCP"){chassisInfo.equipmentType = "WCCP"}
            db.legDao().updateLegEquipmentInfo(chassisInfo)
            // send chassis number & chassis type to the backend

            lateinit var i : Intent
            if (chassisInfo.equipmentType == "PRIVATE"){
                i = Intent(this, MapActivity::class.java)
                i.putExtra("TARGET", "TERMINAL")
                i.putExtra("WITH_CHASSIS", "true")
            } else {
                i = Intent(this, AtTerminalActivity :: class.java)
                i.putExtra("WITH_CHASSIS", "false")
            }

            startActivity(i)
        }
    }

    private fun captureImage() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CAMERA_REQUEST_CODE -> try {
                //When image is captured successfully
                if (resultCode == RESULT_OK) {
                    chassisNumberPhotoTaken = 1

                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    showCapturedImage(imageBitmap)

                    ui.photoIcon.visibility = View.GONE
                    ui.chassisNumberDisplay.visibility = View.VISIBLE

                    ui.chassisNumberPhotoBox.backgroundResource = R.drawable.purple_bordered_round_btn

                    if (allCleared()) { ui.submitBtn.visibility = View.VISIBLE }
                } else
                    toast(R.string.cancel_message)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /*  Show Captured over ImageView  */
    private fun showCapturedImage(image : Bitmap) {
        ui.chassisNumberDisplay.setImageBitmap(image)
    }

    private fun allCleared () : Boolean {
        return ( chassisNumberPhotoTaken == 1 && chassisTypeSelected == 1 )
    }

    private fun clearHighlight () {
        val typeBtns = listOf<LinearLayout>(ui.tracBtn, ui.flexiBtn, ui.dcliBtn, ui.wccpBtn)
        val typeTxts = listOf<TextView>(ui.tracTxt, ui.flexiTxt, ui.dcliTxt, ui.wccpTxt)
        typeBtns.forEach{
            it.backgroundResource = R.drawable.dashed_square
        }

        typeTxts.forEach{
            it.textColor = Color.parseColor("#808080")
        }
    }

    private fun userInputInteraction () {
        ui.tracBtn.setOnClickListener {
            clearHighlight()
            chassisTypeSelected = 1
            ui.tracBtn.backgroundResource= R.drawable.green_rounded_button
            ui.tracTxt.textColor = Color.WHITE
            chassisType = "TRAC Intermodal"
            ui.chassisTypeSelectionArea.backgroundResource = R.drawable.purple_bordered_round_btn
            if (allCleared()) { ui.submitBtn.visibility = View.VISIBLE }
        }

        ui.flexiBtn.setOnClickListener {
            clearHighlight()
            chassisTypeSelected = 1
            ui.flexiBtn.backgroundResource= R.drawable.green_rounded_button
            ui.flexiTxt.textColor = Color.WHITE
            chassisType = "flexi-van"
            ui.chassisTypeSelectionArea.backgroundResource = R.drawable.purple_bordered_round_btn
            if (allCleared()) { ui.submitBtn.visibility = View.VISIBLE }
        }

        ui.dcliBtn.setOnClickListener {
            clearHighlight()
            chassisTypeSelected = 1
            ui.dcliBtn.backgroundResource= R.drawable.green_rounded_button
            ui.dcliTxt.textColor = Color.WHITE
            chassisType = "DCLI"
            ui.chassisTypeSelectionArea.backgroundResource = R.drawable.purple_bordered_round_btn
            if (allCleared()) { ui.submitBtn.visibility = View.VISIBLE }
        }

        ui.wccpBtn.setOnClickListener {
            clearHighlight()
            chassisTypeSelected = 1
            ui.wccpBtn.backgroundResource= R.drawable.green_rounded_button
            ui.wccpTxt.textColor = Color.WHITE
            chassisType = "WCCP"
            ui.chassisTypeSelectionArea.backgroundResource = R.drawable.purple_bordered_round_btn
            if (allCleared()) { ui.submitBtn.visibility = View.VISIBLE }
        }
    }


}
