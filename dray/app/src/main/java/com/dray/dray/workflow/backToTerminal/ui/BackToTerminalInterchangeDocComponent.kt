package com.dray.dray.workflow.backToTerminal.ui

import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import androidx.annotation.RequiresApi
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.workflow.backToTerminal.BackToTerminalActivity
import com.dray.dray.workflow.backToTerminal.BackToTerminalInterchangeDocActivity
import org.jetbrains.anko.*

class BackToTerminalInterchangeDocComponent : AnkoComponent<BackToTerminalInterchangeDocActivity>, AnkoLogger {
    lateinit var photoBtn : LinearLayout
    lateinit var nextBtn : Button
    lateinit var backBtn : Button
    lateinit var reportBtn : LinearLayout

    lateinit var instructionText : LinearLayout
    lateinit var photoDisplayArea : ImageView

    val MEDIUM_SIZE_TEXT = 18f
    val DISPATCH_SIZE_TEXT = 24f

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun createView(ui: AnkoContext<BackToTerminalInterchangeDocActivity>): View = with(ui){
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            textView("Photo Interchange Document"){
                textSize = MEDIUM_SIZE_TEXT
                textColor = resources.getColor(R.color.colorPrimary)
                typeface = Typeface.DEFAULT_BOLD
            }.lparams(){
                this.gravity = Gravity.CENTER_HORIZONTAL
            }

            view(){
                backgroundColor = resources.getColor(R.color.colorPrimary)
            }.lparams(width = dip(300), height = dip(1)){
                bottomMargin = dip(10)
                this.gravity = Gravity.CENTER_HORIZONTAL
            }

            verticalLayout{
                backgroundResource = R.drawable.green_bordered_round_btn
                lparams(width = matchParent){
                    weight = 1f
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }
                photoBtn = linearLayout{
                    lparams(width = matchParent, height = matchParent){
                        verticalPadding = dimen(R.dimen.activity_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    }
                    gravity = Gravity.CENTER

                    imageView(R.drawable.add_photo){
                        adjustViewBounds = true
                        elevation = 10f
                    }.lparams(width = dip(200), height = dip(200))
                }
                photoDisplayArea = imageView{
                    visibility = View.GONE
                    imageResource = R.drawable.example_avatar
                }.lparams(width = matchParent, height = matchParent)
            }

            nextBtn = button("Start My Next Job"){
                visibility = View.GONE
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }

            backBtn = button("Finish and Go Back Home"){
                visibility = View.GONE
                backgroundResource = R.drawable.white_rounded_button
                textColor = resources.getColor(R.color.defaultLightTxtColor)
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dip(10)
            }
        }
    }

}