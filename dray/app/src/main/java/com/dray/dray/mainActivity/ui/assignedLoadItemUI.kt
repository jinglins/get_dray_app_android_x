package com.dray.dray.mainActivity.ui

import android.graphics.Color
import android.os.Build
import android.view.Gravity
import android.view.View
import com.dray.dray.R
import com.dray.dray.customClasses.assignedLoadCardRow
import org.jetbrains.anko.*

class assignedLoadItemUI : AnkoComponent<MainActivityComponent> {
    override fun createView(ui: AnkoContext<MainActivityComponent>): View = with(ui) {
        linearLayout(){

            backgroundColor = Color.WHITE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                elevation = 6f
            }
            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            verticalPadding = dimen(R.dimen.activity_vertical_padding)

            lparams(width = matchParent, height = wrapContent){

            }

            verticalLayout(){
                val assignedLoadCardRows : Array<assignedLoadCardRow> = arrayOf(
                        assignedLoadCardRow("Origin", "PIER E"),
                        assignedLoadCardRow("Destination", "CORONA"),
                        assignedLoadCardRow("Container #", "TCKU4712597"),
                        assignedLoadCardRow("Size/Weight", "45/2331 (lb)")
                )

                for (assignedLoadItem in assignedLoadCardRows) {
                    linearLayout(){
                        textView(assignedLoadItem.label){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(100)){
                        }

                        textView(assignedLoadItem.load_data){

                        }
                    }
                }
            }.lparams(){
                weight = 0.8f
            }

            verticalLayout(){
                gravity = Gravity.CENTER_HORIZONTAL

                textView("Appointment Time"){
                    textColor = R.color.colorPrimary
                }
                textView("10:00 "){
                    textSize = 45f
                    textColor = Color.BLACK
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        textAlignment = View.TEXT_ALIGNMENT_VIEW_END
                    }
                }
            }.lparams(){
                weight = 0.2f
            }
        }
    }

}