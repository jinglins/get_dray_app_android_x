package com.dray.dray.dataClasses.leg

import androidx.room.*

@Entity (tableName = "leg_container_info")
data class LegContainerInfo (
        @PrimaryKey var id : Int,

        @ColumnInfo (name = "container_number") var containerNumber : String,
        @ColumnInfo (name = "container_type") var containerType : String,
        @ColumnInfo (name = "container_size") var containerSize : String,
        @ColumnInfo (name = "container_weight") var containerWeight : String,
        @ColumnInfo (name = "seal_number") var sealNumber : String
)