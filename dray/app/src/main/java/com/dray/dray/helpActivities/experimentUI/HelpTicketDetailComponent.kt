package com.dray.dray.helpActivities.experimentUI

import android.graphics.Color
import android.view.View
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.helpActivities.NewHelpTicketActivity
import org.jetbrains.anko.*
import java.io.FileDescriptor

class HelpTicketDetailComponent : AnkoComponent<NewHelpTicketActivity>, AnkoLogger {
    lateinit var ticketNumber : TextView
    lateinit var dateReceived : TextView
    lateinit var status : TextView
    lateinit var description : TextView
    lateinit var solution : TextView

    val LABEL_WIDTH = 120
    val MEDIUM_TEXT_SIZE = 18f

    override fun createView(ui: AnkoContext<NewHelpTicketActivity>): View = with(ui){
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            linearLayout{
                textView("Ticket No."){
                    textColor = R.color.colorPrimary
                    textSize = MEDIUM_TEXT_SIZE
                }.lparams(width = dip(LABEL_WIDTH))
                ticketNumber = textView("A000"){
                    textColor = Color.BLACK
                    textSize = MEDIUM_TEXT_SIZE
                }
            }
            linearLayout{
                textView("Received on"){
                    textColor = R.color.colorPrimary
                    textSize = MEDIUM_TEXT_SIZE
                }.lparams(width = dip(LABEL_WIDTH))
                dateReceived = textView("Oct. 14, 2018"){
                    textColor = Color.BLACK
                    textSize = MEDIUM_TEXT_SIZE
                }
            }
            linearLayout{
                textView("Status"){
                    textColor = R.color.colorPrimary
                    textSize = MEDIUM_TEXT_SIZE
                }.lparams(width = dip(LABEL_WIDTH))
                status = textView("Solved on Oct. 15, 2018"){
                    textColor = Color.BLACK
                    textSize = MEDIUM_TEXT_SIZE
                }
            }
            linearLayout{
                textView("Description"){
                    textColor = R.color.colorPrimary
                    textSize = MEDIUM_TEXT_SIZE
                }.lparams(width = dip(LABEL_WIDTH))
                description = textView("No description."){
                    textColor = Color.BLACK
                    textSize = MEDIUM_TEXT_SIZE
                }
            }
            linearLayout{
                textView("Solution"){
                    textColor = R.color.colorPrimary
                    textSize = MEDIUM_TEXT_SIZE
                }.lparams(width = dip(LABEL_WIDTH))
                solution = textView("No solution."){
                    textColor = Color.BLACK
                    textSize = MEDIUM_TEXT_SIZE
                }
            }

            button("Cancel This Ticket"){

            }
        }
    }

}