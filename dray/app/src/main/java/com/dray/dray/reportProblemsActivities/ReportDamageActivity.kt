package com.dray.dray.reportProblemsActivities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ui.ReportDamageComponent
import com.dray.dray.customClasses.setToolBar
import org.jetbrains.anko.*
import java.io.ByteArrayOutputStream
import java.io.OutputStream


class ReportDamageActivity : AppCompatActivity() {
    val REQUEST_IMAGE_CAPTURE = 1
    val ui = ReportDamageComponent()
    var photoCount = 0
    private val photoDisplays = ArrayList<ImageView>()
    var damageType : String = ""
    var damageDescription : String = ""
    var photoJPEGs = ArrayList<OutputStream>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        val reportObject = intent.getStringExtra("OBJECT")
        val activityTitle = if (reportObject == "container") "Report Container Damage"
                                    else "Report Equipment Damage"
        val toolbar = supportActionBar!!
        setToolBar(toolbar, activityTitle)

        if (reportObject == "equipment"){
            ui.damageTypeLabel.visibility = View.GONE
            ui.damageTypeBox.visibility = View.GONE
        }

        photoDisplays.addAll(listOf(ui.zeroPhotoDisplay, ui.onePhotoDisplay,
                ui.twoPhotoDisplay, ui.threePhotoDisplay, ui.fourPhotoDisplay, ui.fivePhotoDisplay,
                ui.sixPhotoDisplay, ui.sevenPhotoDisplay, ui.eightPhotoDisplay, ui.ninePhotoDisplay))

        photoDisplays.forEach {
            it.visibility = View.GONE
        }

        typeSelectionInteraction()

        ui.damagePhotoBtn.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(packageManager)?.also {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }

        ui.submitBtn.setOnClickListener{
            val descriptText = ui.descriptionInput.text
            // TODO: Send photos, damage type and damage time and descriptText to the backend
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap

            // convert to jpeg, not tested
            val stream = ByteArrayOutputStream()
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            photoJPEGs.add(stream)

            photoCount ++

            // Show the image in the corresponding display
            if (photoCount < 10){
                photoDisplays[photoCount].setImageBitmap(imageBitmap)
                photoDisplays[photoCount].visibility = View.VISIBLE
            } else {
                photoDisplays[0].setImageBitmap(imageBitmap)
                // Hide take photo button
                ui.damagePhotoBtn.visibility = View.GONE
                // Show the 0th image display area
                ui.zeroPhotoDisplay.visibility = View.VISIBLE
                // Display a toast saying "reach cap"
                Toast.makeText(this, "At most 10 photos.", Toast.LENGTH_SHORT).show()
            }

            if (photoCount > 4) {
                ui.secondRow.visibility = View.VISIBLE
            }
        }
    }

    private fun typeSelectionInteraction () {
        ui.typeAccident.setOnClickListener {
            clearDamageTypeSelection()
            ui.typeAccident.backgroundResource = R.drawable.green_rounded_button
            ui.accidentText.textColor = Color.WHITE
            damageType = "accident"
        }

        ui.typeTheft.setOnClickListener {
            clearDamageTypeSelection()
            ui.typeTheft.backgroundResource = R.drawable.green_rounded_button
            ui.theftText.textColor = Color.WHITE
            damageType = "theft"
        }

        ui.typeWater.setOnClickListener{
            clearDamageTypeSelection()
            ui.typeWater.backgroundResource = R.drawable.green_rounded_button
            ui.waterText.textColor = Color.WHITE
            damageType = "water damage"
        }

        ui.typeFire.setOnClickListener {
            clearDamageTypeSelection()
            ui.typeFire.backgroundResource = R.drawable.green_rounded_button
            ui.fireText.textColor = Color.WHITE
            damageType = "fire damage"
        }

    }

    private fun clearDamageTypeSelection () {
        val damageTypes = listOf<LinearLayout>(ui.typeAccident, ui.typeTheft, ui.typeWater, ui.typeFire)
        val damageTypeTexts = listOf<TextView>(ui.accidentText, ui.theftText, ui.waterText, ui.fireText)
        damageTypes.forEach {
            it.backgroundResource = R.drawable.dashed_square
        }
        damageTypeTexts.forEach {
            it.textColor = Color.parseColor("#808080")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId === android.R.id.home) {
            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }
}
