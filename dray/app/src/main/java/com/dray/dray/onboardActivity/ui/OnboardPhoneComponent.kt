package com.dray.dray.onboardActivity.ui

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.onboardActivity.OnboardPhoneActivity
import org.jetbrains.anko.*

class OnboardPhoneComponent : AnkoComponent<OnboardPhoneActivity>, AnkoLogger {
    val MEDIUM_TEXT_SIZE = 18f
    val DATA_TEXT_SIZE = 24f

    lateinit var verifyBtn : Button
    lateinit var phoneInput : EditText

    override fun createView(ui: AnkoContext<OnboardPhoneActivity>): View = with(ui) {
        themedLinearLayout(R.style.ThemeOverlay_AppCompat_Dark) {
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER

            verticalPadding = dimen(R.dimen.login_activity_margin)
            horizontalPadding = dimen(R.dimen.login_activity_margin)

            backgroundResource = R.drawable.purple_header
            /*imageView(android.R.drawable.sym_def_app_icon) {
                topPadding = dimen(R.dimen.nav_header_vertical_spacing)
            }.lparams {
                gravity = Gravity.CENTER
            }*/

            textView("Please enter your phone number."){
                textSize = MEDIUM_TEXT_SIZE
            }

            linearLayout {
                lparams(width = matchParent){
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                }
                gravity = Gravity.CENTER

                imageView(R.mipmap.ic_local_phone).lparams(height = matchParent)
                phoneInput = editText () {
                    inputType = android.text.InputType.TYPE_CLASS_PHONE
                    textSize = DATA_TEXT_SIZE
                }.lparams(width = matchParent)
            }

            verifyBtn = button("Verify Me"){
                textColor = Color.rgb(108,90,132)
                textSize = MEDIUM_TEXT_SIZE
                backgroundResource = R.drawable.white_rounded_button
            }.lparams(width = matchParent){
                verticalMargin = dimen(R.dimen.login_activity_margin)
            }
        }
    }

}