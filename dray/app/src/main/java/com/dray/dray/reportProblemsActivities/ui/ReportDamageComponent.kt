package com.dray.dray.reportProblemsActivities.ui

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.*
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ReportDamageActivity
import org.jetbrains.anko.*

class ReportDamageComponent : AnkoComponent<ReportDamageActivity>, AnkoLogger {
    val SQUARE_SIDE = 80
    val MEDIUM_SIZE_TEXT = 18f
    val DISPATCH_SIZE_TEXT = 24f
    val TAB_GAP = 20

    lateinit var dispatchBtn : LinearLayout

    lateinit var damagePhotoBtn : LinearLayout

    lateinit var zeroPhotoDisplay: ImageView
    lateinit var onePhotoDisplay: ImageView
    lateinit var twoPhotoDisplay: ImageView
    lateinit var threePhotoDisplay: ImageView
    lateinit var fourPhotoDisplay: ImageView
    lateinit var fivePhotoDisplay: ImageView
    lateinit var sixPhotoDisplay: ImageView
    lateinit var sevenPhotoDisplay: ImageView
    lateinit var eightPhotoDisplay: ImageView
    lateinit var ninePhotoDisplay: ImageView
    lateinit var secondRow : LinearLayout

    lateinit var typeAccident : LinearLayout
    lateinit var typeTheft : LinearLayout
    lateinit var typeWater : LinearLayout
    lateinit var typeFire : LinearLayout

    lateinit var accidentText : TextView
    lateinit var theftText : TextView
    lateinit var waterText : TextView
    lateinit var fireText : TextView

    lateinit var damageTypeLabel : TextView
    lateinit var damageTypeBox : LinearLayout

    lateinit var descriptionInput : EditText
    lateinit var submitBtn : Button
    override fun createView(ui: AnkoContext<ReportDamageActivity>): View = with(ui) {
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            verticalLayout (R.style.ThemeOverlay_AppCompat_Dark) {
                backgroundResource = R.drawable.purple_header
            }

            scrollView(){
                verticalLayout {
                    /* --- Damage photo --- */
                    textView("Photos of Damage (At most 10 photos)") {
                        textColor = R.color.colorPrimary
                    }

                    linearLayout(){
                        backgroundColor = Color.WHITE

                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            topMargin = dimen(R.dimen.item_vertical_margin)
                        }

                        damagePhotoBtn = verticalLayout {
                            lparams(width = dip(60), height = dip(60)){
                                rightMargin = dip(3)
                            }
                            backgroundResource = R.drawable.dashed_square
                            gravity = Gravity.CENTER
                            imageView() {
                                imageResource = R.mipmap.ic_add_a_photo
                            }.lparams(width = dip(60), height = dip(60))
                        }

                        zeroPhotoDisplay = imageView{
                            visibility = View.GONE
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(3)
                        }

                        onePhotoDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(3)
                        }

                        twoPhotoDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(3)
                        }
                        threePhotoDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(3)
                        }

                        fourPhotoDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            leftMargin = dip(3)
                        }
                    }

                    secondRow = linearLayout(){
                        backgroundColor = Color.WHITE
                        visibility = View.GONE

                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            bottomMargin = dimen(R.dimen.item_vertical_margin)
                        }

                        fivePhotoDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(3)
                        }

                        sixPhotoDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(3)
                        }

                        sevenPhotoDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(3)
                        }
                        eightPhotoDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(3)
                        }
                        ninePhotoDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            leftMargin = dip(3)
                        }
                    }


                    // Damage type row
                    damageTypeLabel = textView("Damage Type (Please tap one)") {
                        textColor = R.color.colorPrimary
                    }.lparams() {
                        verticalMargin = dimen(R.dimen.list_item_vertical_padding)
                    }

                    damageTypeBox = verticalLayout {
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent) {
                            verticalPadding = dimen(R.dimen.activity_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                        }

                        linearLayout {
                            typeAccident = verticalLayout {
                                backgroundResource = R.drawable.dashed_square
                                lparams(width = matchParent) {
                                    weight = 0.5f
                                    rightMargin = dip(TAB_GAP / 2)
                                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                                }

                                accidentText = textView("Accident") {
                                    this.gravity = Gravity.CENTER
                                    textSize = MEDIUM_SIZE_TEXT
                                }.lparams(width = matchParent, height = matchParent)
                            }

                            typeTheft = verticalLayout {
                                backgroundResource = R.drawable.dashed_square
                                lparams(width = matchParent) {
                                    weight = 0.5f
                                    leftMargin = dip(TAB_GAP / 2)
                                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                                }
                                theftText = textView("Theft") {
                                    this.gravity = Gravity.CENTER
                                    textSize = MEDIUM_SIZE_TEXT
                                }.lparams(width = matchParent, height = matchParent)
                            }
                        }

                        linearLayout {
                            lparams(width = matchParent) {
                                topMargin = dip(TAB_GAP)
                            }

                            typeWater = verticalLayout {
                                backgroundResource = R.drawable.dashed_square
                                lparams(width = matchParent) {
                                    weight = 0.5f
                                    rightMargin = dip(TAB_GAP / 2)
                                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                                }

                                waterText = textView("Water Damage") {
                                    this.gravity = Gravity.CENTER
                                    textSize = MEDIUM_SIZE_TEXT
                                }.lparams(width = matchParent, height = matchParent)
                            }

                            typeFire = verticalLayout {
                                backgroundResource = R.drawable.dashed_square
                                lparams(width = matchParent) {
                                    weight = 0.5f
                                    leftMargin = dip(TAB_GAP / 2)
                                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                                }
                                fireText = textView("Fire Damage") {
                                    this.gravity = Gravity.CENTER
                                    textSize = MEDIUM_SIZE_TEXT
                                }.lparams(width = matchParent, height = matchParent)
                            }
                        }
                    }

                    // Description row
                    textView("Description (Optional)") {
                        textColor = R.color.colorPrimary
                    }.lparams() {
                        verticalMargin = dimen(R.dimen.list_item_vertical_padding)
                    }

                    descriptionInput = editText {
                        backgroundResource = R.drawable.white_rounded_item
                        horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                        verticalPadding = dimen(R.dimen.activity_vertical_padding)
                        hint = "Please enter here ..."
                    }.lparams(width = matchParent)
                }
            }.lparams(width = matchParent){
                weight = 1f
            }


            submitBtn = button("Submit") {
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }


    }

}