package com.dray.dray.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.dray.dray.R
import com.dray.dray.customClasses.CheckTripHistoryItem
import com.dray.dray.paymentActivity.CheckTripHistoryActivity
import com.dray.dray.paymentActivity.ui.CheckTripHistoryComponent
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk15.listeners.onClick
import java.text.SimpleDateFormat
import java.util.*

class checkTripHistoryAdapter (val activity: CheckTripHistoryActivity,
                               var tripHistoryItems : ArrayList<CheckTripHistoryItem>) : BaseAdapter(){

    @SuppressLint("SetTextI18n")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val item : CheckTripHistoryItem = getItem(p0)!!
        val INDENT = 10
        val SMALL_LABEL_TEXT_SIZE = 8f
        val lightGrey = "#aaaaaa"
        lateinit var btnRow : LinearLayout
        lateinit var resultRow : LinearLayout
        lateinit var resultTxt : TextView

        return with(p2!!.context) {
            verticalLayout{
                backgroundResource = when {
                    item.approved -> R.drawable.purple_bordered_round_btn
                    item.disputed -> R.drawable.red_bordered_round_btn
                    else -> R.drawable.grey_bordered_round_btn
                }

                linearLayout {
                    verticalLayout {
                        verticalPadding = dip(10)
                        horizontalPadding = dip(10)
                        lparams(){
                            weight = 1f
                        }


                        verticalLayout{
                            lparams(width = matchParent){
                                bottomMargin = dip(10)
                            }
                            linearLayout {
                                lparams(width = matchParent)
                                textView(item.containerNo){
                                    textColor = R.color.colorPrimary
                                    typeface = Typeface.DEFAULT_BOLD
                                }.lparams(){
                                    weight = 1f
                                }

                                imageView(R.mipmap.ic_timer){
                                    adjustViewBounds = true
                                }.lparams(width = dip(20), height = dip(20))
                                textView(item.driveTime)
                            }

                            linearLayout {
                                textView("Chassis# ${item.chassisNo}"){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                }.lparams(){
                                    weight = 1f
                                }

                                val status = if (item.isEmpty){
                                    "EMPTY"
                                } else {
                                    "LOAD"
                                }

                                textView(status){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                }
                            }


                        }

                        linearLayout(){
                            imageView(R.drawable.origin_dot){
                                adjustViewBounds = true
                            }.lparams(width = dip(30), height = dip(30))

                            verticalLayout {
                                textView(item.pickUpLoc){
                                    textColor = Color.BLACK
                                    typeface = Typeface.DEFAULT_BOLD
                                }

                                textView("Pick up on ${item.pickUpTime}"){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                }
                            }
                        }

                        linearLayout(){
                            imageView(R.drawable.dest_dot){
                                adjustViewBounds = true
                            }.lparams(width = dip(30), height = dip(30))

                            verticalLayout {
                                textView(item.deliverToLoc){
                                    textColor = Color.BLACK
                                    typeface = Typeface.DEFAULT_BOLD
                                }

                                textView("Deliver to on ${item.deliverToTime}"){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                }
                            }

                        }
                    }

                    view(){
                        backgroundColor = when {
                            item.approved -> resources.getColor(R.color.colorPrimary)
                            item.disputed -> resources.getColor(R.color.colorRed)
                            else -> resources.getColor(R.color.backgroundGrey)
                        }
                    }.lparams(width = dip(1), height = matchParent)

                    verticalLayout(){
                        lparams(){
                            verticalPadding = dip(10)
                            horizontalPadding = dip(10)
                        }
                        linearLayout {
                            textView("Flat Rate"){
                                textSize = 10f
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(){
                                weight = 1f
                            }
                            textView("$"+item.flat){
                                textSize = 10f
                                typeface = Typeface.DEFAULT_BOLD
                            }
                        }

                        linearLayout {
                            textView("Fuel Rate"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$"+item.fuel){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }
                        }

                        linearLayout {
                            textView("Bonus Rate"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$"+item.bonus){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }
                        }

                        linearLayout {
                            textView("Off-Hire"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$"+item.offHire){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }
                        }

                        linearLayout {
                            textView("Addition"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$"+item.addition){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }
                        }

                        linearLayout {
                            lparams(width = matchParent){
                                bottomMargin = dip(5)
                            }

                            textView("Deduction"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = resources.getColor(R.color.colorRed)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$"+item.deduction){
                                textColor = resources.getColor(R.color.colorRed)
                                textSize = SMALL_LABEL_TEXT_SIZE
                            }
                        }

                        view(){
                            backgroundColor = Color.parseColor(lightGrey)
                        }.lparams(width = dip(110), height = dip(1))

                        linearLayout {
                            lparams(width = matchParent){
                                topMargin = dip(5)
                            }

                            textView("Total"){
                                textColor = R.color.colorPrimary
                                textSize = 10f
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(){
                                weight = 1f
                                this.gravity = Gravity.CENTER_VERTICAL
                            }

                            val rateArray = arrayListOf<String>(item.flat, item.fuel,
                                    item.bonus, item.offHire, item.addition)
                            var total = 0.toBigDecimal()
                            rateArray.forEach { it ->
                                total += it.toBigDecimal()
                            }
                            total -= item.deduction.toBigDecimal()

                            textView("$" + total.toString()){
                                textColor = Color.BLACK
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 24f
                            }
                        }
                    }
                }

                view(){
                    backgroundColor = when {
                        item.approved -> resources.getColor(R.color.colorPrimary)
                        item.disputed -> resources.getColor(R.color.colorRed)
                        else -> resources.getColor(R.color.backgroundGrey)
                    }
                }.lparams(width = matchParent, height = dip(1))

                if (item.approved || item.disputed) {
                    linearLayout {
                        verticalPadding = dip(5)
                        horizontalPadding = dip(10)

                        val msg = if (item.approved){
                            "Approved on ${item.approveTime}."
                        } else {
                            "Disputed on ${item.disputeTime}."
                        }

                        resultTxt = textView(msg){
                            textColor = if (item.approved){
                                R.color.colorPrimary
                            } else {
                                resources.getColor(R.color.colorRed)
                            }
                        }.lparams(){
                            weight = 1f
                        }

                        button("Cancel"){
                            backgroundResource = R.drawable.white_rounded_button
                            textColor = resources.getColor(R.color.defaultLightTxtColor)
                        }.lparams(){
                            leftMargin = dimen(R.dimen.countdown_gap)
                        }.onClick {
                            if (item.approved){
                                tripHistoryItems[p0].approved = false
                                notifyDataSetChanged()
                            } else {
                                tripHistoryItems[p0].disputed = false
                                notifyDataSetChanged()
                            }
                        }
                    }
                } else {
                    btnRow = linearLayout {
                        verticalPadding = dip(5)
                        horizontalPadding = dip(10)
                        button("Dispute"){
                            backgroundResource = R.drawable.left_rounded_button
                            textColor = resources.getColor(R.color.defaultLightTxtColor)
                        }.lparams(){
                            weight = 0.5f
                            rightMargin = dimen(R.dimen.countdown_gap)
                        }.onClick {
                            var disputeReason = ""
                            val reason = Dialog(p1!!.context)
                            reason.setContentView(R.layout.report_problem_self_input_template)
                            reason.findViewById<Button>(R.id.reportProblemSubmitBtn).setOnClickListener {
                                val inputBox = reason.findViewById<EditText>(R.id.selfInputTextBox)
                                disputeReason = inputBox.text.toString()
                                if (disputeReason == ""){
                                    inputBox.error = "Please write something to help us understand your problem."
                                } else {
                                    reason.cancel()
                                    toast("Thank you. Your dispute will be reviewed.")
                                    val formatter = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                                    val date = Date()
                                    tripHistoryItems[p0].disputeTime = formatter.format(date)
                                    tripHistoryItems[p0].disputeReason = disputeReason
                                    tripHistoryItems[p0].disputed = true
                                    notifyDataSetChanged()
                                }
                            }
                            reason.show()
                        }

                        button("Approve"){
                            backgroundResource = R.drawable.right_rounded_button
                            textColor = Color.WHITE
                        }.lparams(){
                            weight = 0.5f
                            leftMargin = dimen(R.dimen.countdown_gap)
                        }.onClick {
                            println("Get item id: $p0")
                            val formatter = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                            val date = Date()
                            tripHistoryItems[p0].approveTime = formatter.format(date)
                            tripHistoryItems[p0].approved = true
                            notifyDataSetChanged()
                        }
                    }
                }
            }
        }
    }

    override fun getItem(p0: Int): CheckTripHistoryItem? {
        return tripHistoryItems?.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return tripHistoryItems.count()
    }

}