package com.dray.dray.workflow.secondLeg.select

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.widget.Button
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.customClasses.OffSiteEmptyItem
import com.dray.dray.customClasses.OnSiteEmptyItem
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.workflow.secondLeg.find.FindSecondLegActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk15.listeners.onClick

class SelectSecondLegActivity : AppCompatActivity() {
    //val ui = EmptySelectionComponent()
    /* Empty Selection Activity defines Anko layout in the same file because
    * it is made up of two list views, both of them are extended to full height depending on the
    * children. Right now listview can't be put under scrollview, so I used the traditional
    * for loop rendering the list. The data can't be passed to independent Anko component.
    */

    val MEDIUM_SIZE_TEXT = 18f
    lateinit var reportBtn : LinearLayout

    var offsiteEmpties = ArrayList<OffSiteEmptyItem>()
    var onsiteEmpties = ArrayList<OnSiteEmptyItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Select A Select Leg")

        // TODO: Export, droppull and Has onsite-load
        onsiteEmpties.add(OnSiteEmptyItem("BEAU3245981", "WBCT, PIER E, LCBT"))
        onsiteEmpties.add(OnSiteEmptyItem("BMOU4812818", "LCBT"))
        onsiteEmpties.add(OnSiteEmptyItem("BSIU9242614", "PIER A, PIER E, APM, EAGLE MARINE"))
        onsiteEmpties.add(OnSiteEmptyItem("CBHU9185170", "WBCT, APM, PIER A"))
        onsiteEmpties.add(OnSiteEmptyItem("PCIU8727927", "PIER E, 233"))

        offsiteEmpties.add(OffSiteEmptyItem("0.2", "FSCU8384422", "214, APM, PIER E"))
        offsiteEmpties.add(OffSiteEmptyItem("1.2", "TCNU2032153", "233, PIER A"))
        offsiteEmpties.add(OffSiteEmptyItem("2", "OOLU8796227", "EAGLE MARINE, 214, PIER E"))
        offsiteEmpties.add(OffSiteEmptyItem("2.5", "TEMU7244994", "PIER E"))
        offsiteEmpties.add(OffSiteEmptyItem("2.7", "UETU5414201", "WBCT, 233, APM, PIER E"))

        renderUI ()

        reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }
    }

    private fun renderUI() {
        verticalLayout {
            lparams(width = matchParent, height = matchParent) {
                /*verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)*/
                topPadding = dimen(R.dimen.activity_vertical_margin)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn).lparams(width = matchParent){
                horizontalMargin = dimen(R.dimen.activity_horizontal_margin)
            }

            scrollView(){
                lparams(width = matchParent, height = matchParent)

                verticalLayout {
                    lparams(width = matchParent, height = matchParent)

                    verticalLayout{
                        backgroundColor = R.color.colorPrimary
                        lparams(width = matchParent, height = wrapContent)

                        textView("On-Site") {
                            this.gravity = Gravity.CENTER
                            textColor = Color.WHITE
                            textSize = MEDIUM_SIZE_TEXT
                        }.lparams(width = matchParent, height = wrapContent) {
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        }
                    }



                    onsiteEmpties.forEach {
                        val containerNumber = it.containerNumber
                        val returnToTerminals = it.terminals

                        verticalLayout() {
                            lparams(width = matchParent, height = wrapContent) {
                                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                                verticalMargin = dimen(R.dimen.item_vertical_margin)
                                horizontalMargin = dimen(R.dimen.activity_horizontal_margin)

                                backgroundResource = R.drawable.white_rounded_item
                            }

                            textView("Return to: ${it.terminals}") {
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(width = matchParent, height = matchParent){
                                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            }

                            // Button
                            linearLayout {
                                lparams(width = matchParent, height = wrapContent){
                                    topMargin = dimen(R.dimen.item_vertical_margin)

                                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                                }

                                textView("Pick Up "){
                                    textColor = Color.WHITE
                                    this.gravity = Gravity.END + Gravity.CENTER_VERTICAL
                                }.lparams(){
                                    weight = 0.5f
                                    rightMargin = dip(5)
                                }
                                textView(it.containerNumber){
                                    textColor = Color.WHITE
                                    typeface = Typeface.DEFAULT_BOLD
                                    textSize = MEDIUM_SIZE_TEXT
                                }.lparams(){
                                    weight = 0.5f
                                    leftMargin = dip(5)
                                }
                                backgroundResource = R.drawable.green_rounded_button

                            }.onClick {
                                var i = Intent(context, FindSecondLegActivity::class.java)
                                println("passed in container number $containerNumber")
                                i = passDataToConfirm(containerNumber, returnToTerminals,"0", i)
                                context.startActivity(i)
                            }
                        }
                    }

                    verticalLayout{
                        backgroundColor = R.color.colorPrimary
                        lparams(width = matchParent, height = wrapContent){

                        }

                        textView("Off-Site") {
                            this.gravity = Gravity.CENTER
                            textColor = Color.WHITE
                            textSize = MEDIUM_SIZE_TEXT
                        }.lparams(width = matchParent, height = wrapContent) {
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        }
                    }



                    offsiteEmpties.forEach {
                        val containerNo = it.containerNumber
                        val returnToTerminals = it.terminals
                        val distance = it.distance
                        verticalLayout() {
                            lparams(width = matchParent, height = wrapContent) {
                                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                                verticalMargin = dimen(R.dimen.item_vertical_margin)
                                horizontalMargin = dimen(R.dimen.activity_horizontal_margin)

                                backgroundResource = R.drawable.white_rounded_item
                            }

                            linearLayout{

                                lparams(width = matchParent, height = wrapContent)

                                linearLayout{
                                    lparams(){
                                        weight = 1f
                                    }

                                    textView("Return to ")
                                    textView(it.terminals){
                                        typeface = Typeface.DEFAULT_BOLD
                                    }.lparams(height = wrapContent)
                                }


                                textView(it.distance + " mi").lparams(height = wrapContent){
                                    weight = 0f
                                }
                            }


                            // Button
                            linearLayout {
                                lparams(width = matchParent, height = wrapContent){
                                    topMargin = dimen(R.dimen.item_vertical_margin)

                                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                                }

                                textView("Pick Up "){
                                    textColor = Color.WHITE
                                    this.gravity = Gravity.END + Gravity.CENTER_VERTICAL
                                }.lparams(){
                                    weight = 0.5f
                                    rightMargin = dip(5)
                                }
                                textView(it.containerNumber){
                                    textColor = Color.WHITE
                                    typeface = Typeface.DEFAULT_BOLD
                                    textSize = MEDIUM_SIZE_TEXT
                                }.lparams(){
                                    weight = 0.5f
                                    leftMargin = dip(5)
                                }
                                backgroundResource = R.drawable.green_rounded_button
                            }.onClick {
                                var i = Intent(context, FindSecondLegActivity::class.java)
                                i = passDataToConfirm(containerNo, returnToTerminals, distance, i)
                                context.startActivity(i)
                            }
                        }
                    }
                }

            }
        }
    }

    private fun passDataToConfirm (containerNo : String, returnTo : String,
                                   distance : String, i : Intent) : Intent {
        i.putExtra("CONTAINER_NO", containerNo)
        i.putExtra("RETURN_TO", returnTo)
        i.putExtra("DISTANCE", distance)

        return i
    }
}