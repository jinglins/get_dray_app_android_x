package com.dray.dray.settingsActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.dray.dray.R
import com.dray.dray.settingsActivity.ui.SettingsActivityComponent
import org.jetbrains.anko.setContentView

class SettingsActivity : AppCompatActivity() {

    val ui = SettingsActivityComponent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setTitle(R.string.settings_activity_title)

        // Dummy data for now
        var dummySettingsData = object {
            var notification = object {
                var availLoad = true
                var bestLeaveTime = true
            }
            var language = "English"
        }

        ui.availLoadNotification.isChecked = dummySettingsData.notification.availLoad
        ui.bestLeaveTimeNotification.isChecked = dummySettingsData.notification.bestLeaveTime
        if (dummySettingsData.language == "English"){
            ui.englishOption.isChecked = true
            ui.spanishOption.isChecked = false
        } else if (dummySettingsData.language == "Spanish") {
            ui.englishOption.isChecked = false
            ui.spanishOption.isChecked = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId === android.R.id.home) {
            val newAvailLoadNotification = ui.availLoadNotification.isChecked
            val newBestLeaveTimeNotification = ui.bestLeaveTimeNotification.isChecked
            val newLanguage = if (ui.englishOption.isChecked) "English" else "Spanish"

            // Send new notifications and languages to the backend
            println("New notif 1: $newAvailLoadNotification")
            println("New notif 2: $newBestLeaveTimeNotification")
            println("New language: $newLanguage")

            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }
}
