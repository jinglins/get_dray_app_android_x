package com.dray.dray.workflow.atPlace

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.workflow.scheduleNextLoadQuestion.ScheduleNextLoadQuestionActivity
import com.dray.dray.workflow.mapActivity.MapActivity

class AtYardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_at_yard)

        val toolbar = supportActionBar!!
        setToolBar(toolbar, "Drop the Container At Yard")

        val db = getDatabase(applicationContext)

        val reportBtn = findViewById<LinearLayout>(R.id.report_button).findViewById<Button>(R.id.report_btn)
        val nextBtn  = findViewById<Button>(R.id.nextBtn)

        reportBtn.setOnClickListener { displayReportWindow(applicationContext, "false", "false") }
        nextBtn.setOnClickListener {
            lateinit var i : Intent
            if (db.legDao().getLegInfoById(1).flags.contains("TERMINAL_EXTRACTION")){
                i = Intent (this, ScheduleNextLoadQuestionActivity :: class.java)
                i.putExtra("CUR_PLACE", "yard")
            } else {
                i = Intent (this, MapActivity :: class.java)
                i.putExtra("INGATE_STATUS", "withContainer")
            }
            startActivity(i)
        }
    }
}
