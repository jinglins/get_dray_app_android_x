package com.dray.dray.dataClasses.profile

import androidx.room.*

@Entity (tableName = "profile_basic_info")
data class ProfileBasicInfo (
        @PrimaryKey var id: Int,

        @ColumnInfo (name = "vendor_code")  var vendorCode : String,
        @ColumnInfo (name = "first_name")   var firstName : String,
        @ColumnInfo (name = "last_name")    var lastName : String,
        @ColumnInfo (name = "phone_number") var phone : String,
        @ColumnInfo (name = "email")        var email : String
)