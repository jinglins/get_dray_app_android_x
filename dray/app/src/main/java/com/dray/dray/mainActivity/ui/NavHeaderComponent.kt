package com.dray.dray.mainActivity.ui

import android.os.Build
import com.google.android.material.navigation.NavigationView
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout

import com.dray.dray.R
import com.dray.dray.R.style.TextAppearance_AppCompat_Body1
import com.dray.dray.R.style.ThemeOverlay_AppCompat_Dark
import org.jetbrains.anko.*

class NavHeaderComponent : AnkoComponent<com.google.android.material.navigation.NavigationView> {
    val USER_AVATAR = 1
    val USER_NAME = 2
    val USER_TMC = 3

    override fun createView(ui: AnkoContext<com.google.android.material.navigation.NavigationView>): View = with(ui) {
        themedLinearLayout(ThemeOverlay_AppCompat_Dark) {
            //orientation = LinearLayout.VERTICAL
            backgroundResource = R.drawable.purple_header
            gravity = Gravity.BOTTOM
            lparams(width= matchParent, height = dimen(R.dimen.nav_header_height)){
                verticalPadding = dimen(R.dimen.nav_header_vertical_spacing)
                horizontalPadding = dimen(R.dimen.nav_header_vertical_spacing)
            }

            themedRelativeLayout(ThemeOverlay_AppCompat_Dark){

                imageView(R.drawable.example_avatar) {
                    topPadding = dimen(R.dimen.nav_header_vertical_spacing)
                    id = USER_AVATAR
                }.lparams (width = dip(80), height = dip(80)){
                    gravity = Gravity.START
                }

                textView("Example Driver") {
                    topPadding = dimen(R.dimen.nav_header_vertical_spacing)
                    id = USER_NAME
                    textSize = 20f
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        setTextAppearance(TextAppearance_AppCompat_Body1)
                    } else {
                        @Suppress("DEPRECATION")
                        setTextAppearance(this.context, TextAppearance_AppCompat_Body1)
                    }*/
                }.lparams(){
                    leftMargin = dip(10)
                    topMargin = dip(20)
                    rightOf(USER_AVATAR)
                }

                textView("Harbor Express Inc."){
                    id = USER_TMC
                }.lparams(){
                    rightOf(USER_AVATAR)
                    below(USER_NAME)
                    leftMargin = dip(10)
                    topMargin = dip(5)
                    gravity = Gravity.BOTTOM
                }
            }
        }
    }
}
