package com.dray.dray.helpActivities.ui

import android.graphics.Color
import android.media.Image
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.helpActivities.HelpTicketDetailActivity
import org.jetbrains.anko.*

class HelpTicketDetailComponent : AnkoComponent<HelpTicketDetailActivity>, AnkoLogger {
    lateinit var ticketNumber : TextView
    lateinit var dateReceived : TextView
    lateinit var status : ImageView
    lateinit var description : TextView
    lateinit var solution : TextView
    lateinit var solutionDisplay : LinearLayout
    lateinit var solvedBtn : Button
    lateinit var reopenBtn : Button
    lateinit var cancelBtn : Button

    val LABEL_WIDTH = 100
    val MEDIUM_TEXT_SIZE = 18f

    override fun createView(ui: AnkoContext<HelpTicketDetailActivity>): View = with(ui){
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            scrollView {
                verticalLayout {
                    lparams(width = matchParent)

                    verticalLayout {
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }

                        backgroundColor = Color.WHITE

                        linearLayout {
                            ticketNumber = textView("Ticket No. DC-000"){
                                textColor = R.color.colorPrimary
                                textSize = MEDIUM_TEXT_SIZE
                            }.lparams(){
                                weight = 1f
                            }
                            status = imageView(){
                                imageResource = R.drawable.green_rounded_button
                            }.lparams(width = dip(15), height = dip(15)){
                                this.gravity = Gravity.CENTER_VERTICAL
                            }
                        }

                        linearLayout{
                            textView("Received on ")
                            dateReceived = textView("09/12/2018")
                        }
                    }

                    verticalLayout {
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }

                        backgroundColor = Color.WHITE

                        textView("Description"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        description = textView("Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                                "Torquatus, is qui consul cum Cn. Quod autem in homine praestantissimum atque" +
                                " optimum est, id deseruit. Quae cum dixisset paulumque institisset, Quid est? " ){
                            textSize = MEDIUM_TEXT_SIZE
                        }
                    }

                    solutionDisplay = verticalLayout {
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }

                        backgroundColor = Color.WHITE

                        solution = textView("Solution"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        description = textView("It should serve two purposes:" +
                                "\n1. Deliver the most important message (if the customer never opens " +
                                "the email, what do you want them to come away with?)\n" +
                                "2. Make them want to open the email (give them a good reason!)" ){
                            textColor = Color.BLACK
                            textSize = MEDIUM_TEXT_SIZE
                        }
                    }

                }
            }.lparams(width = matchParent){
                bottomMargin = dip(15)
                weight = 1f
            }

            solvedBtn = button("My Issue is Solved"){
                textSize = MEDIUM_TEXT_SIZE
                textColor = Color.WHITE
                backgroundResource = R.drawable.green_rounded_button
            }.lparams(width = matchParent){
                verticalMargin = dimen(R.dimen.item_vertical_margin)
            }

            reopenBtn = button("Reopen This Ticket"){
                textSize = MEDIUM_TEXT_SIZE
                textColor = Color.parseColor("#808080")
                backgroundResource = R.drawable.white_rounded_button
            }.lparams(width = matchParent){
                verticalMargin = dimen(R.dimen.item_vertical_margin)
            }


            cancelBtn = button("Cancel This Ticket"){
                visibility = View.GONE
                textSize = MEDIUM_TEXT_SIZE
                textColor = Color.WHITE
                backgroundResource = R.drawable.red_rounded_button
            }.lparams(width = matchParent, height = wrapContent){
                weight = 0f
                topMargin = dimen(R.dimen.item_vertical_margin)
            }
        }
    }

}