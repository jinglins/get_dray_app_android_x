package com.dray.dray.workflow.PhotoDocument.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.*
import com.dray.dray.R
import com.dray.dray.workflow.PhotoDocument.PhotoDocumentActivity
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.*

class PhotoDocumentComponent : AnkoComponent<PhotoDocumentActivity>, AnkoLogger{


    lateinit var reportBtn : LinearLayout

    lateinit var interchangeDocDisplayArea : ImageView
    lateinit var containerNoDisplayArea : ImageView
    lateinit var sealNoDisplayArea : ImageView

    lateinit var interchangeDocBox : LinearLayout
    lateinit var containerNoBox : LinearLayout
    lateinit var sealNoBox : LinearLayout

    lateinit var interchangeCameraIcon : ImageView
    lateinit var containerNoCameraIcon : ImageView
    lateinit var sealNoCameraIcon : ImageView

    lateinit var interchangeDocDscrpt : TextView
    lateinit var containerNoDscrpt : TextView
    lateinit var sealNoDscrpt : TextView

    lateinit var finishBtn : Button

    val ROW_GAP = 10
    val CAMERA_WIDTH = 60
    val SMALL_SIZE_TEXT = 10f
    val MEDIUM_SIZE_TEXT = 18f

    override fun createView(ui: AnkoContext<PhotoDocumentActivity>): View = with(ui){
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            textView("Document Checklist"){
                textSize = MEDIUM_SIZE_TEXT
                textColor = resources.getColor(R.color.colorPrimary)
                typeface = Typeface.DEFAULT_BOLD
            }.lparams(){
                this.gravity = Gravity.CENTER_HORIZONTAL
            }

            view(){
                backgroundColor = resources.getColor(R.color.colorPrimary)
            }.lparams(width = dip(200), height = dip(1)){
                bottomMargin = dip(ROW_GAP)
                this.gravity = Gravity.CENTER_HORIZONTAL
            }

            interchangeDocBox = linearLayout(){
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    bottomMargin = dip(ROW_GAP)
                }

                backgroundResource = R.drawable.grey_bordered_round_btn
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                gravity = Gravity.CENTER_VERTICAL


                verticalLayout {
                    lparams(){
                        weight = 1f
                        rightMargin = dip(10)
                    }

                    textView("Interchange Document"){
                        textColor = Color.BLACK
                        typeface = Typeface.DEFAULT_BOLD
                        textSize = 18f
                    }
                    interchangeDocDscrpt = textView("Please take a photo of the interchange document."){
                        textSize = SMALL_SIZE_TEXT
                    }
                }

                interchangeCameraIcon = imageView(R.drawable.add_photo){
                }.lparams(width = dip(CAMERA_WIDTH), height = dip(CAMERA_WIDTH))

                interchangeDocDisplayArea = imageView{
                    visibility = View.GONE
                    adjustViewBounds = true
                    imageResource = R.drawable.example_avatar
                }.lparams(height = matchParent)
            }

            containerNoBox = linearLayout(){
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    bottomMargin = dip(ROW_GAP)
                }

                backgroundResource = R.drawable.grey_bordered_round_btn
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                gravity = Gravity.CENTER_VERTICAL
                verticalLayout {
                    lparams(){
                        weight = 1f
                        rightMargin = dip(10)
                    }

                    textView("Container Number"){
                        textColor = Color.BLACK
                        typeface = Typeface.DEFAULT_BOLD
                        textSize = 18f
                    }
                    containerNoDscrpt = textView("Please take a photo of the container number."){
                        textSize = SMALL_SIZE_TEXT
                    }
                }

                containerNoCameraIcon = imageView(R.drawable.add_photo){
                }.lparams(width = dip(CAMERA_WIDTH), height = dip(CAMERA_WIDTH))

                containerNoDisplayArea = imageView{
                    visibility = View.GONE
                    adjustViewBounds = true
                    imageResource = R.drawable.example_avatar
                }.lparams(height = matchParent)

            }

            sealNoBox = linearLayout(){
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    bottomMargin = dip(ROW_GAP)
                }

                backgroundResource = R.drawable.grey_bordered_round_btn
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                gravity = Gravity.CENTER_VERTICAL
                verticalLayout {
                    lparams(){
                        weight = 1f
                        rightMargin = dip(10)
                    }

                    textView("Seal Number"){
                        textColor = Color.BLACK
                        typeface = Typeface.DEFAULT_BOLD
                        textSize = 18f
                    }
                    sealNoDscrpt = textView("Please take a photo of the seal number."){
                        textSize = SMALL_SIZE_TEXT
                    }
                }

                sealNoCameraIcon = imageView(R.drawable.add_photo){
                }.lparams(width = dip(CAMERA_WIDTH), height = dip(CAMERA_WIDTH))

                sealNoDisplayArea = imageView{
                    visibility = View.GONE
                    adjustViewBounds = true
                    imageResource = R.drawable.example_avatar
                }.lparams(height = matchParent)
            }

            finishBtn = button("Finish"){
                visibility = View.GONE
                backgroundResource = R.drawable.green_rounded_button
                textSize = MEDIUM_SIZE_TEXT
                textColor = Color.WHITE
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}