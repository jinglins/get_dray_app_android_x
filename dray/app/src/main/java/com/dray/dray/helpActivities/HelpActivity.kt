package com.dray.dray.helpActivities

import android.content.Intent
import com.google.android.material.tabs.TabLayout
import androidx.appcompat.app.AppCompatActivity

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button

import com.dray.dray.R
import kotlinx.android.synthetic.main.activity_help.*

class HelpActivity : AppCompatActivity() {

    /*
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    lateinit var tabLayout : com.google.android.material.tabs.TabLayout
    lateinit var viewPager : androidx.viewpager.widget.ViewPager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setTitle(R.string.title_activity_help)

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the orCustomerActivity.
        tabLayout = findViewById(R.id.tabs)
        viewPager = this.findViewById(R.id.container)
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter
        tabLayout.setupWithViewPager(viewPager)

        tabLayout.addOnTabSelectedListener(object : com.google.android.material.tabs.TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: com.google.android.material.tabs.TabLayout.Tab) {

            }

            override fun onTabUnselected(tab: com.google.android.material.tabs.TabLayout.Tab) {

            }

            override fun onTabReselected(tab: com.google.android.material.tabs.TabLayout.Tab) {

            }
        })

        //container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))

        var newBtn : Button = findViewById(R.id.submitHelpBtn)
        newBtn.setOnClickListener {
            val intent = Intent(this, NewHelpTicketActivity::class.java)
            startActivity(intent)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_help, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent orCustomerActivity in AndroidManifest.xml.
        if (item.getItemId() === android.R.id.home) {
            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }



    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): androidx.fragment.app.Fragment? {
            return when (position){
                0 -> {
                    PendingHelpTicketFragmentActivity()
                }

                1 -> {
                    SolvedHelpTicketFragmentActivity()
                }

                else -> null
            }
        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence = when (position) {
            0 -> "Pending"
            1 -> "Solved"
            else -> ""
        }
    }

}
