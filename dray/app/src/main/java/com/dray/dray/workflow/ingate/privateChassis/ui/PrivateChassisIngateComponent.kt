package com.dray.dray.workflow.ingate.privateChassis.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.ingate.privateChassis.PrivateChassisIngateActivity
import org.jetbrains.anko.*

class PrivateChassisIngateComponent : AnkoComponent<PrivateChassisIngateActivity>, AnkoLogger {
    lateinit var releaseNoTxt : TextView
    lateinit var chassisProviderTxt : TextView
    lateinit var hireOffRateTxt : TextView

    lateinit var reportBtn : LinearLayout
    lateinit var nextBtn : Button

    val MEDIUM_SIZE_TEXT = 18f
    val DISPATCH_SIZE_TEXT = 24f
    val DATA_TEXT_SIZE = 30f // Due to the restriction of space, this value happens to be the same as dipatch

    override fun createView(ui: AnkoContext<PrivateChassisIngateActivity>): View = with(ui){
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    bottomMargin = dimen(R.dimen.small_gap)
                }
                textView("Release #"){
                    textColor = R.color.colorPrimary
                }
                releaseNoTxt = textView("PLACEHOLDER"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                    typeface = Typeface.DEFAULT_BOLD
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    verticalMargin = dimen(R.dimen.small_gap)
                }
                textView("Chassis Provider"){
                    textColor = R.color.colorPrimary
                }
                chassisProviderTxt = textView("PLACEHOLDER"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    verticalMargin = dimen(R.dimen.small_gap)
                }
                textView("Off-Hire Rate"){
                    textColor = R.color.colorPrimary
                }
                hireOffRateTxt = textView("PLACEHOLDER"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            nextBtn = button("Next"){
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }

        }
    }

}