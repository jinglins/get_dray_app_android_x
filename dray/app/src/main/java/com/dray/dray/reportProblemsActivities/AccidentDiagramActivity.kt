package com.dray.dray.reportProblemsActivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import com.dray.dray.R
import android.view.MotionEvent
import android.view.View
import android.app.Dialog
import android.text.Editable
import android.text.TextWatcher
import android.widget.*
import com.dray.dray.customClasses.setToolBar
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.sdk15.listeners.onCheckedChange
import org.jetbrains.anko.toast


class AccidentDiagramActivity : AppCompatActivity() {

    lateinit var rootLayout : ViewGroup
    lateinit var truckIcon : ImageView
    lateinit var rotateBtn : Button
    lateinit var submitBtn : Button

    lateinit var frontBackIndicationTxt : LinearLayout
    lateinit var positionInstructionTxt : TextView

    private var _xDelta = 0
    private var _yDelta = 0

    private var dragged = 0
    private var drivingStatus = ""
    private var steerTowards = ""

    private var blankFilled = arrayListOf<Int>(0, 0, 0)

    private val COMPLETE_BORDER = R.drawable.purple_bordered_round_btn
    private val DEFAULT_BORDER = R.drawable.grey_bordered_round_btn

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accident_diagram)

        truckIcon = findViewById(R.id.carIcon)
        rootLayout = findViewById(R.id.view_root)
        rotateBtn = findViewById(R.id.rotateBtn)
        submitBtn = findViewById(R.id.submitBtn)

        frontBackIndicationTxt = findViewById(R.id.frontBackIndication)
        positionInstructionTxt = findViewById(R.id.positionInstruction)

        rotateBtn.visibility = View.GONE

        rotateBtn.setOnClickListener {
            truckIcon.rotation = truckIcon.rotation + 90f
        }

        var listener = View.OnTouchListener(function = {view, event ->

            val X = event.rawX.toInt()
            val Y = event.rawY.toInt()

            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_DOWN -> {
                    if (dragged == 0) {
                        frontBackIndicationTxt.visibility = View.GONE
                        positionInstructionTxt.visibility = View.GONE
                        rotateBtn.visibility = View.VISIBLE
                        submitBtn.visibility = View.VISIBLE
                        dragged = 1
                    }

                    val lParams = view.layoutParams as RelativeLayout.LayoutParams
                    _xDelta = X - lParams.leftMargin
                    _yDelta = Y - lParams.topMargin
                }
                MotionEvent.ACTION_UP -> {
                }
                MotionEvent.ACTION_POINTER_DOWN -> {
                }
                MotionEvent.ACTION_POINTER_UP -> {
                }
                MotionEvent.ACTION_MOVE -> {

                    val layoutParams = view.getLayoutParams() as RelativeLayout.LayoutParams
                    layoutParams.leftMargin = X - _xDelta
                    layoutParams.topMargin = Y - _yDelta
                    layoutParams.rightMargin = -250
                    layoutParams.bottomMargin = -250
                    view.layoutParams = layoutParams
                }
            }

            rootLayout.invalidate()
            true
        })

        submitBtn.setOnClickListener {
            val parameters = Dialog(this@AccidentDiagramActivity)
            parameters.setContentView(R.layout.accident_diagram_template)
            val finishBtn = parameters.findViewById<Button>(R.id.finishBtn)

            // Speed
            var speed = parameters.findViewById<EditText>(R.id.speedInput)
            speed.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    val speedBox = parameters.findViewById<LinearLayout>(R.id.speedBox)
                    if (!p0.isNullOrEmpty()){
                        speedBox.backgroundResource = COMPLETE_BORDER
                        blankFilled[0] = 1

                        if (allRequiredFilled()) {finishBtn.visibility = View.VISIBLE}
                    } else {
                        speedBox.backgroundResource = DEFAULT_BORDER
                        blankFilled[0] = 0
                        finishBtn.visibility = View.GONE
                    }
                }
            })

            // Driving status
            val statusBox = parameters.findViewById<LinearLayout>(R.id.statusBox)
            val stillBtn = parameters.findViewById<RadioButton>(R.id.stillBtn)
            val fwdBtn = parameters.findViewById<RadioButton>(R.id.forwardBtn)
            val rvsBtn = parameters.findViewById<RadioButton>(R.id.reverseBtn)

            stillBtn.onCheckedChange { buttonView, isChecked ->
                if (isChecked) {
                    statusBox.backgroundResource = COMPLETE_BORDER
                    drivingStatus = "still"
                    blankFilled[1] = 1
                    if (allRequiredFilled()) {finishBtn.visibility = View.VISIBLE}
                }
            }


            fwdBtn.onCheckedChange { buttonView, isChecked ->
                if (isChecked) {
                    statusBox.backgroundResource = COMPLETE_BORDER
                    drivingStatus = "forward"
                    blankFilled[1] = 1
                    if (allRequiredFilled()) {finishBtn.visibility = View.VISIBLE}
                }
            }
            rvsBtn.onCheckedChange { buttonView, isChecked ->
                if (isChecked) {
                    statusBox.backgroundResource = COMPLETE_BORDER
                    drivingStatus = "reverse"
                    blankFilled[1] = 1
                    if (allRequiredFilled()) {finishBtn.visibility = View.VISIBLE}
                }
            }

            // steer direction
            val steerBox = parameters.findViewById<LinearLayout>(R.id.steerBox)
            val straightBtn = parameters.findViewById<RadioButton>(R.id.straightBtn)
            val leftBtn = parameters.findViewById<RadioButton>(R.id.leftBtn)
            val rightBtn = parameters.findViewById<RadioButton>(R.id.rightBtn)

            straightBtn.onCheckedChange { buttonView, isChecked ->
                if (isChecked) {
                    steerBox.backgroundResource = COMPLETE_BORDER
                    steerTowards = "straight"
                    blankFilled[2] = 1
                    if (allRequiredFilled()) {finishBtn.visibility = View.VISIBLE}
                }
            }
            leftBtn.onCheckedChange { buttonView, isChecked ->
                if (isChecked) {
                    steerBox.backgroundResource = COMPLETE_BORDER
                    steerTowards = "left"
                    blankFilled[2] = 1
                    if (allRequiredFilled()) {finishBtn.visibility = View.VISIBLE}
                }
            }
            rightBtn.onCheckedChange { buttonView, isChecked ->
                if (isChecked) {
                    steerBox.backgroundResource = COMPLETE_BORDER
                    steerTowards = "right"
                    blankFilled[2] = 1
                    if (allRequiredFilled()) {finishBtn.visibility = View.VISIBLE}
                }
            }

            finishBtn.setOnClickListener {
                if (speed.text.toString().toInt() > 280) {
                    speed.error = "Please enter a number less than 280. "
                } else {
                    //val additionalComment = parameters.findViewById<EditText>(R.id.additionInput).toString()
                    // speed.text.toString(), additionalComment, drivingStatus, steerTowards

                    parameters.cancel()
                    finish()
                }
            }

            parameters.show()
        }

        truckIcon.setOnTouchListener(listener)

    }

    private fun allRequiredFilled () : Boolean {
        blankFilled.forEach { it ->
            if (it == 0) {
                return false
            }
        }

        return true
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    /*fun onTouch(view: View, event: MotionEvent): Boolean {
        val X = event.rawX.toInt()
        val Y = event.rawY.toInt()

        when (event.action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> {
                val lParams = view.layoutParams as RelativeLayout.LayoutParams
                _xDelta = X - lParams.leftMargin
                _yDelta = Y - lParams.topMargin
            }
            MotionEvent.ACTION_UP -> {
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
            }
            MotionEvent.ACTION_POINTER_UP -> {
            }
            MotionEvent.ACTION_MOVE -> {
                val layoutParams = view.getLayoutParams() as RelativeLayout.LayoutParams
                layoutParams.leftMargin = X - _xDelta
                layoutParams.topMargin = Y - _yDelta
                layoutParams.rightMargin = -250
                layoutParams.bottomMargin = -250
                view.layoutParams = layoutParams
            }
        }

        rootLayout.invalidate()
        return true
    }*/

}
