package com.dray.dray.adapter

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ReportProblemsPopUpActivity
import org.jetbrains.anko.*

class reportProblemsGridAdapter (val activity: ReportProblemsPopUpActivity, listIndicator : String) : BaseAdapter() {
    private var icons = when (listIndicator) {
        "FACILITY" -> arrayListOf(R.drawable.utl,
                R.drawable.close_area,
                R.drawable.no_service,
                R.drawable.no_chassis,
                R.drawable.onhold,
                R.drawable.back)
        "EQUIPMENT" -> arrayListOf(R.drawable.flat_tire,
                    R.drawable.container_damage,
                    R.drawable.cargo_damage,
                    R.drawable.equipment_damage,
                    R.drawable.overweight,
                    R.drawable.back)
        "SCHEDULE" -> arrayListOf(R.drawable.wrong_appt_time,
                R.drawable.no_release,
                R.drawable.pay_perdiem,
                R.drawable.pier_pass,
                0,
                R.drawable.back)

        "IN TRANSIT" -> arrayListOf(R.drawable.accident,
                R.drawable.wait,
                R.drawable.flat_tire,
                R.drawable.emergency,
                0,
                R.drawable.self_input)

        else -> arrayListOf(R.drawable.utl,
                R.drawable.container_damage,
                R.drawable.wrong_schedule,
                R.drawable.emergency,
                R.drawable.inside_conjestion,
                R.drawable.self_input)
    }

    private var labels = when (listIndicator) {
        "FACILITY" -> arrayListOf("UTL",
                "Closed Area",
                "Out of Service",
                "Out of Chassis",
                "Container On-hold",
                "BACK")
        "EQUIPMENT" -> arrayListOf("Truck Problem",
                "Container Damage",
                "Cargo Damage",
                "Equipment Damage",
                "Overweight",
                "BACK")
        "SCHEDULE" -> arrayListOf("Wrong Appointment",
                "No Release",
                "Per Diem",
                "Pier Pass",
                "",
                "BACK")

        "IN TRANSIT" -> arrayListOf("Accident",
                "Long Wait",
                "Truck Problem",
                "Emergency",
                "",
                "Other")

        else -> arrayListOf("Facility Issue",
                "Equipment Issue",
                "Schedule Error",
                "Personal Emerg",
                "Inside Congestion",
                "Other")
    }

    private var itemCounts = when (listIndicator) {
        "FACILITY" -> 6
        "EQUIPMENT" -> 6
        "SCHEDULE" -> 6
        "IN TRANSIT" ->6
        else -> 6
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return with (parent!!.context){
            verticalLayout {
                lparams(width = matchParent){
                    verticalPadding = dip(3)
                    horizontalPadding = dip(3)
                }

                verticalLayout {
                    gravity = Gravity.CENTER
                    lparams(width = matchParent){
                        verticalPadding = dip(15)
                        horizontalPadding = dip(10)
                    }
                    if (icons[position] == 0) {

                    } else if (icons[position] == R.drawable.back){
                        backgroundResource = R.drawable.white_rounded_button
                        imageView(icons[position]).lparams(width = dip(60), height = dip(60))
                    } else {
                        backgroundResource = R.drawable.purple_bordered_round_btn
                        imageView(icons[position]).lparams(width = dip(60), height = dip(60))
                    }
                    textView(labels[position]){
                        textSize = 10f
                    }.lparams() {
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        topMargin = dip(5)

                    }
                }
            }
        }
    }

    override fun getItem(position: Int): Any {
        return arrayListOf(icons[position], labels[position])
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return itemCounts
    }

}