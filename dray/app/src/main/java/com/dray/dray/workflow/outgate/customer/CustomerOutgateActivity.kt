package com.dray.dray.workflow.outgate.customer

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.scheduleNextLoadQuestion.ScheduleNextLoadQuestionActivity
import com.dray.dray.workflow.mapActivity.MapActivity
import com.dray.dray.workflow.outgate.customer.ui.*
import com.dray.dray.workflow.PhotoDocument.PhotoDocumentActivity
import org.jetbrains.anko.*

class CustomerOutgateActivity : AppCompatActivity() {
    val bobtailUI = CustomerOutgateBobtailComponent()
    val importUI = CustomerOutgateImportComponent()
    val exportUI = CustomerOutgateExportComponent()
    lateinit var db : AppDatabase

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Customer Out-Gate")

        val exitStatus = intent.getStringExtra("EXIT_STATUS")
        val distance = intent.getStringExtra("DISTANCE")

        db = getDatabase(applicationContext)

        val secondLegInfo = db.legDao().getLegInfoById(2)
        val containerInfo = db.legDao().getLegContainerInfo(2)
        val returnToInfo = db.legDao().getLegDeliverToInfo(2)

        when (exitStatus) {
            "bobtail" -> {
                bobtailUI.setContentView(this)
                bobtailUI.purposeTxt.text = "Leaving, empty container is $distance miles away."

                bobtailUI.containerNo.text = containerInfo.containerNumber
                bobtailUI.steamshipLine.text = secondLegInfo.steamshipLine
                bobtailUI.containerSize.text = containerInfo.containerSize
                bobtailUI.returnTo.text = returnToInfo.destinationName

                bobtailUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                    displayReportWindow(applicationContext, "false", "false")
                }

                bobtailUI.nextBtn.setOnClickListener {
                    val i = Intent(this, MapActivity::class.java)
                    i.putExtra("TARGET", "NEW_CUSTOMER_FACILITY")
                    startActivity(i)
                }
            }
            "load" -> {
                exportUI.setContentView(this)

                val liveLoad = (db.legDao().getLegInfoById(1).receiveType == "LIVE")

                if (containerInfo.containerNumber.isEmpty()){
                    exportUI.containerNoBox.visibility = View.GONE
                } else {
                    exportUI.containerNo.text = containerInfo.containerNumber
                }

                exportUI.poNo.text = db.legDao().getLegInfoById(1).purchaseOrderNumber
                exportUI.steamshipLine.text = secondLegInfo.steamshipLine
                exportUI.containerSize.text = containerInfo.containerSize
                exportUI.returnTo.text = returnToInfo.destinationName

                exportUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                    displayReportWindow(applicationContext, "false", "false")
                }

                exportUI.nextBtn.setOnClickListener {
                    if (liveLoad){
                        selectAnotherJobQuestion()
                    } else {
                        goToNextActivity(exitStatus)
                    }

                }
            }
            else -> {
                importUI.setContentView(this)

                importUI.containerNo.text = containerInfo.containerNumber
                importUI.steamshipLine.text = secondLegInfo.steamshipLine
                importUI.containerSize.text = containerInfo.containerSize
                importUI.returnTo.text = returnToInfo.destinationName
                importUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                    displayReportWindow(applicationContext, "false", "false")
                }

                importUI.nextBtn.setOnClickListener {
                    goToNextActivity(exitStatus)
                }
            }
        }
    }

    private fun goToNextActivity(exitStatus : String) {
        val i = Intent(this, PhotoDocumentActivity::class.java)
        i.putExtra("CUR_PLACE", "customer")
        i.putExtra("EXIT_STATUS", exitStatus)
        startActivity(i)
    }

    private fun selectAnotherJobQuestion () {
        val i = Intent(this, ScheduleNextLoadQuestionActivity::class.java)
        i.putExtra("CUR_PLACE", "customer")
        startActivity(i)
    }

}