package com.dray.dray.reportProblemsActivities

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import com.dray.dray.R
import com.dray.dray.customClasses.getCurrentTimeStamp
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.reportProblemsActivities.ui.ReportOverweightComponent
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast

class ReportOverweightActivity : AppCompatActivity() {
    val ui = ReportOverweightComponent()
    val CAMERA_REQUEST_CODE = 133

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Report Overweight")

        ui.photoBtn.setOnClickListener {
            captureImage()
        }

        ui.submitBtn.setOnClickListener {
            // Send the photo and timestamp to the backend
            val currentTimeStamp = getCurrentTimeStamp()
            finish()
        }
    }

    private fun captureImage() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CAMERA_REQUEST_CODE -> try {
                //When image is captured successfully
                if (resultCode == RESULT_OK) {
                    val imageBitmap = data?.extras?.get("data") as Bitmap

                    // Show buttons
                    ui.photoBtn.visibility = View.GONE
                    ui.submitBtn.visibility = View.VISIBLE
                    ui.photoDisplayArea.visibility = View.VISIBLE
                    // After image capture show captured image over image view
                    showCapturedImage(imageBitmap)

                } else
                    toast(R.string.cancel_message)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    /*  Show Captured over ImageView  */
    private fun showCapturedImage(image : Bitmap) {
        ui.photoDisplayArea.setImageBitmap(image)
    }
}
