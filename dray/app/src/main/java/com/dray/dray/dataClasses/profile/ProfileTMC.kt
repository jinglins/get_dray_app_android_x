package com.dray.dray.dataClasses.profile

import androidx.room.*

@Entity (tableName = "profile_tmc_info")
data class ProfileTMC (
        @PrimaryKey var id : Int,

        @ColumnInfo (name = "tmc_name")      var TMCName : String,
        @ColumnInfo (name = "tmc_scac_code") var SCACCode : String
)