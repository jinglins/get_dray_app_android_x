package com.dray.dray.workflow.newLeg.select.ui

import android.graphics.Color
import android.view.View
import com.dray.dray.R
import com.dray.dray.adapter.selectLoadsAdapter
import com.dray.dray.customClasses.SelectLoadItem
import com.dray.dray.workflow.newLeg.select.SelectNewLegActivity
import org.jetbrains.anko.*

class SelectNewLegComponent : AnkoComponent<SelectNewLegActivity>, AnkoLogger {
    lateinit var mAdapter: selectLoadsAdapter



    override fun createView(ui: AnkoContext<SelectNewLegActivity>): View = with(ui) {
        // Dummy data

        var selectedLoadItems = ArrayList<SelectLoadItem> ()
        /*selectedLoadItems.add(SelectLoadItem("PCIU3344234",
                "PIER E","CHINO","40H","1234",
                "Drop-pull", "Tri-axel", "450", "30"))*/

        // For three loads assigned each time
        selectedLoadItems.add(SelectLoadItem("PCIU3344234","WBCT","ONTARIO",
                "40","1434 (lb)",
                "DROP-PULL", "CHASSIS",
                "400.00", "20.00", "25.32", "100.12"))
       /* selectedLoadItems.add(SelectLoadItem("PCIU3344234","APM","REDWOOD","45","1200 (lb)",
                "Live-load", "Chassis", "480", "15"))*/

        mAdapter = selectLoadsAdapter(SelectNewLegActivity(), selectedLoadItems)

        verticalLayout(){
            lparams(){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            listView(){
                adapter = mAdapter
                divider = null
                backgroundColor = Color.TRANSPARENT
                cacheColorHint = Color.TRANSPARENT
                dividerHeight = dimen(R.dimen.item_vertical_margin)
            }
        }
    }

}