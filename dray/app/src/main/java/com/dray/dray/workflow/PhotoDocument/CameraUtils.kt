package com.dray.testcamera

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import androidx.core.content.FileProvider
import android.util.Log
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

object CameraUtils {

    //Get Uri Of captured Image
    fun getOutputMediaFileUri(context: Context): Uri {
        val mediaStorageDir = File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Camera")
        //If File is not present create directory
        if (!mediaStorageDir.exists()) {
            if (mediaStorageDir.mkdir())
                Log.e("Create Directory", "Main Directory Created : $mediaStorageDir")
        }

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(Date())//Get Current timestamp
        val mediaFile = File(mediaStorageDir.path + File.separator
                + "IMG_" + timeStamp + ".jpg")//create image path with system mill and image format
        //return Uri.fromFile(mediaFile)
        return FileProvider.getUriForFile(
                context, context.applicationContext.packageName + ".provider", mediaFile)
    }

    /*  Convert Captured image path into Bitmap to display over ImageView  */
    fun convertImagePathToBitmap(imagePath: String, scaleBitmap: Boolean): Bitmap {
        val bmOptions = BitmapFactory.Options()
        val bitmap = BitmapFactory.decodeFile(imagePath, bmOptions)//Decode image path

        //If you want to scale bitmap/reduce captured image size then send true
        return if (scaleBitmap)
            Bitmap.createScaledBitmap(bitmap, 500, 500, true)
        else
        //if you don't want to scale bitmap then send false
            bitmap
    }


}