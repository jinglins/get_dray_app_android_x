package com.dray.dray.dataClasses.leg

import androidx.room.*

@Entity (tableName = "leg_deliver_to_info")
data class LegDeliverToInfo (
        @PrimaryKey var id : Int,

        @ColumnInfo (name = "destination_name")    var destinationName : String ,
        @ColumnInfo (name = "destination_address") var destinationAddress : String,
        @ColumnInfo (name = "appt_time")           var apptTime : String,
        @ColumnInfo (name = "appt_number")         var apptNumber : String
)