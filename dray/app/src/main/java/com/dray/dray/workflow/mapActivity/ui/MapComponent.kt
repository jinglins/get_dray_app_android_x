package com.dray.dray.workflow.mapActivity.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.mapActivity.MapActivity
import org.jetbrains.anko.*

class MapComponent : AnkoComponent<MapActivity>, AnkoLogger{
    lateinit var generalInfoBlock : LinearLayout

    lateinit var chassisBlock : LinearLayout
    lateinit var firstLegBlock : LinearLayout
    lateinit var secondLegBlock : LinearLayout

    lateinit var targetPlace : TextView
    lateinit var destNameTxt : TextView
    lateinit var destAddrTxt : TextView

    lateinit var releaseNoTxt : TextView
    lateinit var chassisProviderTxt : TextView
    lateinit var hireoffTxt : TextView

    lateinit var firstLegContainerNoTxt : TextView
    lateinit var firstLegBOLNoTxt : TextView
    lateinit var firstLegDestinationTxt : TextView
    lateinit var firstLegReleaseNoTxt : TextView
    lateinit var firstLegBookingNoTxt : TextView
    lateinit var firstLegSizeTxt : TextView

    lateinit var firstLegContainerNo : LinearLayout
    lateinit var firstLegBOLNo : LinearLayout
    lateinit var firstLegDestination : LinearLayout
    lateinit var firstLegReleaseNo : LinearLayout
    lateinit var firstLegBookingNo : LinearLayout
    lateinit var firstLegSize : LinearLayout

    lateinit var secondLegContainerNoTxt : TextView
    lateinit var returnToTxt : TextView
    lateinit var secondLegSizeTxt : TextView

    lateinit var reportProblemsBtn : ImageButton
    lateinit var nextBtn : Button
    val LABEL_WIDTH = 120

    override fun createView(ui: AnkoContext<MapActivity>): View = with(ui){
        verticalLayout {
            verticalLayout (R.style.Base_ThemeOverlay_AppCompat_Dark){
                backgroundResource = R.drawable.purple_header
                gravity = Gravity.CENTER
                lparams(width = matchParent){
                    weight = 1f
                    horizontalPadding = dip (30)
                }

                targetPlace = textView("Heading"){
                    textSize = 24f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    this.gravity = Gravity.CENTER
                    bottomMargin = dip(20)
                }
                destNameTxt = textView("Destination Name"){
                    textSize = 18f
                }
                destAddrTxt = textView("Destination Address"){
                    textSize = 18f
                }
            }

            generalInfoBlock = linearLayout {
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                backgroundColor = Color.WHITE

                chassisBlock = verticalLayout{
                    visibility = View.GONE
                    lparams(){
                        weight = 1f
                    }

                    linearLayout {
                        textView("Release #"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        releaseNoTxt = textView("PLACEHOLDER"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    linearLayout {
                        textView("Chassis provider"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        chassisProviderTxt = textView("PLACEHOLDER"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    hireoffTxt = textView("Hire off rate"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = matchParent)
                }

                firstLegBlock = verticalLayout {
                    visibility = View.GONE
                    lparams() {
                        weight = 1f
                    }

                    firstLegContainerNo = linearLayout {
                        visibility = View.GONE
                        textView("Container #"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        firstLegContainerNoTxt = textView("PLACEHOLDER"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    firstLegDestination = linearLayout {
                        visibility = View.GONE
                        textView("Deliver To"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        firstLegDestinationTxt = textView("PLACEHOLDER"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    firstLegBOLNo = linearLayout {
                        visibility = View.GONE
                        textView("BOL #"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        firstLegBOLNoTxt = textView("PLACEHOLDER"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    firstLegReleaseNo = linearLayout {
                        visibility = View.GONE
                        textView("Release #"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        firstLegReleaseNoTxt = textView("PLACEHOLDER"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    firstLegBookingNo = linearLayout {
                        visibility = View.GONE
                        textView("Booking #"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        firstLegBookingNoTxt = textView("PLACEHOLDER"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }


                    firstLegSize = linearLayout {
                        visibility = View.GONE
                        textView("Type/Size"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        firstLegSizeTxt = textView("PLACEHOLDER"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }
                }

                secondLegBlock = verticalLayout {
                    visibility = View.GONE

                    lparams() {
                        weight = 1f
                    }

                    linearLayout {
                        textView("Container #"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))
                        secondLegContainerNoTxt = textView("PLACEHOLDER") {
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    linearLayout {
                        textView("Return to"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))
                        returnToTxt = textView("PLACEHOLDER") {
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    linearLayout {
                        textView("Type/Size"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))
                        secondLegSizeTxt = textView("PLACEHOLDER") {
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }
                }

                reportProblemsBtn = imageButton(R.mipmap.ic_report_white){
                    backgroundResource = R.drawable.report_button
                }.lparams(width = dip(60), height = dip(60)){
                    this.gravity = Gravity.CENTER
                }
            }


            verticalLayout {
                bottomPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                backgroundColor = Color.WHITE

                nextBtn = button("Arrived"){
                    backgroundResource = R.drawable.green_rounded_button
                    textColor = Color.WHITE
                }
            }

        }
    }

}