package com.dray.dray.workflow.outgate.terminal.ui

import android.graphics.Color
import android.graphics.Typeface
import android.media.Image
import android.view.Gravity
import android.view.View
import android.widget.*
import com.dray.dray.R
import com.dray.dray.workflow.outgate.terminal.TerminalOutgateActivity
import org.jetbrains.anko.*

class TerminalOutgateFreeflowImportComponent : AnkoComponent<TerminalOutgateActivity>, AnkoLogger{
    lateinit var freeflowCode : TextView
    lateinit var reportBtn : LinearLayout
    lateinit var nextBtn : Button

    val MEDIUM_SIZE_TEXT = 18f
    val DATA_TEXT_SIZE = 30f // Due to the restriction of space, this value happens to be the same as dipatch
    val LABEL_WIDTH = 90

    override fun createView(ui: AnkoContext<TerminalOutgateActivity>): View = with(ui){
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout{

                lparams(width = matchParent){
                    weight = 1f
                }

                backgroundColor = Color.WHITE
                gravity = Gravity.CENTER
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                textView("Free-flow Code/Release #"){
                    textColor = R.color.colorPrimary
                }.lparams(){
                    this.gravity = Gravity.CENTER_HORIZONTAL
                }

                freeflowCode = textView("CMN1S"){
                    textSize = DATA_TEXT_SIZE
                    textColor = Color.BLACK
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    this.gravity = Gravity.CENTER_HORIZONTAL
                }
            }

            nextBtn = button("Next"){
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}