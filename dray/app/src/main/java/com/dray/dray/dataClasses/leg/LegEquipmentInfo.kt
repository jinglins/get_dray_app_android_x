package com.dray.dray.dataClasses.leg

import androidx.room.*

@Entity (tableName = "leg_equipment_info")
data class LegEquipmentInfo (
        @PrimaryKey var id : Int,

        @ColumnInfo (name = "equipment_name")   var equipmentName : String,
        @ColumnInfo (name = "equipment_type")   var equipmentType : String,
        @ColumnInfo (name = "equipment_number") var equipmentNumber : String,
        @ColumnInfo (name = "release_number")   var releaseNumber : String,
        @ColumnInfo (name = "provider_name")    var providerName : String,
        @ColumnInfo (name = "provider_address") var providerAddress : String
)