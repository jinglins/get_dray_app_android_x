package com.dray.dray.workflow.outgate.customer.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.outgate.customer.CustomerOutgateActivity
import org.jetbrains.anko.*

class CustomerOutgateImportComponent : AnkoComponent<CustomerOutgateActivity>, AnkoLogger{
    lateinit var containerNo : TextView
    lateinit var steamshipLine : TextView
    lateinit var containerSize : TextView
    lateinit var returnTo : TextView

    lateinit var reportBtn : LinearLayout
    lateinit var nextBtn : Button
    val MEDIUM_SIZE_TEXT = 18f
    val DATA_TEXT_SIZE = 24f // Due to the restriction of space, this value happens to be the same as dipatch
    val LABEL_WIDTH = 90

    override fun createView(ui: AnkoContext<CustomerOutgateActivity>): View = with(ui){
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout{
                lparams(width = matchParent){
                    bottomMargin = dimen(R.dimen.small_gap)
                }
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                textView("Container #")
                containerNo = textView("ABCD0000000"){
                    textSize = DATA_TEXT_SIZE
                    textColor = Color.BLACK
                    typeface = Typeface.DEFAULT_BOLD
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent){
                    verticalMargin = dimen(R.dimen.small_gap)
                }
                textView("Container Size"){
                    textColor = R.color.colorPrimary
                }
                containerSize = textView("40HC"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent){
                    verticalMargin = dimen(R.dimen.small_gap)
                }
                textView("Steamship Line"){
                    textColor = R.color.colorPrimary
                }
                steamshipLine = textView("YANGMING"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    topMargin = dimen(R.dimen.small_gap)
                }
                textView("Return To"){
                    textColor = R.color.colorPrimary
                }
                returnTo = textView("TERMINAL 1, TERMINAL 2, TERMINAL 3, TERMINAL 4, TERMINAL 5"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            nextBtn = button("Next"){
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}