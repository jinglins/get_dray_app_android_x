package com.dray.dray.adapter

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.R
import com.dray.dray.customClasses.OnSiteEmptyItem
import com.dray.dray.workflow.secondLeg.select.SelectSecondLegActivity
import org.jetbrains.anko.*

class onsiteEmptyLoadsAdapter (val activity: SelectSecondLegActivity, var onsiteEmptyItems : ArrayList<OnSiteEmptyItem>) : BaseAdapter(){
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val item : OnSiteEmptyItem = getItem(p0)!!
        val MEDIUM_SIZE_TEXT = 18f
        return with (p2!!.context){
            linearLayout() {
                lparams(width = matchParent, height = wrapContent) {
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                    backgroundResource = R.drawable.white_rounded_item
                }

                linearLayout{
                    lparams(height= matchParent){
                        weight = 1f
                    }

                    textView(item.containerNumber) {
                        textSize = MEDIUM_SIZE_TEXT
                        textColor = Color.BLACK
                        typeface = Typeface.DEFAULT_BOLD
                        this.gravity = Gravity.CENTER
                    }.lparams(width = dip(150), height = matchParent) {
                    }

                    textView(item.terminals) {
                        this.gravity = Gravity.CENTER
                    }.lparams(width = matchParent, height = matchParent)
                }

                button("Accept") {
                    backgroundResource = R.drawable.green_rounded_button
                    textColor = Color.WHITE
                    textSize = MEDIUM_SIZE_TEXT

                }.lparams(width = dip(120)){
                    weight = 0f
                }
            }
        }
    }

    override fun getItem(p0: Int): OnSiteEmptyItem? {
        return onsiteEmptyItems?.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return onsiteEmptyItems.count()
    }

}