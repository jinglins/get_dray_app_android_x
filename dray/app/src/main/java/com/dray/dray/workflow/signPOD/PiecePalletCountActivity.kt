package com.dray.dray.workflow.signPOD

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.Button
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.workflow.signPOD.ui.PiecePalletCountComponent
import org.jetbrains.anko.setContentView

class PiecePalletCountActivity : AppCompatActivity() {
    val ui = PiecePalletCountComponent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)
        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Piece/Pallet Count")

        ui.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
            displayReportWindow(applicationContext, "true", "false")
        }

        ui.pieceCountEditBtn.setOnClickListener {
            ui.pieceCountDisplayGroup.visibility = View.GONE
            ui.pieceCountEditGroup.visibility = View.VISIBLE
            ui.pieceCountInputBox.inputType = InputType.TYPE_CLASS_NUMBER
            ui.palletCountInputBox.inputType = InputType.TYPE_NULL
        }

        ui.pieceCountFinishBtn.setOnClickListener {
            ui.pieceCountDisplayGroup.visibility = View.VISIBLE
            ui.pieceCountEditGroup.visibility = View.GONE
            ui.pieceCountTxt.text = ui.pieceCountInputBox.text
        }

        ui.palletCountEditBtn.setOnClickListener {
            ui.palletCountDisplayGroup.visibility = View.GONE
            ui.palletCountEditGroup.visibility = View.VISIBLE
            ui.palletCountInputBox.inputType = InputType.TYPE_CLASS_NUMBER
            ui.pieceCountInputBox.inputType = InputType.TYPE_NULL
        }

        ui.palletCountFinishBtn.setOnClickListener {
            ui.palletCountDisplayGroup.visibility = View.VISIBLE
            ui.palletCountEditGroup.visibility = View.GONE
            ui.palletCountTxt.text = ui.palletCountInputBox.text
        }

        ui.signPODBtn.setOnClickListener {
            var i = Intent(this, SignPODActivity::class.java)
            startActivity(i)
        }
    }
}
