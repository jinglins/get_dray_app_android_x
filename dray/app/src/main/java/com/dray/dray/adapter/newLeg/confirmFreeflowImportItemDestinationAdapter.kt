package com.dray.dray.adapter.newLeg

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.R
import com.dray.dray.customClasses.newLeg.ConfirmFreeFlowImportDestinationItem
import com.dray.dray.workflow.newLeg.confirm.ConfirmNewLegActivity
import org.jetbrains.anko.*

class confirmFreeflowImportItemDestinationAdapter (val activity: ConfirmNewLegActivity,
                                                   var destinationsAndRates : ArrayList<ConfirmFreeFlowImportDestinationItem>) : BaseAdapter() {
    val MEDIUM_TEXT_SIZE = 18f
    val DATA_TEXT_SIZE = 24f
    val LARGE_TEXT_SIZE = 30f
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val item = getItem(p0)
        return with (p2!!.context){
            linearLayout {
                lparams(width = matchParent){
                    verticalMargin = dimen(R.dimen.item_vertical_margin)
                }

                verticalLayout{
                    lparams(width = dip(10), height = matchParent)
                    backgroundColor = R.color.colorPrimary
                }
                verticalLayout{
                    lparams(width = matchParent)
                    verticalLayout{
                        lparams(width = matchParent, height = wrapContent) {
                            backgroundColor = Color.WHITE
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.small_gap)
                        }

                        textView("Destination") {
                            textColor = R.color.colorPrimary
                        }

                        textView(item.destination) {
                            textColor = Color.BLACK
                            textSize = DATA_TEXT_SIZE
                        }
                    }

                    verticalLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent){
                            verticalMargin = dimen(R.dimen.small_gap)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        }

                        linearLayout{
                            lparams(width = matchParent){
                                bottomMargin = dip(5)
                            }

                            textView("Flat Rate"){
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 16f
                            }.lparams{
                                weight = 1f
                            }
                            textView("$" + item.flatRate){
                                textColor = Color.BLACK
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 16f
                            }.lparams{
                                weight = 0f
                            }
                        }
                        linearLayout{
                            textView("Fuel Rate"){
                                textColor = R.color.colorPrimary
                            }.lparams{
                                leftPadding = dip(20)
                                weight = 1f
                            }
                            textView("$" + item.fuelRate)
                        }
                        linearLayout{
                            textView("Bonus Rate"){
                                textColor = R.color.colorPrimary

                            }.lparams{
                                leftPadding = dip(20)
                                weight = 1f
                            }
                            textView("$" + item.bonusRate)
                        }
                        linearLayout{
                            textView("Chassis Off-Hire"){
                                textColor = R.color.colorPrimary
                            }.lparams{
                                leftPadding = dip(20)
                                weight = 1f
                            }
                            textView("$" + item.chassisRate)
                        }
                    }
                    linearLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent){
                            verticalMargin = dimen(R.dimen.small_gap)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        }
                        textView("Total Pay").lparams{
                            gravity = Gravity.CENTER_VERTICAL
                            weight = 1f
                        }

                        val totalPrice = (item.flatRate.toBigDecimal() + item.fuelRate.toBigDecimal()
                                + item.bonusRate.toBigDecimal() + item.chassisRate.toBigDecimal()).toString()

                        textView("$$totalPrice"){
                            textColor = Color.BLACK
                            textSize = LARGE_TEXT_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            weight = 0f
                        }
                    }
                }
            }
        }

    }

    override fun getItem(p0: Int): ConfirmFreeFlowImportDestinationItem {
        return destinationsAndRates[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return destinationsAndRates.count()
    }

}