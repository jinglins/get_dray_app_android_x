package com.dray.dray.helpActivities.ui

import android.graphics.Color
import android.view.Gravity
import android.view.View
import com.dray.dray.R
import com.dray.dray.helpActivities.HelpActivity
import org.jetbrains.anko.*

class PendingTicketComponent : AnkoComponent<HelpActivity> , AnkoLogger{
    val MEDIUM_SIZE_TEXT = 18f
    override fun createView(ui: AnkoContext<HelpActivity>): View = with(ui){
        themedLinearLayout(R.style.ThemeOverlay_AppCompat_Dark){
            backgroundResource = R.drawable.purple_header
            lparams(width = matchParent, height = wrapContent){
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
            }

            verticalLayout{
                lparams(height = wrapContent){
                    weight = 0.4f
                    this.gravity = Gravity.CENTER_VERTICAL
                    topPadding = dip(5)
                }
                textView("    START DATE"){
                    textSize = 8f
                }
                linearLayout(){
                    imageView(R.mipmap.ic_date_range)
                    textView("12/01/2017").lparams(){
                        this.gravity = Gravity.CENTER_VERTICAL
                    }
                }
            }

            verticalLayout{
                lparams(height = wrapContent){
                    weight = 0.4f
                    this.gravity = Gravity.CENTER_VERTICAL
                    topPadding = dip(5)
                }
                textView("    End Date"){
                    textSize = 8f
                }
                linearLayout(){
                    imageView(R.mipmap.ic_date_range)
                    textView("12/22/2017").lparams(){
                        this.gravity = Gravity.CENTER_VERTICAL
                    }
                }
            }

            verticalLayout{
                lparams(height = wrapContent){
                    weight = 0.2f
                }
                button("Search"){
                    backgroundColor = Color.TRANSPARENT
                }.lparams(){
                    this.gravity = Gravity.END
                    horizontalPadding = dip(0)
                }
            }
        }

        /*themedLinearLayout(R.style.ThemeOverlay_AppCompat_Dark){
            backgroundResource = R.drawable.purple_header

            lparams(width = matchParent, height = wrapContent){
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
            }

            verticalLayout{
                lparams{
                    weight = 0.4f
                    topMargin = dip(10)
                }
                textView("START DATE"){
                    textSize = 8f
                }.lparams(width = matchParent)
                linearLayout(){
                    lparams(width = matchParent)
                    gravity = Gravity.CENTER
                    imageView(R.mipmap.ic_date_range)
                    textView("12/21/2017")
                }


            }

            verticalLayout{
                lparams{
                    weight = 0.4f
                    topMargin = dip(10)
                }
                textView("END DATE"){
                    textSize = 8f
                }.lparams(width = matchParent)
                linearLayout(){
                    lparams(width = matchParent)
                    gravity = Gravity.CENTER
                    imageView(R.mipmap.ic_date_range)
                    textView("TODAY")
                }
            }

            button("Search"){

            }.lparams{
                verticalPadding = dip(10)
                horizontalPadding = dip(0)
                topMargin = dip(5)
                weight = 0.2f
            }

        }*/

    }

}