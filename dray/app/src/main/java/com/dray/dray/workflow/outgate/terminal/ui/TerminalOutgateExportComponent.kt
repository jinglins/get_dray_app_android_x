package com.dray.dray.workflow.outgate.terminal.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.outgate.terminal.TerminalOutgateActivity
import org.jetbrains.anko.*

class TerminalOutgateExportComponent : AnkoComponent<TerminalOutgateActivity>, AnkoLogger {
    lateinit var bookingNo : TextView
    lateinit var deliverTo : TextView
    lateinit var containerSize : TextView

    lateinit var nextBtn : Button
    lateinit var reportBtn : LinearLayout

    val MEDIUM_SIZE_TEXT = 18f
    val DATA_TEXT_SIZE = 30f
    val LABEL_WIDTH = 110
    override fun createView(ui: AnkoContext<TerminalOutgateActivity>): View = with(ui) {
        verticalLayout {
            lparams(width = matchParent, height = matchParent) {
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout {
                lparams(width = matchParent){
                    weight = 1f
                }

                linearLayout {
                    lparams (width = matchParent){
                        bottomMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }

                    verticalLayout{
                        lparams (height = matchParent){
                            weight = 1f
                            rightMargin = dimen(R.dimen.item_vertical_margin)
                        }
                        gravity = Gravity.CENTER_VERTICAL
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                        textView("Booking #")
                        bookingNo = textView("8PHL056586"){
                            textSize = DATA_TEXT_SIZE
                            textColor = Color.BLACK
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }
                }

                verticalLayout{
                    backgroundColor = Color.WHITE
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent){
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        verticalMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Deliver To"){
                        textColor = R.color.colorPrimary
                    }
                    deliverTo = textView("RANCHO CUCAMONGA"){
                        textSize = DATA_TEXT_SIZE
                        textColor = Color.BLACK
                    }
                }

                verticalLayout {
                    backgroundColor = Color.WHITE
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent){
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        verticalMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Container Size"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(LABEL_WIDTH))

                    containerSize = textView("40HC"){
                        textSize = DATA_TEXT_SIZE
                        textColor = Color.BLACK
                    }
                }
            }

            nextBtn = button("Next"){
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }
}