package com.dray.dray.onboardActivity.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.onboardActivity.OnboardConfirmActivity
import org.jetbrains.anko.*

class OnboardConfirmComponent : AnkoComponent<OnboardConfirmActivity>, AnkoLogger {
    val DATA_SIZE_TEXT = 24f
    val LABEL_SIZE_TEXT = 12f
    val NAME_SIZE_TEXT = 36f

    lateinit var confirmBtn : Button
    lateinit var driverName : TextView
    lateinit var driverCompany : TextView
    lateinit var driverCode : TextView
    lateinit var license : TextView

    override fun createView(ui: AnkoContext<OnboardConfirmActivity>): View = with(ui) {

        themedLinearLayout(R.style.ThemeOverlay_AppCompat_Dark) {
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER

            //verticalPadding = dimen(R.dimen.activity_vertical_margin)
            horizontalPadding = dimen(R.dimen.login_activity_margin)

            backgroundResource = R.drawable.purple_header


            textView("Welcome to Dray,")

            driverName = textView("Driver Name"){
                textSize = NAME_SIZE_TEXT
                typeface = Typeface.DEFAULT_BOLD
            }

            textView("TRUCKING COMPANY"){
                //topPadding = dip(R.dimen.activity_vertical_margin)
                textSize = LABEL_SIZE_TEXT

            }.lparams(width = wrapContent) {
                //verticalMargin = R.dimen.activity_vertical_margin
                topMargin = dip(50)
                gravity = View.TEXT_ALIGNMENT_TEXT_START
            }

            driverCompany = textView("Harbor Express Inc."){
                textSize = DATA_SIZE_TEXT
            }

            textView("DRIVER CODE"){
                textSize = LABEL_SIZE_TEXT
            }.lparams(width = wrapContent) {
                //verticalMargin = R.dimen.activity_vertical_margin
                topMargin = dip(20)
                gravity = View.TEXT_ALIGNMENT_TEXT_START
            }

            driverCode = textView("0000"){
                textSize = DATA_SIZE_TEXT
            }

            textView("DRIVER LICENSE"){
                textSize = LABEL_SIZE_TEXT
            }.lparams(width = wrapContent){
                topMargin = dip(20)
                gravity = View.TEXT_ALIGNMENT_TEXT_START
            }

            license = textView("AA00000"){
                textSize = DATA_SIZE_TEXT
            }

            confirmBtn = button("Yes, it's me!"){
                textColor = Color.rgb(108,90,132)
                backgroundResource = R.drawable.white_rounded_button

            }.lparams(width = matchParent){
                verticalMargin = dimen(R.dimen.login_activity_margin)
            }
        }
    }

}