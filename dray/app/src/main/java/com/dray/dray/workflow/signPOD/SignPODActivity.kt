package com.dray.dray.workflow.signPOD

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import com.dray.dray.R
import com.github.gcacace.signaturepad.views.SignaturePad
import java.sql.Date
import java.sql.Timestamp
import android.graphics.Bitmap
import android.view.View
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.dataClasses.leg.*
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.outgate.customer.CustomerOutgateActivity
import com.dray.dray.workflow.secondLeg.select.SelectSecondLegActivity
import java.io.ByteArrayOutputStream

// Have to use xml because the github api for signature only has xml component
// API Source: https://github.com/gcacace/android-signaturepad

class SignPODActivity : AppCompatActivity() {
    lateinit var reportBtn : ImageButton
    lateinit var clearBtn : Button
    lateinit var countBtn : TextView
    lateinit var finishBtn : Button
    lateinit var printNameField : TextView
    lateinit var dateField : TextView
    private lateinit var mSignaturePad : SignaturePad
    lateinit var db : AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_pod)

        val signPODToolbar = findViewById<Toolbar>(R.id.sign_pod_toolbar)
        setSupportActionBar(signPODToolbar)
        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Sign the Proof of Delivery")
        db = getDatabase(applicationContext)

        findViews()
        // Set today's date to default
        val timestamp = Timestamp(System.currentTimeMillis())
        dateField.text = Date(timestamp.time).toString()

        reportBtn.setOnClickListener {
            displayReportWindow(applicationContext, "true", "false")
        }

        clearBtn.setOnClickListener {
            mSignaturePad.clear();
        }

        /* Wire to next orCustomerActivity */
        val firstLegInfo = db.legDao().getLegInfoById(1)
        val receiveType = firstLegInfo.receiveType
        if (receiveType == "LIVE") {
            countBtn.visibility = View.VISIBLE
            countBtn.setOnClickListener {
                val i = Intent(this, PiecePalletCountActivity::class.java)
                startActivity(i)
            }
        }

        finishBtn.setOnClickListener {
            val printName = printNameField.text

            if (printName.isEmpty()){
                printNameField.error = "Please enter your name here. "
            } else {
                val signatureBitmap = mSignaturePad.signatureBitmap
                val stream = ByteArrayOutputStream()
                val signatureJpeg = signatureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                /* IF WE DECIDE TO STORE EVERY SIGNATURE ON DRIVER'S PHONE
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    Toast.makeText(this@SignPODActivity, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@SignPODActivity, "Unable to store the signature", Toast.LENGTH_SHORT).show()
                }*/

                // TODO: Send printName, date, signatureJepg to backend

                lateinit var i: Intent
                when {
                    firstLegInfo.flags.contains("STREET_TURN") -> {

                    }
                    firstLegInfo.receiveType == "LIVE" -> {
                        injectSecondLegData()
                        i = Intent(this, CustomerOutgateActivity::class.java)
                        i.putExtra("EXIT_STATUS", "load")
                    }
                    else -> i = Intent(this, SelectSecondLegActivity::class.java)
                }
                startActivity(i)
            }
        }
    }


    private fun findViews () {
        mSignaturePad = findViewById(R.id.signature_pad);
        reportBtn = findViewById(R.id.reportBtn)
        countBtn = findViewById(R.id.countBtn)
        finishBtn = findViewById(R.id.finishBtn)
        clearBtn = findViewById(R.id.clearSignatureBtn)
        printNameField = findViewById(R.id.printNameField)
        dateField = findViewById(R.id.dateField)
    }

    private fun injectSecondLegData () {
        // ids of second leg are 2
        val returnTo = "LCBT, PIER E, PIER A"
        val containerInfo = db.legDao().getLegContainerInfo(1)
        containerInfo.id = 2

        val deliverToInfo = db.legDao().getLegDeliverToInfo(1)

        val pickUpInfo = db.legDao().getLegPickUpInfoById(1)
        pickUpInfo.id = 2
        pickUpInfo.originName = deliverToInfo.destinationName
        pickUpInfo.originAddress = deliverToInfo.destinationAddress

        deliverToInfo.destinationName = returnTo
        deliverToInfo.destinationAddress = "return to terminal address"
        deliverToInfo.id = 2

        var equipmentInfo = db.legDao().getLegEquipmentInfoById(1)
        equipmentInfo.id = 2

        val rate = LegRate(2, "129.21", "12.03", "0.00",
                "12.33", "", "")

        val curLeg = db.legDao().getLegInfoById(1)
        curLeg.uid = 2
        curLeg.flags = ""
        curLeg.legType = "LOAD"


        if (db.legDao().getLegInfoById(2)== null) {
            db.legDao().insertLegContainerInfo(containerInfo)
            db.legDao().insertPickUpInfo(pickUpInfo)
            db.legDao().insertDeliverToInfo(deliverToInfo)
            db.legDao().insertEquipmentInfo(equipmentInfo)
            db.legDao().insertLegRate(rate)

            db.legDao().insertAnLeg(curLeg)
        } else {
            db.legDao().updateLegContainerInfo(containerInfo)
            db.legDao().updateLegPickUpInfo(pickUpInfo)
            db.legDao().updateLegDeliverToInfo(deliverToInfo)
            db.legDao().updateLegEquipmentInfo(equipmentInfo)
            db.legDao().updateLegRate(rate)

            db.legDao().updateAnLeg(curLeg)
        }
    }
}

/* Example Code for Signature Pad but we don't use here */
/*
   private fun addJpgSignatureToGallery(signature: Bitmap): Boolean {
       var result = false
       try {
           val photo = File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()))
           saveBitmapToJPG(signature, photo)
           scanMediaFile(photo)
           result = true
       } catch (e: IOException) {
           e.printStackTrace()
       }

       return result
   }*/