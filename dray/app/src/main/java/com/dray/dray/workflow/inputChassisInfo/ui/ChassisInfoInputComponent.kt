package com.dray.dray.workflow.inputChassisInfo.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.*
import com.dray.dray.R
import com.dray.dray.workflow.inputChassisInfo.ChassisInfoInputActivity
import org.jetbrains.anko.*

class ChassisInfoInputComponent : AnkoComponent <ChassisInfoInputActivity> , AnkoLogger{
    val MEDIUM_SIZE_TEXT = 18f
    val DISPATCH_SIZE_TEXT = 24f
    val DATA_TEXT_SIZE = 24f
    val TAB_GAP = 20

    lateinit var tracBtn : LinearLayout
    lateinit var flexiBtn : LinearLayout
    lateinit var dcliBtn : LinearLayout
    lateinit var wccpBtn : LinearLayout

    lateinit var tracTxt : TextView
    lateinit var flexiTxt : TextView
    lateinit var dcliTxt : TextView
    lateinit var wccpTxt : TextView

    lateinit var photoIcon : ImageView
    lateinit var chassisNumberPhotoBox : LinearLayout
    lateinit var chassisNumberDisplay : ImageView

    lateinit var chassisTypeTitleArea : LinearLayout
    lateinit var chassisTypeSelectionArea : LinearLayout

    lateinit var reportBtn : LinearLayout
    lateinit var submitBtn : Button

    override fun createView(ui: AnkoContext<ChassisInfoInputActivity>): View = with (ui){
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }
            // Dispatch button

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout {
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                }

                textView("Chassis Number"){
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = resources.getColor(R.color.colorPrimary)
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    this.gravity = Gravity.CENTER_HORIZONTAL
                }

                view(){
                    backgroundColor = resources.getColor(R.color.colorPrimary)
                }.lparams(width = dip(200), height = dip(1)){
                    bottomMargin = dip(10)
                    this.gravity = Gravity.CENTER_HORIZONTAL
                }

                chassisNumberPhotoBox = verticalLayout{
                    backgroundResource = R.drawable.green_bordered_round_btn
                    gravity = Gravity.CENTER
                    lparams(width = matchParent){
                        weight = 1f
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    }

                    photoIcon = imageView(R.drawable.add_photo){
                        adjustViewBounds = true
                        maxWidth = dip(200)
                        maxHeight = dip(200)
                    }.lparams(){
                        this.gravity = Gravity.CENTER
                    }

                    chassisNumberDisplay = imageView{
                        visibility = View.GONE
                        imageResource = R.drawable.example_avatar
                    }.lparams(width = matchParent, height = matchParent)
                }

                chassisTypeTitleArea = verticalLayout{
                    textView("Chassis Type"){
                        textSize = MEDIUM_SIZE_TEXT
                        textColor = resources.getColor(R.color.colorPrimary)
                        typeface = Typeface.DEFAULT_BOLD
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        topMargin = dip(20)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.colorPrimary)
                    }.lparams(width = dip(200), height = dip(1)){
                        bottomMargin = dip(10)
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }
                }

                chassisTypeSelectionArea = verticalLayout{
                    backgroundResource = R.drawable.green_bordered_round_btn
                    lparams(width = matchParent){
                        verticalPadding = dip(10)
                        horizontalPadding = dip(10)
                    }

                    linearLayout{
                        lparams(width = matchParent, height = wrapContent)
                        tracBtn = verticalLayout{
                            backgroundResource = R.drawable.dashed_square
                            lparams{
                                weight = 0.5f
                                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                                verticalMargin = dip(TAB_GAP /2)
                                horizontalMargin = dip(TAB_GAP /2)
                            }

                            tracTxt = textView("TRAC"){
                                this.gravity = Gravity.CENTER
                                textSize = DATA_TEXT_SIZE
                            }.lparams(width = matchParent, height = matchParent)
                        }

                        flexiBtn = verticalLayout{
                            backgroundResource = R.drawable.dashed_square
                            lparams(width = dimen(R.dimen.avatar_length)){
                                weight = 0.5f
                                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                                verticalMargin = dip(TAB_GAP /2)
                                horizontalMargin = dip(TAB_GAP /2)
                            }
                            flexiTxt = textView("Flexi-Van"){
                                this.gravity = Gravity.CENTER
                                textSize = DATA_TEXT_SIZE
                            }.lparams(width = matchParent, height = matchParent)
                        }
                    }

                    linearLayout{
                        lparams(width = matchParent, height = wrapContent)
                        dcliBtn = verticalLayout{
                            backgroundResource = R.drawable.dashed_square
                            lparams(width = dimen(R.dimen.avatar_length)){
                                weight = 0.5f
                                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                                verticalMargin = dip(TAB_GAP /2)
                                horizontalMargin = dip(TAB_GAP /2)
                            }

                            dcliTxt = textView("DCLI"){
                                this.gravity = Gravity.CENTER
                                textSize = DATA_TEXT_SIZE
                            }.lparams(width = matchParent, height = matchParent)
                        }

                        wccpBtn = verticalLayout{
                            backgroundResource = R.drawable.dashed_square
                            lparams(width = dimen(R.dimen.avatar_length)){
                                weight = 0.5f
                                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                                verticalMargin = dip(TAB_GAP /2)
                                horizontalMargin = dip(TAB_GAP /2)
                            }
                            wccpTxt = textView("WCCP"){
                                this.gravity = Gravity.CENTER
                                textSize = DATA_TEXT_SIZE
                            }.lparams(width = matchParent, height = matchParent)
                        }
                    }

                    /*privateBtn = verticalLayout{
                        backgroundResource = R.drawable.dashed_square
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.activity_vertical_padding)
                            verticalMargin = dip(TAB_GAP /2)
                            horizontalMargin = dip(TAB_GAP /2)
                        }

                        privateTxt = textView("Private"){
                            this.gravity = Gravity.CENTER
                            textSize = DATA_TEXT_SIZE
                        }.lparams(width = matchParent, height = matchParent)
                    }*/
                }
            }

            submitBtn = button("Submit"){
                visibility = View.GONE
                textSize = MEDIUM_SIZE_TEXT
                textColor = Color.WHITE
                backgroundResource = R.drawable.green_rounded_button
            }.lparams(width = matchParent, height = wrapContent){
                weight = 0f
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}