package com.dray.dray.workflow.ingate.privateChassis

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.ingate.privateChassis.ui.PrivateChassisIngateComponent
import com.dray.dray.workflow.inputChassisInfo.ChassisInfoInputActivity
import kotlinx.android.synthetic.main.report_problems_btn.view.*
import org.jetbrains.anko.setContentView

class PrivateChassisIngateActivity : AppCompatActivity() {

    val ui = PrivateChassisIngateComponent()
    lateinit var db : AppDatabase

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        /* -- Set Toolbar and Database-- */
        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Ingate at the Chassis Provider")

        db = getDatabase(applicationContext)

        /* -- Inject Data -- */
        val chassisInfo = db.legDao().getLegEquipmentInfoById(1)
        ui.releaseNoTxt.text = chassisInfo.releaseNumber
        ui.chassisProviderTxt.text = chassisInfo.providerName
        ui.hireOffRateTxt.text = "$" + db.legDao().getLegRateInfo(1).hireOffRate

        /* -- Set Button -- */
        ui.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }

        ui.nextBtn.setOnClickListener {
            val i = Intent(this, ChassisInfoInputActivity::class.java)
            startActivity(i)
        }
    }
}
