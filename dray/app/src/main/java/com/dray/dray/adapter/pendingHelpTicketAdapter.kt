package com.dray.dray.adapter

import android.content.Intent
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.R
import com.dray.dray.customClasses.HelpTicketItem
import com.dray.dray.helpActivities.HelpTicketDetailActivity
import com.dray.dray.helpActivities.PendingHelpTicketFragmentActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk15.listeners.onClick

class pendingHelpTicketAdapter(val activity : PendingHelpTicketFragmentActivity,
                               var helpTicketItems: ArrayList<HelpTicketItem>): BaseAdapter(){
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val item : HelpTicketItem = getItem(p0)
        var MEDIUM_SIZE_TEXT = 18f
        return with (p2!!.context){
            verticalLayout {
                lparams(width = matchParent, height = matchParent)

                linearLayout {
                    backgroundResource = R.drawable.white_rounded_item
                    lparams(width = matchParent, height = wrapContent){
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    }

                    verticalLayout{
                        lparams{
                            weight = 1f
                        }

                        linearLayout{
                            lparams{
                                bottomMargin = dip(5)
                            }
                            textView("No. " + item.ticketNumber){
                                textColor = R.color.colorPrimary
                                textSize = MEDIUM_SIZE_TEXT
                            }.lparams(){
                                rightMargin = dip(10)
                            }
                            textView(item.description.substring(0, 20) + " ..."){
                                textSize = MEDIUM_SIZE_TEXT
                                textColor = Color.BLACK
                            }
                        }

                        linearLayout{
                            imageView(){
                                if (item.status == "Just solved"){
                                    imageResource = R.drawable.green_rounded_button
                                } else if (item.status == "Solving") {
                                    imageResource = R.drawable.orange_rounded_item
                                } else if (item.status == "Received"){
                                    imageResource = R.drawable.grey_rounded_item
                                }


                            }.lparams(width = dip(10), height = dip(10)){
                                this.gravity = Gravity.CENTER_VERTICAL
                                rightMargin = dip(10)
                            }

                            textView(item.status)
                        }
                    }

                    imageView{
                        imageResource = R.mipmap.ic_chevron_right
                    }.lparams(){
                        weight = 0f
                        this.gravity = Gravity.CENTER_VERTICAL
                    }

                }.onClick {
                    val intent= Intent(context, HelpTicketDetailActivity::class.java)
                    intent.putExtra("TICKET_NUMBER",item.ticketNumber)
                    intent.putExtra("DATE_RECEIVED", item.receiveDate.toString())
                    intent.putExtra("STATUS",item.status)
                    intent.putExtra("DESCRIPTION", item.description)
                    intent.putExtra("SOLUTION", item.solution)
                    context.startActivity(intent)
                }
            }
        }
    }

    override fun getItem(p0: Int): HelpTicketItem {
        return helpTicketItems?.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return helpTicketItems.count()
    }


    /* Function briefDescpt
    * -----------------------------------
    * Crop out the first three words in the description paragraph and add ... at the end
    */
    fun briefDescrpt (description : String) : String{
        val separate = description.split(" ".toRegex())
        var result : String = ""
        (0 .. 2).forEach{i->
            result += separate[i] + " "
        }

        result += "..."
        return result
    }

}