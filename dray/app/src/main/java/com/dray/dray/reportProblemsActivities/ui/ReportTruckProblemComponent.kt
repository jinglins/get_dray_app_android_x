package com.dray.dray.reportProblemsActivities.ui

import android.view.Gravity
import android.view.View
import android.widget.Button
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ReportTruckProblemActivity
import org.jetbrains.anko.*

class ReportTruckProblemComponent : AnkoComponent<ReportTruckProblemActivity>, AnkoLogger {
    val MEDIUM_SIZE_TEXT = 18f
    val LARGE_SIZE_TEXT = 30f

    lateinit var noGasBtn : Button
    lateinit var engineBtn : Button
    lateinit var tireBtn : Button
    lateinit var otherBtn : Button

    val gap = 10

    override fun createView(ui: AnkoContext<ReportTruckProblemActivity>): View  = with(ui){
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }
            textView("What truck problem are you having?"){
                textSize = MEDIUM_SIZE_TEXT
                textColor = R.color.colorPrimary
            }.lparams(){
                this.gravity = Gravity.CENTER_HORIZONTAL
            }

            verticalLayout {
                lparams(width = matchParent){
                    weight = 1f
                    verticalPadding = dip(15)
                }
                gravity = Gravity.CENTER
                noGasBtn = button("Out of Fuel"){
                    backgroundResource = R.drawable.white_rounded_button
                    textSize =  LARGE_SIZE_TEXT
                    textColor = resources.getColor(R.color.defaultLightTxtColor)
                }.lparams(width = matchParent, height = matchParent)
            }

            verticalLayout {
                lparams(width = matchParent){
                    weight = 1f
                    verticalPadding = dip(15)
                }
                gravity = Gravity.CENTER
                engineBtn = button("Engine"){
                    backgroundResource = R.drawable.white_rounded_button
                    textSize =  LARGE_SIZE_TEXT
                    textColor = resources.getColor(R.color.defaultLightTxtColor)
                }.lparams(width = matchParent, height = matchParent)
            }

            verticalLayout {
                lparams(width = matchParent){
                    weight = 1f
                    verticalPadding = dip(15)
                }
                gravity = Gravity.CENTER
                tireBtn = button("Tire"){
                    backgroundResource = R.drawable.white_rounded_button
                    textSize = LARGE_SIZE_TEXT
                    textColor = resources.getColor(R.color.defaultLightTxtColor)
                }.lparams(width = matchParent, height = matchParent)
            }

            verticalLayout {
                lparams(width = matchParent){
                    weight = 1f
                    verticalPadding = dip(15)
                }
                gravity = Gravity.CENTER
                otherBtn = button("Other"){
                    backgroundResource = R.drawable.white_rounded_button
                    textSize = LARGE_SIZE_TEXT
                    textColor = resources.getColor(R.color.defaultLightTxtColor)
                }.lparams(width = matchParent, height = matchParent)
            }
        }
    }

}