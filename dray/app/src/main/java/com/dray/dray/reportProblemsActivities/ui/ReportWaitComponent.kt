package com.dray.dray.reportProblemsActivities.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ReportWaitActivity
import org.jetbrains.anko.*

class ReportWaitComponent : AnkoComponent<ReportWaitActivity>, AnkoLogger {

    val TIMER_NUMBER_SIZE = 60f
    val TIMER_LABEL_SIZE = 20f

    lateinit var hrsTxt : TextView
    lateinit var minTxt : TextView
    lateinit var secTxt : TextView

    lateinit var trafficJamBox : LinearLayout
    lateinit var trafficJamTxt : TextView

    lateinit var waitIngateBox : LinearLayout
    lateinit var waitIngateTxt : TextView

    lateinit var submitBtn : Button

    override fun createView(ui: AnkoContext<ReportWaitActivity>): View = with(ui){
        verticalLayout {
            verticalLayout (R.style.ThemeOverlay_AppCompat_Dark) {
                backgroundResource = R.drawable.purple_header
                lparams(width = matchParent){
                    weight = 1f
                }

                gravity = Gravity.CENTER_VERTICAL

                textView("My Waiting Time"){
                    typeface = Typeface.DEFAULT_BOLD
                    textSize = 18f
                }.lparams(){
                    gravity = Gravity.CENTER
                    bottomMargin = dip(10)
                }

                textView("This timer auto records the waiting time for you.\nIt stops once " +
                        "you choose to submit this report."){
                    textSize = 8f
                }.lparams(){
                    gravity = Gravity.CENTER_HORIZONTAL
                }

                linearLayout {
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                    verticalLayout {
                        lparams(width = matchParent){
                            weight = 1f
                        }
                        hrsTxt = textView("00"){
                            textSize = TIMER_NUMBER_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                        textView("HRS"){
                            textSize = TIMER_LABEL_SIZE
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                    }

                    verticalLayout{
                        lparams(height = matchParent)
                        gravity = Gravity.CENTER_VERTICAL
                        textView(":"){
                            textSize = TIMER_LABEL_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    verticalLayout{
                        lparams(width = matchParent){
                            weight = 1f
                        }
                        minTxt = textView("00"){
                            textSize = TIMER_NUMBER_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                        textView("MIN"){
                            textSize = TIMER_LABEL_SIZE
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                    }

                    verticalLayout{
                        lparams(height = matchParent)
                        gravity = Gravity.CENTER_VERTICAL
                        textView(":"){
                            textSize = TIMER_LABEL_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    verticalLayout {
                        lparams(width = matchParent){
                            weight = 1f
                        }
                        secTxt = textView("00"){
                            textSize = TIMER_NUMBER_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                        textView("SEC"){
                            textSize = TIMER_LABEL_SIZE
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                    }
                }


            }

            verticalLayout(){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                backgroundColor = Color.WHITE

                textView("Before submission, please tell us why you are waiting."){
                    textColor = R.color.colorPrimary
                }.lparams(){
                    this.gravity = Gravity.CENTER_HORIZONTAL
                }
                linearLayout{
                    lparams(width = matchParent, height = wrapContent){
                        topMargin = dip(10)
                        bottomMargin = dimen(R.dimen.activity_vertical_margin)
                    }
                    trafficJamBox = verticalLayout{
                        backgroundResource = R.drawable.dashed_square
                        lparams(width = matchParent){
                            weight = 1f
                            verticalPadding = dimen(R.dimen.activity_vertical_padding)
                            verticalMargin = dip(5)
                            horizontalMargin = dip(5)
                        }

                        trafficJamTxt = textView("TRAFFIC JAM"){
                            this.gravity = Gravity.CENTER
                            textSize =18f
                        }.lparams(width = matchParent, height = matchParent)
                    }

                    waitIngateBox = verticalLayout{
                        backgroundResource = R.drawable.dashed_square
                        lparams(width = matchParent){
                            weight = 1f
                            verticalPadding = dimen(R.dimen.activity_vertical_padding)
                            verticalMargin = dip(5)
                            horizontalMargin = dip(5)
                        }
                        waitIngateTxt = textView("WAITING INGATE"){
                            this.gravity = Gravity.CENTER
                            textSize = 18f
                        }.lparams(width = matchParent, height = matchParent)
                    }
                }

                submitBtn = button("Submit"){
                    backgroundResource = R.drawable.green_rounded_button
                    textColor = Color.WHITE
                    textSize = 18f
                }
            }


        }
    }

}