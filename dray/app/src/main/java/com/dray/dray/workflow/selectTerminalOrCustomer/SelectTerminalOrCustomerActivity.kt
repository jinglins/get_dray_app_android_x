package com.dray.dray.workflow.selectTerminalOrCustomer

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.dray.dray.adapter.selectTerminalAdapter
import com.dray.dray.customClasses.*
import com.dray.dray.workflow.selectTerminalOrCustomer.ui.SelectTerminalOrCustomerComponent
import org.jetbrains.anko.setContentView
import android.widget.AdapterView
import android.widget.Button
import com.dray.dray.R
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.mapActivity.MapActivity


class SelectTerminalOrCustomerActivity : AppCompatActivity() {
    val MEDIUM_SIZE_TEXT = 18f
    val DISPATCH_SIZE_TEXT = 24f
    val DATA_TEXT_SIZE = 30f

    val ui = SelectTerminalOrCustomerComponent()
    lateinit var db : AppDatabase
    lateinit var curPlace : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)
        val actionBar = supportActionBar

        curPlace = intent.getStringExtra("CUR_PLACE")
        val title = if (curPlace == "customer") {
            "Select A Terminal"
        } else {
            "Select A Customer"
        }
        setToolBar(actionBar!!, title)
        db = getDatabase(applicationContext)

        ui.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }

        val questionStr = if (curPlace == "customer"){
            "Return to Which Terminal?"
        } else {
            "Deliver to Which Customer?"
        }

        var listViewArr = ArrayList<String>()
        listViewArr = if (curPlace == "customer"){
            // Parse multiple terminal options
            val returnToTerminalStr = db.legDao().getLegDeliverToInfo(2).destinationName
            ArrayList(returnToTerminalStr.split(", "))

        } else {
            // Parse multiple customer options
            val customerStr = db.legDao().getLegDeliverToInfo(1).destinationName
            ArrayList(customerStr.split(", "))
        }

        ui.lv.adapter = selectTerminalAdapter(this, listViewArr)
        ui.lv.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            val selected = listViewArr[i]
            //println("selected" + returnToTerminalArrayList[i])
            val builder = AlertDialog.Builder(this@SelectTerminalOrCustomerActivity)

            val message = if (curPlace == "customer") {
                "Return this container to $selected?"
            } else {
                "Deliver this container to $selected?"
            }

            builder.setMessage(message)
            builder.setPositiveButton("YES") { _, _ ->
                goToNextActivity(listViewArr[i])
            }

            // Display a neutral button on alert dialog
            builder.setNeutralButton("Cancel") { dialog, _ ->
                dialog.cancel()
            }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()
            dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#808080"))
            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#29cc9e"))
        }
    }

    private fun goToNextActivity(selected: String) {
        // Update local backend
        val db = getDatabase(applicationContext)

        val deliverToInfo = if (curPlace == "customer"){
            db.legDao().getLegDeliverToInfo(2)
        } else {
            db.legDao().getLegDeliverToInfo(1)
        }
        deliverToInfo.destinationName = selected
        db.legDao().updateLegDeliverToInfo(deliverToInfo)

        // Go to next activity
        lateinit var i : Intent
        if (curPlace == "customer"){
            i = Intent (this, MapActivity::class.java)
            i.putExtra("TARGET", "BACK_TO_TERMINAL")
            i.putExtra("WANT_NEXT_LOAD", "false")
            startActivity(i)
        } else {
            i = Intent (this, MapActivity::class.java)
            i.putExtra("TARGET", "CUSTOMER")
        }

        startActivity(i)
    }

}
