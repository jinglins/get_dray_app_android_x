package com.dray.dray.reportProblemsActivities

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dray.dray.reportProblemsActivities.ui.ReportWaitComponent
import org.jetbrains.anko.setContentView
import android.os.CountDownTimer
import android.widget.Toast
import com.dray.dray.MainActivity
import com.dray.dray.R
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.textColor

class ReportWaitActivity : AppCompatActivity() {
    val ui = ReportWaitComponent()
    var waitReason = ""
    var curSeconds = 0.toLong()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)
        ui.submitBtn.isEnabled = false
        reasonInteraction()
        val totalSeconds: Long = 86400 // Longest report wait time that can be reported
        val intervalSeconds: Long = 1
        val timer = object : CountDownTimer(totalSeconds * 1000, intervalSeconds * 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val elapsedSeconds = (totalSeconds * 1000 - millisUntilFinished) / 1000
                curSeconds = elapsedSeconds
                renderTxtView(elapsedSeconds)
            }

            override fun onFinish() {
                Toast.makeText(applicationContext, "You have reached the maximum length of report time.", Toast.LENGTH_SHORT).show()
            }
        }
        timer.start()
        ui.submitBtn.setOnClickListener {
            timer.cancel()
            Toast.makeText(applicationContext, "Your waiting time has been recorded.", Toast.LENGTH_SHORT).show()

            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
        }
    }

    private fun renderTxtView (elapsedSeconds : Long) {
        val numberOfHours = (elapsedSeconds % 86400 ) / 3600 ;
        val numberOfMinutes = ((elapsedSeconds % 86400 ) % 3600 ) / 60
        val numberOfSeconds = ((elapsedSeconds % 86400 ) % 3600 ) % 60

        val hours = if (numberOfHours < 10) "0" + numberOfHours.toString() else numberOfHours.toString()
        val minutes = if (numberOfMinutes < 10) "0" + numberOfMinutes.toString() else numberOfMinutes.toString()
        val seconds = if (numberOfSeconds < 10) "0" + numberOfSeconds.toString() else numberOfSeconds.toString()

        ui.hrsTxt.text = hours
        ui.minTxt.text = minutes
        ui.secTxt.text = seconds
    }

    private fun reasonInteraction() {
        ui.trafficJamBox.setOnClickListener {
            // clear the other box highlight
            ui.waitIngateBox.backgroundResource = R.drawable.dashed_square
            ui.waitIngateTxt.textColor = resources.getColor(R.color.defaultLightTxtColor)

            ui.trafficJamBox.backgroundResource = R.drawable.green_rounded_button
            ui.trafficJamTxt.textColor = Color.WHITE

            waitReason = "TRAFFIC JAM"
            ui.submitBtn.isEnabled = true
        }

        ui.waitIngateBox.setOnClickListener {
            ui.trafficJamBox.backgroundResource = R.drawable.dashed_square
            ui.trafficJamTxt.textColor = resources.getColor(R.color.defaultLightTxtColor)

            ui.waitIngateBox.backgroundResource = R.drawable.green_rounded_button
            ui.waitIngateTxt.textColor = Color.WHITE

            waitReason = "WAITING INGATE"
            ui.submitBtn.isEnabled = true
        }
    }
}
