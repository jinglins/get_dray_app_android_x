package com.dray.dray.workflow.ingate.terminal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.atPlace.AtTerminalActivity
import com.dray.dray.workflow.ingate.terminal.ui.TerminalIngateExportComponent
import com.dray.dray.workflow.ingate.terminal.ui.TerminalIngateFreeflowImportComponent
import com.dray.dray.workflow.ingate.terminal.ui.TerminalIngateRegularImportComponent
import com.dray.dray.workflow.inputChassisInfo.ChassisInfoInputActivity
import kotlinx.android.synthetic.main.report_problems_btn.view.*
import org.jetbrains.anko.setContentView

class TerminalIngateActivity : AppCompatActivity() {
    private val regularImportUI = TerminalIngateRegularImportComponent()
    private val freeflowImportUI = TerminalIngateFreeflowImportComponent()
    private val regularExportUI = TerminalIngateExportComponent()
    lateinit var gatePassNumber : String
    lateinit var db : AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Terminal In-Gate")

        db = getDatabase(applicationContext)

        val firstLegInfo = db.legDao().getLegInfoById(1)
        println("firstLegInfoType: " + firstLegInfo.legType)

        if (firstLegInfo.legType == "IMPORT"){
            if (firstLegInfo.flags.contains("FREE_FLOW")){
                freeflowImportUI.setContentView(this)
                injectData("freeflow")

                freeflowImportUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                    displayReportWindow(applicationContext, "false", "false")
                }

                freeflowImportUI.nextBtn.setOnClickListener {
                    gatePassNumber = freeflowImportUI.gatePassNo.text.toString().toUpperCase()
                    if (gatePassNumber.isEmpty()) {
                        freeflowImportUI.gatePassNo.error = "Please enter the gate pass number."
                    } else {
                        goToNextActivity()
                    }
                }
            } else {
                regularImportUI.setContentView(this)
                injectData("import")

                regularImportUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                    println("clicked")
                    displayReportWindow(applicationContext, "false", "false")
                }

                regularImportUI.nextBtn.setOnClickListener {
                    gatePassNumber = regularImportUI.gatePassNo.text.toString().toUpperCase()
                    if (gatePassNumber.isEmpty()) {
                        regularImportUI.gatePassNo.error = "Please enter the gate pass number."
                    } else {
                        goToNextActivity()
                    }
                }
            }
        } else {
            regularExportUI.setContentView(this)
            injectData("export")

            regularExportUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                println("clicked")
                displayReportWindow(applicationContext, "false", "false")
            }

            regularExportUI.nextBtn.setOnClickListener {
                gatePassNumber = regularExportUI.gatePassNo.text.toString().toUpperCase()
                if (gatePassNumber.isEmpty()) {
                    regularExportUI.gatePassNo.error = "Please enter the gate pass number."
                } else {
                    goToNextActivity()
                }
            }
        }
    }

    private fun goToNextActivity (){
        val i = if (intent.getStringExtra("WITH_CHASSIS") == "true") {
            Intent(this, AtTerminalActivity :: class.java)
        } else {
            Intent(this, ChassisInfoInputActivity :: class.java)
        }
        startActivity(i)
    }

    private fun injectData (uiIndicator : String) {
        val apptNumber = db.legDao().getLegPickUpInfoById(1).apptNumber

        when (uiIndicator) {
            "freeflow" -> {
                if (apptNumber.isEmpty()) {
                    freeflowImportUI.appointmentBlock.visibility = View.GONE
                } else {
                    freeflowImportUI.appointmentNo.text = apptNumber
                }
                freeflowImportUI.freeflowCode.text = db.legDao().getLegInfoById(1).freeFlowCode
                freeflowImportUI.SCACCode.text = db.profileDao().getProfileTMCInfoById(1).SCACCode

            }
            "export" -> {
                regularExportUI.bookingNo.text = db.legDao().getLegInfoById(1).bolBookingNumber
                regularExportUI.SCACCode.text = db.profileDao().getProfileTMCInfoById(1).SCACCode
                regularExportUI.equipment.text = db.legDao().getLegEquipmentInfoById(1).equipmentName
                regularExportUI.containerSize.text = db.legDao().getLegContainerInfo(1).containerSize

            }
            else -> {
                regularImportUI.containerNo.text = db.legDao().getLegContainerInfo(1).containerNumber
                regularImportUI.bolNo.text = db.legDao().getLegInfoById(1).bolBookingNumber
                regularImportUI.deliverTo.text = db.legDao().getLegDeliverToInfo(1).destinationName
                regularImportUI.SCACCode.text = db.profileDao().getProfileTMCInfoById(1).SCACCode
                if (apptNumber.isEmpty()) {
                    regularImportUI.appointmentBlock.visibility = View.GONE
                } else {
                    regularImportUI.appointmentNo.text = apptNumber
                }
                regularImportUI.containerSize.text = db.legDao().getLegContainerInfo(1).containerSize
            }
        }
    }

}
