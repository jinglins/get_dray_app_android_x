package com.dray.dray.daos

import androidx.room.*
import com.dray.dray.dataClasses.leg.*
import com.dray.dray.dataClasses.profile.*

@Dao
interface LegDao {
    @Insert
    fun insertAnLeg (leg : Leg)

    @Insert
    fun insertLegContainerInfo (legContainerInfo: LegContainerInfo)

    @Insert
    fun insertPickUpInfo (legPickUpInfo: LegPickUpInfo)

    @Insert
    fun insertDeliverToInfo (legDeliverToInfo: LegDeliverToInfo)

    @Insert
    fun insertEquipmentInfo (legEquipmentInfo: LegEquipmentInfo)

    @Insert
    fun insertLegRate (legRate: LegRate)

    //----------------------

    @Update
    fun updateAnLeg (leg : Leg)

    @Update
    fun updateLegContainerInfo (legContainerInfo: LegContainerInfo)

    @Update
    fun updateLegEquipmentInfo (legEquipmentInfo: LegEquipmentInfo)

    @Update
    fun updateLegRate (legRate: LegRate)

    @Update
    fun updateLegPickUpInfo (legPickUpInfo: LegPickUpInfo)

    @Update
    fun updateLegDeliverToInfo (legDeliverToInfo: LegDeliverToInfo)

    //----------------------

    @Query ("SELECT * FROM leg WHERE uid = :id")
    fun getLegInfoById (id: Int) : Leg

    @Query ("SELECT * FROM leg_container_info WHERE id = :id")
    fun getLegContainerInfo (id: Int) : LegContainerInfo

    @Query ("SELECT * FROM leg_pick_up_info WHERE id = :id")
    fun getLegPickUpInfoById (id: Int) : LegPickUpInfo

    @Query ("SELECT * FROM leg_deliver_to_info WHERE id = :id")
    fun getLegDeliverToInfo (id: Int) : LegDeliverToInfo

    @Query ("SELECT * FROM leg_equipment_info WHERE id = :id")
    fun getLegEquipmentInfoById (id: Int) : LegEquipmentInfo

    @Query ("SELECT * FROM leg_rate WHERE id = :id")
    fun getLegRateInfo (id: Int) : LegRate
}