package com.dray.dray.daos
import androidx.room.*
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.dray.dray.dataClasses.profile.*

@Dao
interface ProfileDao {
    @Insert
    fun insertProfileBasicInfo(profileBasicInfo: ProfileBasicInfo)

    @Insert
    fun insertProfileQualifiedToDrive(profileQualifiedToDrive: ProfileQualifiedToDrive)

    @Insert
    fun insertProfileWorkPreference(profileWorkPreference: ProfileWorkPreference)

    @Insert
    fun insertProfileTMCInfo(profileTMC: ProfileTMC)

    @Insert
    fun insertProfileCarrierInfo(profileCarrier: ProfileCarrier)

// ----------------

    @Query("SELECT * FROM profile_basic_info WHERE id = :id")
    fun getProfileBasicInfoById(id: Int): ProfileBasicInfo

    @Query("SELECT * FROM profile_qualified_to_drive WHERE id = :id")
    fun getProfileQualifiedToDriveById(id: Int): ProfileQualifiedToDrive

    @Query("SELECT * FROM profile_work_preference WHERE id = :id")
    fun getProfileWorkPreferenceById(id: Int): ProfileWorkPreference

    @Query("SELECT * FROM profile_tmc_info  WHERE id = :id")
    fun getProfileTMCInfoById(id: Int): ProfileTMC

    @Query("SELECT * FROM profile_carrier_info WHERE id = :id")
    fun getProfileCarrierInfoById(id: Int): ProfileCarrier

    // ------------------
    @Update
    fun updateProfileBasicInfo(profileBasicInfo: ProfileBasicInfo)

    @Update
    fun updateProfileQualifiedToDrive(profileQualifiedToDrive: ProfileQualifiedToDrive)

    @Update
    fun updateProfileWorkPreference(profileWorkPreference: ProfileWorkPreference)

    @Update
    fun updateProfileTMCInfo(profileTMC: ProfileTMC)

    @Update
    fun updateProfileCarrierInfo(profileCarrier: ProfileCarrier)
}