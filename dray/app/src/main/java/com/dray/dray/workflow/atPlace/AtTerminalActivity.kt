package com.dray.dray.workflow.atPlace

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.workflow.outgate.terminal.TerminalOutgateActivity

class AtTerminalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_at_terminal)

        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "At Terminal")

        val reportBtn = findViewById<LinearLayout>(R.id.report_button).findViewById<Button>(R.id.report_btn)
        val nextBtn  = findViewById<Button>(R.id.nextBtn)


        reportBtn.setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }

        nextBtn.setOnClickListener {
            val i = Intent(this, TerminalOutgateActivity::class.java)
            startActivity(i)
        }

        //ui.reportBtn.setOnClickListener {
        //    displayReportWindow(applicationContext)
        //}
    }
}
