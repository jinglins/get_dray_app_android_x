package com.dray.dray.workflow.mapActivity

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.dataClasses.*
import com.dray.dray.dataClasses.leg.Leg
import com.dray.dray.dataClasses.leg.LegEquipmentInfo
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.atPlace.AtYardActivity
import com.dray.dray.workflow.backToTerminal.BackToTerminalActivity
import com.dray.dray.workflow.ingate.customer.CustomerIngateActivity
import com.dray.dray.workflow.ingate.privateChassis.PrivateChassisIngateActivity
import com.dray.dray.workflow.ingate.terminal.TerminalIngateActivity
import com.dray.dray.workflow.mapActivity.ui.MapComponent
import com.tallygo.tallygoandroid.utils.TGLauncher
import com.tallygo.tallygoandroid.sdk.TallyGo
import com.tallygo.tallygoandroid.sdk.navigation.TGRouteRequest
import com.mapbox.mapboxsdk.geometry.LatLng
import kotlinx.android.synthetic.main.activity_help.*
import org.jetbrains.anko.sdk15.listeners.onClick
import org.jetbrains.anko.setContentView


class MapActivity : AppCompatActivity() {

    val ui = MapComponent()
    lateinit var db : AppDatabase

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)
        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Map Navigation")

        db = getDatabase(applicationContext)

        val legType = db.legDao().getLegInfoById(1).legType
        val firstLegPickUpInfo = db.legDao().getLegPickUpInfoById(1)
        val firstLegDeliverToInfo = db.legDao().getLegDeliverToInfo(1)

        val pickUpName = firstLegPickUpInfo.originName
        val pickUpAddr = firstLegPickUpInfo.originAddress
        val deliverToName = firstLegDeliverToInfo.destinationName
        val deliverToAddr = firstLegDeliverToInfo.destinationAddress

        ui.generalInfoBlock.setOnClickListener {
            displayReportWindow(applicationContext, "false", "true")
        }

        ui.reportProblemsBtn.setOnClickListener {
            displayReportWindow(applicationContext, "false", "true")
        }

        ui.targetPlace.text = "Heading " + intent.getStringExtra("TARGET")

        /* ----------------------------------- Configuration ------------------------------------ */

        if (intent.getStringExtra("TARGET") == "YARD"){
            /* ------------------------------------- Go Yard -------------------------------------*/
            debugCurrentTargetLocation(deliverToName, deliverToAddr)

            ui.nextBtn.setOnClickListener {
                val i = Intent(this, AtYardActivity::class.java)
                startActivity(i)
            }

            /* --------------------------------- Go Private Chassis ------------------------------*/
        } else if  (intent.getStringExtra("TARGET") == "PRIVATE_CHASSIS"){
            val firstLegChassisInfo = db.legDao().getLegEquipmentInfoById(1)
            debugCurrentTargetLocation(firstLegChassisInfo.providerName, firstLegChassisInfo.providerAddress)
            renderChassisInfoBlock(firstLegChassisInfo)
            ui.nextBtn.setOnClickListener {
                val i = Intent(this, PrivateChassisIngateActivity::class.java)
                startActivity(i)
            }

        } else if (intent.getStringExtra("TARGET") == "TERMINAL"){
            /* --------------------------------- Go terminal ------------------------------*/
            debugCurrentTargetLocation(pickUpName, pickUpAddr)
            val withChassis = intent.getStringExtra("WITH_CHASSIS")

            ui.firstLegBlock.visibility = View.VISIBLE
            val firstLegInfo = db.legDao().getLegInfoById(1)
            if (legType == "IMPORT"){
                if (firstLegInfo.flags.contains("FREE_FLOW")){
                    renderFirstLegInfoBlock("freeflowImport")
                } else {
                    renderFirstLegInfoBlock("regularImport")
                }
            } else {
                renderFirstLegInfoBlock("export")
            }

            ui.nextBtn.setOnClickListener {
                val i = Intent(this, TerminalIngateActivity::class.java)
                i.putExtra("WITH_CHASSIS", withChassis)
                startActivity(i)
            }

        } else if (intent.getStringExtra("TARGET") == "CUSTOMER"){
            /* ------------------------------------ Go customer ----------------------------------*/
            debugCurrentTargetLocation(deliverToName, deliverToAddr)

            ui.firstLegBlock.visibility = View.VISIBLE
            if (legType == "IMPORT") {
                renderFirstLegInfoBlock("regularImport")
            } else {
                renderFirstLegInfoBlock("export")
            }

            ui.nextBtn.setOnClickListener {
                val i = Intent (this, CustomerIngateActivity::class.java)
                startActivity(i)
            }
        } else if (intent.getStringExtra("TARGET") == "NEW_CUSTOMER_FACILITY") {
            /* ----------------------------- Go new customer facility ----------------------------*/
            val secondLegPickUpInfo = db.legDao().getLegPickUpInfoById(2)
            debugCurrentTargetLocation(secondLegPickUpInfo.originName, secondLegPickUpInfo.originAddress)
            ui.nextBtn.setOnClickListener{
                val i = Intent(this, CustomerIngateActivity::class.java)
                i.putExtra("INGATE_STATUS", "bobtail")
                startActivity(i)
            }

        }else {
            // Back to terminal, TARGET = "BACK_TO_TERMINAL"
            val wantNextLoad = intent.getStringExtra("WANT_NEXT_LOAD")
            val secondLegDeliverToInfo = db.legDao().getLegDeliverToInfo(2)
            val secondLegContainerInfo = db.legDao().getLegContainerInfo(2)
            debugCurrentTargetLocation(secondLegDeliverToInfo.destinationName,
                    secondLegDeliverToInfo.destinationAddress)

            ui.secondLegContainerNoTxt.text = secondLegContainerInfo.containerNumber
            ui.secondLegSizeTxt.text = secondLegContainerInfo.containerSize
            ui.returnToTxt.text = secondLegDeliverToInfo.destinationName

            ui.nextBtn.setOnClickListener {
                var i = Intent (this, BackToTerminalActivity::class.java)
                i.putExtra("WANT_NEXT_LOAD", wantNextLoad)
                startActivity(i)
            }
        }
    }


    private fun debugCurrentTargetLocation (destName : String, destAddr : String) {
        ui.destNameTxt.text = destName
        ui.destAddrTxt.text = destAddr
    }

    private fun renderChassisInfoBlock (firstLegChassisInfo : LegEquipmentInfo){
     val hireOffRate = db.legDao().getLegRateInfo(1).hireOffRate
     val hireoffText = "The $$hireOffRate hire-off rate will be paid by ${firstLegChassisInfo.providerName}."

     ui.chassisBlock.visibility = View.VISIBLE
     ui.releaseNoTxt.text = firstLegChassisInfo.releaseNumber
     ui.chassisProviderTxt.text = firstLegChassisInfo.providerName
     ui.hireoffTxt.text = hireoffText
    }

    private fun renderFirstLegInfoBlock (type : String) {
     val containerInfo = db.legDao().getLegContainerInfo(1)
     val legInfo = db.legDao().getLegInfoById(1)
     when (type) {
         "freeflowImport" -> {
             showInfo(listOf(ui.firstLegReleaseNo, ui.firstLegSize))
             ui.firstLegReleaseNoTxt.text = legInfo.freeFlowCode
             ui.firstLegSizeTxt.text = containerInfo.containerSize
         }
         "regularImport" -> {
             showInfo(listOf(ui.firstLegContainerNo, ui.firstLegDestination,
                     ui.firstLegBOLNo, ui.firstLegSize))
             ui.firstLegContainerNoTxt.text = containerInfo.containerNumber
             ui.firstLegSizeTxt.text = containerInfo.containerSize
             ui.firstLegBOLNoTxt.text = legInfo.bolBookingNumber
             ui.firstLegDestinationTxt.text = db.legDao().getLegDeliverToInfo(1).destinationName

         }
         "export" -> {
             showInfo(listOf(ui.firstLegBookingNo, ui.firstLegSize))
             ui.firstLegBookingNoTxt.text = legInfo.bolBookingNumber
             ui.firstLegSizeTxt.text = containerInfo.containerSize
         }
     }
    }

    private fun startTallyGo() {
        // specify coordinates
        val currentLocation = LatLng(34.101558, -118.340944) // Grauman's Chinese Theatre
        val destinationCoordinate = LatLng(34.011441, -118.494932) // Santa Monica Pier

        val waypoints : List<LatLng> = listOf(currentLocation, destinationCoordinate)


        //val waypoints = Arrays.asList(currentLocation, destinationCoordinate)

        // create the request with the date/time supplied as the departure time
        val routeRequest = TGRouteRequest.Builder(waypoints).build()

        // simulate navigation
        TallyGo.getInstance(this).enableSimulation(true, false, currentLocation)

        // start TallyGo
        TGLauncher.startPreview(this, routeRequest)
    }

    private fun showInfo(linearLayouts : List<LinearLayout>){
        linearLayouts.forEach { it -> it.visibility = View.VISIBLE }
    }
}
