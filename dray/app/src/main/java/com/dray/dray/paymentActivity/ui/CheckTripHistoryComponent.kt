package com.dray.dray.paymentActivity.ui

import android.graphics.Color
import android.view.View
import android.widget.Button
import android.widget.ListView
import com.dray.dray.R
import com.dray.dray.adapter.checkTripHistoryAdapter
import com.dray.dray.paymentActivity.CheckTripHistoryActivity
import org.jetbrains.anko.*

class CheckTripHistoryComponent : AnkoComponent<CheckTripHistoryActivity>, AnkoLogger{
    lateinit var lv : ListView
    lateinit var submitBtn : Button
    override fun createView(ui: AnkoContext<CheckTripHistoryActivity>): View = with(ui){
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
            }

            lv = listView(){
                divider = null
                backgroundColor = Color.TRANSPARENT
                cacheColorHint = Color.TRANSPARENT
                dividerHeight = dip(10)
            }.lparams(width = matchParent){
                weight = 1f
            }

            submitBtn = button("Confirm All"){
                backgroundResource = R.drawable.green_rounded_button
                textSize = 18f
                textColor = Color.WHITE
            }.lparams(width = matchParent){
                topMargin = dip(20)
                weight = 0f
            }
        }
    }

}