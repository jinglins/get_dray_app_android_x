package com.dray.dray.reportProblemsActivities.ui

import android.graphics.Color
import android.graphics.Typeface
import android.media.Image
import android.media.MediaPlayer
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.AccidentTheOtherPartyInfoActivity
import org.jetbrains.anko.*

class AccidentTheOtherPartyInfoComponent : AnkoComponent<AccidentTheOtherPartyInfoActivity> , AnkoLogger {
    val MEDIUM_SIZE_TEXT = 18f
    val LABEL_WIDTH = 80
    val PHOTO_HORIZONTAL_MARGIN = 4
    val ROW_GAP = 10f
    val CAMERA_WIDTH = 80

    val defaultBackgroundResource = R.drawable.grey_bordered_round_btn

    lateinit var nameInput : EditText
    lateinit var phoneInput : EditText
    lateinit var companyInput : EditText
    lateinit var emailInput : EditText
    lateinit var nameInputBox : LinearLayout
    lateinit var phoneInputBox : LinearLayout
    lateinit var companyInputBox : LinearLayout
    lateinit var emailInputBox : LinearLayout

    lateinit var ddlPhotoBtn : ImageView
    lateinit var insurancePhotoBtn :  ImageView
    lateinit var ddlPhotoDisplay : ImageView
    lateinit var insurancePhotoDisplay: ImageView
    lateinit var ddlPhotoBox : LinearLayout
    lateinit var insurancePhotoBox : LinearLayout

    lateinit var vehicleDmgBox : LinearLayout
    lateinit var damagePhotoBtn : ImageView

    lateinit var vADisplay: ImageView
    lateinit var vBDisplay: ImageView
    lateinit var vCDisplay: ImageView
    lateinit var vDDisplay: ImageView
    lateinit var vEDisplay: ImageView
    lateinit var vFDisplay: ImageView
    lateinit var vGDisplay: ImageView
    lateinit var vHDisplay: ImageView
    lateinit var vIDisplay: ImageView
    lateinit var vJDisplay: ImageView

    lateinit var nextBtn : Button

    override fun createView(ui: AnkoContext<AccidentTheOtherPartyInfoActivity>): View = with(ui){
        verticalLayout{
            verticalLayout {
                backgroundColor = Color.WHITE
                textView("Step 2: Information of the other party"){
                    textColor = R.color.colorPrimary
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    verticalMargin = dimen(R.dimen.activity_vertical_margin)
                    horizontalMargin = dimen(R.dimen.activity_horizontal_margin)
                }
            }

            linearLayout(){
                weightSum = 3f

                verticalLayout{
                    backgroundResource = R.drawable.purple_header
                    lparams(height = dip(5)){
                        weight = 2f
                    }
                }
            }

            scrollView {
                verticalLayout {
                    lparams(width = matchParent){
                        horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                        verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    }

                    textView("Contact Information"){
                        textSize = MEDIUM_SIZE_TEXT
                        textColor = resources.getColor(R.color.colorPrimary)
                        typeface = Typeface.DEFAULT_BOLD
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.colorPrimary)
                    }.lparams(width = matchParent, height = dip(1)){
                        bottomMargin = dimen(R.dimen.list_item_vertical_padding)
                    }


                    nameInputBox = linearLayout() {
                        lparams(width = matchParent) {
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                        backgroundResource = defaultBackgroundResource

                        textView("Name"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        nameInput = editText {
                            hint = "Please enter name."
                            textSize = MEDIUM_SIZE_TEXT
                            inputType = InputType.TYPE_CLASS_TEXT
                            maxLines = 1
                            backgroundColor = Color.TRANSPARENT
                        }.lparams(width = matchParent, height = wrapContent)
                    }

                    phoneInputBox = linearLayout() {
                        lparams(width = matchParent) {
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                        backgroundResource = defaultBackgroundResource

                        textView("Phone"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        phoneInput = editText {
                            hint = "Please enter phone number."
                            textSize = MEDIUM_SIZE_TEXT
                            inputType = InputType.TYPE_CLASS_NUMBER
                            backgroundColor = Color.TRANSPARENT
                        }.lparams(width = matchParent, height = wrapContent)
                    }

                    companyInputBox = linearLayout {
                        lparams(width = matchParent) {
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                        backgroundResource = defaultBackgroundResource

                        textView("Company"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        companyInput = editText {
                            hint = "Please enter company name."
                            textSize = MEDIUM_SIZE_TEXT
                            inputType = InputType.TYPE_CLASS_TEXT
                            maxLines = 1
                            backgroundColor = Color.TRANSPARENT
                        }.lparams(width = matchParent, height = wrapContent)
                    }

                    emailInputBox = linearLayout {
                        lparams(width = matchParent) {
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                        backgroundResource = defaultBackgroundResource

                        textView("Email"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))

                        emailInput = editText {
                            hint = "Please enter email."
                            textSize = MEDIUM_SIZE_TEXT
                            inputType = InputType.TYPE_CLASS_TEXT
                            maxLines = 1
                            backgroundColor = Color.TRANSPARENT
                        }.lparams(width = matchParent, height = wrapContent)
                    }

                    /* --- Qualified to Drive --- */
                    textView("Qualified to Drive"){
                        textSize = MEDIUM_SIZE_TEXT
                        textColor = resources.getColor(R.color.colorPrimary)
                        typeface = Typeface.DEFAULT_BOLD
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        topMargin = dimen(R.dimen.activity_vertical_margin)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.colorPrimary)
                    }.lparams(width = matchParent, height = dip(1)){
                        bottomMargin = dimen(R.dimen.list_item_vertical_padding)
                    }

                    linearLayout {
                        ddlPhotoBox = verticalLayout(){
                            lparams(width = matchParent, height = matchParent){
                                weight = 1f
                                rightMargin = dip(ROW_GAP/2)
                            }

                            backgroundResource = defaultBackgroundResource
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            gravity = Gravity.CENTER

                            ddlPhotoBtn = imageView(R.drawable.add_photo){

                            }.lparams(width = dip(CAMERA_WIDTH), height = dip(CAMERA_WIDTH))

                            ddlPhotoDisplay = imageView{
                                visibility = View.GONE
                                adjustViewBounds = true
                                imageResource = R.drawable.example_avatar
                            }

                            textView("Driving License"){
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(){
                                gravity = Gravity.CENTER
                                topMargin = dip(10)
                            }
                        }

                        insurancePhotoBox = verticalLayout(){
                            lparams(width = matchParent, height = matchParent){
                                weight = 1f
                                leftMargin = dip(ROW_GAP/2)
                            }

                            backgroundResource = defaultBackgroundResource
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            gravity = Gravity.CENTER

                            insurancePhotoBtn = imageView(R.drawable.add_photo){

                            }.lparams(width = dip(CAMERA_WIDTH), height = dip(CAMERA_WIDTH))

                            insurancePhotoDisplay = imageView{
                                visibility = View.GONE
                                adjustViewBounds = true
                                imageResource = R.drawable.example_avatar
                            }

                            textView("Insurance Document"){
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(){
                                gravity = Gravity.CENTER
                                topMargin = dip(10)
                            }
                        }
                    }

                    /* --- Damage photo --- */
                    textView("Photos of Vehicle Damage"){
                        textSize = MEDIUM_SIZE_TEXT
                        textColor = resources.getColor(R.color.colorPrimary)
                        typeface = Typeface.DEFAULT_BOLD
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        topMargin = dimen(R.dimen.activity_vertical_margin)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.colorPrimary)
                    }.lparams(width = matchParent, height = dip(1)){
                        bottomMargin = dimen(R.dimen.list_item_vertical_padding)
                    }

                    vehicleDmgBox = verticalLayout {
                        lparams(width = matchParent)

                        backgroundResource = defaultBackgroundResource
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                        linearLayout(){
                            damagePhotoBtn = imageView() {
                                imageResource = R.drawable.add_photo
                            }.lparams(width = dip(60), height = dip(60)){
                                rightMargin = dip(PHOTO_HORIZONTAL_MARGIN )
                            }

                            vADisplay = imageView{
                                visibility = View.GONE
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                rightMargin = dip(PHOTO_HORIZONTAL_MARGIN )
                            }

                            vBDisplay = imageView{
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN )
                            }

                            vCDisplay = imageView{
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                            }

                            vDDisplay = imageView{
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                            }

                            vEDisplay = imageView{
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                leftMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                            }
                        }

                        linearLayout(){
                            visibility = View.GONE

                            vFDisplay = imageView{
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                            }

                            vGDisplay = imageView{
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                            }

                            vHDisplay = imageView{
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                            }

                            vIDisplay = imageView{
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                            }

                            vJDisplay = imageView{
                                imageResource = R.drawable.dashed_square
                            }.lparams(width = dip(60), height = dip(60)){
                                leftMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                            }
                        }
                    }

                    /* --- Submit Button --- */
                    nextBtn = button("Next"){
                        backgroundResource = R.drawable.green_rounded_button
                        textSize = MEDIUM_SIZE_TEXT
                        textColor = Color.WHITE
                    }.lparams(width = matchParent){
                        topMargin = dimen(R.dimen.list_item_vertical_padding)
                    }

                }
            }
        }
    }

}