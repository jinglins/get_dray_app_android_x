package com.dray.dray.workflow.newLeg.select.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.View
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.newLeg.select.SelectNewLegActivity
import org.jetbrains.anko.*

class FreeflowImportItemComponent : AnkoComponent<SelectNewLegActivity>, AnkoLogger {
    val LABEL_WIDTH = 90
    lateinit var freeflowCode : TextView
    lateinit var equipmentNeeded : TextView
    lateinit var originTerminal : TextView
    lateinit var lv : ListView

    lateinit var rejectBtn : Button
    lateinit var acceptBtn : Button

    override fun createView(ui: AnkoContext<SelectNewLegActivity>): View = with(ui){
        verticalLayout {
            // Experiment wrapper
            verticalPadding = dimen(R.dimen.activity_vertical_padding)
            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)

            verticalLayout{
                themedLinearLayout (R.style.ThemeOverlay_AppCompat_Dark) {
                    backgroundResource = R.drawable.purple_header
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                    verticalLayout {
                        lparams(){
                            weight = 1f
                        }
                        textView("IMPORT")
                        textView("Free-flow"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    freeflowCode = textView("CMABN"){
                        textColor = Color.WHITE
                        textSize = 30f
                    }
                }

                verticalLayout{
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                    lparams (width = matchParent){
                        bottomMargin = dip(1)
                    }

                    linearLayout{
                        textView("Equipment"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))
                        equipmentNeeded = textView("Chassis")
                    }
                    linearLayout{
                        textView("Origin") {
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(LABEL_WIDTH))
                        originTerminal = textView("EAGLE MARINE")
                    }
                }

                verticalLayout{
                    lparams (width = matchParent){
                        topMargin = dip(1)
                    }
                    // Change to listview with adapter
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)


                    linearLayout{
                        lparams (width = matchParent, height = wrapContent){
                            bottomMargin = dip(5)
                        }

                        textView("Destination"){
                            textColor = R.color.colorPrimary
                        }.lparams(){
                            weight = 1f
                        }
                        textView("Leg Rate"){
                            textColor = R.color.colorPrimary
                        }
                    }

                    lv = listView(){
                        // init adapter in Activity
                        divider = null
                    }
                }

                linearLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                    lparams(width = matchParent){
                        topMargin = dip(2)
                    }

                    rejectBtn = button("Reject"){
                        backgroundResource = R.drawable.left_rounded_red_button
                        textColor = Color.WHITE
                        textSize = 18f
                    }.lparams(){
                        weight = 1f
                        rightMargin = dip(10)
                    }

                    acceptBtn = button("Accept"){
                        backgroundResource = R.drawable.right_rounded_button
                        textColor = Color.WHITE
                        textSize = 18f
                    }.lparams{
                        weight = 1f
                        leftMargin = dip(10)
                    }
                }
            }
        }
    }

}