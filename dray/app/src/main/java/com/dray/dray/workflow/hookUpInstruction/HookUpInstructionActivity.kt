package com.dray.dray.workflow.hookUpInstruction

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.workflow.outgate.customer.CustomerOutgateActivity

class HookUpInstructionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hook_up_instruction)
        val exitStatus = intent.getStringExtra("EXIT_STATUS")
        //val atExportFacility = !(intent.getStringExtra("EXPORT_FACILITY") == "false")

        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Proceed to Customer Gate")

        val reportBtn = findViewById<LinearLayout>(R.id.report_button).findViewById<Button>(R.id.report_btn)
        reportBtn.setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }

        val nextBtn = findViewById<Button>(R.id.nextBtn)
        nextBtn.setOnClickListener {
            //if (atExportFacility){
                //exportCustomerOutgateActivity, EXIT_STATUS = "load"

           // } else {
                val i = Intent(this, CustomerOutgateActivity::class.java)
                i.putExtra("EXIT_STATUS", exitStatus)
                startActivity(i)
            //}
        }
    }
}
