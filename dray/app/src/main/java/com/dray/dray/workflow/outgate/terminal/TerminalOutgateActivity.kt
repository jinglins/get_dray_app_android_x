package com.dray.dray.workflow.outgate.terminal

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.workflow.outgate.terminal.ui.TerminalOutgateExportComponent
import com.dray.dray.workflow.outgate.terminal.ui.TerminalOutgateFreeflowImportComponent
import com.dray.dray.workflow.outgate.terminal.ui.TerminalOutgateRegularImportComponent
import com.dray.dray.workflow.PhotoDocument.PhotoDocumentActivity
import org.jetbrains.anko.setContentView

/* Take photo code refer to: https://developer.android.com/training/camera/photobasics*/

class TerminalOutgateActivity : AppCompatActivity() {
    val CAMERA_REQUEST_CODE = 133

    private val regularImportUI = TerminalOutgateRegularImportComponent()
    private val freeflowImportUI = TerminalOutgateFreeflowImportComponent()
    private val exportUI = TerminalOutgateExportComponent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Terminal Out-Gate")

        val db = getDatabase(applicationContext)

        val firstLegInfo = db.legDao().getLegInfoById(1)
        if (firstLegInfo.legType == "IMPORT") {
            if (firstLegInfo.flags.contains("FREE_FLOW")){
                freeflowImportUI.setContentView(this)
                freeflowImportUI.freeflowCode.text = db.legDao().getLegInfoById(1).freeFlowCode
                freeflowImportUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                    displayReportWindow(applicationContext, "false", "false")
                }
                freeflowImportUI.nextBtn.setOnClickListener {
                    goToNextActivity()
                }
            } else {
                regularImportUI.setContentView(this)
                regularImportUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                    displayReportWindow(applicationContext, "false", "false")
                }

                regularImportUI.containerNo.text = db.legDao().getLegContainerInfo(1).containerNumber
                regularImportUI.bolNo.text = db.legDao().getLegInfoById(1).bolBookingNumber
                regularImportUI.deliverTo.text = db.legDao().getLegDeliverToInfo(1).destinationName
                regularImportUI.containerSize.text = db.legDao().getLegContainerInfo(1).containerSize

                regularImportUI.nextBtn.setOnClickListener {
                    goToNextActivity()
                }
            }
        } else {
            exportUI.setContentView(this)
            exportUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                displayReportWindow(applicationContext, "false", "false")
            }

            exportUI.bookingNo.text = db.legDao().getLegInfoById(1).bolBookingNumber
            exportUI.deliverTo.text = db.legDao().getLegDeliverToInfo(1).destinationName
            exportUI.containerSize.text = db.legDao().getLegContainerInfo(1).containerSize

            exportUI.nextBtn.setOnClickListener {
                goToNextActivity()
            }
        }
    }

    private fun goToNextActivity () {
        val i = Intent(this, PhotoDocumentActivity::class.java)
        i.putExtra("CUR_PLACE", "terminal")
        startActivity(i)
    }
}
