package com.dray.dray

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.dray.dray.feedbackActivity.FeedbackActivity
import com.dray.dray.helpActivities.HelpActivity
import com.dray.dray.mainActivity.ui.MainActivityComponent
import com.dray.dray.paymentActivity.PaymentActivity
import com.dray.dray.profileActivity.ProfileActivity
import com.dray.dray.settingsActivity.SettingsActivity
import com.dray.dray.workflow.newLeg.select.SelectNewLegActivity

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.sdk15.listeners.onClick
import org.jetbrains.anko.setContentView
import java.text.SimpleDateFormat
import java.util.*
import android.content.pm.PackageManager
import android.view.View
import com.dray.dray.adapter.assignedLoadsAdapter
import com.dray.dray.customClasses.AssignedLoadItem
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.dataClasses.profile.*
import com.dray.dray.database.AppDatabase
import com.dray.dray.onboardActivity.OnboardPhoneActivity
import com.dray.dray.reportProblemsActivities.AccidentDiagramActivity
import com.dray.dray.reportProblemsActivities.AccidentPhotoDamageActivity
import com.dray.dray.reportProblemsActivities.AccidentTheOtherPartyInfoActivity
import com.dray.dray.workflow.inputChassisInfo.ChassisInfoInputActivity
import com.dray.dray.workflow.mapActivity.MapActivity
import kotlin.collections.ArrayList


open class MainActivity : AppCompatActivity(), com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener, AnkoLogger {

    val ui = MainActivityComponent()
    lateinit var drawer: androidx.drawerlayout.widget.DrawerLayout
    lateinit var db : AppDatabase
    lateinit var mAdapter : assignedLoadsAdapter

    val REQUEST_READ_PHONE_STATE = 131

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /* ----------------------------- Set UI Frame----------------------------- */
        ui.setContentView(this)
        drawer = ui.drawer
        setSupportActionBar(ui.toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false);

        val toggle = ActionBarDrawerToggle(
                this, drawer, ui.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        db = getDatabase(applicationContext)
        injectProfileData()
        /* --------------------- Set IMEI and Check Onboard ---------------------- */
        // val phoneIMEI = getPhoneInfo()
        //val isOnboard = checkOnboardStatus(phoneIMEI)

        val isOnboard = true //temporary for emulator
        if (isOnboard) {
            //checkJobAssignmentStatus (phoneIMEI)
            checkJobAssignmentStatus()
        } else {
            val i = Intent (this, OnboardPhoneActivity::class.java)
            startActivity(i)
        }

        /* -------------------------- Render UI Content --------------------------- */
        // TODO: Make API call to database, use the result
        val infoInjected = intent.getStringExtra("INFO_INJECTED")
        println("info injected: $infoInjected")
        if (infoInjected == "yes") {
            var assignedLoadsArray = ArrayList<AssignedLoadItem> ()
            val containerInfo = db.legDao().getLegContainerInfo(1)
            assignedLoadsArray.add(AssignedLoadItem(db.legDao().getLegPickUpInfoById(1).originName,
                    db.legDao().getLegDeliverToInfo(1).destinationName,
                    containerInfo.containerNumber, db.legDao().getLegInfoById(1).bolBookingNumber,
                    db.legDao().getLegInfoById(1).flags.contains("FREE_FLOW"),
                    db.legDao().getLegInfoById(1).freeFlowCode,
                    containerInfo.containerSize, containerInfo.containerWeight,
                    db.legDao().getLegPickUpInfoById(1).apptTime))
            mAdapter = assignedLoadsAdapter(this, assignedLoadsArray)
            ui.lv.adapter = mAdapter
        } else {
            ui.noLoadPlaceholder.visibility = View.VISIBLE
            ui.selectLoadBtn.visibility = View.VISIBLE
            ui.loadListView.visibility = View.GONE
            ui.startNowBtn.visibility = View.GONE
        }

        ui.selectLoadBtn.onClick {
            //displayReportWindow(applicationContext)

            val i = Intent(this, SelectNewLegActivity::class.java)
            startActivity(i)
        }

        ui.startNowBtn.onClick {
            val i = Intent(this, MapActivity::class.java)

            val firstLegPickUpFrom = db.legDao().getLegPickUpInfoById(1).originName
            val chassisType = db.legDao().getLegEquipmentInfoById(1).equipmentType
            if (firstLegPickUpFrom == "YARD") {
                i.putExtra("TARGET", "CUSTOMER")
            } else {
                if (chassisType == "PRIVATE") {
                    i.putExtra("TARGET", "PRIVATE_CHASSIS")
                } else {
                    i.putExtra("TARGET", "TERMINAL")
                    i.putExtra("WITH_CHASSIS", "false")
                }
            }
            startActivity(i)
        }

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_signUp -> {
                val intent = Intent(this, OnboardPhoneActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.nav_profile -> {
                val intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.nav_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.nav_payment -> {
                val intent = Intent(this, PaymentActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.nav_help -> {
                val intent = Intent(this, HelpActivity::class.java)
                startActivity(intent)
                return true
            }

            R.id.nav_feedback -> {
                val intent = Intent(this, FeedbackActivity::class.java)
                startActivity(intent)
                return true
            }
        }

        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun injectProfileData () {

        val basicInfo = ProfileBasicInfo(1, "1234","Susie", "Ro",
                "6501234567", "")
        val qualifiedToDrive = ProfileQualifiedToDrive(1, "01/02/2018",
                "12/03/2018", "01/04/2019", "06/05/2019",
                "11/06/2018", "11/30/2018", "123242",
                true, 2)
        val workPreference = ProfileWorkPreference (1,"Day", "Long")
        val tmcInfo = ProfileTMC(1, "Harbor Express Inc", "HRBR")
        val carrierInfo = ProfileCarrier(1, "CMW1032", "RE1320",
                "12003.45", "MCVNIO3W")

        if (db.profileDao().getProfileBasicInfoById(1)== null) {
            db.profileDao().insertProfileBasicInfo(basicInfo)
        } else {
            //db.userDao().updateProfileBasicInfo(basicInfo)
        }

        if (db.profileDao().getProfileQualifiedToDriveById(1)== null) {
            db.profileDao().insertProfileQualifiedToDrive(qualifiedToDrive)
        } else {
            //db.userDao().updateProfileQualifiedToDrive(qualifiedToDrive)
        }

        if (db.profileDao().getProfileWorkPreferenceById(1)== null) {
            db.profileDao().insertProfileWorkPreference(workPreference)
        } else {
            //db.userDao().updateProfileWorkPreference(workPreference)
        }

        if (db.profileDao().getProfileTMCInfoById(1)== null) {
            db.profileDao().insertProfileTMCInfo(tmcInfo)
        } else {
            //db.userDao().updateProfileTMCInfo(tmcInfo)
        }

        if (db.profileDao().getProfileCarrierInfoById(1)== null) {
            db.profileDao().insertProfileCarrierInfo(carrierInfo)
        } else {
            //db.userDao().updateProfileCarrierInfo(carrierInfo)
        }

    }

    /*private fun getPhoneInfo () : String{
        lateinit var phoneNumber : String
        lateinit var phoneIMEI : String

        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_PHONE_STATE),
                    REQUEST_READ_PHONE_STATE)
        } else {
            val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            phoneNumber = tm.line1Number
            phoneIMEI = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                tm.imei
            } else {
                tm.deviceId
            }

            println("IMEI: $phoneIMEI")
            println("PHONE NUMBER: $phoneNumber")
        }

        return phoneIMEI
    }*/

    private fun checkOnboardStatus (phoneIMEI: String) : Boolean {
        /*if (onboard){
             return true
        } else {
             val i = intent (this, OnboardPhoneActivity::class.java)
             startActivity(i)
             return false
        }*/
        return true
    }

    private fun checkJobAssignmentStatus (/*phoneIMEI: String*/){
        /*if (hasLoad){
            val firstLoadTime = ...
            renderCountdownTimer()
            ui.startNowBtn.visibility = View.VISIBLE
            ui.selectLoadBtn.visibility = View.GONE
            ui.noLoadPlaceholder.visibility = View.GONE
            ui.loadListView.visibility = View.VISIBLE

            injectData()
        } else {
            ui.selectLoadBtn.visibility = View.VISIBLE
            ui.startNowBtn.visibility = View.GONE
            ui.noLoadPlaceholder.visibility = View.VISIBLE
            ui.loadListView.visibility = View.GONE
            ui.hoursLeft.text = "00"
            ui.minutesLeft.text = "00"
        }*/

        renderCountdownTimer("11/8/2018 23:45:00")
        ui.startNowBtn.visibility = View.VISIBLE
        ui.selectLoadBtn.visibility = View.GONE
        ui.noLoadPlaceholder.visibility = View.GONE
        ui.loadListView.visibility = View.VISIBLE
    }

    private fun renderCountdownTimer(firstLoadTime : String){
        // Get current time
        val curDate = Date()
        //HH converts hour in 24 hours format (0-23), day calculation
        val format = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
        val formattedFirstLoadTime: Date? = format.parse(firstLoadTime)

        //in milliseconds
        val diff = formattedFirstLoadTime!!.time - curDate.time

        val diffSeconds = diff / 1000 % 60
        val diffMinutes = diff / (60 * 1000) % 60
        val diffHours = diff / (60 * 60 * 1000) % 24
        val diffDays = diff / (24 * 60 * 60 * 1000)

        // Debug
        println(diffDays.toString() + " days, ")
        println(diffHours.toString() + " hours, ")
        println(diffMinutes.toString() + " minutes, ")
        println(diffSeconds.toString() + " seconds.")

        val calculateHours = diffDays * 24 + diffHours
        if (calculateHours <= 0.toLong() && diffMinutes <= 0.toLong()){
            ui.hoursLeft.text = "00"
            ui.minutesLeft.text = "00"
        } else {
            val displayHours = if (calculateHours > 9) calculateHours.toString() else ("0" + calculateHours.toString())
            val displayMinutes = if (diffMinutes > 9) diffMinutes.toString() else ("0" + diffMinutes.toString())
            ui.hoursLeft.text = displayHours
            ui.minutesLeft.text = displayMinutes

            // Count down interval 1 second (1000) * 60 seconds
            val countDownInterval:Long = 60000

            timer(diff,countDownInterval).start()
        }
    }

    private fun timer(millisInFuture:Long,countDownInterval:Long):CountDownTimer{
        return object: CountDownTimer(millisInFuture,countDownInterval){
            override fun onTick(millisUntilFinished: Long){
                //val timeRemaining = timeString(millisUntilFinished)
                //textTimer.text = timeRemaining
                val hoursRemaining = ((millisUntilFinished / (1000*60*60)) % 24)
                val minutesRemaining = ((millisUntilFinished / (1000*60)) % 60)
                println("millis : $millisUntilFinished")
                println("hours : $hoursRemaining")
                println("minutes : $minutesRemaining")

                ui.hoursLeft.text = hoursRemaining.toString()
                ui.minutesLeft.text = minutesRemaining.toString()
            }

            override fun onFinish() {

            }
        }
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_READ_PHONE_STATE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                println("permission granted")
            }
            else -> {
            }
        }
    }

    /* Alarm manager
    val manager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val alarmI = Intent(this@MainActivity, AlarmNotificationReceiver::class.java)
        val pendingI = PendingIntent.getBroadcast(applicationContext, 0, alarmI, 0)
        manager.set(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime() + 3000, pendingI)*/

    /*override fun onCreateOptionsMenu(menu: Menu): Boolean {
      // Inflate the menu; this adds items to the action bar if it is present.
      menuInflater.inflate(R.menu.menu_main, menu)
      return true
  }*/

    /*override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent orCustomerActivity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }*/

}
