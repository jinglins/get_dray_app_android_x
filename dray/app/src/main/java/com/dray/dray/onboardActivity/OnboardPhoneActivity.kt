package com.dray.dray.onboardActivity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.telephony.TelephonyManager
import com.dray.dray.onboardActivity.ui.OnboardPhoneComponent
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.setContentView
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan



class OnboardPhoneActivity : AppCompatActivity() , AnkoLogger{

    val ui = OnboardPhoneComponent()
    val REQUEST_READ_PHONE_STATE = 1
    lateinit var IMEI : String
    lateinit var phoneNumber : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE), REQUEST_READ_PHONE_STATE)

        } else {
            println("Get phone into")
            val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            phoneNumber = tm.line1Number
            IMEI = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                tm.imei
            } else {
                tm.deviceId
            }

            println("IMEI; $IMEI")
            println("PHONE NUMBER; $phoneNumber")
        }

        ui.verifyBtn.setOnClickListener{
            val phoneInput = ui.phoneInput.text
            val formattedPhoneInput = "+1" + phoneInput.toString()
            println("Phone input: $formattedPhoneInput")
            if (phoneNumber != formattedPhoneInput || formattedPhoneInput == ""){
                val errorString = "Please enter a correct phone number."  // Your custom error message.
                val foregroundColorSpan = ForegroundColorSpan(Color.WHITE)
                val spannableStringBuilder = SpannableStringBuilder(errorString)
                spannableStringBuilder.setSpan(foregroundColorSpan, 0, errorString.length, 0)
                ui.phoneInput.error = spannableStringBuilder

            } else {
                // send phone input to the backend

                val intent = Intent(this, OnboardConfirmActivity::class.java)
                startActivity(intent)
            }


        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_READ_PHONE_STATE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                println("permission granted")
            }

            else -> {
            }
        }
    }

}
