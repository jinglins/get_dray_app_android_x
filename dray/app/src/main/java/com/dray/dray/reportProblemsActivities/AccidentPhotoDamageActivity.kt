package com.dray.dray.reportProblemsActivities

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ui.AccidentPhotoDamageComponent
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast

class AccidentPhotoDamageActivity : AppCompatActivity() {

    val ui = AccidentPhotoDamageComponent()

    private val REQUEST_C_PHOTO = 133
    private val REQUEST_V_PHOTO = 135

    private var cCount = 0
    private var vCount = 0

    private var cDisplayArr = ArrayList<ImageView>()
    private var vDisplayArr = ArrayList<ImageView>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        cDisplayArr = arrayListOf<ImageView>(
                ui.cADisplay, ui.cBDisplay, ui.cCDisplay, ui.cDDisplay, ui.cEDisplay,
                ui.cFDisplay, ui.cGDisplay, ui.cHDisplay, ui.cIDisplay, ui.cJDisplay)

        vDisplayArr = arrayListOf<ImageView>(
                ui.vADisplay, ui.vBDisplay, ui.vCDisplay, ui.vDDisplay, ui.vEDisplay,
                ui.vFDisplay, ui.vGDisplay, ui.vHDisplay, ui.vIDisplay, ui.vJDisplay
        )

        cDisplayArr.forEach { it ->
            it.visibility = View.GONE
        }

        vDisplayArr.forEach { it ->
            it.visibility = View.GONE
        }

        ui.containerDmgPhotoBtn.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(packageManager)?.also {
                    startActivityForResult(takePictureIntent, REQUEST_C_PHOTO)
                }
            }
        }

        ui.vehicleDmgPhotoBtn.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(packageManager)?.also {
                    startActivityForResult(takePictureIntent, REQUEST_V_PHOTO)
                }
            }
        }

        ui.nextBtn.setOnClickListener {
            val i = Intent(this, AccidentTheOtherPartyInfoActivity::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_C_PHOTO && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            cCount ++

            if (cCount == 1) {
                ui.cDmgBox.backgroundResource = R.drawable.purple_bordered_round_btn
            }

            if (cCount < 10) {
                cDisplayArr[cCount].visibility = View.VISIBLE
                cDisplayArr[cCount].setImageBitmap(imageBitmap)
            } else {
                // Hide take photo button
                ui.containerDmgPhotoBtn.visibility = View.GONE
                cDisplayArr[0].setImageBitmap(imageBitmap)
                cDisplayArr[0].visibility = View.VISIBLE
                toast("You may take at most 10 photos.")
            }

            if (vCount > 0) {
                ui.nextBtn.visibility = View.VISIBLE
            }
        }

        if (requestCode == REQUEST_V_PHOTO && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            vCount ++

            if (vCount == 1) {
                ui.vDmgBox.backgroundResource = R.drawable.purple_bordered_round_btn
            }

            if (vCount < 10) {
                vDisplayArr[vCount].visibility = View.VISIBLE
                vDisplayArr[vCount].setImageBitmap(imageBitmap)
            } else {
                // Hide take photo button
                ui.vehicleDmgPhotoBtn.visibility = View.GONE
                vDisplayArr[0].setImageBitmap(imageBitmap)
                vDisplayArr[0].visibility = View.VISIBLE
                toast("You may take at most 10 photos.")
            }

            if (cCount > 0) {
                ui.nextBtn.visibility = View.VISIBLE
            }
        }
    }
}
