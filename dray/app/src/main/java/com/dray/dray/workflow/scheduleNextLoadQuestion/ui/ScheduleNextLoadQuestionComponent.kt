package com.dray.dray.workflow.scheduleNextLoadQuestion.ui

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.workflow.scheduleNextLoadQuestion.ScheduleNextLoadQuestionActivity
import org.jetbrains.anko.*

class ScheduleNextLoadQuestionComponent : AnkoComponent <ScheduleNextLoadQuestionActivity> , AnkoLogger{
    lateinit var reportBtn : LinearLayout
    lateinit var yesBtn : Button
    lateinit var noBtn : Button
    val MEDIUM_SIZE_TEXT = 18f
    val DATA_TEXT_SIZE = 30f

    override fun createView(ui: AnkoContext<ScheduleNextLoadQuestionActivity>): View = with (ui) {
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_horizontal_padding)
                horizontalPadding = dimen(R.dimen.activity_vertical_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout {
                backgroundColor = Color.WHITE
                gravity = Gravity.CENTER

                lparams(width = matchParent){
                    weight = 1f
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                }

                imageView(R.drawable.another_load){
                    adjustViewBounds = true
                }.lparams(width = dip(150), height =  dip(150))

                textView("Would you like another work after this turn is done? ") {
                    this.gravity = Gravity.CENTER
                    textSize = 24f
                    textColor = R.color.colorPrimary
                }.lparams(width = matchParent){
                    topMargin = dip(20)
                }
            }

            verticalLayout{
                yesBtn = button("Yes, I Want Another Load"){
                    backgroundResource = R.drawable.green_rounded_button
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = Color.WHITE
                }

                noBtn = button("No, Just Return This Container"){
                    backgroundResource = R.drawable.white_rounded_button
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = resources.getColor(R.color.defaultLightTxtColor)
                }.lparams(width = matchParent){
                    topMargin = dimen(R.dimen.item_normal_gap)
                }
            }.lparams(width = matchParent){
                weight = 0f
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}