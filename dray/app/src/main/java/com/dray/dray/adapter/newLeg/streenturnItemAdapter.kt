package com.dray.dray.adapter.newLeg

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.R
import com.dray.dray.customClasses.AssignedLoadItem
import com.dray.dray.workflow.newLeg.select.SelectNewLegActivity
import org.jetbrains.anko.*

class streetturnItemAdapter (val activity: SelectNewLegActivity,
                             var assignedLoadItems : ArrayList<AssignedLoadItem>) : BaseAdapter() {
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val LABEL_WIDTH = 90
        return with(p2!!.context) {
            verticalLayout{
                themedLinearLayout (R.style.ThemeOverlay_AppCompat_Dark) {
                    backgroundResource = R.drawable.purple_header
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                    textView("STREET-TURN"){
                        textColor = Color.WHITE
                        typeface = Typeface.DEFAULT_BOLD
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(width = matchParent)
                }

                themedLinearLayout (R.style.ThemeOverlay_AppCompat_Dark) {
                    backgroundColor = R.color.colorPrimary
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                    verticalLayout {
                        lparams(){
                            weight = 1f
                        }
                        textView("IMPORT")
                        textView("Drop-pull"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    textView("TCNU1234567"){
                        textColor = Color.WHITE
                        textSize = 30f
                    }
                }

                linearLayout{
                    verticalLayout {
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                        lparams{
                            weight = 1f
                            rightMargin = dip(1)
                        }

                        linearLayout {
                            textView("Origin") {
                                textColor = R.color.colorPrimary
                            }.lparams(width = dip(LABEL_WIDTH))
                            textView("EAGLE MARINE")
                        }

                        linearLayout {
                            textView("Destination") {
                                textColor = R.color.colorPrimary
                            }.lparams(width = dip(LABEL_WIDTH))
                            textView("CHINO")
                        }

                        linearLayout {
                            textView("Type/Size") {
                                textColor = R.color.colorPrimary
                            }.lparams(width = dip(LABEL_WIDTH))
                            textView("40HC")
                        }

                        linearLayout {
                            textView("Weight") {
                                textColor = R.color.colorPrimary
                            }.lparams(width = dip(LABEL_WIDTH))
                            textView("5433.12(lb)")
                        }

                        linearLayout {
                            textView("Equipment") {
                                textColor = R.color.colorPrimary
                            }.lparams(width = dip(LABEL_WIDTH))
                            textView("Chassis")
                        }

                    }

                    verticalLayout {
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        gravity = Gravity.CENTER_VERTICAL
                        lparams(height = matchParent){
                            leftMargin = dip(1)
                        }

                        textView("LEG RATE"){
                            textColor = R.color.colorPrimary
                            textAlignment = View.TEXT_ALIGNMENT_CENTER
                            textSize = 10f
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(width = matchParent){
                            bottomMargin = dip(5)
                        }

                        verticalLayout{
                            textView("$503.92"){
                                textSize = 24f
                                textColor = Color.BLACK
                                typeface = Typeface.DEFAULT_BOLD
                                textAlignment = View.TEXT_ALIGNMENT_CENTER
                            }
                            textView("+ $54.23 bonus"){
                                textColor = resources.getColor(R.color.colorRed)
                                textAlignment = View.TEXT_ALIGNMENT_CENTER
                            }
                        }
                    }

                }

                themedLinearLayout (R.style.ThemeOverlay_AppCompat_Dark) {
                    backgroundColor = R.color.colorPrimary
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                    verticalLayout {
                        lparams(){
                            weight = 1f
                        }
                        textView("EXPORT")
                        textView("Live-load"){
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    textView("8PHL056586"){
                        textColor = Color.WHITE
                        textSize = 30f
                    }
                }

                linearLayout{
                    verticalLayout {
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                        lparams{
                            weight = 1f
                            rightMargin = dip(1)
                        }

                        linearLayout {
                            textView("Origin") {
                                textColor = R.color.colorPrimary
                            }.lparams(width = dip(LABEL_WIDTH))
                            textView("EAGLE MARINE")
                        }

                        linearLayout {
                            textView("Destination") {
                                textColor = R.color.colorPrimary
                            }.lparams(width = dip(LABEL_WIDTH))
                            textView("ONTARIO")
                        }

                        linearLayout {
                            textView("Type/Size") {
                                textColor = R.color.colorPrimary
                            }.lparams(width = dip(LABEL_WIDTH))
                            textView("40HC")
                        }

                        linearLayout {
                            textView("Equipment") {
                                textColor = R.color.colorPrimary
                            }.lparams(width = dip(LABEL_WIDTH))
                            textView("Chassis")
                        }

                    }

                    verticalLayout {
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        gravity = Gravity.CENTER_VERTICAL
                        lparams(height = matchParent){
                            leftMargin = dip(1)
                        }

                        verticalLayout{
                            textView("LEG RATE"){
                                textColor = R.color.colorPrimary
                                textAlignment = View.TEXT_ALIGNMENT_CENTER
                                textSize = 10f
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(width = matchParent){
                                bottomMargin = dip(5)
                            }
                            textView("$203.10"){
                                textSize = 24f
                                textColor = Color.BLACK
                                typeface = Typeface.DEFAULT_BOLD
                                textAlignment = View.TEXT_ALIGNMENT_CENTER
                            }
                            textView("+ $9.13 bonus"){
                                textColor = resources.getColor(R.color.colorRed)
                                textAlignment = View.TEXT_ALIGNMENT_CENTER
                            }
                        }
                    }

                }

                linearLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                    lparams(width = matchParent){
                        topMargin = dip(2)
                    }
                    button("Reject"){
                        backgroundResource = R.drawable.left_rounded_red_button
                        textColor = Color.WHITE
                        textSize = 18f
                    }.lparams(){
                        weight = 1f
                        rightMargin = dip(10)
                    }
                    button("Accept"){
                        backgroundResource = R.drawable.right_rounded_button
                        textColor = Color.WHITE
                        textSize = 18f
                    }.lparams{
                        weight = 1f
                        leftMargin = dip(10)
                    }
                }
            }
        }

    }

    override fun getItem(p0: Int): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}