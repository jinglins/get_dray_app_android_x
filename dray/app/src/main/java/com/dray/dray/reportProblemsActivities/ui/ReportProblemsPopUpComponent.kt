package com.dray.dray.reportProblemsActivities.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.*
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ReportProblemsPopUpActivity
import org.jetbrains.anko.*
class ReportProblemsPopUpComponent : AnkoComponent<ReportProblemsPopUpActivity>, AnkoLogger {
    lateinit var defaultGV : GridView
    lateinit var inTransitGV :GridView
    lateinit var facilityGV : GridView
    lateinit var equipmentGV : GridView
    lateinit var scheduleGV : GridView

    lateinit var waitLayout : LinearLayout

    val TIMER_LABEL_SIZE = 16f
    val TIMER_NUMBER_SIZE = 40f
    val MEDIUM_SIZE_TEXT = 18f

    lateinit var echoCategory : TextView

    lateinit var hrsTxt : TextView
    lateinit var minTxt : TextView
    lateinit var secTxt : TextView

    lateinit var submitWaitReportBtn : Button

    override fun createView(ui: AnkoContext<ReportProblemsPopUpActivity>): View = with(ui){
        verticalLayout(){
            lparams(width = matchParent, height = matchParent)
            verticalLayout(R.style.ThemeOverlay_AppCompat_Dark){
                lparams(width = matchParent, height = dip(100))

                gravity = Gravity.CENTER

                backgroundResource = R.drawable.purple_header
                textView("Hi! How can I help you?") {
                    textSize = 24f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    this.gravity = Gravity.CENTER
                }

                echoCategory = textView("I want to report ... ").lparams(){
                    this.gravity = Gravity.CENTER
                    topMargin = dip(10)
                }
            }

            waitLayout = verticalLayout {
                visibility = View.GONE
                gravity = Gravity.CENTER_VERTICAL
                backgroundColor = Color.WHITE

                lparams (width = matchParent, height = matchParent) {
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                }

                textView("My Waiting Time"){
                    typeface = Typeface.DEFAULT_BOLD
                    textSize = 18f
                }.lparams(){
                    gravity = Gravity.CENTER
                    bottomMargin = dip(10)
                }

                textView("This timer auto records the waiting time for you.\nIt stops once " +
                        "you choose to submit this report."){
                    textSize = 8f
                }.lparams(){
                    gravity = Gravity.CENTER_HORIZONTAL
                }

                linearLayout {
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                    backgroundColor = Color.WHITE
                    verticalLayout {
                        lparams(width = matchParent){
                            weight = 1f
                        }
                        hrsTxt = textView("00"){
                            textSize = TIMER_NUMBER_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                        textView("HRS"){
                            textSize = TIMER_LABEL_SIZE
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                    }

                    verticalLayout{
                        lparams(height = matchParent)
                        gravity = Gravity.CENTER_VERTICAL
                        textView(":"){
                            textSize = TIMER_LABEL_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    verticalLayout{
                        lparams(width = matchParent){
                            weight = 1f
                        }
                        minTxt = textView("00"){
                            textSize = TIMER_NUMBER_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                        textView("MIN"){
                            textSize = TIMER_LABEL_SIZE
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                    }

                    verticalLayout{
                        lparams(height = matchParent)
                        gravity = Gravity.CENTER_VERTICAL
                        textView(":"){
                            textSize = TIMER_LABEL_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                    verticalLayout {
                        lparams(width = matchParent){
                            weight = 1f
                        }
                        secTxt = textView("00"){
                            textSize = TIMER_NUMBER_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                        textView("SEC"){
                            textSize = TIMER_LABEL_SIZE
                        }.lparams(){
                            this.gravity = Gravity.CENTER
                        }
                    }
                }

                submitWaitReportBtn = button("Submit the Report"){
                    backgroundResource = R.drawable.green_rounded_button
                    textColor = Color.WHITE
                    textSize = MEDIUM_SIZE_TEXT
                }.lparams(width = matchParent){
                    topMargin = dip(20)
                }
            }

            defaultGV = gridView {
                visibility = View.GONE
                numColumns = 3
                gravity = Gravity.CENTER
            }.lparams(width = matchParent, height = matchParent){
                verticalMargin = dimen(R.dimen.activity_vertical_margin)
                horizontalMargin = dimen(R.dimen.activity_horizontal_margin)
            }

            inTransitGV = gridView {
                visibility = View.GONE
                numColumns = 3
                gravity = Gravity.CENTER
            }.lparams(width = matchParent, height = matchParent){
                verticalMargin = dimen(R.dimen.activity_vertical_margin)
                horizontalMargin = dimen(R.dimen.activity_horizontal_margin)
            }

            facilityGV= gridView {
                visibility = View.GONE
                numColumns = 3
                gravity = Gravity.CENTER
            }.lparams(width = matchParent, height = matchParent){
                verticalMargin = dimen(R.dimen.activity_vertical_margin)
                horizontalMargin = dimen(R.dimen.activity_horizontal_margin)
            }

            equipmentGV= gridView {
                visibility = View.GONE
                numColumns = 3
                gravity = Gravity.CENTER
            }.lparams(width = matchParent, height = matchParent){
                verticalMargin = dimen(R.dimen.activity_vertical_margin)
                horizontalMargin = dimen(R.dimen.activity_horizontal_margin)
            }

            scheduleGV = gridView(){
                visibility = View.GONE
                numColumns = 3
                gravity = Gravity.CENTER
            }.lparams(width = matchParent, height = matchParent){
                verticalMargin = dimen(R.dimen.activity_vertical_margin)
                horizontalMargin = dimen(R.dimen.activity_horizontal_margin)
            }
        }
    }

}