package com.dray.dray.dataClasses.profile

import androidx.room.*

@Entity (tableName = "profile_work_preference")
data class ProfileWorkPreference (
    @PrimaryKey var id : Int,

    @ColumnInfo var shift : String ,        // "Day"/"Night"
    @ColumnInfo var distance : String       // "Long"/"Short"
)