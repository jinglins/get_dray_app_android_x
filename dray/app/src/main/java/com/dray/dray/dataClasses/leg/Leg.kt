package com.dray.dray.dataClasses.leg

import androidx.room.*

@Entity (tableName = "leg",
        foreignKeys = [
            ForeignKey(entity = LegContainerInfo::class,
                    parentColumns = ["id"],
                    childColumns = ["container_info_id"]),

            ForeignKey(entity = LegPickUpInfo::class,
                    parentColumns = ["id"],
                    childColumns = ["pick_up_info_id"]),

            ForeignKey(entity = LegDeliverToInfo::class,
                    parentColumns = ["id"],
                    childColumns = ["deliver_to_info_id"]),

            ForeignKey(entity = LegEquipmentInfo::class,
                    parentColumns = ["id"],
                    childColumns = ["equipment_info_id"]),

            ForeignKey(entity = LegRate::class,
                    parentColumns = ["id"],
                    childColumns = ["rate_id"])
        ])
data class Leg (
        @PrimaryKey var uid : Int,

        @ColumnInfo (name = "leg_type")           var legType : String,             // "import/export"
        @ColumnInfo (name = "receive_type")       var receiveType : String,
        @ColumnInfo (name = "flags")              var flags : String,
        @ColumnInfo (name = "bol_booking_no")     var bolBookingNumber : String,
        @ColumnInfo (name = "purchase_order_no")  var purchaseOrderNumber : String, // Only for export
        @ColumnInfo (name = "free_flow_code")     var freeFlowCode : String,        // Only for freeflow
        @ColumnInfo (name = "steamship_line")     var steamshipLine : String,
        @ColumnInfo (name = "container_info_id")  var containerInfoId : Int,
        @ColumnInfo (name = "pick_up_info_id")    var pickUpInfoId : Int,
        @ColumnInfo (name = "deliver_to_info_id") var deliverToInfoId : Int,
        @ColumnInfo (name = "equipment_info_id")  var equipmentInfoId : Int,
        @ColumnInfo (name = "rate_id")            var rateId : Int
)