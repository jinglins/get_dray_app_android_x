package com.dray.dray.workflow.selectTerminalOrCustomer.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.selectTerminalOrCustomer.SelectTerminalOrCustomerActivity
import org.jetbrains.anko.*

class SelectTerminalOrCustomerComponent : AnkoComponent<SelectTerminalOrCustomerActivity>, AnkoLogger {
    lateinit var reportBtn : LinearLayout
    lateinit var lv : ListView
    lateinit var questionTxt : TextView

    val MEDIUM_SIZE_TEXT = 18f
    val ROW_GAP = 10
    override fun createView(ui: AnkoContext<SelectTerminalOrCustomerActivity>): View = with (ui){
        verticalLayout(){
            lparams(width = matchParent, height = matchParent) {
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout {

                lparams(width = matchParent, height = matchParent)

                questionTxt = textView("Return To Which Terminal?"){
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = resources.getColor(R.color.colorPrimary)
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    this.gravity = Gravity.CENTER_HORIZONTAL
                }

                view(){
                    backgroundColor = resources.getColor(R.color.colorPrimary)
                }.lparams(width = dip(230), height = dip(1)){
                    bottomMargin = dip(ROW_GAP)
                    this.gravity = Gravity.CENTER_HORIZONTAL
                }

                lv = listView{
                    // init adapter in Activity
                    divider = null
                    backgroundColor = Color.TRANSPARENT
                    dividerHeight = 3*dimen(R.dimen.item_vertical_margin)
                }.lparams(width = matchParent, height = matchParent)
            }

        }
    }

}