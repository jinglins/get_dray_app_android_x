package com.dray.dray.paymentActivity

import android.app.DatePickerDialog
import android.graphics.Color
import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.adapter.paymentHistoryAdapter
import com.dray.dray.customClasses.PaymentHistoryItem
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk15.listeners.onClick
import org.jetbrains.anko.support.v4.UI
import java.time.LocalDateTime
import java.util.*

class HistoryPaymentFragmentActivity : androidx.fragment.app.Fragment() {

    lateinit var mAdapter : paymentHistoryAdapter
    lateinit var startDate : TextView
    lateinit var endDate : TextView

    var selectedStartDate = ""
    var selectedEndDate = ""

    val c = Calendar.getInstance()
    val cyear = c.get(Calendar.YEAR)
    val cmonth = c.get(Calendar.MONTH)
    val cday = c.get(Calendar.DAY_OF_MONTH)



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //return inflater.inflate(R.layout.fragment_payment_history, container, false)
        var historyPaymentItems = ArrayList<PaymentHistoryItem> ()
        historyPaymentItems.add(PaymentHistoryItem(3480.00, "12/23/2018 12:03:23", "End in 4455"))
        historyPaymentItems.add(PaymentHistoryItem(2030.20, "12/20/2018 12:04:32", "End in 4455"))
        historyPaymentItems.add(PaymentHistoryItem(1200.32, "12/17/2018 12:14:13", "End in 4455"))
        historyPaymentItems.add(PaymentHistoryItem(2643.89, "12/14/2018 12:56:26", "Check"))
        historyPaymentItems.add(PaymentHistoryItem(3242.94, "12/11/2018 12:19:12", "Check"))

        mAdapter = paymentHistoryAdapter(this, historyPaymentItems)

        return UI {
            verticalLayout{
                themedLinearLayout(R.style.ThemeOverlay_AppCompat_Dark){
                    backgroundResource = R.drawable.purple_header
                    lparams(width = matchParent, height = wrapContent){
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    }

                    verticalLayout{
                        lparams(height = wrapContent){
                            weight = 0.4f
                            this.gravity = Gravity.CENTER_VERTICAL
                            topPadding = dip(5)
                        }
                        textView("    START DATE"){
                            textSize = 8f
                        }
                        linearLayout(){
                            imageView(R.mipmap.ic_date_range)
                            val cMonthString = if ( (cmonth + 1) > 9 ) (cmonth+1).toString() else "0" + (cmonth+1).toString()
                            val fDate = cMonthString + "/01" + "/" + cyear.toString()
                            startDate = textView(fDate).lparams(){
                                this.gravity = Gravity.CENTER_VERTICAL
                            }
                        }
                    }.onClick {
                        val c = Calendar.getInstance()
                        val year = c.get(Calendar.YEAR)
                        val month = c.get(Calendar.MONTH)
                        val day = c.get(Calendar.DAY_OF_MONTH)


                        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                            // Display Selected date in textbox
                            val monthString = if ((monthOfYear + 1)> 9) (monthOfYear+1).toString() else "0" + (monthOfYear+1).toString()
                            val dayString = if (dayOfMonth > 9) dayOfMonth.toString() else "0" + dayOfMonth.toString()
                            val dateString = monthString + "/" + dayString + "/" + year.toString()
                            startDate.text = dateString
                            selectedStartDate = dateString
                        }, year, month, day)
                        dpd.show()
                    }

                    verticalLayout{
                        lparams(height = wrapContent){
                            weight = 0.4f
                            this.gravity = Gravity.CENTER_VERTICAL
                            topPadding = dip(5)
                        }
                        textView("    End Date"){
                            textSize = 8f
                        }
                        val cMonthString = if ( (cmonth + 1) > 9 ) (cmonth+1).toString() else "0" + (cmonth+1).toString()
                        val cDayString = if ( cday > 9) cday.toString() else "0" + cday.toString()
                        val cDate = cMonthString + "/" + cDayString + "/" + cyear.toString()
                        linearLayout(){
                            imageView(R.mipmap.ic_date_range)
                            endDate = textView(cDate).lparams(){
                                this.gravity = Gravity.CENTER_VERTICAL
                            }
                        }
                    }.onClick {
                        val c = Calendar.getInstance()
                        val year = c.get(Calendar.YEAR)
                        val month = c.get(Calendar.MONTH)
                        val day = c.get(Calendar.DAY_OF_MONTH)


                        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                            // Display Selected date in textbox
                            val monthString = if ((monthOfYear + 1)> 9) (monthOfYear+1).toString() else "0" + (monthOfYear+1).toString()
                            val dayString = if (dayOfMonth > 9) dayOfMonth.toString() else "0" + dayOfMonth.toString()
                            val dateString = monthString + "/" + dayString + "/" + year.toString()
                            endDate.text = dateString
                            selectedEndDate = dateString
                        }, year, month, day)
                        dpd.show()
                    }

                    verticalLayout{
                        lparams(height = wrapContent){
                            weight = 0.2f
                        }
                        button("Search"){
                            backgroundColor = R.color.colorPrimaryDark
                        }.lparams(){
                            this.gravity = Gravity.END
                            horizontalPadding = dip(0)
                        }.onClick {
                            // Check if the two dates are valid (end is after start)
                            // Ask for search results

                        }
                    }
                }

                listView(){
                    adapter = mAdapter
                    divider = null
                    backgroundColor = Color.TRANSPARENT
                    cacheColorHint = Color.TRANSPARENT
                    dividerHeight = dimen(R.dimen.item_vertical_margin)
                }.lparams(width = matchParent, height = matchParent){
                    horizontalMargin = dimen(R.dimen.activity_horizontal_padding)
                    verticalMargin = dimen(R.dimen.activity_vertical_padding)
                }
            }

        }.view
    }
}