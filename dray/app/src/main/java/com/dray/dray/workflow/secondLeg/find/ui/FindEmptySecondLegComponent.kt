package com.dray.dray.workflow.secondLeg.find.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.secondLeg.find.FindSecondLegActivity
import org.jetbrains.anko.*

class FindEmptySecondLegComponent : AnkoComponent<FindSecondLegActivity>, AnkoLogger{
    val MEDIUM_SIZE_TEXT = 18f
    val DATA_TEXT_SIZE = 24f // Happen to be the same as dispatch size
    val SMALL_GAP = 1
    lateinit var backBtn : Button
    lateinit var confirmBtn : Button
    lateinit var reportBtn : LinearLayout // Because Anko doesn't support drawable left for now

    lateinit var containerNumber : TextView
    lateinit var location : TextView
    lateinit var returnTerminal : TextView
    lateinit var steamshipline : TextView
    lateinit var size : TextView

    lateinit var flatRate : TextView
    lateinit var bonusRate : TextView
    lateinit var chassisRate : TextView
    lateinit var fuelRate : TextView
    lateinit var totalRate : TextView


    override fun createView(ui: AnkoContext<FindSecondLegActivity>): View = with(ui){
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            scrollView{
                verticalLayout {
                    linearLayout{
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dip(SMALL_GAP)
                        }

                        backgroundColor = Color.WHITE
                        textView("Container #"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(120)){
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                        containerNumber = textView("ABCD0000000"){
                            textSize = DATA_TEXT_SIZE
                            textColor = Color.BLACK
                        }
                    }

                    linearLayout{
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dip(SMALL_GAP)
                        }

                        backgroundColor = Color.WHITE
                        textView("Location"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(120)){
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                        location = textView("SA1"){
                            textSize = DATA_TEXT_SIZE
                            textColor = Color.BLACK
                        }
                    }

                    linearLayout{
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dip(SMALL_GAP)
                        }

                        backgroundColor = Color.WHITE
                        textView("Steamship Line"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(120)){
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                        steamshipline = textView("EXAMPLE"){
                            textSize = DATA_TEXT_SIZE
                            textColor = Color.BLACK
                        }
                    }

                    linearLayout{
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalMargin = dip(SMALL_GAP)
                        }

                        backgroundColor = Color.WHITE
                        textView("Type/Size"){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(120)){
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                        size = textView("40H"){
                            textSize = DATA_TEXT_SIZE
                            textColor = Color.BLACK
                        }
                    }
                    verticalLayout{
                        lparams(width = matchParent, height = matchParent){
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            topMargin = dip(SMALL_GAP)
                            bottomMargin = dimen(R.dimen.item_vertical_margin)
                        }

                        backgroundColor = Color.WHITE
                        textView("Return to "){
                            textColor = R.color.colorPrimary
                        }.lparams(width = dip(120)){
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                        returnTerminal = textView("TERMINAL, TERMINAL"){
                            textSize = DATA_TEXT_SIZE
                            textColor = Color.BLACK
                        }
                    }

                    verticalLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent){
                            topMargin = dimen(R.dimen.item_vertical_margin)
                            bottomMargin = dip(SMALL_GAP)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        }

                        linearLayout{
                            lparams(width = matchParent){
                                bottomMargin = dip(5)
                            }

                            textView("Flat Rate"){
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 16f
                            }.lparams{
                                weight = 1f
                            }
                            flatRate = textView("$0.00"){
                                textColor = Color.BLACK
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 16f
                            }.lparams{
                                weight = 0f
                            }
                        }
                        linearLayout{
                            textView("Fuel Rate"){
                                textColor = R.color.colorPrimary
                            }.lparams{
                                leftPadding = dip(20)
                                weight = 1f
                            }
                            fuelRate = textView("$0.00")
                        }
                        linearLayout{
                            textView("Bonus Rate"){
                                textColor = R.color.colorPrimary

                            }.lparams{
                                leftPadding = dip(20)
                                weight = 1f
                            }
                            bonusRate = textView("$0.00")
                        }
                        linearLayout{
                            textView("Chassis Rate"){
                                textColor = R.color.colorPrimary
                            }.lparams{
                                leftPadding = dip(20)
                                weight = 1f
                            }
                            chassisRate = textView("$0.00")
                        }

                    }
                    linearLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent){
                            verticalMargin = dip(SMALL_GAP)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        }
                        textView("Total Pay").lparams{
                            gravity = Gravity.CENTER_VERTICAL
                            weight = 1f
                        }

                        totalRate = textView("$400.00"){
                            textColor = Color.BLACK
                            textSize = 30f
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            weight = 0f
                        }
                    }
                }
            }.lparams(width = matchParent){
                weight = 1f
            }

            // Button Row
            linearLayout(){
                lparams(width = matchParent, height = wrapContent){
                    topMargin = dimen(R.dimen.activity_vertical_margin)
                    gravity = Gravity.BOTTOM
                    weight = 0f
                }

                backBtn = button("back"){
                    backgroundResource = R.drawable.left_rounded_button
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = resources.getColor(R.color.defaultLightTxtColor)
                }.lparams(){
                    weight = 0.5f
                    rightMargin = dimen(R.dimen.countdown_gap)
                }

                confirmBtn = button("confirm"){
                    backgroundResource = R.drawable.right_rounded_button
                    textColor = Color.WHITE
                    textSize = MEDIUM_SIZE_TEXT
                }.lparams(){
                    weight = 0.5f
                    leftMargin = dimen(R.dimen.countdown_gap)
                }
            }
        }
    }

}