package com.dray.dray.workflow.outgate.privateChassis.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.workflow.ingate.privateChassis.PrivateChassisIngateActivity
import org.jetbrains.anko.*
class PrivateChassisOutgateComponent : AnkoComponent<PrivateChassisIngateActivity>, AnkoLogger{
    lateinit var photoBtn : LinearLayout
    lateinit var nextBtn : Button
    lateinit var reportBtn : LinearLayout
    val MEDIUM_SIZE_TEXT = 18f
    override fun createView(ui: AnkoContext<PrivateChassisIngateActivity>): View = with(ui){
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout{
                backgroundResource = R.drawable.white_rounded_item
                lparams(width = matchParent){
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    weight = 1f
                }
                textView("Interchange Document"){
                    textColor = R.color.colorPrimary
                }.lparams(){
                    this.gravity = Gravity.CENTER_HORIZONTAL
                    bottomMargin = dimen(R.dimen.list_item_vertical_padding)
                }

                photoBtn = linearLayout{
                    this.gravity = Gravity.CENTER
                    backgroundResource = R.drawable.dashed_square
                    lparams(width = matchParent, height = matchParent){
                        verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    }
                    imageView(){
                        imageResource = R.mipmap.ic_add_a_photo
                    }.lparams(width = dip(60), height = dip(60))
                }
            }

            nextBtn = button("Submit"){
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}