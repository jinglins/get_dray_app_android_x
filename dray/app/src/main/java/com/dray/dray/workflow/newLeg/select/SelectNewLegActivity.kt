package com.dray.dray.workflow.newLeg.select

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.dray.dray.MainActivity
import com.dray.dray.R
import com.dray.dray.adapter.newLeg.selectFreeflowImportItemDestinationAdapter
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.newLeg.SelectFreeflowImportDestinationItem
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.dataClasses.leg.*
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.newLeg.confirm.ConfirmNewLegActivity
import com.dray.dray.workflow.newLeg.select.ui.DIYLegComponent
import com.dray.dray.workflow.newLeg.select.ui.FreeflowImportItemComponent
import org.jetbrains.anko.setContentView
import android.widget.RadioGroup



class SelectNewLegActivity : AppCompatActivity() {

    val ui = DIYLegComponent()

    lateinit var db : AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        val toolbar = supportActionBar!!
        setToolBar(toolbar, "Select A Leg")
        db = getDatabase(applicationContext)

        // curplace

        ui.legType.setOnClickListener {
            buildDialogue("legType")
        }

        ui.freeFlowFlag.setOnCheckedChangeListener { buttonView, isChecked ->
            if (ui.freeFlowFlag.isChecked) {
                ui.importContainerInfoBox.visibility = View.GONE
            } else {
                ui.importContainerInfoBox.visibility = View.VISIBLE
            }
        }

        ui.importReceiveType.setOnClickListener {
            buildDialogue("importReceiveType")
        }

        ui.exportReceiveType.setOnClickListener {
            buildDialogue("exportReceiveType")
        }

        ui.containerSize.setOnClickListener {
            buildDialogue("containerSize")
        }

        ui.exContainerSize.setOnClickListener {
            buildDialogue("exContainerSize")
        }

        ui.equipmentType.setOnClickListener {
            buildDialogue("equipmentType")
        }

        ui.reuseBtn.setOnClickListener {
            var i = Intent (this, ConfirmNewLegActivity::class.java)
            startActivity(i)
        }

        ui.confirmBtn.setOnClickListener {
            getUserInputs ()
            var i = Intent (this, ConfirmNewLegActivity::class.java)
            startActivity(i)
        }
    }

    private fun getUserInputs() {
        // ids of first leg are 1
        val equipmentInfo = LegEquipmentInfo(1, ui.equipmentName.text.toString(), ui.equipmentType.text.toString(),
                "", ui.releaseNumber.text.toString().toUpperCase(), ui.providerName.text.toString().toUpperCase(),
                "default provider addr")

        val equipmentInfoCopy = LegEquipmentInfo(2, ui.equipmentName.text.toString(), ui.equipmentType.text.toString(),
                "", ui.releaseNumber.text.toString().toUpperCase(), ui.providerName.text.toString().toUpperCase(),
                "default provider addr")

        when {
            ui.legType.text == "IMPORT" -> {
                val containerInfo = LegContainerInfo(1, ui.containerNumber.text.toString().toUpperCase(), "REGULAR",
                        ui.containerSize.text.toString(), ui.containerWeight.text.toString(), "")
                val pickUpInfo = LegPickUpInfo(1, ui.imPickUpPlaceName.text.toString().toUpperCase(), "default pick up addr",
                        ui.imPickUpApptTime.text.toString(), ui.imPickUpApptNumber.text.toString().toUpperCase())
                val deliverToInfo = LegDeliverToInfo(1, ui.imDeliverPlaceName.text.toString().toUpperCase(),
                        "default deliver to addr", ui.imDeliverToApptTime.text.toString(),
                        ui.imDeliverToApptNumber.text.toString().toUpperCase())
                val rate = LegRate(1, ui.imFlat.text.toString(), ui.imFuel.text.toString(),
                        ui.imOffHire.text.toString(), ui.imBonus.text.toString(), "", "")

                var flags = ""
                if (ui.freeFlowFlag.isChecked) flags += "FREE_FLOW,"
                if (ui.importTerminalExtractionFlag.isChecked) flags += "TERMINAL_EXTRACTION"
                val leg = Leg(1, "IMPORT", ui.importReceiveType.text.toString(),
                        flags, ui.bolNumber.text.toString().toUpperCase(),
                        "",ui.freeflowCode.text.toString().toUpperCase(),
                        ui.importSteamshipLine.text.toString().toUpperCase(),
                        1, 1, 1, 1, 1)
                insertIntoDB(containerInfo, pickUpInfo, deliverToInfo, equipmentInfo, rate, leg)
            }
            ui.legType.text == "EXPORT" -> {
                val containerInfo = LegContainerInfo(1, "", "",
                        ui.exContainerSize.text.toString(), "", "")
                val pickUpInfo = LegPickUpInfo(1, ui.exPickUpPlaceName.text.toString().toUpperCase(), "default pick up addr",
                        ui.exPickUpApptTime.text.toString(), ui.exPickUpApptNumber.text.toString().toUpperCase())
                val deliverToInfo = LegDeliverToInfo(1, ui.exDeliverPlaceName.text.toString().toUpperCase(),
                        "default deliver to addr", ui.exDeliverToApptTime.text.toString(),
                        ui.exDeliverToApptNumber.text.toString().toUpperCase())
                val rate = LegRate(1, ui.exFlat.text.toString(), ui.exFuel.text.toString(),
                        ui.exOffHire.text.toString(), ui.exBonus.text.toString(), "", "")

                var flags = ""
                if (ui.exportTerminalExtractionFlag.isChecked) flags += "TERMINAL_EXTRACTION"
                val leg = Leg(1, "EXPORT", ui.exportReceiveType.text.toString(),
                        flags, ui.bookingNumber.text.toString().toUpperCase(),
                        ui.poNumber.text.toString().toUpperCase(),"",
                        ui.exportSteamshipLine.text.toString().toUpperCase(),
                        1, 1, 1, 1, 1)
                insertIntoDB(containerInfo, pickUpInfo, deliverToInfo, equipmentInfo, rate, leg)
            }
            else -> {
                // Import to id=1, export to id=2
                val imContainerInfo = LegContainerInfo(1, ui.containerNumber.text.toString().toUpperCase(), "REGULAR",
                        ui.containerSize.text.toString(), ui.containerWeight.text.toString(), "")
                val imPickUpInfo = LegPickUpInfo(1, ui.imPickUpPlaceName.text.toString().toUpperCase(), "default pick up addr",
                        ui.imPickUpApptTime.text.toString(), ui.imPickUpApptNumber.text.toString().toUpperCase())
                val imDeliverToInfo = LegDeliverToInfo(1, ui.imDeliverPlaceName.text.toString().toUpperCase(),
                        "default deliver to addr", ui.imDeliverToApptTime.text.toString(),
                        ui.imDeliverToApptNumber.text.toString().toUpperCase())
                val imRate = LegRate(1, ui.imFlat.text.toString(), ui.imFuel.text.toString(),
                        ui.imOffHire.text.toString(), ui.imBonus.text.toString(), "", "")

                var imFlags = "STREET_TURN"
                if (ui.freeFlowFlag.isChecked) imFlags += "FREE_FLOW,"
                val imLeg = Leg(1, "IMPORT", ui.importReceiveType.text.toString(),
                        imFlags, ui.bolNumber.text.toString().toUpperCase(),
                        "",ui.freeflowCode.text.toString().toUpperCase(),
                        ui.importSteamshipLine.text.toString().toUpperCase(),
                        1, 1, 1, 1, 1)
                insertIntoDB(imContainerInfo, imPickUpInfo, imDeliverToInfo, equipmentInfo, imRate, imLeg)

                val exContainerInfo = LegContainerInfo(2, "", "",
                        ui.exContainerSize.text.toString(), "", "")
                val exPickUpInfo = LegPickUpInfo(2, ui.exPickUpPlaceName.text.toString().toUpperCase(), "default pick up addr",
                        ui.exPickUpApptTime.text.toString(), ui.exPickUpApptNumber.text.toString().toUpperCase())
                val exDeliverToInfo = LegDeliverToInfo(2, ui.exDeliverPlaceName.text.toString().toUpperCase(),
                        "default deliver to addr", ui.exDeliverToApptTime.text.toString(),
                        ui.exDeliverToApptNumber.text.toString().toUpperCase())
                val exRate = LegRate(2, ui.exFlat.text.toString(), ui.exFuel.text.toString(),
                        ui.exOffHire.text.toString(), ui.exBonus.text.toString(), "", "")

                val exLeg = Leg(2, "EXPORT", ui.exportReceiveType.text.toString(),
                        "", ui.bookingNumber.text.toString().toUpperCase(),
                        ui.poNumber.text.toString().toUpperCase(),"",
                        ui.exportSteamshipLine.text.toString().toUpperCase(),
                        2, 2, 2, 2, 2)
                insertIntoDBSecondRow(exContainerInfo, exPickUpInfo, exDeliverToInfo, equipmentInfoCopy, exRate, exLeg)
            }
        }
    }

    private fun insertIntoDB (containerInfo : LegContainerInfo, pickUpInfo : LegPickUpInfo,
                              deliverToInfo : LegDeliverToInfo, equipmentInfo : LegEquipmentInfo,
                              rate : LegRate, leg : Leg) {
        if (db.legDao().getLegInfoById(1)== null) {
            db.legDao().insertLegContainerInfo(containerInfo)
            db.legDao().insertPickUpInfo(pickUpInfo)
            db.legDao().insertDeliverToInfo(deliverToInfo)
            db.legDao().insertEquipmentInfo(equipmentInfo)
            db.legDao().insertLegRate(rate)

            db.legDao().insertAnLeg(leg)
        } else {
            db.legDao().updateLegContainerInfo(containerInfo)
            db.legDao().updateLegPickUpInfo(pickUpInfo)
            db.legDao().updateLegDeliverToInfo(deliverToInfo)
            db.legDao().updateLegEquipmentInfo(equipmentInfo)
            db.legDao().updateLegRate(rate)

            db.legDao().updateAnLeg(leg)
        }
    }

    private fun insertIntoDBSecondRow (containerInfo : LegContainerInfo, pickUpInfo : LegPickUpInfo,
                              deliverToInfo : LegDeliverToInfo, equipmentInfo : LegEquipmentInfo,
                              rate : LegRate, leg : Leg) {
        if (db.legDao().getLegInfoById(2)== null) {
            db.legDao().insertLegContainerInfo(containerInfo)
            db.legDao().insertPickUpInfo(pickUpInfo)
            db.legDao().insertDeliverToInfo(deliverToInfo)
            db.legDao().insertEquipmentInfo(equipmentInfo)
            db.legDao().insertLegRate(rate)

            db.legDao().insertAnLeg(leg)
        } else {
            db.legDao().updateLegContainerInfo(containerInfo)
            db.legDao().updateLegPickUpInfo(pickUpInfo)
            db.legDao().updateLegDeliverToInfo(deliverToInfo)
            db.legDao().updateLegEquipmentInfo(equipmentInfo)
            db.legDao().updateLegRate(rate)

            db.legDao().updateAnLeg(leg)
        }
    }

    private fun buildDialogue (item : String) {
        // Show dialog
        lateinit var options : Array<String>
        when (item) {
            "legType" -> options = arrayOf("IMPORT", "EXPORT", "STREET_TURN")
            "importReceiveType" -> options = arrayOf("DROPPULL", "LIVE")
            "exportReceiveType" -> options = arrayOf("DROPPULL", "LIVE")
            "containerSize" -> options = arrayOf("20", "40", "40H", "40HC", "45", "45H")
            "exContainerSize" -> options = arrayOf("20", "40", "40H", "40HC", "45", "45H")
            "equipmentType" -> options = arrayOf("PRIVATE", "POOL")
        }
        val builder = AlertDialog.Builder(this)

        builder.setTitle("Please select one.")
        builder.setItems(options){_, which ->
            when (item) {
                "legType" -> {
                    ui.legType.text = options[which]
                    when (which) {
                        0 -> {
                            ui.importForm.visibility = View.VISIBLE
                            ui.exportForm.visibility = View.GONE
                        }
                        1 -> {
                            ui.importForm.visibility = View.GONE
                            ui.exportForm.visibility = View.VISIBLE
                        }
                        else -> {
                            ui.importTerminalExtractionFlag.visibility = View.GONE
                            ui.exportTerminalExtractionFlag.visibility = View.GONE

                            ui.importForm.visibility = View.VISIBLE
                            ui.exportForm.visibility = View.VISIBLE
                        }
                    }
                }
                "equipmentType" -> {
                    ui.equipmentType.text = options[which]
                    if (which == 1) {
                        ui.releaseNumber.visibility = View.GONE
                        ui.providerName.visibility = View.GONE
                    } else {
                        ui.releaseNumber.visibility = View.VISIBLE
                        ui.providerName.visibility = View.VISIBLE
                    }
                }

                "importReceiveType" -> ui.importReceiveType.text = options[which]
                "exportReceiveType" -> ui.exportReceiveType.text = options[which]
                "containerSize" -> ui.containerSize.text = options[which]
                "exContainerSize" -> ui.exContainerSize.text = options[which]

            }
        }
        builder.show()
    }
}
