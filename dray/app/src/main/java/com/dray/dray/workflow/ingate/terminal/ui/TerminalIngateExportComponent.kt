package com.dray.dray.workflow.ingate.terminal.ui

import android.graphics.Color
import android.graphics.Typeface
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.ingate.terminal.TerminalIngateActivity
import org.jetbrains.anko.*

class TerminalIngateExportComponent : AnkoComponent<TerminalIngateActivity>, AnkoLogger{
    lateinit var gatePassNo : EditText
    lateinit var bookingNo : TextView
    lateinit var equipment : TextView
    lateinit var containerSize : TextView
    lateinit var SCACCode : TextView
    lateinit var nextBtn : Button

    lateinit var reportBtn : LinearLayout
    val MEDIUM_SIZE_TEXT = 18f
    val DISPATCH_SIZE_TEXT = 24f
    val DATA_TEXT_SIZE = 30f

    override fun createView(ui: AnkoContext<TerminalIngateActivity>): View = with(ui){
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            linearLayout(){
                backgroundResource = R.drawable.green_bordered_round_btn
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                lparams(width = matchParent){
                    weight = 0f
                    bottomMargin = dimen(R.dimen.item_vertical_margin) * 2
                }

                textView("Gate Pass #"){
                    textColor = R.color.colorPrimary
                }.lparams(width = dip(100))

                gatePassNo = editText(){
                    hint = "Please enter here. "
                    maxLines = 1
                    inputType = InputType.TYPE_CLASS_TEXT
                    backgroundColor = Color.TRANSPARENT
                }.lparams(width = matchParent)
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    bottomMargin = dimen(R.dimen.small_gap)
                }
                textView("Booking #"){
                    textColor = R.color.colorPrimary
                }
                bookingNo = textView("8PHL056584"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                    typeface = Typeface.DEFAULT_BOLD
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    verticalMargin = dimen(R.dimen.small_gap)
                }
                textView("SCAC Code"){
                    textColor = R.color.colorPrimary
                }
                SCACCode = textView("HRBR"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    verticalMargin = dimen(R.dimen.small_gap)
                }
                textView("Equipment Needed"){
                    textColor = R.color.colorPrimary
                }
                equipment = textView("Chassis"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    topMargin = dimen(R.dimen.small_gap)
                }
                textView("Container Size"){
                    textColor = R.color.colorPrimary
                }
                containerSize = textView("40HC"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            nextBtn = button("Next"){
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}