package com.dray.dray.helpActivities

import androidx.fragment.app.Fragment
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dray.dray.R
import com.dray.dray.adapter.solvedHelpTicketAdapter
import com.dray.dray.customClasses.HelpTicketItem
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.UI
import java.sql.Date
import java.sql.Timestamp

class SolvedHelpTicketFragmentActivity : androidx.fragment.app.Fragment() {
    lateinit var mAdapter: solvedHelpTicketAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val MEDIUM_SIZE_TEXT = 18f

        var pendingHelpTickets = ArrayList<HelpTicketItem>()
        val timestamp = Timestamp(System.currentTimeMillis())
        println(Date(timestamp.time).toString())

        // DUMMY DATA
        pendingHelpTickets.add(HelpTicketItem("Solved", "A002",
                "An est aliquid, quod te sua sponte delectet? Si est nihil nisi corpus, " +
                        "summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera.",
                timestamp, "One solution"))
        pendingHelpTickets.add(HelpTicketItem("Solved", "A105",
                "Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. " +
                        "Id mihi magnum videtur. At cum de plurimis eadem dicit, tum certe de maximis.",
                timestamp, "One solution"))

        mAdapter = solvedHelpTicketAdapter(this, pendingHelpTickets)

        return UI{
            verticalLayout {
                lparams(width = matchParent, height = matchParent){
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    horizontalPadding = dimen (R.dimen.list_item_horizontal_padding)
                }

                listView{
                    adapter = mAdapter
                    divider = null
                    backgroundColor = Color.TRANSPARENT
                    cacheColorHint = Color.TRANSPARENT
                    dividerHeight = dimen(R.dimen.item_vertical_margin)
                }.lparams(width = matchParent, height = matchParent)
            }
        }.view

    }
}