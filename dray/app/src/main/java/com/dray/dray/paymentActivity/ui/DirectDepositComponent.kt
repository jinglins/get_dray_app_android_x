package com.dray.dray.paymentActivity.ui

import android.graphics.Color
import android.text.InputType.TYPE_CLASS_NUMBER
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.paymentActivity.DirectDepositActivity
import org.jetbrains.anko.*

class DirectDepositComponent : AnkoComponent<DirectDepositActivity>, AnkoLogger{
    val MEDIUM_TEXT_SIZE = 18f
    val SMALL_GAP = 2
    val TAB_GAP = 20
    val redColorHex = "#b22222"

    lateinit var routingNumber : EditText
    lateinit var confirmRoutingNumber : EditText
    lateinit var accountNumber: EditText
    lateinit var confirmAccountNumber : EditText

    lateinit var typeCheck : LinearLayout
    lateinit var typeSave : LinearLayout
    lateinit var typeCheckTxt : TextView
    lateinit var typeSaveTxt : TextView

    lateinit var paidFrequencySelection : LinearLayout
    lateinit var paidFrequencyTxt : TextView

    lateinit var routingNumberDigitCountError : TextView // 9 Digit
    lateinit var accountNumberDigitCountError : TextView // 8-12 Digit
    lateinit var routingNumberConfirmFailError : TextView
    lateinit var accountNumberConfirmFailError : TextView


    lateinit var finishBtn : Button

    override fun createView(ui: AnkoContext<DirectDepositActivity>): View = with(ui) {
        scrollView{
            verticalLayout(){
                lparams(width= matchParent, height = matchParent){
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                }

                verticalLayout {
                    lparams(width = matchParent)

                    imageView(){
                        setImageResource(R.drawable.example_check)
                        adjustViewBounds = true
                    }.lparams(width = matchParent, height = wrapContent){
                        bottomMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    textView("Basic Information"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        topMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    verticalLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent, height = wrapContent) {
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            verticalMargin = SMALL_GAP/2
                        }

                        routingNumber = editText(){
                            inputType = TYPE_CLASS_NUMBER
                            textSize = MEDIUM_TEXT_SIZE
                            hint = "Routing Number"
                        }.lparams(width= matchParent)

                        routingNumberDigitCountError = textView(R.string.routing_number_count_error){
                            textColor = Color.parseColor(redColorHex)
                            visibility = View.GONE
                        }
                    }

                    verticalLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent, height = wrapContent) {
                            verticalMargin = SMALL_GAP/2
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        }

                        confirmRoutingNumber = editText(){
                            inputType = TYPE_CLASS_NUMBER
                            textSize = MEDIUM_TEXT_SIZE
                            hint = "Confirm Routing Number"
                        }.lparams(width= matchParent)

                        routingNumberConfirmFailError = textView(R.string.confirm_routing_number_error){
                            textColor = Color.parseColor(redColorHex)
                            visibility = View.GONE
                        }
                    }

                    verticalLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent, height = wrapContent) {
                            topMargin = dip(10)
                            bottomMargin = SMALL_GAP/2
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        }

                        accountNumber = editText(){
                            inputType = TYPE_CLASS_NUMBER
                            textSize = MEDIUM_TEXT_SIZE
                            hint = "Account Number"
                        }.lparams(width= matchParent)

                        accountNumberDigitCountError = textView(R.string.account_number_count_error){
                            textColor = Color.parseColor(redColorHex)
                            visibility = View.GONE
                        }
                    }

                    verticalLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent, height = wrapContent) {
                            verticalMargin = SMALL_GAP/2
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        }

                        confirmAccountNumber = editText(){
                            inputType = TYPE_CLASS_NUMBER
                            textSize = MEDIUM_TEXT_SIZE
                            hint = "Confirm Account Number"
                        }.lparams(width= matchParent)

                        accountNumberConfirmFailError = textView(R.string.confirm_account_number_error){
                            textColor = Color.parseColor(redColorHex)
                            visibility = View.GONE
                        }
                    }

                    textView("Account Type"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        topMargin = dimen(R.dimen.item_vertical_margin)
                    }
                    linearLayout {
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.activity_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                        }

                        typeCheck = verticalLayout {
                            backgroundResource = R.drawable.green_rounded_button
                            lparams(width = matchParent) {
                                weight = 0.5f
                                rightMargin = dip(TAB_GAP / 2)
                                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                            }

                            typeCheckTxt = textView("Checking") {
                                this.gravity = Gravity.CENTER
                                textSize = MEDIUM_TEXT_SIZE
                                textColor = Color.WHITE
                            }.lparams(width = matchParent, height = matchParent)
                        }

                        typeSave = verticalLayout {
                            backgroundResource = R.drawable.dashed_square
                            lparams(width = matchParent) {
                                weight = 0.5f
                                leftMargin = dip(TAB_GAP / 2)
                                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                            }
                            typeSaveTxt = textView("Savings") {
                                this.gravity = Gravity.CENTER
                                textSize = MEDIUM_TEXT_SIZE
                            }.lparams(width = matchParent, height = matchParent)
                        }
                    }

                    textView("Paid Frequency"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        topMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    linearLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = matchParent, height = wrapContent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        }

                        textView("I want to get paid every "){
                            textSize = MEDIUM_TEXT_SIZE
                        }

                        paidFrequencySelection = verticalLayout {
                            backgroundResource = R.drawable.dashed_square
                            lparams(width = dip(60), height = matchParent)

                            paidFrequencyTxt = textView("2"){
                                textSize = MEDIUM_TEXT_SIZE
                                this.gravity = Gravity.CENTER
                            }.lparams(width = matchParent)
                        }

                        textView(" days. "){
                            textSize = MEDIUM_TEXT_SIZE
                        }
                    }

                }

                finishBtn = button("FINISH") {
                    backgroundResource = R.drawable.green_rounded_button
                    textColor = Color.WHITE
                    textSize = MEDIUM_TEXT_SIZE

                }.lparams(width = matchParent){
                    verticalMargin = dimen(R.dimen.item_vertical_margin)
                }
            }
        }
    }
}