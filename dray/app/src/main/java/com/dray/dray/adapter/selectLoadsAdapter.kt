package com.dray.dray.adapter

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.R
import com.dray.dray.customClasses.SelectLoadItem
import com.dray.dray.workflow.newLeg.confirm.ConfirmNewLegActivity
import com.dray.dray.workflow.newLeg.select.SelectNewLegActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk15.listeners.onClick

class selectLoadsAdapter (val activity: SelectNewLegActivity, var selectLoadItems : ArrayList<SelectLoadItem>) : BaseAdapter(){

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val item : SelectLoadItem = getItem(p0)
        var MEDIUM_TEXT_SIZE = 18f
        var PRICE_TEXT_SIZE = 36f
        var SMALL_GAP = 1

        return with (p2 !!.context){
            verticalLayout{
                lparams(width = matchParent, height = wrapContent){
                    verticalMargin = dimen(R.dimen.item_vertical_margin)
                    //verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    //horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    //backgroundColor = Color.WHITE
                }

                linearLayout(){
                    verticalLayout {
                        lparams(){
                            weight = 1f
                            backgroundColor = Color.WHITE
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            leftPadding = dimen(R.dimen.list_item_horizontal_padding)
                            rightMargin = dip(SMALL_GAP)
                            bottomMargin = dip(SMALL_GAP)
                        }

                        var labelArray = listOf("Origin", "Destination", "Size/Weight")
                        var dataArray = listOf(item.origin, item.destination, item.size + " / " + item.weight)

                        for (i : Int in 0..2){
                            linearLayout{
                                lparams(){
                                    topMargin = dimen(R.dimen.item_vertical_margin)
                                }

                                textView(labelArray[i]){
                                    textColor = R.color.colorPrimary
                                }.lparams(width = dip(80))
                                textView(dataArray[i]){
                                }
                            }
                        }
                    }

                    verticalLayout{
                        backgroundColor = Color.WHITE
                        lparams(width = wrapContent, height = matchParent){
                            weight = 0f
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                            leftMargin = dip(SMALL_GAP)
                            bottomMargin = dip(SMALL_GAP)
                        }


                        textView("$${item.flat}"){
                            textSize = PRICE_TEXT_SIZE
                            textColor = Color.BLACK
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams(){
                            this.gravity = Gravity.END
                        }

                        if (item.bonus !== "0"){
                            textView("+$${item.bonus} Bonus"){
                                textSize = MEDIUM_TEXT_SIZE
                                textColor = Color.parseColor("#B22222")
                            }.lparams(){
                                this.gravity = Gravity.END
                            }
                        }

                    }

                }

                verticalLayout{
                    lparams(width = matchParent, height = wrapContent){
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        backgroundColor = Color.WHITE
                        topMargin = dip(SMALL_GAP)
                    }

                    if (item.equipment == "none"){
                        textView("${item.loadMethod}."){
                        }.lparams(){
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                    } else {
                        textView("${item.loadMethod}. ${item.equipment} is needed."){
                        }.lparams(){
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                    }
                }

                // Row of buttons

                linearLayout(){
                    lparams(width = matchParent, height = wrapContent){
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_vertical_padding) //Special
                        verticalMargin = dip(SMALL_GAP)
                        gravity = Gravity.BOTTOM
                        weight = 0f
                    }

                    button("Reject"){
                        backgroundResource = R.drawable.left_rounded_red_button
                        textSize = MEDIUM_TEXT_SIZE
                        textColor = Color.WHITE
                    }.lparams(){
                        weight = 0.5f
                        rightMargin = dimen(R.dimen.countdown_gap)
                    }

                    button("Accept"){
                        backgroundResource = R.drawable.right_rounded_button
                        textColor = Color.WHITE
                        textSize = MEDIUM_TEXT_SIZE
                        onClick {
                            val intent = Intent(context, ConfirmNewLegActivity::class.java)  //Temporal wire to confirmation page

                            // Send response to backend

                            // Pass load data to confirmation
                            intent.putExtra("CONTAINER_NUMBER", item.containerNumber)
                            intent.putExtra("ORIGIN", item.origin)
                            intent.putExtra("DESTINATION", item.destination)
                            intent.putExtra("SIZE", item.size)
                            intent.putExtra("WEIGHT", item.weight)
                            intent.putExtra("LOAD_METHOD", item.loadMethod)
                            intent.putExtra("EQUIPMENT", item.equipment)
                            intent.putExtra("FLAT", item.flat)
                            intent.putExtra("BONUS", item.bonus)
                            intent.putExtra("CHASSIS", item.chassis)
                            intent.putExtra("FUEL", item.fuel)

                            context.startActivity(intent)
                        }
                    }.lparams(){
                        weight = 0.5f
                        leftMargin = dimen(R.dimen.countdown_gap)
                    }
                }

                // Later when assign three loads at one time.
                /*button("Accept This Load"){
                    backgroundResource = R.drawable.green_rounded_button
                    textColor = Color.WHITE
                    textSize = MEDIUM_TEXT_SIZE
                }.lparams(width = matchParent){
                    topMargin = dip(20)
                    gravity = Gravity.BOTTOM
                    weight = 0f
                }*/
            }
        }
    }

    override fun getItem(p0: Int): SelectLoadItem {
        return selectLoadItems?.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
       return selectLoadItems.count()
    }

}