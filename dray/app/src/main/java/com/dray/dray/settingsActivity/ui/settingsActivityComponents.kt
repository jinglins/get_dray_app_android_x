package com.dray.dray.settingsActivity.ui

import android.graphics.Color
import android.renderscript.Allocation
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.Switch
import com.dray.dray.R
import com.dray.dray.settingsActivity.SettingsActivity
import org.jetbrains.anko.*

class SettingsActivityComponent : AnkoComponent<SettingsActivity>, AnkoLogger {

    val MEDIUM_TEXT_SIZE = 18f
    lateinit var availLoadNotification : Switch
    lateinit var bestLeaveTimeNotification : Switch
    lateinit var englishOption : RadioButton
    lateinit var spanishOption : RadioButton

    override fun createView(ui: AnkoContext<SettingsActivity>): View = with(ui) {

        verticalLayout(){
            lparams(width = matchParent, height = matchParent){
            }
            verticalLayout() {

                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                verticalPadding = dimen(R.dimen.activity_vertical_padding)

                textView("Notifications"){
                    textColor = R.color.colorPrimary
                }

                linearLayout{
                    lparams(width = matchParent, height = wrapContent){
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                        backgroundResource = R.drawable.white_rounded_button

                        verticalPadding = dimen(R.dimen.activity_horizontal_padding)
                        horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                    }

                    textView("Available loads"){
                        textColor = R.color.colorPrimary
                        textSize = MEDIUM_TEXT_SIZE
                    }.lparams(width = dip(50), height= wrapContent){
                        weight = 0.6f
                    }

                    availLoadNotification = switch(){

                   }.lparams(){
                       weight = 0.4f
                   }
                }

                linearLayout {
                    lparams(width = matchParent, height = wrapContent) {
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                        backgroundResource = R.drawable.white_rounded_button

                        verticalPadding = dimen(R.dimen.activity_horizontal_padding)
                        horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                    }

                    textView("Best time to leave") {
                        textColor = R.color.colorPrimary
                        textSize = MEDIUM_TEXT_SIZE
                    }.lparams(width = dip(50), height = wrapContent) {
                        weight = 0.6f
                    }

                    bestLeaveTimeNotification = switch() {

                    }.lparams() {
                        weight = 0.4f
                    }
                }

                textView("Language"){
                    textColor = R.color.colorPrimary
                }.lparams(){
                    topMargin = dip(10)
                }

                linearLayout {
                    lparams(width = matchParent, height = wrapContent) {
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                        backgroundResource = R.drawable.white_rounded_button

                        verticalPadding = dimen(R.dimen.item_vertical_margin)
                        horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                    }

                    radioGroup(){
                        orientation = LinearLayout.HORIZONTAL


                        englishOption = radioButton(){
                            text = "English"
                            textSize = MEDIUM_TEXT_SIZE
                            textColor = R.color.colorPrimary
                        }.lparams(){
                            weight = 0.5f
                        }

                        spanishOption = radioButton(){
                            text = "Español"
                            textSize = MEDIUM_TEXT_SIZE
                            textColor = R.color.colorPrimary
                        }.lparams(){
                            weight = 0.5f
                        }

                    }.lparams(width= matchParent, height = wrapContent){
                        verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    }
                }


            }

        }


    }

}