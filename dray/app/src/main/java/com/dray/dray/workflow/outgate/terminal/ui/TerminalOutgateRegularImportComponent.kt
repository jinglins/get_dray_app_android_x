package com.dray.dray.workflow.outgate.terminal.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.*
import com.dray.dray.R
import com.dray.dray.workflow.outgate.terminal.TerminalOutgateActivity
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.*

class TerminalOutgateRegularImportComponent : AnkoComponent<TerminalOutgateActivity>, AnkoLogger{
    lateinit var containerNo : TextView
    lateinit var bolNo : TextView
    lateinit var deliverTo : TextView
    lateinit var containerSize : TextView

    lateinit var reportBtn : LinearLayout
    lateinit var nextBtn : Button

    val MEDIUM_SIZE_TEXT = 18f
    val DATA_TEXT_SIZE = 30f // Due to the restriction of space, this value happens to be the same as dipatch
    val LABEL_WIDTH = 90
    override fun createView(ui: AnkoContext<TerminalOutgateActivity>): View = with(ui) {
        verticalLayout{

            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)


            verticalLayout{
                lparams(width = matchParent){
                    weight = 1f
                }

                linearLayout {
                    lparams (width = matchParent, height = matchParent){
                        weight = 1f
                        bottomMargin = dimen(R.dimen.small_gap)
                    }

                    verticalLayout{
                        backgroundColor = Color.WHITE
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        gravity = Gravity.CENTER_VERTICAL
                        lparams(width = matchParent, height = matchParent){
                            rightMargin = dimen(R.dimen.small_gap)
                            weight = 1f
                        }
                        textView("Container #"){
                            textColor = R.color.colorPrimary
                        }
                        containerNo = textView("ABCD0000000"){
                            textColor = Color.BLACK
                            textSize = DATA_TEXT_SIZE
                            typeface = Typeface.DEFAULT_BOLD
                        }
                    }

                }

                verticalLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent, height = matchParent){
                        verticalMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("BOL #"){
                        textColor = R.color.colorPrimary
                    }
                    bolNo = textView("ONEYHKGUD4349600"){
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }

                verticalLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent, height = matchParent){
                        verticalMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Deliver To"){
                        textColor = R.color.colorPrimary
                    }
                    deliverTo = textView("RANCHO CUCAMONGA"){
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }

                verticalLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent, height = matchParent){
                        topMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Type/Size"){
                        textColor = R.color.colorPrimary
                    }
                    containerSize = textView("40HC"){
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }
            }


            nextBtn = button("NEXT"){
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                weight = 0f
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}