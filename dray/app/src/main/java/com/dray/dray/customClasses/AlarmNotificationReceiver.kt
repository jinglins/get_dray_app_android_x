package com.dray.dray.customClasses

import android.app.Notification
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Vibrator
import androidx.annotation.RequiresApi
import android.widget.Toast
import org.jetbrains.anko.vibrator
import android.app.NotificationManager
import androidx.core.app.NotificationCompat
import com.dray.dray.R
import com.dray.dray.R.mipmap.ic_launcher
import java.lang.System.currentTimeMillis



class AlarmNotificationReceiver : BroadcastReceiver(){
    override fun onReceive(p0: Context?, p1: Intent?) {
        println("in alarm.kt")
        val builder = NotificationCompat.Builder(p0)
        builder.setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("ALarm Activated")
                .setContentText("This is my alarm")
                .setDefaults(Notification.DEFAULT_LIGHTS or Notification.DEFAULT_SOUND)
                .setContentInfo("Info")
        val notificationManager = p0!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(1, builder.build())
    }

}