package com.dray.dray.onboardActivity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dray.dray.MainActivity
import com.dray.dray.onboardActivity.ui.OnboardConfirmComponent

import org.jetbrains.anko.setContentView

class OnboardConfirmActivity : AppCompatActivity() {

    val ui = OnboardConfirmComponent();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        // Dummy Data
        val dummyDriverInfo = object {
            var name = "Jane Doe"
            var vendorCode = "2401"
            var company = "Harbor Express Inc."
            var license = "123456789"
        }

        val formattedName = dummyDriverInfo.name + "!"
        ui.driverName.text = formattedName
        ui.driverCompany.text = dummyDriverInfo.company
        ui.driverCode.text = dummyDriverInfo.vendorCode
        ui.license.text = dummyDriverInfo.license

        ui.confirmBtn.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

}
