package com.dray.dray.workflow.newLeg.confirm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dray.dray.MainActivity
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setToolBar
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.mapActivity.MapActivity
import com.dray.dray.workflow.newLeg.confirm.ui.FreeflowImportItemComponent
import com.dray.dray.workflow.newLeg.confirm.ui.RegularExportItemComponent
import com.dray.dray.workflow.newLeg.confirm.ui.RegularImportItemComponent
import org.jetbrains.anko.*

class ConfirmNewLegActivity : AppCompatActivity() {

    val importUI = RegularImportItemComponent()
    val exportUI = RegularExportItemComponent()
    val freeflowUI = FreeflowImportItemComponent()
    //val ui = StreetturnItemComponent()

    lateinit var db : AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = getDatabase(applicationContext)

        val toolbar = supportActionBar!!
        setToolBar(toolbar, "Confirm Your Selection")

        val firstLegInfo = db.legDao().getLegInfoById(1)

        val bolBookingNumber = db.legDao().getLegInfoById(1).bolBookingNumber
        val containerNumber = db.legDao().getLegContainerInfo(1).containerNumber
        val receiveType = db.legDao().getLegInfoById(1).receiveType
        val originName = db.legDao().getLegPickUpInfoById(1).originName
        val destinationName = db.legDao().getLegDeliverToInfo(1).destinationName
        val size = db.legDao().getLegContainerInfo(1).containerSize + " (lb)"
        val equipment = db.legDao().getLegEquipmentInfoById(1).equipmentName

        val rateInfo = db.legDao().getLegRateInfo(1)
        val flat = rateInfo.flatRate
        val fuel = rateInfo.fuelRate
        val bonus = rateInfo.bonusRate
        val hireOff = rateInfo.hireOffRate
        val total = (flat.toBigDecimal() + fuel.toBigDecimal() + bonus.toBigDecimal() + hireOff.toBigDecimal()).toString()

        if (firstLegInfo.legType == "IMPORT"){
            if (firstLegInfo.flags.contains("FREE_FLOW")){
                freeflowUI.setContentView(this)

                freeflowUI.freeflowCodeTxt.text = db.legDao().getLegInfoById(1).freeFlowCode
                freeflowUI.equipmentTxt.text = db.legDao().getLegEquipmentInfoById(1).equipmentName
                freeflowUI.originTxt.text = db.legDao().getLegPickUpInfoById(1).originName

                freeflowUI.backBtn.setOnClickListener {
                    finish()
                }
                freeflowUI.confirmBtn.setOnClickListener {
                    goToNextActivity()
                }
            } else {
                importUI.setContentView(this)

                importUI.containerNumberTxt.text = containerNumber
                importUI.originTxt.text = originName
                importUI.destinationTxt.text = destinationName
                importUI.sizeTxt.text = size
                importUI.weightTxt.text = db.legDao().getLegContainerInfo(1).containerWeight
                importUI.receiveTypeTxt.text = receiveType
                importUI.equipmentTxt.text = equipment

                importUI.flatRateTxt.text = "$" + flat
                importUI.fuelTxt.text = "$" + fuel
                importUI.bonusTxt.text = "$" + bonus
                importUI.chassisTxt.text = "$" + hireOff
                importUI.totalTxt.text = "$" + total

                importUI.backBtn.setOnClickListener {
                    finish()
                }
                importUI.confirmBtn.setOnClickListener {
                    goToNextActivity()
                }
            }
        } else{
            exportUI.setContentView(this)

            exportUI.bookingNumberTxt.text = bolBookingNumber
            exportUI.originTxt.text = originName
            exportUI.destinationTxt.text = destinationName
            exportUI.sizeTxt.text = size
            exportUI.receiveTypeTxt.text = receiveType
            exportUI.equipmentTxt.text = equipment

            exportUI.flatRateTxt.text = "$" + flat
            exportUI.fuelTxt.text = "$" + fuel
            exportUI.bonusTxt.text = "$" + bonus
            exportUI.chassisTxt.text = "$" + hireOff
            exportUI.totalTxt.text = "$" + total

            exportUI.backBtn.setOnClickListener {
                finish()
            }
            exportUI.confirmBtn.setOnClickListener {
                goToNextActivity()
            }
        }
    }

    private fun goToNextActivity () {
        val curPlace = intent.getStringExtra("CUR_PLACE")
        lateinit var i : Intent
        when (curPlace) {
            "yard" -> {
                i = Intent(this, MapActivity::class.java)
                i.putExtra("TARGET", "TERMINAL")
            }
            "customer" -> {
                i = Intent(this, MapActivity::class.java)
                i.putExtra("TARGET", "BACKTERMINAL")
                i.putExtra("WANT_NEXT_LOAD", "true")
            }
            else -> {
                i = Intent(this, MainActivity::class.java)
                i.putExtra("INFO_INJECTED", "yes")
            }
        }
        startActivity(i)
    }
}
