package com.dray.dray.adapter

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.R
import com.dray.dray.paymentActivity.HistoryPaymentFragmentActivity
import com.dray.dray.customClasses.PaymentHistoryItem
import org.jetbrains.anko.*
import java.text.NumberFormat
import java.util.*

class paymentHistoryAdapter(val activity: HistoryPaymentFragmentActivity, var paymentHistoryItems : ArrayList<PaymentHistoryItem>): BaseAdapter(){
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val item : PaymentHistoryItem = getItem(p0)!!
        var MEDIUM_TEXT_SIZE = 18f
        var AMOUNT_TEXT_SIZE = 24f
        return with(p2!!.context) {
            linearLayout(){
                lparams(width = matchParent){
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }


                backgroundColor = Color.WHITE

                verticalLayout{
                    lparams(){
                        weight = 1f
                    }

                    textView(item.paymentMethod) {
                        textColor = R.color.colorPrimary
                        textSize = MEDIUM_TEXT_SIZE
                        typeface = Typeface.DEFAULT_BOLD
                    }.lparams() {
                    }

                    textView("Paid on " + item.paymentDate) {
                    }.lparams(height = wrapContent) {
                    }
                }

                textView("$" + NumberFormat.getNumberInstance(Locale.US).format(item.paymentAmount)) {
                    textColor = Color.BLACK
                    textSize = AMOUNT_TEXT_SIZE
                    gravity = Gravity.CENTER_VERTICAL
                }.lparams(height = matchParent) {
                }


            }
        }
    }

    override fun getItem(p0: Int): PaymentHistoryItem? {
        return paymentHistoryItems?.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return paymentHistoryItems.count()
    }

}