package com.dray.dray.profileActivity.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity

import android.view.View
import android.widget.*
import com.dray.dray.R
import com.dray.dray.profileActivity.ProfileActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class ProfileActivityComponent : AnkoComponent<ProfileActivity>, AnkoLogger {

    val EXTRA_SMALL_TEXT_SIZE = 8f
    val SMALL_TEXT_SIZE = 12f
    val MEDIUM_TEXT_SIZE = 18f
    val LARGE_TEXT_SIZE = 30f
    val LABEL_WIDTH = 130
    val DOT_DIAMETER = 10

    lateinit var nameTxt : TextView
    lateinit var vendorCodeTxt : TextView
    lateinit var phoneTxt : TextView

    lateinit var medicalExpTxt : TextView
    lateinit var insuranceExpTxt : TextView
    lateinit var twiccardExpTxt : TextView
    lateinit var bitExpTxt : TextView
    lateinit var annualExpTxt : TextView
    lateinit var ddlExpTxt : TextView

    lateinit var medicalStatus : ImageView
    lateinit var insuranceStatus : ImageView
    lateinit var twiccardStatus : ImageView
    lateinit var bitStatus : ImageView
    lateinit var annualStatus : ImageView
    lateinit var ddlStatus : ImageView

    lateinit var dmvPrintoutStatus : TextView
    lateinit var pointsAndViolation : TextView

    lateinit var shiftTxt : TextView
    lateinit var distanceTxt : TextView
    lateinit var shiftEditBtn : LinearLayout
    lateinit var distanceEditBtn : LinearLayout

    lateinit var companyTxt : TextView
    lateinit var scacTxt : TextView
    lateinit var docketTxt : TextView
    lateinit var dotTxt : TextView
    lateinit var insuredTxt : TextView
    lateinit var plateTxt : TextView

    lateinit var emailTxt : TextView
    lateinit var emailEditBtn : LinearLayout
    lateinit var emailInput : EditText
    lateinit var emailUpdateBtn : ImageButton

    override fun createView(ui: AnkoContext<ProfileActivity>): View = with(ui) {

        verticalLayout() {
            lparams(width = matchParent, height = matchParent) {
            }


            linearLayout {
                backgroundResource = R.drawable.purple_header
                lparams(width = matchParent, height = dimen(R.dimen.profile_header_height)) {
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                }
                // -------------------------------------
                linearLayout {
                    // Weight wrapper for image view
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(height = matchParent) {
                        rightPadding = dip(20)
                    }
                    imageView(R.drawable.example_avatar).lparams(width = dimen(R.dimen.avatar_length),
                            height = dimen(R.dimen.avatar_length)) {
                    }
                }

                verticalLayout(R.style.ThemeOverlay_AppCompat_Dark) {
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(height = matchParent) {
                    }
                    nameTxt = textView("Driver Name") {
                        textSize = LARGE_TEXT_SIZE
                        typeface = Typeface.DEFAULT_BOLD
                    }
                    vendorCodeTxt = textView("Vendor Code") {
                        textSize = MEDIUM_TEXT_SIZE
                    }
                    phoneTxt = textView("Phone Number") {
                        textSize = MEDIUM_TEXT_SIZE
                    }
                }
            }
            scrollView {
                lparams(width = matchParent, height = matchParent)

                verticalLayout() {
                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)

                    textView("Qualified to Drive") {
                        textColor = R.color.colorPrimary
                    }

                    linearLayout() {
                        backgroundColor = Color.WHITE

                        lparams(width = matchParent) {
                            topMargin = dimen(R.dimen.item_vertical_margin)
                            bottomMargin = dip(2)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        }

                        verticalLayout {
                            lparams() {
                                weight = 1f
                            }

                            textView("Medical") {
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView("Expires") {
                                textSize = SMALL_TEXT_SIZE
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width = matchParent) {
                                topMargin = dip(10)
                            }
                            medicalExpTxt = textView("MM/DD/YYYY") {
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                textColor = Color.BLACK
                            }

                            medicalStatus = imageView() {
                                imageResource = R.drawable.green_rounded_button
                            }.lparams(width = dip(DOT_DIAMETER), height = dip(DOT_DIAMETER)) {
                                topMargin = dip(10)
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }

                        verticalLayout {
                            lparams() {
                                weight = 1f
                            }
                            textView("Insurance") {
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView("Expires") {
                                textSize = SMALL_TEXT_SIZE
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width = matchParent) {
                                topMargin = dip(10)
                            }
                            insuranceExpTxt = textView("MM/DD/YYYY") {
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                textColor = Color.BLACK
                            }

                            insuranceStatus = imageView() {
                                imageResource = R.drawable.green_rounded_button
                            }.lparams(width = dip(DOT_DIAMETER), height = dip(DOT_DIAMETER)) {
                                topMargin = dip(10)
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }

                        verticalLayout {
                            lparams() {
                                weight = 1f
                            }
                            textView("TWIC Card") {
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView("Expires") {
                                textSize = SMALL_TEXT_SIZE
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width = matchParent) {
                                topMargin = dip(10)
                            }
                            twiccardExpTxt = textView("MM/DD/YYYY") {
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                textColor = Color.BLACK
                            }
                            twiccardStatus = imageView() {
                                imageResource = R.drawable.green_rounded_button
                            }.lparams(width = dip(DOT_DIAMETER), height = dip(DOT_DIAMETER)) {
                                topMargin = dip(10)
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }
                    }

                    linearLayout() {
                        backgroundColor = Color.WHITE

                        lparams(width = matchParent) {
                            bottomMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        }

                        verticalLayout {
                            lparams() {
                                weight = 1f
                            }

                            textView("BIT") {
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView("Truck Inspection"){
                                textColor = R.color.colorPrimary
                                textSize = EXTRA_SMALL_TEXT_SIZE
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView("Expires") {
                                textSize = SMALL_TEXT_SIZE
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width = matchParent) {
                                topMargin = dip(10)
                            }
                            bitExpTxt = textView("MM/DD/YYYY") {
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                textColor = Color.BLACK
                            }

                            bitStatus = imageView() {
                                imageResource = R.drawable.green_rounded_button
                            }.lparams(width = dip(DOT_DIAMETER), height = dip(DOT_DIAMETER)) {
                                topMargin = dip(10)
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }

                        verticalLayout {
                            lparams() {
                                weight = 1f
                            }
                            textView("Annual") {
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }

                            textView("Truck Inspection"){
                                textColor = R.color.colorPrimary
                                textSize = EXTRA_SMALL_TEXT_SIZE
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }

                            textView("Expires") {
                                textSize = SMALL_TEXT_SIZE
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width = matchParent) {
                                topMargin = dip(10)
                            }
                            annualExpTxt = textView("MM/DD/YYYY") {
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                textColor = Color.BLACK
                            }

                            annualStatus = imageView() {
                                imageResource = R.drawable.green_rounded_button
                            }.lparams(width = dip(DOT_DIAMETER), height = dip(DOT_DIAMETER)) {
                                topMargin = dip(10)
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }

                        verticalLayout {
                            lparams() {
                                weight = 1f
                            }
                            textView("DDL") {
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView("Driver's Driving License"){
                                textColor = R.color.colorPrimary
                                textSize = EXTRA_SMALL_TEXT_SIZE
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }

                            textView("Expires") {
                                textSize = SMALL_TEXT_SIZE
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width = matchParent) {
                                topMargin = dip(10)
                            }
                            ddlExpTxt = textView("MM/DD/YYYY") {
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                textColor = Color.BLACK
                            }
                            ddlStatus = imageView() {
                                imageResource = R.drawable.green_rounded_button
                            }.lparams(width = dip(DOT_DIAMETER), height = dip(DOT_DIAMETER)) {
                                topMargin = dip(10)
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }
                    }

                    linearLayout() {
                        backgroundColor = Color.WHITE

                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                        }

                        imageView(R.drawable.dmvlogo) {
                            adjustViewBounds = true
                        }.lparams(width = dip(60)) {
                            weight = 1f
                        }

                        verticalLayout {
                            lparams() {
                                weight = 1f
                            }
                            textView("Printout") {
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }

                            dmvPrintoutStatus = textView("Received") {
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }

                        verticalLayout {
                            lparams() {
                                weight = 1f
                            }
                            textView("Points & Violations") {
                                textColor = R.color.colorPrimary
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }

                            pointsAndViolation = textView("0") {
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                textColor = Color.BLACK
                            }
                        }
                    }

                    textView("Working Preferences") {
                        textColor = R.color.colorPrimary
                    }.lparams() {
                        topMargin = dip(10)
                    }

                    linearLayout {
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            backgroundColor = Color.WHITE
                        }

                        textView("Shift") {
                            textColor = R.color.colorPrimary
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams(width = dip(LABEL_WIDTH)) {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        shiftTxt = textView("Default") {
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        shiftEditBtn = linearLayout {
                            backgroundResource = R.drawable.green_rounded_button
                            horizontalPadding = dip(10)
                            gravity = Gravity.CENTER
                            imageView(R.mipmap.ic_edit) {
                                padding = dip(0)
                            }
                        }
                    }

                    linearLayout {
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            backgroundColor = Color.WHITE
                        }

                        textView("Distance") {
                            textColor = R.color.colorPrimary
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams(width = dip(LABEL_WIDTH)) {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        distanceTxt = textView("Default") {
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        distanceEditBtn = linearLayout {
                            backgroundResource = R.drawable.green_rounded_button
                            horizontalPadding = dip(10)
                            gravity = Gravity.CENTER
                            imageView(R.mipmap.ic_edit) {
                                padding = dip(0)
                            }
                        }
                    }

                    textView("Trucking Management Company") {
                        textColor = R.color.colorPrimary
                    }.lparams() {
                        topMargin = dip(10)
                    }
                    /* --- Company name --- */
                    linearLayout {
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            backgroundColor = Color.WHITE
                        }

                        textView("Name") {
                            textColor = R.color.colorPrimary
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams(width = dip(LABEL_WIDTH)) {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        companyTxt = textView("Default") {
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                    }

                    /* --- SCAC Code --- */
                    linearLayout {
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            backgroundColor = Color.WHITE
                        }

                        textView("SCAC Code") {
                            textColor = R.color.colorPrimary
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams(width = dip(LABEL_WIDTH)) {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        scacTxt = textView("Default") {
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                    }

                    textView("Carriers") {
                        textColor = R.color.colorPrimary
                    }.lparams() {
                        topMargin = dip(10)
                    }
                    /* --- Docket Number --- */
                    linearLayout {
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            backgroundColor = Color.WHITE
                        }

                        textView("Docket #") {
                            textColor = R.color.colorPrimary
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams(width = dip(LABEL_WIDTH)) {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        docketTxt = textView("Default") {
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                    }

                    /* --- DOT Number --- */
                    linearLayout {
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            backgroundColor = Color.WHITE
                        }

                        textView("DOT #") {
                            textColor = R.color.colorPrimary
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams(width = dip(LABEL_WIDTH)) {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        dotTxt = textView("Default") {
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                    }

                    /* --- Cargo Insured --- */
                    linearLayout {
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            backgroundColor = Color.WHITE
                        }

                        textView("Cargo Insured") {
                            textColor = R.color.colorPrimary
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams(width = dip(LABEL_WIDTH)) {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        insuredTxt = textView("Default") {
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                    }

                    /* --- Licence Plate--- */
                    linearLayout {
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            backgroundColor = Color.WHITE
                        }

                        textView("License Plate") {
                            textColor = R.color.colorPrimary
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams(width = dip(LABEL_WIDTH)) {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        plateTxt = textView("Default") {
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                    }


                    textView("Other") {
                        textColor = R.color.colorPrimary
                    }.lparams() {
                        topMargin = dip(10)
                    }

                    /* --- Email --- */
                    linearLayout {
                        lparams(width = matchParent) {
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                            verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            backgroundColor = Color.WHITE
                        }

                        textView("Email") {
                            textColor = R.color.colorPrimary
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams(width = dip(LABEL_WIDTH)) {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        emailTxt = textView("Not Set") {

                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        emailEditBtn = linearLayout {

                            backgroundResource = R.drawable.green_rounded_button
                            horizontalPadding = dip(10)
                            gravity = Gravity.CENTER
                            imageView(R.mipmap.ic_edit) {
                                padding = dip(0)
                            }
                        }

                        emailInput = editText {
                            visibility = View.GONE
                            textSize = MEDIUM_TEXT_SIZE
                        }.lparams() {
                            weight = 1f
                            this.gravity = Gravity.CENTER_VERTICAL
                        }

                        emailUpdateBtn = imageButton(R.mipmap.ic_check) {
                            visibility = View.GONE
                            backgroundResource = R.drawable.green_rounded_button
                            verticalPadding = dip(0)
                            horizontalPadding = dip(10)
                        }.lparams() {
                            this.gravity = Gravity.CENTER_VERTICAL
                        }
                    }
                }
            }
        }
    }



}