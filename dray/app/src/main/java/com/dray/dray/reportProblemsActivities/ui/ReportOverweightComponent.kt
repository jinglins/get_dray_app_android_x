package com.dray.dray.reportProblemsActivities.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ReportOverweightActivity
import org.jetbrains.anko.*

class ReportOverweightComponent : AnkoComponent<ReportOverweightActivity>, AnkoLogger {
    val MEDIUM_SIZE_TEXT = 18f
    lateinit var photoBtn : LinearLayout
    lateinit var photoDisplayArea : ImageView
    lateinit var submitBtn : Button

    override fun createView(ui: AnkoContext<ReportOverweightActivity>): View = with(ui){
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            textView("Photo Weight on the Scale"){
                textSize = MEDIUM_SIZE_TEXT
                textColor = resources.getColor(R.color.colorPrimary)
                typeface = Typeface.DEFAULT_BOLD
            }.lparams(){
                this.gravity = Gravity.CENTER_HORIZONTAL
            }

            view(){
                backgroundColor = resources.getColor(R.color.colorPrimary)
            }.lparams(width = dip(300), height = dip(1)){
                bottomMargin = dip(10)
                this.gravity = Gravity.CENTER_HORIZONTAL
            }

            verticalLayout{
                backgroundResource = R.drawable.green_bordered_round_btn
                lparams(width = matchParent){
                    weight = 1f
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }
                photoBtn = linearLayout{
                    lparams(width = matchParent, height = matchParent){
                        verticalPadding = dimen(R.dimen.activity_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    }
                    gravity = Gravity.CENTER

                    imageView(R.drawable.add_photo){
                        adjustViewBounds = true
                    }.lparams(width = dip(200), height = dip(200))
                }
                photoDisplayArea = imageView{
                    visibility = View.GONE
                    imageResource = R.drawable.example_avatar
                }.lparams(width = matchParent, height = matchParent)
            }

            submitBtn = button("Submit"){
                visibility = View.GONE
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}