package com.dray.dray.feedbackActivity.ui

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.dray.dray.R
import com.dray.dray.feedbackActivity.FeedbackActivity
import org.jetbrains.anko.*

class FeedbackComponent: AnkoComponent<FeedbackActivity>, AnkoLogger{
    lateinit var feedbackInput : EditText
    lateinit var submitBtn : Button

    override fun createView(ui: AnkoContext<FeedbackActivity>): View = with(ui){
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            textView("We appreciate your feedback."){
                textColor = R.color.colorPrimary
                this.gravity = Gravity.CENTER_HORIZONTAL
            }.lparams(width = matchParent){
                verticalMargin = dimen(R.dimen.item_vertical_margin)
            }

            verticalLayout {
                backgroundResource = R.drawable.white_rounded_item
                lparams(width = matchParent){
                    verticalMargin = dimen(R.dimen.item_vertical_margin)
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    weight = 1f
                }

                feedbackInput = editText{
                    textSize = 18f
                    backgroundColor = Color.TRANSPARENT
                    hint = "Please tell us something ..."
                }
            }

            submitBtn = button("Submit"){
                backgroundResource = R.drawable.green_rounded_button
                textSize = 18f
                textColor = Color.WHITE
            }.lparams(width = matchParent){
                verticalMargin = dimen(R.dimen.item_vertical_margin)
            }

        }
    }

}