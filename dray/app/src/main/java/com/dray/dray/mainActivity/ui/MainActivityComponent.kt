package com.dray.dray.mainActivity.ui

import android.graphics.Color
import android.graphics.DiscretePathEffect
import com.google.android.material.appbar.AppBarLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.GravityCompat
import androidx.appcompat.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.TextView
import androidx.drawerlayout.widget.DrawerLayout
import com.dray.dray.MainActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.navigationView
import org.jetbrains.anko.design.themedAppBarLayout
import org.jetbrains.anko.support.v4.drawerLayout

import com.dray.dray.R
import com.dray.dray.R.style.AppTheme_PopupOverlay
import com.dray.dray.adapter.assignedLoadsAdapter
import com.dray.dray.customClasses.AssignedLoadItem

class MainActivityComponent : AnkoComponent<MainActivity>, AnkoLogger {

    val MEDIUM_SIZE_TEXT = 22f

    lateinit var noLoadPlaceholder : LinearLayout
    lateinit var loadListView : LinearLayout
    lateinit var selectLoadBtn : Button
    lateinit var startNowBtn : Button

    lateinit var hoursLeft : TextView
    lateinit var minutesLeft : TextView

    lateinit var lv : ListView

    lateinit var drawer : androidx.drawerlayout.widget.DrawerLayout
    lateinit var toolbar: Toolbar

    override fun createView(ui: AnkoContext<MainActivity>): View = with(ui) {
        drawer = drawerLayout {

            //fitsSystemWindows = true

            coordinatorLayout() {
                fitsSystemWindows = true

                themedAppBarLayout(R.style.AppTheme_AppBarOverlay) {
                    toolbar = toolbar {
                        //backgroundColor = colorAttr(R.attr.colorPrimary)
                        backgroundResource = R.drawable.purple_header
                        popupTheme = AppTheme_PopupOverlay

                        verticalLayout(){
                            textView("Your next load starts within"){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }.lparams(width= matchParent){
                                topMargin = dimen(R.dimen.activity_vertical_margin)
                            }

                            linearLayout(){
                                this.gravity = Gravity.CENTER_HORIZONTAL

                                hoursLeft = textView("- -"){
                                    textSize = 60f
                                }
                                textView("hrs"){
                                    textSize = 22f
                                }.lparams(){
                                    horizontalMargin = dimen(R.dimen.countdown_gap)
                                }

                                minutesLeft = textView("- -"){
                                    textSize = 60f
                                }.lparams(){
                                    leftMargin = dimen(R.dimen.countdown_gap)
                                }

                                textView("min") {
                                    textSize = 22f
                                }.lparams(){
                                    horizontalMargin = dimen(R.dimen.countdown_gap)
                                }
                            }

                        }.lparams(width = matchParent){
                            rightMargin = dip(68) // width of default hambuger button
                        }


                    }.lparams(width = matchParent, height = dimen(R.dimen.nav_header_height)) //dimenAttr(R.attr.actionBarSize)
                }.lparams(width = matchParent)

                noLoadPlaceholder = verticalLayout{
                    visibility = View.GONE
                    gravity = Gravity.CENTER
                    lparams(width = matchParent, height = matchParent)

                    imageView(){
                        imageResource = R.mipmap.ic_sleep
                    }
                    textView("No work scheduled. "){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                    }

                    selectLoadBtn = button("Start Selecting A Load"){
                        backgroundResource = R.drawable.green_rounded_button
                        textColor = Color.WHITE
                    }.lparams(width = dip(250)){
                        topMargin = dip(20)
                    }
                }

                loadListView = verticalLayout {
                    horizontalPadding = dip(10)
                    verticalPadding = dip(10)

                    lv = listView(){
                    }.lparams(width = matchParent, height = wrapContent)

                }.lparams(width = matchParent, height = wrapContent) {
                    behavior = com.google.android.material.appbar.AppBarLayout.ScrollingViewBehavior() as androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior<*>?
                }

                startNowBtn = button("Start Now"){
                    textColor = Color.WHITE
                    textSize = MEDIUM_SIZE_TEXT
                    backgroundResource = R.drawable.green_rounded_button

                }.lparams(width= matchParent){
                    gravity = Gravity.BOTTOM
                    margin = dimen(R.dimen.activity_horizontal_margin)
                }

            }.lparams(width = matchParent, height = matchParent)

            navigationView {
                fitsSystemWindows = true
                val headerContext = AnkoContext.create(ctx, this)
                val headerView = NavHeaderComponent()
                        .createView(headerContext)
                //FIXME check this layout parameters, required or not
//                  .lparams(width = matchParent, height = dimen(R.dimen.nav_header_height))
                addHeaderView(headerView)
                inflateMenu(R.menu.drawer_menu)
                if (!isInEditMode) {
                    setNavigationItemSelectedListener(ui.owner)
                }
            }.lparams(height = matchParent) {
                gravity = GravityCompat.START
            }

            if (isInEditMode) {
                openDrawer(GravityCompat.START)
            }


        }
        drawer
    }
}
