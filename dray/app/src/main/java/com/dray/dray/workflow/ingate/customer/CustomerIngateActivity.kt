package com.dray.dray.workflow.ingate.customer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.ingate.customer.ui.CustomerIngateExportComponent
import com.dray.dray.workflow.ingate.customer.ui.CustomerIngateImportComponent
import com.dray.dray.workflow.secondLeg.find.FindSecondLegActivity
import com.dray.dray.workflow.signPOD.SignPODActivity
import org.jetbrains.anko.setContentView

class CustomerIngateActivity : AppCompatActivity() {

    val importUI = CustomerIngateImportComponent()
    val exportUI = CustomerIngateExportComponent()

    lateinit var db : AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, "Customer In-Gate")

        db = getDatabase(applicationContext)
        val bobtailIngate = (intent.getStringExtra("INGATE_STATUS") == "bobtail")

        if (db.legDao().getLegInfoById(1).legType == "IMPORT") {
            importUI.setContentView(this)
            injectData("import")
            if (bobtailIngate){
                importUI.nextBtn.text = "Find the Empty Container"
            }
            importUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                displayReportWindow(applicationContext, "false", "false")
            }
            importUI.nextBtn.setOnClickListener {
                goToNextActivity (bobtailIngate)
            }
        } else {
            exportUI.setContentView(this)
            injectData("export")
            if (bobtailIngate){
                exportUI.nextBtn.text = "Find the Empty Container"
            }
            exportUI.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
                displayReportWindow(applicationContext, "false", "false")
            }
            exportUI.nextBtn.setOnClickListener {
                goToNextActivity (bobtailIngate)
            }
        }
    }

    private fun goToNextActivity (bobtailIngate : Boolean){
        lateinit var i : Intent
        if (bobtailIngate) {
            i = Intent(this, FindSecondLegActivity::class.java)
            i.putExtra("CONTAINER_NO", db.legDao().getLegContainerInfo(2).containerNumber)
            i.putExtra("RETURN_TO", db.legDao().getLegDeliverToInfo(2).destinationName)
            i.putExtra("DISTANCE", "0")
        } else {
            i = Intent(this, SignPODActivity :: class.java)
        }
        startActivity(i)
    }

    private fun injectData (uiIndicator : String) {
        val apptNumber = db.legDao().getLegDeliverToInfo(1).apptNumber
        when (uiIndicator){
            "import" -> {
                importUI.containerNo.text = db.legDao().getLegContainerInfo(1).containerNumber
                importUI.containerSize.text= db.legDao().getLegContainerInfo(1).containerSize

                if (apptNumber == "") {
                    importUI.appointmentBlock.visibility = View.GONE
                } else {
                    importUI.appointmentNo.text = apptNumber
                }

                importUI.bolNo.text = db.legDao().getLegInfoById(1).bolBookingNumber
            }

            else -> {
                exportUI.bookingNo.text = db.legDao().getLegInfoById(1).bolBookingNumber
                exportUI.referenceNo.text = db.legDao().getLegInfoById(1).purchaseOrderNumber

                if (apptNumber == "") {
                    exportUI.appointmentBlock.visibility = View.GONE
                } else {
                    exportUI.appointmentNo.text = apptNumber
                }

                exportUI.containerNo.text = db.legDao().getLegContainerInfo(1).containerNumber
            }
        }
    }

}
