package com.dray.dray.workflow.scheduleNextLoadQuestion

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import com.dray.dray.MainActivity
import com.dray.dray.R
import com.dray.dray.customClasses.*
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.mapActivity.MapActivity
import com.dray.dray.workflow.newLeg.select.SelectNewLegActivity
import com.dray.dray.workflow.scheduleNextLoadQuestion.ui.ScheduleNextLoadQuestionComponent
import com.dray.dray.workflow.selectTerminalOrCustomer.SelectTerminalOrCustomerActivity
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast

class ScheduleNextLoadQuestionActivity : AppCompatActivity() {

    val ui = ScheduleNextLoadQuestionComponent()
    lateinit var db : AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set UI
        ui.setContentView(this)

        // Set toolbar
        val actionBar = supportActionBar
        setWorkFlowToolBar(actionBar!!, "Schedule Another Load")

        // Read passed down data
        val curPlace = intent.getStringExtra("CUR_PLACE")

        // Set button listener
        ui.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }

        ui.yesBtn.setOnClickListener {
            // If no internet connection
            toast ("No internet connection. ")
            val builder = AlertDialog.Builder(this@ScheduleNextLoadQuestionActivity)
            builder.setMessage("Please either retry in an internet-connected area, or call a dispatcher" +
                    " to schedule your next load. If you choose the second solution, please proceed the app " +
                    "by selecting the terminal to which you want to return current container. ")
            builder.setPositiveButton("Select The Terminal") { dialog, which ->
                val i = Intent (this, SelectTerminalOrCustomerActivity::class.java)
                i.putExtra("CUR_PLACE", "customer")
                startActivity(i)
            }
            builder.setNeutralButton("Cancel") { dialog, _ ->
                dialog.cancel()
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
            dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#808080"))
            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#29cc9e"))

            // If has internet connection
            // goToNewLegSelection(curPlace)
        }

        ui.noBtn.setOnClickListener{
            if (curPlace == "yard"){
                val i = Intent (this, MainActivity::class.java)
                startActivity(i)
            } else {
                db = getDatabase(applicationContext)
                val returnToTerminalStr = db.legDao().getLegDeliverToInfo(2).destinationName
                var returnToTerminalArr = returnToTerminalStr.split(", ")

                if (returnToTerminalArr.size > 1) {
                    var i = Intent (this, SelectTerminalOrCustomerActivity :: class.java)
                    i.putExtra("CUR_PLACE", "customer")
                    startActivity(i)
                } else {
                    val i = Intent (this, MapActivity::class.java)
                    i.putExtra("TARGET", "BACK_TO_TERMINAL")
                    i.putExtra("WANT_NEXT_LOAD", "false")
                    startActivity(i)
                }
            }
        }

    }

    private fun goToNewLegSelection (curPlace : String) {
        var i = Intent (this, SelectNewLegActivity :: class.java)
        i.putExtra("CUR_PLACE", curPlace)
        startActivity(i)
    }


}
