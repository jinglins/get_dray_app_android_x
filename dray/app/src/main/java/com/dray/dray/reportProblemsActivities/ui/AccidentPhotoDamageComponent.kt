package com.dray.dray.reportProblemsActivities.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.AccidentPhotoDamageActivity
import org.jetbrains.anko.*

class AccidentPhotoDamageComponent : AnkoComponent<AccidentPhotoDamageActivity>, AnkoLogger {

    val MEDIUM_SIZE_TEXT = 18f
    val PHOTO_HORIZONTAL_MARGIN = 4
    lateinit var nextBtn : Button

    lateinit var containerDmgPhotoBtn : ImageView
    lateinit var vehicleDmgPhotoBtn : ImageView

    lateinit var cDmgBox : LinearLayout
    lateinit var vDmgBox : LinearLayout

    lateinit var cADisplay : ImageView
    lateinit var cBDisplay : ImageView
    lateinit var cCDisplay : ImageView
    lateinit var cDDisplay : ImageView
    lateinit var cEDisplay : ImageView
    lateinit var cFDisplay : ImageView
    lateinit var cGDisplay : ImageView
    lateinit var cHDisplay : ImageView
    lateinit var cIDisplay : ImageView
    lateinit var cJDisplay : ImageView

    lateinit var vADisplay : ImageView
    lateinit var vBDisplay : ImageView
    lateinit var vCDisplay : ImageView
    lateinit var vDDisplay : ImageView
    lateinit var vEDisplay : ImageView
    lateinit var vFDisplay : ImageView
    lateinit var vGDisplay : ImageView
    lateinit var vHDisplay : ImageView
    lateinit var vIDisplay : ImageView
    lateinit var vJDisplay : ImageView

    override fun createView(ui: AnkoContext<AccidentPhotoDamageActivity>): View = with(ui){
        verticalLayout {
            verticalLayout {
                backgroundColor = Color.WHITE
                textView("Step 1: Photos of damage on my side"){
                    textColor = R.color.colorPrimary
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    verticalMargin = dimen(R.dimen.activity_vertical_margin)
                    horizontalMargin = dimen(R.dimen.activity_horizontal_margin)
                }
            }

            linearLayout(){
                weightSum = 3f

                verticalLayout{
                    backgroundResource = R.drawable.purple_header
                    lparams(height = dip(5)){
                        weight = 1f
                    }
                }
            }

            verticalLayout {
                lparams(width = matchParent, height = matchParent){
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                }

                textView("Container Damage"){
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = resources.getColor(R.color.colorPrimary)
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    this.gravity = Gravity.CENTER_HORIZONTAL
                }

                view(){
                    backgroundColor = resources.getColor(R.color.colorPrimary)
                }.lparams(width = matchParent, height = dip(1)){
                    bottomMargin = dimen(R.dimen.list_item_vertical_padding)
                }

                cDmgBox = verticalLayout {
                    lparams(width = matchParent){
                        weight = 1f
                    }

                    backgroundResource = R.drawable.grey_bordered_round_btn
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER

                    linearLayout(){
                        lparams(){
                            bottomMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        containerDmgPhotoBtn = imageView() {
                            imageResource = R.drawable.add_photo
                        }.lparams(width = dip(60), height = dip(60)){
                            rightMargin = dip(PHOTO_HORIZONTAL_MARGIN )
                        }

                        cADisplay = imageView{
                            visibility = View.GONE
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            rightMargin = dip(PHOTO_HORIZONTAL_MARGIN )
                        }

                        cBDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN )
                        }

                        cCDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                        cDDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        cEDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            leftMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                    }

                    linearLayout(){
                        lparams(){
                            topMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                        cFDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            rightMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        cGDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        cHDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                        cIDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        cJDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            leftMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                    }
                }

                textView("Vehicle Damage"){
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = resources.getColor(R.color.colorPrimary)
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    this.gravity = Gravity.CENTER_HORIZONTAL
                    topMargin = dimen(R.dimen.activity_vertical_margin)
                }

                view(){
                    backgroundColor = resources.getColor(R.color.colorPrimary)
                }.lparams(width = matchParent, height = dip(1)){
                    bottomMargin = dimen(R.dimen.list_item_vertical_padding)
                }

                vDmgBox = verticalLayout {
                    lparams(width = matchParent){
                        weight = 1f
                    }

                    backgroundResource = R.drawable.grey_bordered_round_btn
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER

                    linearLayout(){

                        lparams(){
                            bottomMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        vehicleDmgPhotoBtn = imageView() {
                            imageResource = R.drawable.add_photo
                        }.lparams(width = dip(60), height = dip(60)){
                            rightMargin = dip(PHOTO_HORIZONTAL_MARGIN )
                        }

                        vADisplay = imageView{
                            visibility = View.GONE
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            rightMargin = dip(PHOTO_HORIZONTAL_MARGIN )
                        }

                        vBDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN )
                        }

                        vCDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                        vDDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        vEDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            leftMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                    }

                    linearLayout(){
                        lparams(){
                            topMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                        vFDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            rightMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        vGDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        vHDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                        vIDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            horizontalMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }

                        vJDisplay = imageView{
                            imageResource = R.drawable.dashed_square
                        }.lparams(width = dip(60), height = dip(60)){
                            leftMargin = dip(PHOTO_HORIZONTAL_MARGIN)
                        }
                    }
                }

                nextBtn = button("Next"){
                    visibility = View.GONE
                    backgroundResource = R.drawable.green_rounded_button
                    textSize = MEDIUM_SIZE_TEXT
                    textColor = Color.WHITE
                }.lparams(width = matchParent){
                    topMargin = dimen(R.dimen.activity_vertical_margin)
                }
            }
        }
    }

}