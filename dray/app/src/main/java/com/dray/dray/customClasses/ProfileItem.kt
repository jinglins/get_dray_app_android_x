package com.dray.dray.customClasses

class ProfileItem (val label : String, val carrier_data: String) {
    fun print() {
        println("Label: " + label + "; data: " + carrier_data)
    }
}