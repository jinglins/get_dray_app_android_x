package com.dray.dray.paymentActivity

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import com.dray.dray.R
import org.jetbrains.anko.*
import androidx.appcompat.app.AlertDialog
import android.view.*
import android.widget.PopupMenu


class CurrentPaymentFragmentActivity : androidx.fragment.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //return inflater.inflate(R.layout.fragment_payment_current, container, false)
        return inflater.inflate(R.layout.fragment_payment_history, container, false)

        val MEDIUM_TEXT_SIZE = 18f
        val EXTRA_SMALL_TEXT_SIZE = 8f
        val LARGE_TEXT_SIZE = 36f

        // Read data from backend
        /*val dummyDataPayment = object {
            var directDeposit = object {
                var method = object {
                    var exist = false
                    var accountNumber = "1234567890"
                    var routeNumber = "123456789"
                    var bank = "Chase"
                    var type = "checking"
                    var paidFrequency = "3"
                }
                var pending = object {
                    var exist = true
                    var amount = 1233.45
                    var date = "12/23/2018"
                }
            }
            var check = object {
                var pending = object {
                    var exist = false
                    var amount = 2270.67
                    var date = "11/23/2018"
                }
            }
        }


        return UI {
            scrollView{
                verticalLayout(){
                    lparams(width = matchParent, height = matchParent)

                    verticalLayout(R.style.ThemeOverlay_AppCompat_Dark){
                        gravity = Gravity.CENTER
                        backgroundResource = R.drawable.purple_header
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.activity_vertical_padding)
                        }

                        textView("Direct Deposit"){
                            this.gravity = Gravity.CENTER_HORIZONTAL
                        }

                        if (dummyDataPayment.directDeposit.pending.exist){
                            textView("PENDING"){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                textSize = EXTRA_SMALL_TEXT_SIZE
                            }.lparams(){
                                topMargin = dip(10)
                            }
                            textView("$"+dummyDataPayment.directDeposit.pending.amount){
                                textSize = LARGE_TEXT_SIZE
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView("Estimate to arrive on ${dummyDataPayment.directDeposit.pending.date}"){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }

                    }

                    verticalLayout(){
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.activity_vertical_padding)
                            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                        }

                        if (dummyDataPayment.directDeposit.method.exist){
                            linearLayout{
                                backgroundColor = Color.WHITE
                                lparams(width = matchParent){
                                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                                }

                                verticalLayout{
                                    lparams(){
                                        weight = 1f
                                    }
                                    textView(dummyDataPayment.directDeposit.method.type.capitalize() + " Account"){
                                        textColor = R.color.colorPrimary

                                    }.lparams(width = matchParent, height = wrapContent)

                                    textView("End in ${dummyDataPayment.directDeposit.method.accountNumber.takeLast(4)}"){
                                        textSize = MEDIUM_TEXT_SIZE
                                    }

                                    textView(dummyDataPayment.directDeposit.method.bank + " Bank"){
                                    }.lparams(){
                                        topMargin = dimen(R.dimen.activity_vertical_margin)
                                    }

                                    textView("Get Paid every ${dummyDataPayment.directDeposit.method.paidFrequency} days"){
                                    }.lparams(){
                                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                                    }
                                }

                                imageButton(R.mipmap.ic_more_vert){
                                    backgroundColor = Color.TRANSPARENT
                                    onClick {
                                        println("clicked more button")
                                        var popupMenu = PopupMenu(context, this)
                                        popupMenu.menuInflater.inflate(R.menu.direct_deposit_popup, popupMenu.menu)
                                        // popupMenu set onitemclicklistener
                                        popupMenu.setOnMenuItemClickListener {
                                            println("menu item$it")
                                            if (it.title == "Edit"){
                                                // Go to New Direct Deposit Component
                                                val i = Intent(context, DirectDepositActivity::class.java)

                                                i.putExtra("ACTION", "EDIT")
                                                i.putExtra("ROUTING_NUMBER", dummyDataPayment.directDeposit.method.routeNumber)
                                                i.putExtra("ACCOUNT_NUMBER", dummyDataPayment.directDeposit.method.accountNumber)
                                                i.putExtra("ACCOUNT_TYPE", dummyDataPayment.directDeposit.method.type)
                                                i.putExtra("PAID_FREQ", dummyDataPayment.directDeposit.method.paidFrequency)

                                                startActivity(i)
                                            } else if (it.title == "Delete"){
                                                // Alert dialog for confirmation

                                                val builder = AlertDialog.Builder(context)

                                                builder.setMessage("Do you want to delete this account? ")
                                                builder.setPositiveButton("YES") { _, _ ->
                                                    // Refresh this orCustomerActivity
                                                    context.toast("Update the backend and refresh the orCustomerActivity")
                                                    // Refresh the orCustomerActivity
                                                    val intent = activity!!.intent
                                                    activity!!.finish()
                                                    startActivity(intent)
                                                }

                                                // Display a neutral button on alert dialog
                                                builder.setNeutralButton("Cancel") { dialog, _ ->
                                                    dialog.cancel()
                                                }

                                                // Finally, make the alert dialog using builder
                                                val dialog: AlertDialog = builder.create()

                                                // Display the alert dialog on app interface

                                                dialog.show()
                                                dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#808080"))
                                                dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#29cc9e"))

                                            }
                                            true
                                        }
                                        popupMenu.show()
                                    }
                                }.lparams(){
                                    weight = 0f
                                }
                            }
                        }

                        if (!dummyDataPayment.directDeposit.method.exist){
                            button("Set Up a New Direct Deposit"){
                                backgroundResource = R.drawable.green_rounded_button
                                textColor = Color.WHITE
                                onClick {
                                    val intent = Intent(activity, DirectDepositActivity::class.java)
                                    intent.putExtra("ACTION", "CREATE")
                                    startActivity(intent)
                                }
                            }.lparams(width= matchParent)
                        }
                    }

                    verticalLayout(R.style.ThemeOverlay_AppCompat_Dark){
                        gravity = Gravity.CENTER
                        backgroundResource = R.drawable.purple_header
                        lparams(width = matchParent){
                            verticalPadding = dimen(R.dimen.activity_vertical_padding)
                        }

                        textView("Check"){
                            this.gravity = Gravity.CENTER_HORIZONTAL
                        }

                        if (dummyDataPayment.check.pending.exist){
                            textView("PENDING"){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                                textSize = EXTRA_SMALL_TEXT_SIZE
                            }.lparams(){
                                topMargin = dip(10)
                            }
                            textView("$" + dummyDataPayment.check.pending.amount){
                                textSize = LARGE_TEXT_SIZE
                                typeface = Typeface.DEFAULT_BOLD
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView("Estimate to arrive on ${dummyDataPayment.check.pending.date}"){
                                this.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }

                    }

                    if (!dummyDataPayment.check.pending.exist && !dummyDataPayment.directDeposit.method.exist){
                        verticalLayout{
                            lparams(width = matchParent, height = wrapContent){
                                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                            }
                            button("Request a Check Now"){
                                backgroundResource = R.drawable.green_rounded_button
                                textColor = Color.WHITE
                            }.lparams(width= matchParent).onClick {
                                val i = Intent(context, CheckTripHistoryActivity::class.java)
                                startActivity(i)
                            }
                        }
                    }

                }
            }

        }.view*/
    }
}