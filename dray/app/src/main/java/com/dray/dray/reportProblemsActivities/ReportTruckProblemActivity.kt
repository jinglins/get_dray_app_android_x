package com.dray.dray.reportProblemsActivities

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ui.ReportTruckProblemComponent
import com.dray.dray.customClasses.setToolBar
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast

class ReportTruckProblemActivity : AppCompatActivity() {

    val ui = ReportTruckProblemComponent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        val toolbar = supportActionBar!!
        setToolBar(toolbar, "Report A Truck Problem")

        userInteraction ()
    }

    private fun userInteraction() {
        ui.noGasBtn.setOnClickListener {
            buildDialog("Do you want to report that your vehicle is out of gas?")
        }

        ui.engineBtn.setOnClickListener {
            buildDialog("Do you want to report an engine problem with your vehicle?")
        }

        ui.tireBtn.setOnClickListener {
            buildDialog("Do you want to report a flat tire problem")
        }

        ui.otherBtn.setOnClickListener {
            val reason = Dialog(this@ReportTruckProblemActivity)
            reason.setContentView(R.layout.report_problem_self_input_template)
            reason.findViewById<Button>(R.id.reportProblemSubmitBtn).setOnClickListener {
                val inputBox = reason.findViewById<EditText>(R.id.selfInputTextBox)
                val userInput =inputBox.text.toString()
                if (userInput == ""){
                    inputBox.error = "Please write something to help us understand your problem."
                } else {
                    reason.cancel()
                    toast("Thank you. We will come back to you with a solution as soon as we can.")
                    finish()
                }
            }

            reason.show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId === android.R.id.home) {
            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun buildDialog (message : String){
        val builder = AlertDialog.Builder(this@ReportTruckProblemActivity)

        builder.setPositiveButton("YES") { dialog, which ->
            // Go to previous orCustomerActivity
            Toast.makeText(this@ReportTruckProblemActivity,
                    "Thank you. We will come back to you with a solution as soon as we can.",
                    Toast.LENGTH_LONG).show()
            finish()
        }

        builder.setMessage(message)

        // Display a neutral button on alert dialog
        builder.setNeutralButton("Cancel") { dialog, _ ->
            dialog.cancel()
        }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface

        dialog.show()
        dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#808080"))
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#29cc9e"))
    }

}