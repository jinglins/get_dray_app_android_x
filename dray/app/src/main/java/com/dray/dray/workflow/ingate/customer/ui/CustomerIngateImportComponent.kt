package com.dray.dray.workflow.ingate.customer.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.ingate.customer.CustomerIngateActivity
import kotlinx.android.synthetic.main.report_problems_btn.view.*
import org.jetbrains.anko.*

class CustomerIngateImportComponent : AnkoComponent<CustomerIngateActivity>, AnkoLogger {
    lateinit var containerNo : TextView
    lateinit var bolNo : TextView
    lateinit var appointmentNo : TextView
    lateinit var containerSize : TextView

    lateinit var appointmentBlock : LinearLayout
    lateinit var nextBtn : Button

    lateinit var reportBtn : LinearLayout
    val MEDIUM_SIZE_TEXT = 18f
    val DISPATCH_SIZE_TEXT = 24f
    val DATA_TEXT_SIZE = 30f

    override fun createView(ui: AnkoContext<CustomerIngateActivity>): View = with(ui){
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    bottomMargin = dimen(R.dimen.small_gap)
                }
                textView("Container #"){
                    textColor = R.color.colorPrimary
                }
                containerNo = textView("ABCD0000000"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    verticalMargin = dimen(R.dimen.small_gap)
                }
                textView("BOL #"){
                    textColor = R.color.colorPrimary
                }
                bolNo = textView("ONEYHKGUD4349600"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            appointmentBlock = verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    verticalMargin = dimen(R.dimen.small_gap)
                }
                textView("Appointment #"){
                    textColor = R.color.colorPrimary
                }
                appointmentNo = textView("CMS2"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            verticalLayout {
                backgroundColor = Color.WHITE
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                gravity = Gravity.CENTER_VERTICAL
                lparams(width = matchParent, height = matchParent){
                    weight = 1f
                    topMargin = dimen(R.dimen.small_gap)
                }
                textView("Container Size"){
                    textColor = R.color.colorPrimary
                }
                containerSize = textView("40HC"){
                    textColor = Color.BLACK
                    textSize = DATA_TEXT_SIZE
                }
            }

            nextBtn = button("Sign Proof of Delivery"){
                backgroundResource = R.drawable.green_rounded_button
                textColor = Color.WHITE
                textSize = MEDIUM_SIZE_TEXT
            }.lparams(width = matchParent){
                topMargin = dimen(R.dimen.activity_vertical_margin)
            }
        }
    }

}