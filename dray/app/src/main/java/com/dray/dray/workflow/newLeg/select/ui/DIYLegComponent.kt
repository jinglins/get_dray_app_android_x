package com.dray.dray.workflow.newLeg.select.ui

import android.graphics.Color
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.room.EntityDeletionOrUpdateAdapter
import com.dray.dray.R
import com.dray.dray.workflow.newLeg.select.SelectNewLegActivity
import org.jetbrains.anko.*

class DIYLegComponent : AnkoComponent<SelectNewLegActivity>, AnkoLogger {
    val LABEL_WIDTH = 100

    lateinit var equipmentName : TextView
    lateinit var equipmentType : TextView
    lateinit var releaseNumber : EditText
    lateinit var providerName : EditText

    lateinit var legType : TextView
    lateinit var freeFlowFlag : CheckBox
    lateinit var importTerminalExtractionFlag : CheckBox
    lateinit var exportTerminalExtractionFlag : CheckBox

    lateinit var importReceiveType : TextView
    lateinit var bolNumber : EditText
    lateinit var freeflowCode : EditText
    lateinit var importSteamshipLine : EditText

    lateinit var exportReceiveType: TextView
    lateinit var bookingNumber : EditText
    lateinit var poNumber : EditText
    lateinit var exportSteamshipLine : EditText

    lateinit var containerNumber : EditText
    lateinit var containerSize : TextView
    lateinit var exContainerSize : TextView
    lateinit var containerWeight : EditText

    lateinit var importContainerInfoBox : LinearLayout

    lateinit var imPickUpPlaceName : EditText
    lateinit var imPickUpApptTime : EditText
    lateinit var imPickUpApptNumber : EditText

    lateinit var imDeliverPlaceName : EditText
    lateinit var imDeliverToApptTime : EditText
    lateinit var imDeliverToApptNumber : EditText

    lateinit var imFlat : EditText
    lateinit var imFuel : EditText
    lateinit var imBonus : EditText
    lateinit var imOffHire : EditText

    lateinit var exPickUpPlaceName : EditText
    lateinit var exPickUpApptTime : EditText
    lateinit var exPickUpApptNumber : EditText

    lateinit var exDeliverPlaceName : EditText
    lateinit var exDeliverToApptTime : EditText
    lateinit var exDeliverToApptNumber : EditText

    lateinit var exFlat : EditText
    lateinit var exFuel : EditText
    lateinit var exBonus : EditText
    lateinit var exOffHire : EditText

    lateinit var importForm : LinearLayout
    lateinit var exportForm : LinearLayout

    lateinit var reuseBtn : Button
    lateinit var confirmBtn : Button
    override fun createView(ui: AnkoContext<SelectNewLegActivity>): View = with (ui){
        scrollView(){
            lparams(width = matchParent, height = matchParent)
            verticalLayout {
                lparams(width = matchParent){
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                }

                reuseBtn = button("Use Data from Last Time"){
                    backgroundResource = R.drawable.white_rounded_button
                    textColor = resources.getColor(R.color.defaultLightTxtColor)
                }

                verticalLayout {
                    backgroundColor = Color.WHITE
                    lparams(width = matchParent) {
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    }
                    linearLayout {
                        textView("Type").lparams(width = dip(LABEL_WIDTH))
                        legType = textView("Please select one")
                    }
                }

                verticalLayout {
                    lparams(width = matchParent){
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    }
                    backgroundColor = Color.WHITE
                    textView("Equipment Info"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        bottomMargin = dip(10)
                    }

                    linearLayout {
                        textView("Equipment Needed").lparams(width = dip(LABEL_WIDTH))
                        equipmentName = textView("Chassis").lparams(width = matchParent)
                    }

                    linearLayout {
                        lparams(){
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                        textView("Type").lparams(width = dip(LABEL_WIDTH))
                        equipmentType = textView("Please select one type.").lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Release#").lparams(width = dip(LABEL_WIDTH))
                        releaseNumber = editText(){
                            hint = "Enter release number."
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Provider Name").lparams(width = dip(LABEL_WIDTH))
                        providerName = editText(){
                            hint = "Please enter provider name."
                        }.lparams(width = matchParent)
                    }
                }

                importForm = verticalLayout {

                    lparams(width = matchParent){
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    }
                    backgroundColor = Color.WHITE

                    textView("IMPORT"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    linearLayout {
                        textView("Add Flags").lparams(width = dip(LABEL_WIDTH))
                        verticalLayout {
                            freeFlowFlag = checkBox("Free Flow")
                            importTerminalExtractionFlag = checkBox("Terminal Extraction")
                        }
                    }

                    linearLayout {
                        lparams(){
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                        textView("Receive Type").lparams(width = dip(LABEL_WIDTH))
                        importReceiveType = textView("Please select one").lparams(width = matchParent)
                    }
                    linearLayout {
                        textView("BOL#").lparams(width = dip(LABEL_WIDTH))
                        bolNumber = editText(){
                            hint = "Enter BOL#."
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Steamship Line").lparams(width = dip(LABEL_WIDTH))
                        importSteamshipLine = editText(){
                            hint = "Enter steamship line."
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Freeflow Code").lparams(width = dip(LABEL_WIDTH))
                        freeflowCode = editText(){
                            hint = "If freeflow."
                        }.lparams(width = matchParent)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.backgroundGrey)
                    }.lparams(width = matchParent, height = dip(1))

                    importContainerInfoBox = verticalLayout {
                        linearLayout {
                            textView("Container#").lparams(width = dip(LABEL_WIDTH))
                            containerNumber = editText(){
                                hint = "AAAA0000000"
                            }.lparams(width = matchParent)
                        }

                        linearLayout {
                            lparams(){
                                verticalMargin = dimen(R.dimen.item_vertical_margin)
                            }
                            textView("Size").lparams(width = dip(LABEL_WIDTH))
                            containerSize = textView("Please select a size.").lparams(width = matchParent)
                        }

                        linearLayout {
                            textView("Weight").lparams(width = dip(LABEL_WIDTH))
                            containerWeight = editText(){
                                hint = "An integer. "
                            }.lparams(width = matchParent)
                        }

                        view(){
                            backgroundColor = resources.getColor(R.color.backgroundGrey)
                        }.lparams(width = matchParent, height = dip(1))
                    }

                    textView("Pick Up Info"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    linearLayout {
                        textView("Place Name").lparams(width = dip(LABEL_WIDTH))
                        imPickUpPlaceName = editText(){
                            hint = "'YARD' if pick up from yard."
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Appointment Time").lparams(width = dip(LABEL_WIDTH))
                        imPickUpApptTime = editText(){
                            hint = "MM/DD/YYYY hh:mm:ss"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Appointment Number").lparams(width = dip(LABEL_WIDTH))
                        imPickUpApptNumber = editText(){
                            hint = "Leave it blank if none."
                        }.lparams(width = matchParent)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.backgroundGrey)
                    }.lparams(width = matchParent, height = dip(1))

                    textView("Deliver To Info"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    linearLayout {
                        textView("Place Name").lparams(width = dip(LABEL_WIDTH))
                        imDeliverPlaceName = editText(){
                            hint = "No effect if terminal extraction."
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Appointment Time").lparams(width = dip(LABEL_WIDTH))
                        imDeliverToApptTime = editText(){
                            hint = "MM/DD/YYYY hh:mm:ss"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Appointment Number").lparams(width = dip(LABEL_WIDTH))
                        imDeliverToApptNumber = editText(){
                            hint = "Leave it blank if none."
                        }.lparams(width = matchParent)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.backgroundGrey)
                    }.lparams(width = matchParent, height = dip(1))

                    linearLayout {
                        textView("Flat Rate").lparams(width = dip(LABEL_WIDTH))
                        imFlat = editText(){
                            hint = "0.00"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Fuel Rate").lparams(width = dip(LABEL_WIDTH))
                        imFuel = editText(){
                            hint = "0.00"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Bonus Rate").lparams(width = dip(LABEL_WIDTH))
                        imBonus = editText(){
                            hint = "0.00"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Off Hire").lparams(width = dip(LABEL_WIDTH))
                        imOffHire = editText(){
                            hint = "0.00"
                        }.lparams(width = matchParent)
                    }
                }

                // Export
                exportForm = verticalLayout {
                    lparams(width = matchParent){
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    }
                    backgroundColor = Color.WHITE

                    textView("EXPORT"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    linearLayout {
                        textView("Add Flags").lparams(width = dip(LABEL_WIDTH))
                        verticalLayout {
                            exportTerminalExtractionFlag = checkBox("Terminal Extraction")
                        }
                    }

                    linearLayout {
                        lparams(){
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                        textView("Receive Type").lparams(width = dip(LABEL_WIDTH))
                        exportReceiveType = textView("Please select one").lparams(width = matchParent)
                    }
                    linearLayout {
                        textView("Booking#").lparams(width = dip(LABEL_WIDTH))
                        bookingNumber = editText(){
                            hint = "Enter booking#."
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("P.O.#/Reference#").lparams(width = dip(LABEL_WIDTH))
                        poNumber = editText(){
                            hint = "Enter P.O#."
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Steamship Line").lparams(width = dip(LABEL_WIDTH))
                        exportSteamshipLine = editText(){
                            hint = "Enter steamship line."
                        }.lparams(width = matchParent)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.backgroundGrey)
                    }.lparams(width = matchParent, height = dip(1))

                    linearLayout {
                        lparams(){
                            verticalMargin = dimen(R.dimen.item_vertical_margin)
                        }
                        textView("Size").lparams(width = dip(LABEL_WIDTH))
                        exContainerSize = textView("Please select a size.").lparams(width = matchParent)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.backgroundGrey)
                    }.lparams(width = matchParent, height = dip(1))

                    textView("Pick Up Info"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    linearLayout {
                        textView("Place Name").lparams(width = dip(LABEL_WIDTH))
                        exPickUpPlaceName = editText(){
                            hint = "'YARD' if pick up from yard."
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Appointment Time").lparams(width = dip(LABEL_WIDTH))
                        exPickUpApptTime = editText(){
                            hint = "MM/DD/YYYY hh:mm:ss"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Appointment Number").lparams(width = dip(LABEL_WIDTH))
                        exPickUpApptNumber = editText(){
                            hint = "Leave it blank if none."
                        }.lparams(width = matchParent)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.backgroundGrey)
                    }.lparams(width = matchParent, height = dip(1))

                    textView("Deliver To Info"){
                        textColor = R.color.colorPrimary
                    }.lparams(){
                        this.gravity = Gravity.CENTER_HORIZONTAL
                        verticalMargin = dimen(R.dimen.item_vertical_margin)
                    }

                    linearLayout {
                        textView("Place Name").lparams(width = dip(LABEL_WIDTH))
                        exDeliverPlaceName = editText(){
                            hint = "No effect if terminal extraction."
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Appointment Time").lparams(width = dip(LABEL_WIDTH))
                        exDeliverToApptTime = editText(){
                            hint = "MM/DD/YYYY hh:mm:ss"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Appointment Number").lparams(width = dip(LABEL_WIDTH))
                        exDeliverToApptNumber = editText(){
                            hint = "Leave it blank if none."
                        }.lparams(width = matchParent)
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.backgroundGrey)
                    }.lparams(width = matchParent, height = dip(1))

                    linearLayout {
                        textView("Flat Rate").lparams(width = dip(LABEL_WIDTH))
                        exFlat = editText(){
                            hint = "0.00"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Fuel Rate").lparams(width = dip(LABEL_WIDTH))
                        exFuel = editText(){
                            hint = "0.00"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Bonus Rate").lparams(width = dip(LABEL_WIDTH))
                        exBonus = editText(){
                            hint = "0.00"
                        }.lparams(width = matchParent)
                    }

                    linearLayout {
                        textView("Off Hire").lparams(width = dip(LABEL_WIDTH))
                        exOffHire = editText(){
                            hint = "0.00"
                        }.lparams(width = matchParent)
                    }
                }

                confirmBtn = button("Confirm"){
                    backgroundResource = R.drawable.green_rounded_button
                    textColor = Color.WHITE
                }
            }
        }
    }
}