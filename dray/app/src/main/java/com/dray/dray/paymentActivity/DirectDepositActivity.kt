package com.dray.dray.paymentActivity

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.paymentActivity.ui.DirectDepositComponent
import kotlinx.android.synthetic.main.activity_sign_pod.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.textColor

class DirectDepositActivity : AppCompatActivity() {

    val ui = DirectDepositComponent()
    var accountType = "checking"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ui.setContentView(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)

        if (intent.getStringExtra("ACTION") == "EDIT"){
            supportActionBar!!.setTitle(R.string.payment_changeActivity_title)
            ui.routingNumber.setText(intent.getStringExtra("ROUTING_NUMBER"))
            ui.accountNumber.setText(intent.getStringExtra("ACCOUNT_NUMBER"))
            if (intent.getStringExtra("ACCOUNT_TYPE") == "checking"){
                clearSelection()
                ui.typeCheck.backgroundResource = R.drawable.green_rounded_button
                ui.typeCheckTxt.textColor = Color.WHITE
                accountType = "checking"
            } else {
                clearSelection()
                ui.typeSave.backgroundResource = R.drawable.green_rounded_button
                ui.typeSaveTxt.textColor = Color.WHITE
                accountType = "savings"
            }
            ui.paidFrequencyTxt.text = intent.getStringExtra("PAID_FREQ")
        } else {
            supportActionBar!!.setTitle(R.string.payment_setUpActivity_title)
        }


        ui.typeCheck.setOnClickListener {
            clearSelection()
            ui.typeCheck.backgroundResource = R.drawable.green_rounded_button
            ui.typeCheckTxt.textColor = Color.WHITE
            accountType = "checking"
        }

        ui.typeSave.setOnClickListener {
            clearSelection()
            ui.typeSave.backgroundResource = R.drawable.green_rounded_button
            ui.typeSaveTxt.textColor = Color.WHITE
            accountType = "savings"
        }

        ui.paidFrequencySelection.setOnClickListener {
            val paidFreqs = arrayOf("2", "3", "7", "14")

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Please select a number")
            builder.setItems(paidFreqs){_, which ->
                ui.paidFrequencyTxt.text = paidFreqs[which]
            }
            builder.show()
        }

        ui.finishBtn.setOnClickListener {
            // Set error indicators to default
            var hasError = false
            val errorMsgs = listOf<TextView>(ui.accountNumberDigitCountError,
                    ui.accountNumberConfirmFailError, ui.routingNumberDigitCountError,
                    ui.routingNumberConfirmFailError)
            errorMsgs.forEach { it ->
                it.visibility = View.GONE
            }

            // Check
            if (ui.routingNumber.text.length != 9){
                ui.routingNumber.error = getString(R.string.routing_number_count_error)
                ui.routingNumberDigitCountError.visibility = View.VISIBLE
                hasError = true
            }

            if (ui.routingNumber.text.toString() == ui.confirmRoutingNumber.text.toString()){

            } else {
                ui.confirmRoutingNumber.error = getString(R.string.confirm_routing_number_error)
                ui.routingNumberConfirmFailError.visibility = View.VISIBLE
                hasError = true
            }

            if (ui.accountNumber.text.length < 8 || ui.accountNumber.text.length > 12){
                ui.accountNumber.error = getString(R.string.account_number_count_error)
                ui.accountNumberDigitCountError.visibility = View.VISIBLE
                hasError = true
            }

            if (ui.accountNumber.text.toString() == ui.confirmAccountNumber.text.toString()){

            } else {
                ui.confirmAccountNumber.error = getString(R.string.confirm_account_number_error)
                ui.accountNumberConfirmFailError.visibility = View.VISIBLE
                hasError = true
            }

            if (!hasError){
                // Send data to third party api
                val routingNumber = ui.routingNumber.text.toString()
                val accountNumber = ui.accountNumber.text.toString()
                val paidFreq = ui.paidFrequencyTxt.text.toString()
                // Send routing number, accountnumber, accounttype, paidfrequency
                val i = Intent(this, PaymentActivity::class.java)
                startActivity(i)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.getItemId() === android.R.id.home) {
            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun clearSelection(){
        val types = listOf<LinearLayout>(ui.typeCheck, ui.typeSave)
        val typeTxts = listOf<TextView>(ui.typeCheckTxt, ui.typeSaveTxt)
        types.forEach {
            it.backgroundResource = R.drawable.dashed_square
        }

        typeTxts.forEach {
            it.textColor = Color.parseColor("#808080")
        }
    }
}
