package com.dray.dray.helpActivities

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.dray.dray.R
import com.dray.dray.helpActivities.ui.HelpTicketDetailComponent
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast

class HelpTicketDetailActivity : AppCompatActivity() {

    //lateinit var status : String
    val ui = HelpTicketDetailComponent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setTitle(R.string.title_activity_help)

        // Set correct text for textviews
        val status = intent.getStringExtra("STATUS")
        val ticketNumber = intent.getStringExtra("TICKET_NUMBER")
        val dateReceived = intent.getStringExtra("DATE_RECEIVED")
        val description = intent.getStringExtra("DESCRIPTION")
        val solution = intent.getStringExtra("SOLUTION")


        ui.solution.text = solution

        val ticketDisplay = "Ticket No.$ticketNumber"
        ui.ticketNumber.text = ticketDisplay
        ui.dateReceived.text = dateReceived
        ui.description.text = description

        if (status == "Just solved"){
            ui.status.imageResource = R.drawable.green_rounded_button

            ui.solvedBtn.setOnClickListener {
                // Send {ticket number, status = "solved"} to backend
                finish()
            }

            ui.reopenBtn.setOnClickListener {
                val reason = Dialog(this@HelpTicketDetailActivity)
                reason.setContentView(R.layout.reopen_comment_template)
                reason.findViewById<Button>(R.id.commentSubmitBtn).setOnClickListener {
                    val commentBox = reason.findViewById<EditText>(R.id.commentTextBox)
                    val comment = commentBox.text.toString()
                    println("comment : $comment")
                    if (comment == ""){
                        commentBox.error = "Please write something to help us understand your problem."
                    } else {
                        // send {comment, ticket number, status = "received"} to backend
                        reason.cancel()
                        toast("This ticket is reopened. ")
                        finish()
                    }
                }

                reason.show()
            }

        } else if (status == "Solved") {
            ui.solvedBtn.visibility = View.GONE
            ui.reopenBtn.visibility = View.GONE
        } else {
            // hide solution, show only cancel buton
            ui.solutionDisplay.visibility = View.GONE
            ui.solvedBtn.visibility = View.GONE
            ui.reopenBtn.visibility = View.GONE
            ui.cancelBtn.visibility = View.VISIBLE

            if (status == "Solving"){
                ui.status.imageResource = R.drawable.orange_rounded_item
            } else {
                ui.status.imageResource = R.drawable.grey_rounded_item
            }

            ui.cancelBtn.setOnClickListener {
                val builder = AlertDialog.Builder(this@HelpTicketDetailActivity)

                builder.setMessage("Do you want to cancel this ticket? ")
                builder.setPositiveButton("YES") { dialog, which ->

                    finish()
                }

                // Display a neutral button on alert dialog
                builder.setNeutralButton("Cancel") { dialog, _ ->

                    dialog.cancel()
                }

                // Finally, make the alert dialog using builder
                val dialog: AlertDialog = builder.create()

                // Display the alert dialog on app interface

                dialog.show()
                dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#808080"))
                dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#29cc9e"))
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId === android.R.id.home) {
            finish() // close this orCustomerActivity and return to preview orCustomerActivity (if there is any)
        }

        return super.onOptionsItemSelected(item)
    }
}
