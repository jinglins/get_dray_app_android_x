package com.dray.dray.extensions

import android.content.Context
import android.os.Build
import androidx.annotation.AttrRes
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import android.util.TypedValue
import android.view.View
import android.widget.FrameLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.ctx
import org.jetbrains.anko.wrapContent

fun collapseModePin(): com.google.android.material.appbar.CollapsingToolbarLayout.LayoutParams.() -> Unit = { collapseMode = com.google.android.material.appbar.CollapsingToolbarLayout.LayoutParams.COLLAPSE_MODE_PIN }
fun lParamsWithScrollFlags(): com.google.android.material.appbar.AppBarLayout.LayoutParams.() -> Unit = { scrollFlags = com.google.android.material.appbar.AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or com.google.android.material.appbar.AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED }
fun lParamsDefault(): com.google.android.material.appbar.CollapsingToolbarLayout.LayoutParams.() -> Unit = {}

fun Context.snackbar(view: View, text: CharSequence, length: Int = com.google.android.material.snackbar.Snackbar.LENGTH_SHORT, snackbar: com.google.android.material.snackbar.Snackbar.() -> Unit) = com.google.android.material.snackbar.Snackbar.make(view, text, length).apply { snackbar() }.show()
fun View.snackbar(text: CharSequence, length: Int = com.google.android.material.snackbar.Snackbar.LENGTH_SHORT, snackbar: com.google.android.material.snackbar.Snackbar.() -> Unit) = context.snackbar(this, text, length, snackbar)
fun androidx.fragment.app.Fragment.snackbar(view: View, text: CharSequence, length: Int = com.google.android.material.snackbar.Snackbar.LENGTH_SHORT, snackbar: com.google.android.material.snackbar.Snackbar.() -> Unit) = requireActivity().snackbar(view, text, length, snackbar)

fun Context.attr(@AttrRes attribute: Int): TypedValue {
    val typed = TypedValue()
    ctx.theme.resolveAttribute(attribute, typed, true)
    return typed
}

//returns px
fun Context.dimenAttr(@AttrRes attribute: Int): Int = TypedValue.complexToDimensionPixelSize(attr(attribute).data, resources.displayMetrics)

//returns color
fun Context.colorAttr(@AttrRes attribute: Int): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        resources.getColor(attr(attribute).resourceId, ctx.theme)
    } else {
        @Suppress("DEPRECATION")
        resources.getColor(attr(attribute).resourceId)
    }
}

fun AnkoContext<*>.dimenAttr(@AttrRes attribute: Int): Int = ctx.dimenAttr(attribute)
fun AnkoContext<*>.colorAttr(@AttrRes attribute: Int): Int = ctx.colorAttr(attribute)
fun AnkoContext<*>.attribute(@AttrRes attribute: Int): TypedValue = ctx.attr(attribute)

fun View.dimenAttr(@AttrRes attribute: Int): Int = context.dimenAttr(attribute)
fun View.colorAttr(@AttrRes attribute: Int): Int = context.colorAttr(attribute)
fun View.attr(@AttrRes attribute: Int): TypedValue = context.attr(attribute)

fun androidx.fragment.app.Fragment.dimenAttr(@AttrRes attribute: Int): Int = requireActivity().dimenAttr(attribute)
fun androidx.fragment.app.Fragment.colorAttr(@AttrRes attribute: Int): Int = requireActivity().colorAttr(attribute)
fun androidx.fragment.app.Fragment.attr(@AttrRes attribute: Int): TypedValue = requireActivity().attr(attribute)

object FrameLayout {
    fun <T : View> T.lparams(
        width: kotlin.Int = wrapContent, height: kotlin.Int = wrapContent,
        init: FrameLayout.LayoutParams.() -> kotlin.Unit = {}): T {
        val layoutParams = FrameLayout.LayoutParams(width, height)
        layoutParams.init()
        this@lparams.layoutParams = layoutParams
        return this
    }
}

object CollapsingToolbar {
    fun <T : View> T.lparams(
        width: kotlin.Int = wrapContent, height: kotlin.Int = wrapContent,
        init: com.google.android.material.appbar.CollapsingToolbarLayout.LayoutParams.() -> kotlin.Unit = {}): T {
        val layoutParams = com.google.android.material.appbar.CollapsingToolbarLayout.LayoutParams(width, height)
        layoutParams.init()
        this@lparams.layoutParams = layoutParams
        return this
    }
}
