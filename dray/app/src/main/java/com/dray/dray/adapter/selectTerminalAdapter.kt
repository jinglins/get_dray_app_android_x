package com.dray.dray.adapter

import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.dray.dray.R
import com.dray.dray.workflow.selectTerminalOrCustomer.SelectTerminalOrCustomerActivity
import org.jetbrains.anko.*

class selectTerminalAdapter (val orCustomerActivity: SelectTerminalOrCustomerActivity, var returnToTerminals : ArrayList<String>) : BaseAdapter(){
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val item : String = getItem(p0)
        return with (p2!!.context){
            verticalLayout {
                backgroundResource = R.drawable.grey_bordered_round_btn
                verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)

                textView(item) {
                    textSize = 24f
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams(){
                    this.gravity = Gravity.CENTER_HORIZONTAL
                }
            }
        }
    }

    override fun getItem(p0: Int): String {
        return returnToTerminals[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return returnToTerminals.count()
    }

}