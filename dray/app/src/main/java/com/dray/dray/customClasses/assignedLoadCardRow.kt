package com.dray.dray.customClasses

// Definition of object for simple rendering in anko - assignedLoadItemUI

class assignedLoadCardRow (val label : String, val load_data: String) {
    fun print() {
        println("Label: " + label + "; data: " + load_data)
    }
}