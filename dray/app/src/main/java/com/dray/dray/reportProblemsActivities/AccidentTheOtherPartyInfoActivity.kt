package com.dray.dray.reportProblemsActivities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.dray.dray.R
import com.dray.dray.reportProblemsActivities.ui.AccidentTheOtherPartyInfoComponent
import org.jetbrains.anko.*
import java.io.ByteArrayOutputStream
import java.io.OutputStream

class AccidentTheOtherPartyInfoActivity : AppCompatActivity() {
    val REQUEST_DDL_PHOTO = 133
    val REQUEST_INSURANCE_PHOTO = 135
    val REQUEST_DAMAGE_PHOTO = 137
    var photoCount = 0
    var photoJPEGs = ArrayList<OutputStream>()
    var photoDisplays = ArrayList<ImageView>()

    // Name, phone, company, email, ddl, insurance, vehicle damage
    var blankFilledBools = arrayListOf<Int>(0, 0, 0, 0, 0, 0)
    var errorMsgs = arrayListOf<String>(
            "the name", "the phone number", "the company", "the email",
            "the driver's driving license", "the insurance document",
            "vehicle damages"
    )

    val ui = AccidentTheOtherPartyInfoComponent()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        // HIDE PLACEHOLDER FOR DAMAGE PHOTO
        photoDisplays = arrayListOf<ImageView>(
                ui.vADisplay, ui.vBDisplay, ui.vCDisplay, ui.vDDisplay, ui.vEDisplay,
                ui.vFDisplay, ui.vGDisplay, ui.vHDisplay, ui.vIDisplay, ui.vJDisplay)

        photoDisplays.forEach {
            it.visibility = View.GONE
        }

        userInputInteraction()

        ui.ddlPhotoBtn.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(packageManager)?.also {
                    startActivityForResult(takePictureIntent, REQUEST_DDL_PHOTO)
                }
            }
        }

        ui.insurancePhotoBtn.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(packageManager)?.also {
                    startActivityForResult(takePictureIntent, REQUEST_INSURANCE_PHOTO)
                }
            }
        }

        ui.damagePhotoBtn.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(packageManager)?.also {
                    startActivityForResult(takePictureIntent, REQUEST_DAMAGE_PHOTO)
                }
            }
        }

        ui.nextBtn.setOnClickListener {
            if (allRequiredFilled()) {
                val name = ui.nameInput.text
                val phoneNumber = ui.phoneInput.text
                val email = ui.emailInput.text
                val company = ui.companyInput.text

                val i = Intent (this, AccidentDiagramActivity::class.java)
                startActivity(i)
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            } else {
                toast ("Please make sure that you have filled all the blanks.")
            }


        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_DDL_PHOTO && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            ui.ddlPhotoBtn.visibility = View.GONE
            ui.ddlPhotoDisplay.setImageBitmap(imageBitmap)
            ui.ddlPhotoDisplay.visibility = View.VISIBLE
            ui.ddlPhotoBox.backgroundResource = R.drawable.purple_bordered_round_btn

            blankFilledBools[4] = 1
        }

        if (requestCode == REQUEST_INSURANCE_PHOTO && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            ui.insurancePhotoBtn.visibility = View.GONE
            ui.insurancePhotoDisplay.setImageBitmap(imageBitmap)
            ui.insurancePhotoDisplay.visibility = View.VISIBLE
            ui.insurancePhotoBox.backgroundResource = R.drawable.purple_bordered_round_btn

            blankFilledBools[5] = 1
        }

        if (requestCode == REQUEST_DAMAGE_PHOTO && resultCode == RESULT_OK) {
            // Get the image
            val imageBitmap = data?.extras?.get("data") as Bitmap

            val stream = ByteArrayOutputStream()
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            photoJPEGs.add(stream)

            photoCount ++

            if (photoCount == 1) {
                ui.vehicleDmgBox.backgroundResource = R.drawable.purple_bordered_round_btn
            }

            // Show the image in the corresponding display
            if (photoCount < 10){
                photoDisplays[photoCount].setImageBitmap(imageBitmap)
                photoDisplays[photoCount].visibility = View.VISIBLE
            } else {
                photoDisplays[0].setImageBitmap(imageBitmap)
                // Hide take photo button
                ui.damagePhotoBtn.visibility = View.GONE
                // Show the 0th image display area
                ui.vADisplay.visibility = View.VISIBLE
                // Display a toast saying "reach cap"
                toast("You may take at most 10 photos. ")
            }
        }
    }

    private fun userInputInteraction () {
        val defaultBorder = R.drawable.grey_bordered_round_btn
        val completeBorder = R.drawable.purple_bordered_round_btn

        ui.nameInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()){
                    ui.nameInputBox.backgroundResource = completeBorder
                    blankFilledBools[0] = 1
                } else {
                    ui.nameInputBox.backgroundResource = defaultBorder
                    blankFilledBools[0] = 0
                }
            }
        })

        ui.phoneInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()){
                    ui.phoneInputBox.backgroundResource = completeBorder
                    blankFilledBools[1] = 1
                } else {
                    ui.phoneInputBox.backgroundResource = defaultBorder
                    blankFilledBools[1] = 0
                }
            }
        })

        ui.companyInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()){
                    ui.companyInputBox.backgroundResource = completeBorder
                    blankFilledBools[2] = 1
                } else {
                    ui.companyInputBox.backgroundResource = defaultBorder
                    blankFilledBools[2] = 0
                }
            }
        })

        ui.emailInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()){
                    ui.emailInputBox.backgroundResource = completeBorder
                    blankFilledBools[3] = 1
                } else {
                    ui.emailInputBox.backgroundResource = defaultBorder
                    blankFilledBools[3] = 0
                }
            }
        })
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    private fun allRequiredFilled () : Boolean {
        blankFilledBools.forEach { it ->
            if (it == 0) {
                return false
            }
        }

        return true
    }

    private fun rotateBitmap (imageBitmap : Bitmap) : Bitmap{
        val bitmapWidth = imageBitmap.width
        val bitmapHeight = imageBitmap.height

        val matrix = Matrix()
        matrix.preRotate(-90f)

        return Bitmap.createBitmap(imageBitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true)
    }
}
