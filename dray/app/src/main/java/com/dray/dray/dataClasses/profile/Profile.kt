package com.dray.dray.dataClasses.profile

import androidx.room.*

@Entity(tableName = "profile" ,
        foreignKeys = [
            ForeignKey(entity = ProfileBasicInfo::class,
                    parentColumns = ["id"],
                    childColumns = ["basic_info_id"]),

            ForeignKey(entity = ProfileQualifiedToDrive::class,
                    parentColumns = ["id"],
                    childColumns = ["qualified_to_drive_id"]),

            ForeignKey(entity = ProfileWorkPreference::class,
                    parentColumns = ["id"],
                    childColumns = ["work_preference_id"]),

            ForeignKey(entity = ProfileTMC::class,
                    parentColumns = ["id"],
                    childColumns = ["tmc_info_id"]),

            ForeignKey(entity = ProfileCarrier::class,
                    parentColumns = ["id"],
                    childColumns = ["carrier_info_id"])
        ])
data class Profile (
        @PrimaryKey var uid : Int,

        @ColumnInfo (name = "basic_info_id")         var basicInfo : String ,
        @ColumnInfo (name = "qualified_to_drive_id") var qualifiedToDrive : String,
        @ColumnInfo (name = "work_preference_id")    var workPreference : String,
        @ColumnInfo (name = "tmc_info_id")           var TMCInfo : String ,
        @ColumnInfo (name = "carrier_info_id")       var carrierInfo : String
)