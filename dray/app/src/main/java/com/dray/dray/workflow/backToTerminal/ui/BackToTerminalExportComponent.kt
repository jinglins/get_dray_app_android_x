package com.dray.dray.workflow.backToTerminal.ui

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.dray.dray.R
import com.dray.dray.workflow.backToTerminal.BackToTerminalActivity
import org.jetbrains.anko.*

class BackToTerminalExportComponent : AnkoComponent<BackToTerminalActivity>, AnkoLogger{
    lateinit var bookingNo : TextView
    lateinit var containerNo : TextView
    lateinit var containerSize : TextView
    lateinit var SCACCode : TextView
    lateinit var appointmentNo : TextView
    lateinit var sealNo : TextView
    lateinit var chassisOwner : TextView
    lateinit var nextBtn : Button

    lateinit var containerNoBox : LinearLayout
    lateinit var sealNoBox : LinearLayout
    lateinit var appointmentBox : LinearLayout

    lateinit var reportBtn : LinearLayout

    val MEDIUM_SIZE_TEXT = 18f
    val DISPATCH_SIZE_TEXT = 24f
    val DATA_TEXT_SIZE = 24f
    val LABEL_WIDTH = 120

    override fun createView(ui: AnkoContext<BackToTerminalActivity>): View = with(ui){
        verticalLayout {
            verticalLayout{
                lparams(width = matchParent, height = matchParent){
                    verticalPadding = dimen(R.dimen.activity_vertical_padding)
                    horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
                }

                reportBtn = include<LinearLayout>(R.layout.report_problems_btn)

                verticalLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent){
                        bottomMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Booking #"){
                        textColor = R.color.colorPrimary
                    }
                    bookingNo = textView("8PHL056584"){
                        typeface = Typeface.DEFAULT_BOLD
                        textColor = Color.BLACK
                        textSize = 30f
                    }
                }

                appointmentBox = linearLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent){
                        verticalMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Appointment #"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(LABEL_WIDTH))
                    appointmentNo = textView("JCFOIWS"){
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }

                linearLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent){
                        verticalMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("SCAC Code"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(LABEL_WIDTH))
                    SCACCode = textView("HRBR"){
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }

                containerNoBox = linearLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent){
                        verticalMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Container #"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(LABEL_WIDTH))
                    containerNo = textView("ABCD0000000"){
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }

                linearLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent){
                        verticalMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Type/Size"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(LABEL_WIDTH))
                    containerSize = textView("40HC"){
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }

                sealNoBox = linearLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent){
                        verticalMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Seal #"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(LABEL_WIDTH))
                    sealNo = textView("VCASH98D3"){
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }

                linearLayout {
                    backgroundColor = Color.WHITE
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    gravity = Gravity.CENTER_VERTICAL
                    lparams(width = matchParent){
                        topMargin = dimen(R.dimen.small_gap)
                        weight = 1f
                    }
                    textView("Chassis Owner"){
                        textColor = R.color.colorPrimary
                    }.lparams(width = dip(LABEL_WIDTH))
                    chassisOwner = textView("POOL"){
                        textColor = Color.BLACK
                        textSize = DATA_TEXT_SIZE
                    }
                }


                nextBtn = button("Next"){
                    backgroundResource = R.drawable.green_rounded_button
                    textColor = Color.WHITE
                    textSize = MEDIUM_SIZE_TEXT
                }.lparams(width = matchParent){
                    topMargin = dimen(R.dimen.activity_vertical_margin)
                }
            }
        }
    }

}