package com.dray.dray.customClasses

class CheckTripHistoryItem (
        val containerNo: String, val chassisNo : String,
        val driveTime : String, val isEmpty: Boolean,
        val pickUpLoc : String, val pickUpTime : String,
        val deliverToLoc : String, val deliverToTime : String,
        val flat : String, val fuel : String, val offHire : String,
        val bonus: String, val addition : String, val deduction : String,
        var approved : Boolean, var disputed : Boolean,
        var approveTime : String, var disputeTime : String,
        var disputeReason : String) {

}