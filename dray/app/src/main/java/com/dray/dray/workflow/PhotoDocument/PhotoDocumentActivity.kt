package com.dray.dray.workflow.PhotoDocument

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.dray.dray.R
import com.dray.dray.customClasses.displayReportWindow
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.customClasses.setWorkFlowToolBar
import com.dray.dray.dataClasses.leg.Leg
import com.dray.dray.database.AppDatabase
import com.dray.dray.workflow.scheduleNextLoadQuestion.ScheduleNextLoadQuestionActivity
import com.dray.dray.workflow.mapActivity.MapActivity
import com.dray.dray.workflow.PhotoDocument.ui.PhotoDocumentComponent
import com.dray.dray.workflow.selectTerminalOrCustomer.SelectTerminalOrCustomerActivity
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast

class PhotoDocumentActivity : AppCompatActivity() {
    val ui = PhotoDocumentComponent()
    lateinit var db : AppDatabase
    lateinit var firstLegInfo : Leg
    lateinit var curPlace : String
    var photoTakenArray = arrayListOf<Int>(0, 0, 0)
    lateinit var completeSoundMP : MediaPlayer

    private val REQUEST_INTERCHANGE_DOC_CODE = 133//request code for capture image
    private val REQUEST_CONTAINER_NO_CODE = 135
    private val REQUEST_SEAL_NO_CODE = 137

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)

        curPlace = intent.getStringExtra("CUR_PLACE")
        val activityTitle = if (curPlace == "customer"){
            "Customer Out-Gate"
        } else {
            "Terminal Out-Gate"
        }

        val toolbar = supportActionBar!!
        setWorkFlowToolBar(toolbar, activityTitle)

        completeSoundMP = MediaPlayer.create(this, R.raw.complete)
        db = getDatabase(applicationContext)
        firstLegInfo = db.legDao().getLegInfoById(1)

        if (curPlace == "customer"){
            ui.interchangeDocBox.visibility = View.GONE
        }

        ui.interchangeDocBox.setOnClickListener { captureImage(REQUEST_INTERCHANGE_DOC_CODE) }
        ui.containerNoBox.setOnClickListener { captureImage(REQUEST_CONTAINER_NO_CODE) }
        ui.sealNoBox.setOnClickListener { captureImage(REQUEST_SEAL_NO_CODE) }

        ui.reportBtn.findViewById<Button>(R.id.report_btn).setOnClickListener {
            displayReportWindow(applicationContext, "false", "false")
        }

        ui.finishBtn.setOnClickListener {
            if (curPlace == "terminal"){
                // TODO: Record seal#/container# into objects
                if (firstLegInfo.flags.contains("TERMINAL_EXTRACTION")){
                    var i = Intent(this, MapActivity::class.java)
                    i.putExtra("TARGET", "YARD")
                    startActivity(i)
                } else {
                    var i = Intent(this, MapActivity::class.java)
                    i.putExtra("INGATE_STATUS", "withContainer")

                    if (firstLegInfo.flags.contains("FREE_FLOW") && multipleCustomers()){
                        // Check Internet Connect
                        // if (noInternet ())
                        toast("No internet connection.")

                        val builder = AlertDialog.Builder(this@PhotoDocumentActivity)

                        builder.setMessage("Please call a dispatcher for the customer to which this " +
                                "container should be delivered")
                        builder.setPositiveButton("Select The Customer") { _, _ ->
                            var i = Intent (this, SelectTerminalOrCustomerActivity :: class.java)
                            i.putExtra("CUR_PLACE", "terminal")
                            startActivity(i)
                        }

                        // Finally, make the alert dialog using builder
                        val dialog: AlertDialog = builder.create()

                        // Display the alert dialog on app interface
                        dialog.show()
                        dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#808080"))
                        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#29cc9e"))

                    } else {
                        i.putExtra("TARGET", "CUSTOMER")
                        startActivity(i)
                    }
                }
            } else {
                val exitStatus = intent.getStringExtra("EXIT_STATUS")
                if (firstLegInfo.flags.contains("STREETTURN")){
                    // if streetturn.preplanned
                    if (exitStatus == "empty"){
                        var i = Intent(this, MapActivity::class.java)
                        i.putExtra("TARGET", "EXPORT_FACILITY")
                        startActivity(i)
                    } else {
                        var i = Intent(this, ScheduleNextLoadQuestionActivity::class.java)
                        i.putExtra("CUR_PLACE", "customer")
                        startActivity(i)
                    }
                } else {
                    var i = Intent(this, ScheduleNextLoadQuestionActivity::class.java)
                    i.putExtra("CUR_PLACE", "customer")
                    startActivity(i)
                }
            }
        }
    }

    /*  Capture Image Method  */
    private fun captureImage(requestCode: Int) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, requestCode)
            }
        }
    }

    private fun multipleCustomers () : Boolean{
        val customerStr = db.legDao().getLegDeliverToInfo(1).destinationName
        var customerArr = customerStr.split(", ")
        return customerArr.size > 1
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            val imageBitmap = data!!.extras.get("data") as Bitmap

            when (requestCode){
                REQUEST_INTERCHANGE_DOC_CODE -> {
                    ui.interchangeDocBox.backgroundResource = R.drawable.purple_bordered_round_btn
                    ui.interchangeCameraIcon.visibility = View.GONE
                    ui.interchangeDocDisplayArea.setImageBitmap(imageBitmap)
                    ui.interchangeDocDisplayArea.visibility = View.VISIBLE
                    ui.interchangeDocDscrpt.text = "Thank you. Your photo of the interchange document is recorded. "

                    photoTakenArray[0] = 1
                    if (allPhotosAreTaken()){
                        ui.finishBtn.visibility = View.VISIBLE
                    }

                    completeSoundMP.start()
                }

                REQUEST_CONTAINER_NO_CODE -> {
                    ui.containerNoBox.backgroundResource = R.drawable.purple_bordered_round_btn
                    ui.containerNoCameraIcon.visibility = View.GONE
                    ui.containerNoDisplayArea.setImageBitmap(imageBitmap)
                    ui.containerNoDisplayArea.visibility = View.VISIBLE
                    ui.containerNoDscrpt.text = "Thank you. Your photo of the container number is recorded. "

                    photoTakenArray[1] = 1
                    if (allPhotosAreTaken()){
                        ui.finishBtn.visibility = View.VISIBLE
                    }
                    completeSoundMP.start()
                }

                REQUEST_SEAL_NO_CODE -> {
                    ui.sealNoBox.backgroundResource = R.drawable.purple_bordered_round_btn
                    ui.sealNoCameraIcon.visibility = View.GONE
                    ui.sealNoDisplayArea.setImageBitmap(imageBitmap)
                    ui.sealNoDisplayArea.visibility = View.VISIBLE
                    ui.sealNoDscrpt.text = "Thank you. Your photo of the seal number is recorded. "

                    photoTakenArray[2] = 1
                    if (allPhotosAreTaken()){
                        ui.finishBtn.visibility = View.VISIBLE
                    }
                    completeSoundMP.start()
                }
            }
        } else{
            Toast.makeText(this, R.string.cancel_message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun allPhotosAreTaken () : Boolean {
        if (curPlace == "terminal"){
            photoTakenArray.forEach { it ->
                if (it == 0){
                    return false
                }
            }

            return true
        } else {
            return photoTakenArray[1] == 1 && photoTakenArray[2] == 1
        }
    }
}
