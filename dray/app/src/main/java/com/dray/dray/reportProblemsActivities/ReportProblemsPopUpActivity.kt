package com.dray.dray.reportProblemsActivities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dray.dray.reportProblemsActivities.ui.ReportProblemsPopUpComponent
import org.jetbrains.anko.setContentView
import android.util.DisplayMetrics
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.dray.dray.R
import com.dray.dray.adapter.reportProblemsGridAdapter
import com.dray.dray.customClasses.countUpTimer
import com.dray.dray.customClasses.getDatabase
import com.dray.dray.database.AppDatabase
import org.jetbrains.anko.toast


class ReportProblemsPopUpActivity : AppCompatActivity() {

    val ui = ReportProblemsPopUpComponent()
    lateinit var db : AppDatabase

    lateinit var terminal : String
    lateinit var containerNo : String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui.setContentView(this)
        db = getDatabase(applicationContext)

        setWindowMetrics()

        terminal = db.legDao().getLegPickUpInfoById(1).originName
        containerNo = db.legDao().getLegContainerInfo(1).containerNumber

        ui.inTransitGV.adapter = reportProblemsGridAdapter(this, "IN TRANSIT")
        ui.defaultGV.adapter = reportProblemsGridAdapter(this, "DEFAULT")
        ui.facilityGV.adapter = reportProblemsGridAdapter(this, "FACILITY")
        ui.equipmentGV.adapter = reportProblemsGridAdapter(this, "EQUIPMENT")
        ui.scheduleGV.adapter = reportProblemsGridAdapter(this, "SCHEDULE")

        if (intent.getStringExtra("IS_IN_TRANSIT") == "true") {
            ui.inTransitGV.visibility = View.VISIBLE
        } else {
            ui.defaultGV.visibility = View.VISIBLE
        }

        setDefaultGridInteraction()
        setSecondLevelGridInteraction()
    }

    private fun hideAll () {
        val all = arrayListOf(ui.defaultGV, ui.inTransitGV, ui.facilityGV,
                ui.equipmentGV, ui.scheduleGV, ui.waitLayout)

        all.forEach {
            it.visibility = View.GONE
        }
    }

    private fun buildDialog (message : String) {
        //println("selected" + returnToTerminalArrayList[i])
        val builder = AlertDialog.Builder(this@ReportProblemsPopUpActivity)

        builder.setPositiveButton("YES") { dialog, which ->
            // Go to previous orCustomerActivity
            Toast.makeText(this@ReportProblemsPopUpActivity,
                    "Thank you. We will come back to you with a solution as soon as we can.",
                    Toast.LENGTH_LONG).show()
            finish()
        }

        builder.setMessage(message)

        // Display a neutral button on alert dialog
        builder.setNeutralButton("Cancel") { dialog, _ ->
            dialog.cancel()
        }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface

        dialog.show()
        dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#808080"))
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.parseColor("#29cc9e"))
    }

    private fun setWindowMetrics () {
        val dm = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(dm)
        val width = dm.widthPixels
        val height = dm.heightPixels

        lateinit var widthPercentage : Number
        lateinit var heightPercentage : Number

        if (intent.getStringExtra("IS_LANDSCAPE") == "true"){
            widthPercentage = .6
            heightPercentage = 1.0
        } else {
            widthPercentage = .95
            heightPercentage = .6
        }
        window.setLayout((width * widthPercentage).toInt(), (height * heightPercentage).toInt())
    }

    @SuppressLint("SetTextI18n")
    private fun setDefaultGridInteraction (){
        ui.defaultGV.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            hideAll()
            when (i) {
                0 -> {
                    ui.facilityGV.visibility = View.VISIBLE
                    ui.echoCategory.text = "I want to report an issue of the facility. "
                }
                1 -> {
                    ui.equipmentGV.visibility = View.VISIBLE
                    ui.echoCategory.text = "I want to report an equipment issue."
                }
                2 -> {
                    ui.scheduleGV.visibility = View.VISIBLE
                    ui.echoCategory.text = "I want to report an appointment/schedule error."
                }
                3 -> {
                    ui.defaultGV.visibility = View.VISIBLE
                    setHintToDefaultCategory()
                    buildSelfInputDialog ()
                }
                4 -> {
                    ui.waitLayout.visibility = View.VISIBLE
                    ui.echoCategory.text = "I want to report a congestion inside the facility."

                    waitLayoutInteraction()
                }
                5 -> {
                    ui.defaultGV.visibility = View.VISIBLE
                    setHintToDefaultCategory()
                    buildSelfInputDialog ()
                }
            }
        }

        ui.inTransitGV.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            hideAll()
            when (i) {
                0 -> {
                    val i = Intent (this, AccidentPhotoDamageActivity::class.java)
                    startActivity(i)
                }
                1 -> {
                    ui.waitLayout.visibility = View.VISIBLE
                    ui.echoCategory.text = "I want to report a long wait."

                    waitLayoutInteraction()
                }
                2 -> {
                    val i = Intent (this, ReportTruckProblemActivity::class.java)
                    startActivity(i)
                }
                3 -> {
                    ui.defaultGV.visibility = View.VISIBLE
                    setHintToDefaultCategory()
                    buildSelfInputDialog ()
                }
                4 -> {

                }
                5 -> {
                    ui.inTransitGV.visibility = View.VISIBLE
                    setHintToDefaultCategory()
                    buildSelfInputDialog ()
                }
            }
        }
    }

    private fun setSecondLevelGridInteraction() {
        ui.facilityGV.onItemClickListener = AdapterView.OnItemClickListener{ adapterView, view, i, l ->
            when (i) {
                0 -> {
                    buildDialog("Do you want to submit a UTL (Unable To Locate) " +
                            "report for the container #$containerNo?")
                }
                1-> {
                    buildDialog("Do you want to submit a Closed Area report for" +
                            " the terminal $terminal?")
                }
                2-> {
                    buildDialog("Do you want to report that $terminal" +
                            " is currently out of service (i.e. The crane is not working / there " +
                            "is no response from Clerk Pedestals)?")
                }
                3 -> {
                    buildDialog("Do you want to report that $terminal is " +
                            "out of chassis? ")
                }

                4 -> {
                    buildDialog("Do you want to report that the " +
                            "container #$containerNo is currently on-hold? ")
                }

                5 -> {
                    hideAll()
                    ui.defaultGV.visibility = View.VISIBLE
                    setHintToDefaultCategory()
                }
            }
        }

        ui.equipmentGV.onItemClickListener = AdapterView.OnItemClickListener{ adapterView, view, i, l ->
            when (i) {
                0 -> {
                    val i = Intent (this, ReportTruckProblemActivity::class.java)
                    startActivity(i)
                }
                1 -> {
                    val i = Intent (this, ReportDamageActivity::class.java)
                    i.putExtra("OBJECT", "container")
                    startActivity(i)
                }
                2 -> {
                    val i = Intent (this, ReportDamageActivity::class.java)
                    i.putExtra("OBJECT", "cargo")
                    startActivity(i)
                }
                3 -> {
                    val i = Intent (this, ReportDamageActivity::class.java)
                    i.putExtra("OBJECT", "equipment")
                    startActivity(i)
                }
                4 -> {
                    val i = Intent (this, ReportOverweightActivity::class.java)
                    startActivity(i)
                }
                5 -> {
                    hideAll()
                    ui.defaultGV.visibility = View.VISIBLE
                    setHintToDefaultCategory()
                }
            }
        }

        ui.scheduleGV.onItemClickListener = AdapterView.OnItemClickListener{ adapterView, view, i, l ->
            when (i) {
                0-> {
                    buildDialog("Do you want to submit a report for having a wrong appointment number or time?")
                }
                1 -> {
                    buildDialog("Do you want to report that the container #$containerNo is not released yet?")
                }
                2-> {
                    buildDialog("Do you want to report that the per diem has not been paid yet?")
                }
                3 -> {
                    buildDialog("Do you want to report that the pier pass has not been cleared yet?")
                }
                4 -> {

                }
                5 -> {
                    hideAll()
                    ui.defaultGV.visibility = View.VISIBLE
                    setHintToDefaultCategory()
                }
            }
        }
    }

    private fun waitLayoutInteraction() {
        val timer = countUpTimer(ui.hrsTxt, ui.minTxt, ui.secTxt)
        timer.start()

        ui.submitWaitReportBtn.setOnClickListener {
            timer.cancel()
            toast("Thank you! Your waiting time has been recorded. ")
            finish()
        }
    }

    private fun setHintToDefaultCategory () {
        ui.echoCategory.text = "I want to report ..."
    }

    private fun buildSelfInputDialog () {
        val reason = Dialog(this@ReportProblemsPopUpActivity)
        reason.setContentView(R.layout.report_problem_self_input_template)
        reason.findViewById<Button>(R.id.reportProblemSubmitBtn).setOnClickListener {
            val inputBox = reason.findViewById<EditText>(R.id.selfInputTextBox)
            val userInput =inputBox.text.toString()
            if (userInput == ""){
                inputBox.error = "Please write something to help us understand your problem."
            } else {
                reason.cancel()
                toast("Thank you. We will come back to you with a solution as soon as we can.")
                finish()
            }
        }

        reason.show()
    }

}
