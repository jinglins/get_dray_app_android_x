package com.dray.dray.dataClasses

import androidx.room.*
import com.dray.dray.dataClasses.leg.Leg
import com.dray.dray.dataClasses.profile.Profile

@Entity (tableName = "user",
        foreignKeys = [
            ForeignKey(entity = Profile::class,
                    parentColumns = ["uid"],
                    childColumns = ["profile_id"]),

            ForeignKey(entity = Leg::class,
                    parentColumns = ["uid"],
                    childColumns = ["leg_id"])
        ])

data class User (
        @PrimaryKey var uid : Int,

        @ColumnInfo (name = "profile_id") var profile : String,
        @ColumnInfo (name = "leg_id")    var job : String
        // @ColumnInfo (name = "payment" var payment : String
)