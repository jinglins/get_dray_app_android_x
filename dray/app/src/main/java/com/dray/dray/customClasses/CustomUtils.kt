package com.dray.dray.customClasses

import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.room.Room
import com.dray.dray.reportProblemsActivities.ReportProblemsPopUpActivity
import com.dray.dray.database.AppDatabase
import java.sql.Timestamp

fun setToolBar(actionBar : ActionBar, activityTitle : String){
    actionBar.setDisplayHomeAsUpEnabled(true)
    actionBar.setHomeButtonEnabled(true)
    actionBar.title = activityTitle
}

fun setWorkFlowToolBar (actionBar: ActionBar, activityTitle: String) {
    actionBar.setDisplayHomeAsUpEnabled(false)
    actionBar.title = activityTitle
}

fun displayReportWindow(context : Context, isLandscape : String, isInTransit : String) {
    println("in display report window")
    val i = Intent(context, ReportProblemsPopUpActivity::class.java)
    i.putExtra("IS_LANDSCAPE", isLandscape)
    i.putExtra("IS_IN_TRANSIT", isInTransit)
    i.flags = Intent.FLAG_ACTIVITY_NEW_TASK;
    context.startActivity(i)
}

fun getDatabase (applicationContext : Context) : AppDatabase{

    return Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
    ).allowMainThreadQueries().fallbackToDestructiveMigration().build()

}

fun getCurrentTimeStamp () : Timestamp {
    return Timestamp(System.currentTimeMillis())
}

fun countdownTimer (millisInFuture:Long,countDownInterval:Long): CountDownTimer {
    return object : CountDownTimer(millisInFuture,countDownInterval){
        override fun onTick(millisUntilFinished: Long){
            val hoursRemaining = ((millisUntilFinished / (1000*60*60)) % 24)
            val minutesRemaining = ((millisUntilFinished / (1000*60)) % 60)

            println("Countdown - millis until finished: $millisUntilFinished")
            println("Countdown - hours until finished: $hoursRemaining")
            println("Countdown - minutes until finished : $minutesRemaining")
        }

        override fun onFinish() {
            // TODO: Write pop up alarm
        }
    }
}

fun countUpTimer (hrsTxt : TextView, minTxt : TextView, secTxt : TextView) : CountDownTimer {

    val totalSeconds: Long = 86400 // Longest report wait time that can be reported
    val intervalSeconds: Long = 1

    return object : CountDownTimer(totalSeconds * 1000, intervalSeconds * 1000) {
        override fun onTick(millisUntilFinished: Long) {
            val elapsedSeconds = (totalSeconds * 1000 - millisUntilFinished) / 1000
            renderTxtView(elapsedSeconds, hrsTxt, minTxt, secTxt)
        }

        override fun onFinish() {

        }
    }
}

private fun renderTxtView (elapsedSeconds : Long, hrsTxt: TextView, minTxt: TextView, secTxt: TextView) {
    val numberOfHours = (elapsedSeconds % 86400 ) / 3600 ;
    val numberOfMinutes = ((elapsedSeconds % 86400 ) % 3600 ) / 60
    val numberOfSeconds = ((elapsedSeconds % 86400 ) % 3600 ) % 60

    val hours = if (numberOfHours < 10) "0" + numberOfHours.toString() else numberOfHours.toString()
    val minutes = if (numberOfMinutes < 10) "0" + numberOfMinutes.toString() else numberOfMinutes.toString()
    val seconds = if (numberOfSeconds < 10) "0" + numberOfSeconds.toString() else numberOfSeconds.toString()

    hrsTxt.text = hours
    minTxt.text = minutes
    secTxt.text = seconds
}