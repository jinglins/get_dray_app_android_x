package com.dray.dray.helpActivities

import androidx.fragment.app.Fragment
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dray.dray.R
import com.dray.dray.adapter.pendingHelpTicketAdapter
import com.dray.dray.customClasses.HelpTicketItem
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.UI
import java.sql.Timestamp

class PendingHelpTicketFragmentActivity : androidx.fragment.app.Fragment() {
    lateinit var mAdapter: pendingHelpTicketAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val MEDIUM_SIZE_TEXT = 18f

        var pendingHelpTickets = ArrayList<HelpTicketItem>()
        val timestamp = Timestamp(System.currentTimeMillis())
        // DUMMY DATA
        pendingHelpTickets.add(HelpTicketItem("Just solved", "C341",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                        "Cur igitur easdem res, inquam, Peripateticis dicentibus verbum " +
                        "nullum est, quod non intellegatur? ", timestamp, "No solution"))
        pendingHelpTickets.add(HelpTicketItem("Solving", "A000",
                "Ut proverbia non nulla veriora sint quam vestra dogmata." +
                        " Hinc ceteri particulas arripere conati suam quisque videro " +
                        "voluit afferre sententiam. Haec quo modo conveniant, non sane intellego.",
                timestamp, "No solution"))
        pendingHelpTickets.add(HelpTicketItem("Received", "B220",
                "Post enim Chrysippum eum non sane est disputatum. Scrupulum, inquam," +
                        " abeunti; Ego quoque, inquit, didicerim libentius si quid attuleris, " +
                        "quam te reprehenderim.", timestamp, "No solution"))

        mAdapter = pendingHelpTicketAdapter(this, pendingHelpTickets)

        return UI{
            verticalLayout {
                lparams(width = matchParent, height = matchParent)

                verticalLayout(R.style.ThemeOverlay_AppCompat_Dark){
                    backgroundResource = R.drawable.purple_header

                    lparams(width = matchParent, height = wrapContent){
                        verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                        horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    }

                    linearLayout {
                        lparams(width = matchParent)

                        linearLayout{
                            gravity = Gravity.CENTER
                            lparams{
                                weight = 1f
                            }

                            imageView(){
                                imageResource = R.mipmap.ic_swap_vert
                            }
                            textView("Status"){
                                typeface = Typeface.DEFAULT_BOLD
                            }
                        }

                        linearLayout{
                            gravity = Gravity.CENTER
                            lparams{
                                weight = 1f
                            }
                            imageView(){
                                imageResource = R.mipmap.ic_swap_vert
                            }
                            textView("Date")
                        }

                        linearLayout{
                            gravity = Gravity.CENTER
                            lparams{
                                weight = 1f
                            }
                            imageView(){
                                imageResource = R.mipmap.ic_check
                            }
                            textView("Just Solved")
                        }

                    }
                }

                verticalLayout {
                    lparams(width = matchParent, height = matchParent){
                        verticalPadding = dimen(R.dimen.activity_vertical_padding)
                        horizontalPadding = dimen (R.dimen.list_item_horizontal_padding)
                    }
                    listView{
                        adapter = mAdapter
                        divider = null
                        backgroundColor = Color.TRANSPARENT
                        cacheColorHint = Color.TRANSPARENT
                        dividerHeight = dimen(R.dimen.item_vertical_margin)
                    }.lparams(width = matchParent, height = matchParent)
                }


            }
        }.view

    }

}