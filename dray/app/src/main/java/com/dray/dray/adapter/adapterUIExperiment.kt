package com.dray.dray.adapter

import android.graphics.Color
import android.graphics.Typeface
import android.view.View
import com.dray.dray.R
import com.dray.dray.workflow.scheduleNextLoadQuestion.ScheduleNextLoadQuestionActivity
import org.jetbrains.anko.*

class AnotherLoadQuestionComponent : AnkoComponent<ScheduleNextLoadQuestionActivity>, AnkoLogger {
    val INDENT = 10
    val LABEL_WIDTH = 90
    val SMALL_LABEL_TEXT_SIZE = 8f
    val lightGrey = "#aaaaaa"
    override fun createView(ui: AnkoContext<ScheduleNextLoadQuestionActivity>): View = with(ui) {
        verticalLayout {
            // Experiment wrapper
            verticalPadding = dimen(R.dimen.activity_vertical_padding)
            horizontalPadding = dimen(R.dimen.activity_horizontal_padding)

            verticalLayout{
                backgroundResource = R.drawable.grey_bordered_round_btn

                linearLayout {
                    verticalLayout {
                        verticalPadding = dip(10)
                        horizontalPadding = dip(10)
                        lparams(){
                            weight = 1f
                        }


                        verticalLayout{
                            lparams(width = matchParent){
                                bottomMargin = dip(10)
                            }
                            linearLayout {
                                lparams(width = matchParent)
                                textView("ABCD000000"){
                                    textColor = R.color.colorPrimary
                                    typeface = Typeface.DEFAULT_BOLD
                                }.lparams(){
                                    weight = 1f
                                }
                                imageView(R.mipmap.ic_timer){
                                    adjustViewBounds = true
                                }.lparams(width = dip(20), height = dip(20))
                                textView("4h 45m")
                            }

                            linearLayout {
                                textView("Chassis# 45FV0414"){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                }.lparams(){
                                    weight = 1f
                                }
                                textView("LOAD"){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                }
                            }


                        }

                        linearLayout(){
                            imageView(R.drawable.origin_dot){
                                adjustViewBounds = true
                            }.lparams(width = dip(30), height = dip(30))

                            verticalLayout {
                                textView("WBCT"){
                                    textColor = Color.BLACK
                                    typeface = Typeface.DEFAULT_BOLD
                                }

                                textView("Pick up on 12/24/2018 12:34:03"){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                }
                            }
                        }

                        linearLayout(){
                            imageView(R.drawable.dest_dot){
                                adjustViewBounds = true
                            }.lparams(width = dip(30), height = dip(30))

                            verticalLayout {
                                textView("RANCHO CUCAMONGA"){
                                    textColor = Color.BLACK
                                    typeface = Typeface.DEFAULT_BOLD
                                }

                                textView("Deliver to on 12/24/2018 19:33:32"){
                                    textSize = SMALL_LABEL_TEXT_SIZE
                                }
                            }

                        }
                    }

                    view(){
                        backgroundColor = resources.getColor(R.color.backgroundGrey)
                    }.lparams(width = dip(1), height = matchParent)

                    verticalLayout(){
                        lparams(){
                            verticalPadding = dip(10)
                            horizontalPadding = dip(10)
                        }
                        linearLayout {
                            textView("Flat Rate"){
                                textSize = 10f
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(){
                                weight = 1f
                            }
                            textView("$230.23"){
                                textSize = 10f
                                typeface = Typeface.DEFAULT_BOLD
                            }
                        }

                        linearLayout {
                            textView("Fuel Rate"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$34.50"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }
                        }

                        linearLayout {
                            textView("Bonus Rate"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$29.32"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }
                        }

                        linearLayout {
                            textView("Hire-off"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$23.43"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }
                        }

                        linearLayout {
                            textView("Addition"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$23.45"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = Color.parseColor(lightGrey)
                            }
                        }

                        linearLayout {
                            lparams(width = matchParent){
                                bottomMargin = dip(5)
                            }

                            textView("Deduction"){
                                textSize = SMALL_LABEL_TEXT_SIZE
                                textColor = resources.getColor(R.color.colorRed)
                            }.lparams(){
                                weight = 1f
                                leftMargin = dip(INDENT)
                            }
                            textView("$43.90"){
                                textColor = resources.getColor(R.color.colorRed)
                                textSize = SMALL_LABEL_TEXT_SIZE
                            }
                        }

                        view(){
                            backgroundColor = Color.parseColor(lightGrey)
                        }.lparams(width = dip(110), height = dip(1))

                        linearLayout {
                            lparams(width = matchParent){
                                topMargin = dip(5)
                            }

                            textView("Total"){
                                textColor = R.color.colorPrimary
                                textSize = 10f
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams(){
                                weight = 1f
                            }
                            textView("$159.53"){
                                textColor = Color.BLACK
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 18f
                            }
                        }
                    }
                }

                view(){
                    backgroundColor = resources.getColor(R.color.backgroundGrey)
                }.lparams(width = matchParent, height = dip(1))

                linearLayout {
                    verticalPadding = dip(5)
                    horizontalPadding = dip(10)
                    button("Dispute"){
                        backgroundResource = R.drawable.left_rounded_button
                        textColor = resources.getColor(R.color.defaultLightTxtColor)
                    }.lparams(){
                        weight = 0.5f
                        rightMargin = dimen(R.dimen.countdown_gap)
                    }

                    button("Approve"){
                        backgroundResource = R.drawable.right_rounded_button
                        textColor = Color.WHITE
                    }.lparams(){
                        weight = 0.5f
                        leftMargin = dimen(R.dimen.countdown_gap)
                    }
                }

                linearLayout {
                    visibility = View.GONE
                    verticalPadding = dip(5)
                    horizontalPadding = dip(10)
                    textView("Approved on 2018/32.").lparams(){
                        weight = 1f
                    }

                    button("Cancel"){
                        backgroundResource = R.drawable.green_rounded_button
                        textColor = Color.WHITE
                    }.lparams(){
                        leftMargin = dimen(R.dimen.countdown_gap)
                    }
                }
            }
        }
    }

}