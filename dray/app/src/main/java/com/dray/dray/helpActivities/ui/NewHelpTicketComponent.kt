package com.dray.dray.helpActivities.ui

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.dray.dray.R
import com.dray.dray.helpActivities.NewHelpTicketActivity
import org.jetbrains.anko.*

class NewHelpTicketComponent : AnkoComponent<NewHelpTicketActivity>, AnkoLogger{
    lateinit var inputText : EditText
    lateinit var submitBtn : Button
    val MEDIUM_SIZE_TEXT = 18f
    override fun createView(ui: AnkoContext<NewHelpTicketActivity>): View = with(ui) {
        verticalLayout{
            lparams(width = matchParent, height = matchParent){
                verticalPadding = dimen(R.dimen.activity_vertical_padding)
                horizontalPadding = dimen(R.dimen.activity_horizontal_padding)
            }

            textView("If you have an issue with our app\nPlease tell us the issue in the " +
                    "box below. "){
                textColor = R.color.colorPrimary
                this.gravity = Gravity.CENTER_HORIZONTAL
            }
            verticalLayout {
                backgroundResource = R.drawable.white_rounded_item
                lparams(width = matchParent){
                    verticalPadding = dimen(R.dimen.list_item_vertical_padding)
                    horizontalPadding = dimen(R.dimen.list_item_horizontal_padding)
                    verticalMargin = dip(10)
                    weight = 1f
                }

                inputText = editText(){
                    backgroundColor = Color.TRANSPARENT
                    hint = "Please tell us about the issue ... "
                }
            }

            submitBtn = button("Submit"){
                backgroundResource = R.drawable.green_rounded_button
                textSize = MEDIUM_SIZE_TEXT
                textColor = Color.WHITE
            }.lparams(width = matchParent, height = wrapContent){
                topMargin = dip(10)
                weight = 0f
            }
        }
    }

}